#ifndef TIGLRWREADADJLIST_HPP_
#define TIGLRWREADADJLIST_HPP_

/*****************************************************************************/
/*!
 * @file tiglRWReadAdjList.hpp
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/

/******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/

#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <string>
#include "tiglContainersVertexBase.hpp"

/*****************************************************************************/
/*!
 * @ingroup file_io
 * @brief Reads the adjacency list of vertices from a file
 * @details The file format is (for each line):
 * vertex_id: child1_id, child2_id ...
 *
 * The number of vertices is limited to the maximum size of long int
 */
/*****************************************************************************/
template<typename VertexClass>
bool TIGLRwReadAdjList(vector<VertexClass> & verticesList, char *fileName)
{
   typedef typename VertexClass::ID_TYPE ID_TYPE;
   typedef typename VertexClass::EDGE_TYPE EDGE_TYPE;

   ifstream file(fileName);
   string line;

   if (!file)
   {
      cout << "Error: cannot open " << fileName << " to read !" << endl << endl;
      return false;
   }

   ID_TYPE vertexCount = 0;
   ID_TYPE edgeCount = 0;

   // counting the number of vertices
   while (getline(file, line))
      vertexCount++;

   // constructing an empty list of vertices
   for (ID_TYPE v = 0; v < vertexCount; v++)
   {
      VertexClass newVertex(v);
      verticesList.push_back(newVertex);
   }
   file.close();

   // now returning to the start of the file to read the edges
   file.open(fileName);

   while (getline(file, line))
   {
      unsigned pos = line.find(":");
      string tmpStr = line.substr(0, pos);
      ID_TYPE id = atol(tmpStr.c_str());
      line = line.substr(pos + 1);

      while (line.size() > 1)
      {
         pos = line.find(", ");
         tmpStr = line.substr(0, pos);
         line = line.substr(pos + 1);

         ID_TYPE target = atol(tmpStr.c_str());
         EDGE_TYPE newEdge(target);
         if (verticesList[id].isConnected(target) == true)
            continue;
         verticesList[id].addEdge(newEdge);
         if (verticesList[id].isBiVertex())
         {
            EDGE_TYPE newInEdge(id);
            verticesList[target].addInEdge(newInEdge);
         }

         edgeCount++;
      }
   }
   file.close();
   return true;
}

#endif /* READ_ADJ_LIST_HPP_ */
