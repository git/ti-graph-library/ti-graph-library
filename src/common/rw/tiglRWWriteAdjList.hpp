#ifndef TIGLRWWRITEADJLIST_HPP_
#define TIGLRWWRITEADJLIST_HPP_

/*****************************************************************************/
/*!
 * @file tiglRWWriteAdjList.hpp
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/

/******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/
#include <iostream>
#include <fstream>

#include "tiglContainersVertexBase.hpp"

/*****************************************************************************/
/*!
 * @ingroup file_io
 * @brief Writes the adjacency list of vertices (of unweighted graph) to a file
 * @details The file format is (for each line):
 * vertex_id: child1_id, child2_id ...
 *
 * The number of vertices is limited to the maximum size of long int
 */
/*****************************************************************************/
template<typename VertexClass>
bool TIGLRwWriteAdjList(vector<VertexClass> & verticesList, char *fileName)
{
   ofstream file(fileName);

   if (!file)
   {
      cout << "Error: cannot open " << fileName << " to write !" << endl << endl;
      return false;
   }
   for (typename vector<VertexClass>::iterator vertexIterator = verticesList.begin(); vertexIterator != verticesList.end(); ++vertexIterator)
   {
      file << vertexIterator->getId() << ": ";
      for (typename VertexClass::OUT_EDGE_ITERATOR edgeIterator = vertexIterator->getBeginOutEdgeIterator();
            edgeIterator != vertexIterator->getEndOutEdgeIterator(); ++edgeIterator)
         file << edgeIterator->getNbr() << ", ";
      file << endl;
   }
   file.close();
   return true;
}

/*****************************************************************************/
/*!
 * @ingroup file_io
 * @brief Writes the adjacency list of vertices of a weighted graph to a file
 * @details The file format is (for each line):
 * vertex_id: (child1_id, edge1_weight), (child2_id,edge2_weight), ...
 *
 * The number of vertices is limited to the maximum size of long int
 */
/*****************************************************************************/
template<typename VertexClass>
bool WriteAdjListWeighted(vector<VertexClass> & verticesList, char *fileName)
{
   ofstream file(fileName);

   if (!file)
   {
      cout << "Error: cannot open " << fileName << " to write !" << endl << endl;
      return false;
   }
   for (typename vector<VertexClass>::iterator vertexIterator = verticesList.begin(); vertexIterator != verticesList.end(); ++vertexIterator)
   {
      file << vertexIterator->getId() << ": ";
      for (typename VertexClass::OUT_EDGE_ITERATOR edgeIterator = vertexIterator->getBeginOutEdgeIterator(); edgeIterator != vertexIterator->getEndOutEdgeIterator(); ++edgeIterator)
         file << "(" << edgeIterator->getNbr() << ", " << edgeIterator->getData() << "), ";
      file << endl;
   }
   file.close();
   return true;
}

#endif /* TIGLRWWRITEADJLIST_HPP_ */
