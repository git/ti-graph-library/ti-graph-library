#ifndef TIGLMUTATIONMESSAGEIMPLEMENTATION_HPP
#define TIGLMUTATIONMESSAGEIMPLEMENTATION_HPP

/*****************************************************************************/
/*!
 *@file tiglMutationMessageImplementation.hpp
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/

/******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/
#include  <stdint.h>
#include  <algorithm>
#include  <vector>
#include  <string.h>

using namespace std;

/*****************************************************************************/
/*!
 * @brief Prepares the ADD_OUT/IN_EDGE mutation message
 * @param pEdge pointer to the edge to be added
 * @param targetVertex pointer to the ID of the target vertex
 * @return returns the message size in bytes
 */
/*****************************************************************************/
template<typename EdgeType, typename ID_TYPE>
uint_fast32_t TIGLMutationMessage::prepareAddEdgeMessage(EdgeType *pEdge, ID_TYPE *targetVertex)
{
   messageBody.clear();
   uint8_t *p = (uint8_t *) targetVertex;
   messageBody.insert(messageBody.begin(), p, p + sizeof(ID_TYPE));
   p = (uint8_t *) pEdge;
   messageBody.insert(messageBody.end(), p, p + sizeof(EdgeType));
   return messageBody.size() + 1;   // one uint8_t is added for the message code
}

/*****************************************************************************/
/*!
 * @brief Reads the ADD/REMOVE_EDGE mutation message
 * @param pEdge: pointer to the read buffer of the edge
 * @param targetVertex: pointer to the ID of targe tvertex
 * @return true if successful, otherwise false
 *
 */
/*****************************************************************************/
template<typename EdgeType, typename ID_TYPE>
bool TIGLMutationMessage::readAddEdgeMessage(EdgeType *pEdge, ID_TYPE *targetVertex)
{
   if (messageBody.size() != sizeof(EdgeType) + sizeof(ID_TYPE))
      return false;

   uint8_t *p = (uint8_t *) targetVertex;
   std::copy(messageBody.begin(), messageBody.begin() + sizeof(ID_TYPE), p);
   p = (uint8_t *) pEdge;
   std::copy(messageBody.begin() + sizeof(ID_TYPE), messageBody.end(), p);
   return true;
}

/*****************************************************************************/
/*!
 * @brief Prepares the REMOVE_OUT/IN_EDGE mutation message
 * @param edge: pointer to the the edge object
 * @param targetVertex: pointer to the target vertex where we remove this edge
 * @return returns the message size in bytes
 *
 */
/*****************************************************************************/
template<typename EDGE_TYPE, typename ID_TYPE>
uint_fast32_t TIGLMutationMessage::prepareRemoveEdgeMessage(EDGE_TYPE *edge, ID_TYPE *targetVertex)
{
   messageBody.clear();
   uint8_t *p = (uint8_t *) targetVertex;
   messageBody.insert(messageBody.begin(), p, p + sizeof(ID_TYPE));
   ID_TYPE nbrVertex = edge->getNbr();
   p = (uint8_t *) &nbrVertex;
   messageBody.insert(messageBody.begin(), p, p + sizeof(ID_TYPE));
   return messageBody.size() + 1;   // one uint8_t is added for the message code
}

/*****************************************************************************/
/*!
 * @brief Prepares the REMOVE_OUT/IN_EDGE mutation message
 * @param nbrVertex: pointer to the other side of the vertex
 * @param targetVertex: pointer to the target vertex where we remove this edge
 * @return returns the message size in bytes
 *
 */
/*****************************************************************************/
template<typename ID_TYPE>
uint_fast32_t TIGLMutationMessage::prepareRemoveEdgeMessage(ID_TYPE *nbrVertex, ID_TYPE *targetVertex)
{
   messageBody.clear();
   uint8_t *p = (uint8_t *) targetVertex;
   messageBody.insert(messageBody.begin(), p, p + sizeof(ID_TYPE));
   p = (uint8_t *) nbrVertex;
   messageBody.insert(messageBody.end(), p, p + sizeof(ID_TYPE));   //std::copy(p, p+sizeof(ID_TYPE), messageBody.end());
   return messageBody.size() + 1;   // one uint8_t is added for the message code
}

/*****************************************************************************/
/*!
 * @brief Reads the ADD/REMOVE_EDGE mutation message
 * @param nbrVertex: pointer to the other side of the vertex
 * @param targetVertex: pointer to the target vertex where we remove this edge
 * @return true if successful, otherwise false
 *
 */
/*****************************************************************************/
template<typename ID_TYPE>
bool TIGLMutationMessage::readRemoveEdgeMessage(ID_TYPE *nbrVertex, ID_TYPE *targetVertex)
{
   if (messageBody.size() != 2 * sizeof(ID_TYPE))
      return false;

   uint8_t *p = (uint8_t *) &targetVertex;
   std::copy(messageBody.begin(), messageBody.begin() + sizeof(ID_TYPE), p);
   p = (uint8_t *) nbrVertex;
   std::copy(messageBody.begin() + sizeof(ID_TYPE), messageBody.end(), p);
   return true;
}

/*****************************************************************************/
/*!
 * @brief Prepares the ADD_EMPTY_VERTEX or REMOVE_VERTEX mutation message
 * @param vertexId vertex ID
 * @return returns the message size in bytes
 *
 */
/*****************************************************************************/
template<typename ID_TYPE>
uint_fast32_t TIGLMutationMessage::prepareEmptyVertexMessage(ID_TYPE *vertexId)
{
   messageBody.clear();
   //messageBody.reserve(sizeof(ID_TYPE));
   uint8_t *p = (uint8_t *) vertexId;
   messageBody.insert(messageBody.begin(), p, p + sizeof(ID_TYPE));
   return messageBody.size() + 1;   // one uint8_t is added for the message code
}

/*****************************************************************************/
/*!
 * @brief Reads the ADD_EMPTY_VERTEX or REMOVE_VERTEX mutation message
 * @param vertexId: output vertex ID
 * @return true if successful, otherwise false
 *
 */
/*****************************************************************************/
template<typename ID_TYPE>
bool TIGLMutationMessage::readEmptyVertexMessage(ID_TYPE *vertexId)
{
   if (messageBody.size() != sizeof(ID_TYPE))
      return false;
   uint8_t *p = (uint8_t *) vertexId;
   std::copy(messageBody.begin(), messageBody.end(), p);

   return true;
}

/*****************************************************************************/
/*!
 * @brief Prepares the ADD_VERTEX message
 * @param vertex pointer to the vertex object
 * @return returns the size of the message body
 *
 */
/*****************************************************************************/
template<typename CVertex>
uint_fast32_t TIGLMutationMessage::prepareAddVertexMessage(CVertex *vertex)
{
   typedef typename CVertex::ID_TYPE ID_TYPE;
   typedef typename CVertex::EDGE_TYPE EDGE_TYPE;
   typedef typename CVertex::OUT_EDGE_ITERATOR EDGE_ITERATOR;
   typedef typename CVertex::ALG_DATA_TYPE ALG_DATA_TYPE;
   typedef typename CVertex::DATA_TYPE DATA_TYPE;

   messageBody.clear();

   // adding the objecte information
   ID_TYPE id = vertex->getId();
   uint8_t *p = (uint8_t *) &id;
   messageBody.insert(messageBody.begin(), p, p + sizeof(ID_TYPE));

   // adding vertexData information
   DATA_TYPE vertexData = vertex->get_value();
   p = (uint8_t *) &vertexData;
   messageBody.insert(messageBody.end(), p, p + sizeof(DATA_TYPE));

   // adding algData information
   ALG_DATA_TYPE algData = vertex->getAlgData();
   p = (uint8_t *) &algData;
   messageBody.insert(messageBody.end(), p, p + sizeof(ALG_DATA_TYPE));

   // finally adding edge information
   // adding number of output edges
   uint_fast32_t numEdges = vertex->getNumOutEdges();
   p = (uint8_t *) &numEdges;
   messageBody.insert(messageBody.end(), p, p + sizeof(uint_fast32_t));

   for (EDGE_ITERATOR it = vertex->getBeginOutEdgeIterator(); it != vertex->getEndOutEdgeIterator(); ++it)
   {
      EDGE_TYPE edge = *it;
      p = (uint8_t *) &edge;
      messageBody.insert(messageBody.end(), p, p + sizeof(EDGE_TYPE));
   }
   // checking if it has input edge buffer
   if (vertex->isBiVertex())
   {
      uint_fast32_t numInEdges = vertex->get_num_in_edges();
      p = (uint8_t *) &numInEdges;
      messageBody.insert(messageBody.end(), p, p + sizeof(uint_fast32_t));

      for (EDGE_ITERATOR it = vertex->getBeginInEdgeIterator(); it != vertex->getEndInEdgeIterator(); ++it)
      {
         EDGE_TYPE edge = *it;
         p = (uint8_t *) &edge;
         messageBody.insert(messageBody.end(), p, p + sizeof(EDGE_TYPE));
      }
   }
   return messageBody.size() + 1;   // one uint8_t is added for the message code
}

/*****************************************************************************/
/*!
 * @brief Reads the ADD_VERTEX
 * @param pVertex pointer to the empty vertex object that's filled inside the function
 *
 */
/*****************************************************************************/
template<typename CVertex>
bool TIGLMutationMessage::readAddVertexMessage(CVertex *pVertex)
{
   typedef typename CVertex::ID_TYPE ID_TYPE;
   typedef typename CVertex::EDGE_TYPE EDGE_TYPE;
   typedef typename CVertex::ALG_DATA_TYPE ALG_DATA_TYPE;
   typedef typename CVertex::DATA_TYPE DATA_TYPE;

   //uint_fast32_t index;
   // adding the objecte information
   ID_TYPE id;
   uint8_t *p = (uint8_t *) &id;
   BYTE_ITERATOR it = messageBody.begin();
   BYTE_ITERATOR itBound = it + sizeof(ID_TYPE);
   if (itBound == messageBody.end())
   {
      return false;
   }
   std::copy(it, itBound, p);

   // adding vertexData information
   DATA_TYPE vertexData;
   p = (uint8_t *) &vertexData;
   it = itBound;
   itBound += sizeof(DATA_TYPE);
   if (itBound == messageBody.end())
   {
      return false;
   }
   std::copy(it, itBound, p);

   // adding algData information
   ALG_DATA_TYPE algData;
   p = (uint8_t *) &algData;
   it = itBound;
   itBound += sizeof(ALG_DATA_TYPE);
   if (itBound == messageBody.end())
   {
      return false;
   }
   std::copy(it, itBound, p);

   pVertex->setParams(id, vertexData, algData);

   // finally adding edge information
   // adding number of output edges
   uint_fast32_t numEdges;
   p = (uint8_t *) &numEdges;
   it = itBound;
   itBound += sizeof(uint_fast32_t);
   std::copy(it, itBound, p);
   if (itBound == messageBody.end())
   {
      if (numEdges)
         return false;
   }

   for (uint_fast32_t i = 0; i < numEdges; i++)
   {
      EDGE_TYPE edge(0);
      p = (uint8_t *) &edge;
      it = itBound;
      itBound += sizeof(EDGE_TYPE);
      if (itBound == messageBody.end() && i < numEdges - 1)
      {
         return false;
      }
      std::copy(it, itBound, p);
      pVertex->addEdge(edge);
   }
   // checking if it has input edge buffer
   if (pVertex->isBiVertex())
   {
      uint_fast32_t numInEdges;
      it = itBound;
      itBound += sizeof(uint_fast32_t);
      std::copy(it, itBound, p);
      if (itBound == messageBody.end())
      {
         if (numInEdges)
            return false;
      }

      for (uint_fast32_t i = 0; i < numInEdges; i++)
      {
         EDGE_TYPE edge(0);
         p = (uint8_t *) &edge;
         it = itBound;
         itBound += sizeof(EDGE_TYPE);
         if (itBound == messageBody.end() && i < numEdges - 1)
         {
            return false;
         }
         std::copy(it, itBound, p);
         pVertex->addInEdge(edge);
      }
   }

   return true;
}

/*****************************************************************************/
/*!
 ///@brief returns the mutation message code
 *
 */
/*****************************************************************************/
inline MUTATION_MESSAGE_CODE TIGLMutationMessage::getMessageCode()
{
   return messageCode;
}

/*****************************************************************************/
/*!
 ///@brief reading the overall message body
 *
 */
/*****************************************************************************/
inline uint_fast32_t TIGLMutationMessage::getSizeInBytes()
{
   return messageBody.size() + sizeof(uint_fast32_t) + 1;
}

/*****************************************************************************/
/*!
 ///@brief reading teh destination device
 *
 */
/*****************************************************************************/
inline int_fast32_t TIGLMutationMessage::getDstDev()
{
   return otherDevice;
}

/*****************************************************************************/
/*!
 ///@brief returns destination vertex
 *
 */
/*****************************************************************************/
template<typename ID_TYPE>
bool TIGLMutationMessage::getDstVertex(ID_TYPE *targetVertex)
{

   if (messageCode == ADD_VERTEX)
   {
      //tobe defined
      return false;
   }
   // all other cases the target vertex is set at the beginning of the message
   uint8_t *p = (uint8_t *) targetVertex;
   std::copy(messageBody.begin(), messageBody.begin() + sizeof(ID_TYPE), p);
   return true;
}

/*****************************************************************************/
/*!
 * @brief Packs the mutation message into bytes
 * the packed message contains the message code and the message body
 * @param buf output buffer for packed message
 * @return the message size in bytes
 *
 */
/*****************************************************************************/
inline uint_fast32_t TIGLMutationMessage::pack(uint8_t *buf)
{
   // putting the message code
   uint8_t firstByte = (uint8_t) messageCode;   //
   *(buf++) = firstByte;
   uint_fast32_t bodySize = messageBody.size();
   memcpy(buf, &bodySize, sizeof(uint_fast32_t));   // including the size of message body in the message header
   buf += sizeof(uint_fast32_t);

   if (bodySize)
      std::copy(messageBody.begin(), messageBody.end(), buf);

   return bodySize + sizeof(uint_fast32_t) + 1;   // including the size (4 bytes) and the message code (1 uint8_t)
}

/*****************************************************************************/
/*!
 * @brief Unpacks a mutation message to the object fields
 * @param buf input buffer for the packed message
 * @return the size (in bytes) of the unpacked message
 *
 */
/*****************************************************************************/
inline uint_fast32_t TIGLMutationMessage::unpack(uint8_t *buf)
{
   messageCode = (MUTATION_MESSAGE_CODE) (*(buf++));
   uint_fast32_t bodySize;
   memcpy(&bodySize, buf, sizeof(uint_fast32_t));
   buf += sizeof(uint_fast32_t);
   messageBody.clear();

   if (bodySize)
      messageBody.insert(messageBody.begin(), buf, buf + bodySize);

   return bodySize + 1 + sizeof(uint_fast32_t);
}

/*****************************************************************************/
/*!
 * @brief Procedure for reading add/migrate vertex messages
 * @param vertex pointer to the vertex object
 *
 */
/*****************************************************************************/
template<typename CVertex>
bool TIGLMutationMessage::readVertexMessage(CVertex *vertex)
{
   switch (messageCode) {
      case ADD_VERTEX:
         return readAddVertexMessage(vertex);
         break;
//    case MIGRATE_VERTEX:
//       return readMigrateVertexMessage(vertex);
//       break;
      default:
         cout << "!!!!!!!! ERROR in TIGLMutationMessage::readVertexMessage wrong call" << endl;
   }
   return false;
}

/*****************************************************************************/
/*!
 * @brief Procedure for reading add/migrate vertex messages. It
 * @param id pointer the vertex ID
 *
 */
/*****************************************************************************/
template<typename ID_TYPE>
bool TIGLMutationMessage::readSimpleVertexMessage(ID_TYPE *id)
{
   if (messageCode == ADD_EMPTY_VERTEX || messageCode == REMOVE_VERTEX)
      return readEmptyVertexMessage(id);
   return false;
}

/*****************************************************************************/
/*!
 * @brief Procedure for reading add edge messages.
 * @param edge pointer to the edge object,
 * @param targetVertex and the target vertex
 * @return true if successful, false otherwise
 *
 */
/*****************************************************************************/
template<typename EDGE_TYPE, typename ID_TYPE>
bool TIGLMutationMessage::readEdgeMessage(EDGE_TYPE *edge, ID_TYPE *targetVertex)
{
   if (messageCode == ADD_OUT_EDGE || messageCode == ADD_IN_EDGE)
      return readAddEdgeMessage(edge, targetVertex);

   cout << "!!!!!!!! ERROR in TIGLMutationMessage::readEdgeMessage: wrong call" << endl;
   return false;
}

/*****************************************************************************/
/*!
 * @brief Procedure for reading remove edge messages. It has two outupts
 * @param nbrVertex pointer to other side of the edge,
 * @param targetVertex pointer to the vertex ID of the vertex from which the edge is removed
 * @return true if successful, false otherwise
 *
 */
/*****************************************************************************/
template<typename ID_TYPE>
bool TIGLMutationMessage::readSimpleEdgeMessage(ID_TYPE *nbrVertex, ID_TYPE *targetVertex)
{
   if (messageCode != REMOVE_OUT_EDGE && messageCode != REMOVE_IN_EDGE)
      return false;
   return readRemoveEdgeMessage(nbrVertex, targetVertex);
}

/*****************************************************************************/
/*!
 * @brief Procedure for writing ADD edge messages.
 * @param code message code: can be either ADD_OUT_EDGE or ADD_IN_EDGE
 * @param edge pointer to the edge object
 * @param targetVertex pointer to the vertex ID of the vertex to which the edge is added
 *
 */
/*****************************************************************************/
template<typename EdgeType, typename ID_TYPE>
bool TIGLMutationMessage::writeEdgeMessage(MUTATION_MESSAGE_CODE code, EdgeType *edge, ID_TYPE *targetVertex)
{
   messageCode = code;
   if (code != ADD_OUT_EDGE && code != ADD_IN_EDGE)
   {
      cout << "!!!!!! ERROR TIGLMutationMessage::writeEdgeMessage  wrong call" << endl;
      return false;
   }
   prepareAddEdgeMessage(edge, targetVertex);
   return true;
}

/*****************************************************************************/
/*!
 * @brief Procedure for writing REMOVE edge messages.
 * @param code message code: can be either REMOVE_OUT_EDGE or REMOVE_IN_EDGE
 * @param nbrVertex pointer to the vertex ID of the other side of the edge
 * @param targetVertex pointer to the vertex ID of the vertex to which the edge is added
 *
 */
/*****************************************************************************/
template<typename ID_TYPE>
bool TIGLMutationMessage::writeSimpleEdgeMessage(MUTATION_MESSAGE_CODE code, ID_TYPE *nbrVertex, ID_TYPE *targetVertex)
{
   messageCode = code;
   if (code != REMOVE_OUT_EDGE && code != REMOVE_IN_EDGE)
   {
      cout << "!!!!!! ERROR TIGLMutationMessage::setSimpleEdgeMessage  wrong call" << endl;
      return false;
   }
   prepareRemoveEdgeMessage(nbrVertex, targetVertex);
   return true;
}

/*****************************************************************************/
/*!
 * @brief Procedure for writing ADD/MIGRATE vertex messages.
 * @param code message code: can be either ADD_VERTEX or MIGRATE_VERTEX
 * @param vertex pointer to the vertex object
 *
 */
/*****************************************************************************/
template<typename CVertex>
bool TIGLMutationMessage::writeVertexMessage(MUTATION_MESSAGE_CODE code, CVertex *vertex)
{
   messageCode = code;

   if (code != ADD_VERTEX /*&& code != MIGRATE_VERTEX*/)
   {
      cout << "!!!!!! ERROR TIGLMutationMessage::writeVertexMessage  wrong call" << endl;
      return false;
   }

   //if(code == ADD_VERTEX)
   prepareAddVertexMessage(vertex);
//    else // code == MIGRATE_VERTEX
//       prepareMigrateVertexMessage(vertex);
   return true;
}

/*****************************************************************************/
/*!
 * @brief Procedure for writing add empty/remove vertex messages.
 * @param code message code: can be either ADD_EMPTY_VERTEX or REMOVE_VERTEX
 * @param id pointer to the vertex id
 *
 */
/*****************************************************************************/
template<typename ID_TYPE>
bool TIGLMutationMessage::writeSimpleVertexMessage(MUTATION_MESSAGE_CODE code, ID_TYPE *id)
{
   messageCode = code;
   if (code != ADD_EMPTY_VERTEX && code != REMOVE_VERTEX)
   {
      cout << "!!!!!! ERROR TIGLMutationMessage::writeSimpleVertexMessage  wrong call" << endl;
      return false;
   }
   prepareEmptyVertexMessage(id);
   return true;
}
#endif
