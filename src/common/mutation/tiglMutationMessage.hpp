#ifndef TIGLMUTATIONMESSAGE_HPP_
#define TIGLMUTATIONMESSAGE_HPP_

/*****************************************************************************/
/*!
 *@file tiglMutationMessage.hpp
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/

/******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/
#include  "tiglCommunication.hpp"

/******************************************************************************
 *
 * MUTATION_MESSAGE_CODE enumeration
 *
 ******************************************************************************/
typedef enum
{
   ADD_VERTEX, ADD_EMPTY_VERTEX, REMOVE_VERTEX, ADD_OUT_EDGE, ADD_IN_EDGE, REMOVE_OUT_EDGE, REMOVE_IN_EDGE, UNDEFINED_MUTATION_MESSAGE
} MUTATION_MESSAGE_CODE;

/******************************************************************************
 *
 * TIGLMutationMessage class
 *
 ******************************************************************************/

/*****************************************************************************/
/*!
 * @ingroup mutation_engine
 * @class TIGLMutationMessage
 * @brief Core mutation Massage container class
 */
/*****************************************************************************/
class TIGLMutationMessage
{

   /******************************************************************************
    *
    * type definitions
    *
    ******************************************************************************/
protected:
   typedef vector<uint8_t>::iterator BYTE_ITERATOR;

   /******************************************************************************
    *
    * constructor/destructor
    *
    ******************************************************************************/
public:
   TIGLMutationMessage()
   {
      messageCode = UNDEFINED_MUTATION_MESSAGE;
      otherDevice = UNKNOWN_DEV;
      messageBody.clear();
   }

   TIGLMutationMessage(int_fast32_t dev)
   {
      otherDevice = dev;
      messageCode = UNDEFINED_MUTATION_MESSAGE;
      messageBody.clear();
   }

   ~TIGLMutationMessage()
   {
   }

   /******************************************************************************
    *
    * attributes
    *
    ******************************************************************************/
protected:
   /// code of the mutation message
   MUTATION_MESSAGE_CODE messageCode;
   /// message body in bytes
   vector<uint8_t> messageBody;
   /// the destination device
   uint_fast32_t otherDevice;

   /******************************************************************************
    *
    * methods
    *
    ******************************************************************************/
protected:
   template<typename EdgeType, typename ID_TYPE>
   uint_fast32_t prepareAddEdgeMessage(EdgeType *pEdge, ID_TYPE *targetVertex);
   template<typename EdgeType, typename ID_TYPE>
   bool readAddEdgeMessage(EdgeType *pEdge, ID_TYPE *targetVertex);
   template<typename EDGE_TYPE, typename ID_TYPE>
   uint_fast32_t prepareRemoveEdgeMessage(EDGE_TYPE *edge, ID_TYPE *targetVertex);
   template<typename ID_TYPE>
   uint_fast32_t prepareRemoveEdgeMessage(ID_TYPE *nbrVertex, ID_TYPE *targetVertex);
   template<typename ID_TYPE> bool readRemoveEdgeMessage(ID_TYPE *nbrVertex, ID_TYPE *targetVertex);
   template<typename ID_TYPE> uint_fast32_t prepareEmptyVertexMessage(ID_TYPE *vertexId);
   template<typename ID_TYPE> bool readEmptyVertexMessage(ID_TYPE *vertexId);
   template<typename CVertex> uint_fast32_t prepareAddVertexMessage(CVertex *vertex);
   template<typename CVertex> bool readAddVertexMessage(CVertex *pVertex);
//
public:
   inline MUTATION_MESSAGE_CODE getMessageCode();
   inline uint_fast32_t getSizeInBytes();
   inline int_fast32_t getDstDev();
   template<typename ID_TYPE> bool getDstVertex(ID_TYPE *targetVertex);
   inline uint_fast32_t pack(uint8_t *buf);
   inline uint_fast32_t unpack(uint8_t *buf);
   template<typename CVertex> bool readVertexMessage(CVertex *vertex);
   template<typename ID_TYPE> bool readSimpleVertexMessage(ID_TYPE *id);
   template<typename EDGE_TYPE, typename ID_TYPE>
   bool readEdgeMessage(EDGE_TYPE *edge, ID_TYPE *targetVertex);
   template<typename ID_TYPE>
   bool readSimpleEdgeMessage(ID_TYPE *nbrVertex, ID_TYPE *targetVertex);
   template<typename EdgeType, typename ID_TYPE>
   bool writeEdgeMessage(MUTATION_MESSAGE_CODE code, EdgeType *edge, ID_TYPE *targetVertex);
   template<typename ID_TYPE>
   bool writeSimpleEdgeMessage(MUTATION_MESSAGE_CODE code, ID_TYPE *nbrVertex, ID_TYPE *targetVertex);
   template<typename CVertex>
   bool writeVertexMessage(MUTATION_MESSAGE_CODE code, CVertex *vertex);
   template<typename ID_TYPE>
   bool writeSimpleVertexMessage(MUTATION_MESSAGE_CODE code, ID_TYPE *id);
};

#include "tiglMutationMessageImplementation.hpp"
#endif /* TIGLMUTATIONMESSAGE_HPP_ */
