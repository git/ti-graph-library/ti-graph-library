#ifndef TIGLMUTATIONMANAGER_HPP_
#define TIGLMUTATIONMANAGER_HPP_

/*****************************************************************************/
/*!
 * @file tiglMutationManager.hpp
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/

/******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/
#include "tiglMutationMessage.hpp"

/*****************************************************************************/
/*!
 * @ingroup mutation_engine
 * @class TIGLMutationManager
 * @brief  Mutation manager class.
 * @details It handles all graph mutation operations. It is a template class that is parameterized by
 * @param SUBGRAPH_CLASS the type of the data manager
 */
/*****************************************************************************/
template<typename SUBGRAPH_CLASS>
class TIGLMutationManager
{
   /******************************************************************************
    *
    * type definitions
    *
    ******************************************************************************/
public:
   typedef SUBGRAPH_CLASS DATA_CONTAINER_TYPE;
   typedef typename SUBGRAPH_CLASS::ID_TYPE ID_TYPE;
   typedef typename SUBGRAPH_CLASS::VERTEX_ITERATOR VERTEX_ITERATOR;
   typedef typename SUBGRAPH_CLASS::VERTEX_TYPE VERTEX_TYPE;
   typedef typename VERTEX_TYPE::EDGE_TYPE EDGE_TYPE;
   typedef typename VERTEX_TYPE::EDGE_ITERATOR EDGE_ITERATOR;

   /******************************************************************************
    *
    * constructor/destructor
    *
    ******************************************************************************/
public:
   TIGLMutationManager(DATA_CONTAINER_TYPE *pManager)
   {
      dataManager = pManager;
   }
   ~TIGLMutationManager()
   {
   }

   /******************************************************************************
    *
    * attributes
    *
    ******************************************************************************/
protected:
   /// pointer to the data manager object
   DATA_CONTAINER_TYPE *dataManager;

   /******************************************************************************
    *
    * methods
    *
    ******************************************************************************/
public:
   /*****************************************************************************/
   /*!
    * @brief Processes all mutation messages of all threads
    */
   /*****************************************************************************/
   void processAllMutationMessages()
   {
      int numAllocThreads = dataManager->getNumAllocThreads();
#if defined(_MPI)
      numAllocThreads--;
#endif
      OMP("omp parallel for")
      for (int thread = 0; thread < numAllocThreads; thread++)
         processMutationMessages(thread);
   }

   /*****************************************************************************/
   /*!
    * @brief Processes all mutation messages at a given superstep
    * @param thread the current thread (always 0 if single device)
    *
    */
   /*****************************************************************************/
   void processMutationMessages(int thread = 0)
   {
      vector<TIGLMutationMessage> *messBuf = dataManager->getInMutationMessageBuf(thread);
      if (messBuf->empty())
         return;
      vector<TIGLMutationMessage>::iterator itMessage;
      vector<TIGLMutationMessage>::iterator itBegin = messBuf->begin();

      /// \b Procedure
      /// 1. Iterate over all message in mutation message buffer
      for (itMessage = itBegin; itMessage != messBuf->end(); ++itMessage)
      {
         /// 2. Read the mutation message code
         MUTATION_MESSAGE_CODE messCode = itMessage->getMessageCode();

         /// 3. Depending on the message code call the appropriate procedure from the data manager @link TIGLContainersSubgraph object @endlink
         switch (messCode) {
            case ADD_VERTEX: {
               VERTEX_TYPE newVertex(0);   // uinitialized vertex
               if (itMessage->readVertexMessage(&newVertex) == false)
               {
                  OMP("omp critical (PRINT)")
                  cout << "!!!! ERROR reading ADD_VERTEX message " << endl;
                  continue;
               }

               dataManager->addVertex(newVertex);
               break;
            }
            case ADD_EMPTY_VERTEX: {
               ID_TYPE id;
               if (itMessage->readSimpleVertexMessage(&id) == false)
               {
                  cout << "!!!! ERROR reading ADD_EMPTY_VERTEX message " << endl;
                  continue;
               }

               dataManager->addVertex(id);
               break;
            }
            case REMOVE_VERTEX: {
               ID_TYPE id;
               if (itMessage->readSimpleVertexMessage(&id) == false)
               {
                  cout << "!!!! ERROR reading REMOVE_VERTEX message " << endl;
                  continue;
               }
               VERTEX_ITERATOR itv;
               if (dataManager->getVertex(id, &itv) == false)
                  continue;
               // removing all edges of this vertex
               EDGE_ITERATOR itEdge;
               if (itv->getNumOutEdges())
               {
                  if (dataManager->isDirected() == false)
                  {   // undirected graph
                     for (itEdge = itv->getBeginOutEdgeIterator(); itEdge != itv->getEndOutEdgeIterator(); ++itEdge)
                     {
                        ID_TYPE targetVertex = itEdge->getNbr();
                        if (dataManager->exist(targetVertex))
                        {   // removing the corresponding output edge
                           dataManager->removeOutEdge(targetVertex, id);
                        }
#if defined(_MPI)
                        else
                        {
                           TIGLMutationMessage mess(UNKNOWN_DEV);   //mess(REMOVE_OUT_EDGE, UNKNOWN_DEV, &id, &targetVertex);
                           mess.writeSimpleEdgeMessage(REMOVE_OUT_EDGE, &id, &targetVertex);
                           dataManager->pushExternalMutationMessage(mess, thread);
                        }
#endif
                     }
                  }
               }
               else if (itv->isBiVertex() == true && dataManager->isDirected() == true)
               {   // directed and bivertex
                   // removing output edges (which are input edges to target vertics)
                  for (itEdge = itv->getBeginOutEdgeIterator(); itEdge != itv->getEndOutEdgeIterator(); ++itEdge)
                  {
                     ID_TYPE targetVertex = itEdge->getNbr();
                     if (dataManager->exist(targetVertex))
                     {
                        dataManager->removeInEdge(id, targetVertex);
                     }
#if defined(_MPI)
                     else
                     {
                        TIGLMutationMessage mess(UNKNOWN_DEV);					//
                        mess.writeSimpleEdgeMessage(REMOVE_IN_EDGE, &id, &targetVertex);
                        dataManager->pushExternalMutationMessage(mess, thread);
                     }
#endif
                  }
                  // now removing input edges (which are output edges to target vertices)
                  for (itEdge = itv->getBeginInEdgeIterator(); itEdge != itv->getEndInEdgeIterator(); ++itEdge)
                  {
                     ID_TYPE srcVertex = itEdge->getNbr();
                     if (dataManager->exist(srcVertex))
                     {
                        dataManager->removeOutEdge(srcVertex, id);
                     }
#if defined(_MPI)
                     else
                     {
                        TIGLMutationMessage mess(UNKNOWN_DEV);					//
                        mess.writeSimpleEdgeMessage(REMOVE_OUT_EDGE, &id, &srcVertex);
                        dataManager->pushExternalMutationMessage(mess, thread);
                     }
#endif
                  }
               }
               // there is a third hole case where it is directed and not bivertex; which cannot be handled here
               dataManager->removeVertex(id);
               break;
            }
            case ADD_OUT_EDGE:
            case ADD_IN_EDGE: {
               EDGE_TYPE edge(0);
               ID_TYPE vertex;
               if (itMessage->readEdgeMessage(&edge, &vertex) == false)
               {
                  OMP("omp critical(PRINT)")
                  cout << "!!!!!!!!! ERROR in thread " << thread << " reading edge message " << endl;
                  continue;
               }
               if (dataManager->exist(vertex) == false)
               {
                  OMP("omp critical(PRINT)")
                  cout << "!!!!!!!!! ERROR in thread " << thread << ": vertex " << vertex << " does not exist" << endl;
                  continue;
               }
               if (messCode == ADD_OUT_EDGE)
               {
                  if (dataManager->addOutEdge(vertex, edge) == false)
                  {
                     continue;
                  }
               }
               else
               {
                  dataManager->addInEdge(vertex, edge);
               }
               break;
            }
            case REMOVE_OUT_EDGE:
            case REMOVE_IN_EDGE:
               ID_TYPE src, target;
               if (itMessage->readSimpleEdgeMessage(&target, &src) == false)
               {
                  cout << "!!!! ERROR reading REMOVE_EDGE message " << endl;
                  continue;
               }

               if (messCode == REMOVE_OUT_EDGE)
                  dataManager->removeOutEdge(src, target);
               else
                  dataManager->removeInEdge(src, target);
               break;
            case UNDEFINED_MUTATION_MESSAGE:
               cout << "!!! warning in TIGLMutationManager::processMutationMessages : message  UNDEFINED_MUTATION_MESSAGE unsupported" << endl << endl;
         }
      }
      messBuf->clear();   // clearing the mutation message buffer at the end
   }
};

#endif /* TIGLENGINEMUTATIONMANAGER_HPP_ */
