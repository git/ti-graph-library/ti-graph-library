#ifndef TIGLCOMMUNICATION_HPP_
#define TIGLCOMMUNICATION_HPP_

/*****************************************************************************/
/*!
 *@file tiglCommunication.hpp
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/

/******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/
#include  <stdint.h>
#if defined(_MPI)
#include <mpi.h>
#endif

/******************************************************************************
 *
 * typedefs/Macros
 *
 ******************************************************************************/
#if !defined(_MPI)
// dummy definitions for consistency
typedef int MPI_Request;
typedef int MPI_Status;
#endif

#define MAX_MESSAGE_SIZE                   32768//2048//2048
#define MPI_DATA_MESSAGE_TAG		          10
#define MPI_SYSTEM_MESSAGE_TAG		       11
#define MPI_MUTATION_MESSAGE_TAG 	       12
#define MPI_DEV_MESSAGE_COUNT_TAG	       13
#define  UNKNOWN_DEV                       -1
#define NONBLOCKING_RX_THREADS             16//64 // number of concurrent nonblocking mpi read
#define NONBLOCKING_TX_THREADS             16//64 // number of concurrent nonblocking mpi read
#define NONBLOCKING_MUTATATION_BUILD_FLAG  0

/******************************************************************************
 *
 * general communication routines
 *
 ******************************************************************************/
/*!
 * @ingroup comm_module
 * @defgroup comm_routines General Communication Routines
 * @brief Contains general-purpose communication functions for TX/RX and monitoring.
 */
int_fast32_t TIGLCommunicationFindDevice(uint_fast32_t vtxID, uint_fast32_t totalNumVertices, uint_fast32_t numDevices);
// Rx routines
int TIGLCommunicationRecvMPICommand(uint8_t *rxBuf, uint_fast32_t bufSize, int_fast32_t src);
int_fast32_t TIGLCommunicationRecvMPICommand(uint8_t *rxBuf, uint_fast32_t bufSize, int_fast32_t src, int tag);
int TIGLCommunicationRecvMPICommand(uint8_t *rxBuf, int_fast32_t *numMessageuint8_ts);
int_fast32_t TIGLCommunicationRecvTaggedMPICommand(uint8_t *rxBuf, int tag);
int_fast32_t TIGLCommunicationRecvTaggedMPICommandfromDev(uint8_t *rxBuf, int tag, int dev, int count);
int TIGLCommunicationRecvMPICommandNonblocking(uint8_t *rxBuf, MPI_Request *req);
int TIGLCommunicationRecvMPICommandNonblocking(uint8_t *rxBuf, int messageSize, int dev, int tag, MPI_Request *req);

// Tx routines
int TIGLCommunicationSendMPICommand(uint8_t *txBuf, uint_fast32_t bufSize, int_fast32_t dst, int tag);
int TIGLCommunicationSendMPICommandNonblocking(uint8_t *txBuf, uint_fast32_t bufSize, int_fast32_t dst, int tag, MPI_Request *req);

// monitoring routines
bool TIGLCommunicationExistMutationMessages();
bool TIGLCommunicationExistMessages();
bool TIGLCommunicationExistMessages(int *tag, int *dev, int *messageSize);
bool TIGLCommunicationExistMessagesFromDev(uint_fast32_t dev);
bool TIGLCommunicationExistMessagesWithTag(int tag);
bool TIGLCommunicationExistTaggedMessageFromDev(int tag, uint_fast32_t dev, int_fast32_t *messageSize);
int TIGLCommunicationWaitMPINonblocking(MPI_Request *req);
bool TIGLCommunicationTestMPINonblocking(MPI_Request *req, int *tag, int *messageSize);
bool TIGLCommunicationTestMPINonblocking(MPI_Request *req);

/******************************************************************************
 *
 * TIGLCommunicationMessageDev structure
 *
 ******************************************************************************/
/*!
 * @ingroup comm_engine
 * @struct TIGLCommunicationMessageDev
 * @brief message container for TX/RX
 */
typedef struct TIGLCommunicationMessageDev
{
   /******************************************************************************
    *
    * attributes
    *
    ******************************************************************************/
   /// byte buffer for message storage
   uint8_t buf[MAX_MESSAGE_SIZE];
   /// accumulative size in buf
   uint_fast32_t accMessageSize;
   /// accumulative number of data message transmissions
   uint_fast32_t numTx;
   /// accumulative number of system message transmissions
   uint_fast32_t numSysTx;
   /// accumulative number of mutation message transmissions
   uint_fast32_t numMutationTx;

   /******************************************************************************
    *
    * Methods
    *
    ******************************************************************************/
public:
   /*****************************************************************************/
   /*!
    * @brief constructor
    */
   /*****************************************************************************/
   TIGLCommunicationMessageDev()
   {
      this->reset();
   }

   /*****************************************************************************/
   /*!
    * @brief resetting all counters
    */
   /*****************************************************************************/
   inline void reset()
   {
      accMessageSize = 0;
      numTx = 0;
      numSysTx = 0;
      numMutationTx = 0;
   }

   /*****************************************************************************/
   /*!
    * @brief returns the total number of messages of all types
    */
   /*****************************************************************************/
   inline uint_fast32_t getMessageCount()
   {
      return numTx + numSysTx + numMutationTx;
   }
} TIGLCommunicationMessageDev;

#include "tiglCommunicationManager.hpp"
#include "tiglCommunicationSysMessage.hpp"

#endif /* TIGLCOMMUNICATION_HPP_ */
