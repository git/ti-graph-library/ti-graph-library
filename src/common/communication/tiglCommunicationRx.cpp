/*****************************************************************************/
/*!
 * @file tiglCommunicationRx.cpp
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/

/******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/
#include "tiglCommunication.hpp"
/******************************************************************************/

/******************************************************************************/
/*!
 * @ingroup comm_module comm_routines
 * @brief Receives a message from a particular src with any tag
 * @param rxBuf receiver buffer
 * @param bufSize buffer size in bytes
 * @param src the message source device
 * @return the message tag
 *
 */
/******************************************************************************/
int TIGLCommunicationRecvMPICommand(uint8_t *rxBuf, uint_fast32_t bufSize, int_fast32_t src)
{
#if defined(_MPI)
   MPI_Status stat;
   MPI_Recv(rxBuf, bufSize, MPI_CHAR, src, MPI_ANY_TAG, MPI_COMM_WORLD, &stat);
   return stat.MPI_TAG;
#else
   return 0;
#endif
}

/******************************************************************************/
/*!
 * @ingroup comm_module comm_routines
 * @brief Receives a message from a particular src with a given tag
 * @param rxBuf receiver buffer
 * @param bufSize buffer size in bytes
 * @param src the message source device
 * @param tag the message tag
 * @return the size of the message in bytes
 */
/******************************************************************************/
int_fast32_t TIGLCommunicationRecvMPICommand(uint8_t *rxBuf, uint_fast32_t bufSize, int_fast32_t src, int tag)
{
#if defined(_MPI)
   MPI_Status stat;
   MPI_Recv(rxBuf, bufSize, MPI_CHAR, src, tag, MPI_COMM_WORLD, &stat);
   int_fast32_t numMessageBytes;
   int count;
   MPI_Get_count(&stat, MPI_CHAR, &count);
   numMessageBytes = (int_fast32_t)count; // for compatibility with g++
   return numMessageBytes;
#else
   return 0;
#endif
}

/******************************************************************************/
/*!
 * @ingroup comm_module comm_routines
 * @brief Receives a message with any tag from any source
 * @param rxBuf receiver buffer
 * @param numMessageBytes pointer in which the message size is written
 * @return the message tag
 */
/******************************************************************************/
int TIGLCommunicationRecvMPICommand(uint8_t *rxBuf, int_fast32_t *numMessageBytes)
{
#if defined(_MPI)
   MPI_Status status;
   MPI_Recv(rxBuf, MAX_MESSAGE_SIZE, MPI_CHAR, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
   //fromDevId = status.MPI_SOURCE;
   int count;
   MPI_Get_count(&status, MPI_CHAR, &count);
   *numMessageBytes = (int_fast32_t) count;
   return status.MPI_TAG;
#else
   return 0;
#endif
}

/******************************************************************************/
/*!
 * @ingroup comm_module comm_routines
 * @brief Receives a message with any tag from any source
 * @param rxBuf receiver buffer
 * @param req pointer to MPI_Request object (for nonblocking calls)
 * @return standard return of MPI_Irecv
 */
/******************************************************************************/
int TIGLCommunicationRecvMPICommandNonblocking(uint8_t *rxBuf, MPI_Request *req)
{
#if defined(_MPI)
   return MPI_Irecv(rxBuf, MAX_MESSAGE_SIZE, MPI_CHAR, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, req);
#else
   return 0;
#endif
}

/******************************************************************************/
/*!
 * @ingroup comm_module comm_routines
 * @brief Nonblocking function for receiving a message with a tag from a given source
 * @param rxBuf receiver buffer
 * @param messageSize message size in bytes
 * @param dev device rank
 * @param tag message tag
 * @param req pointer to MPI_Request object (for nonblocking calls)
 * @return @return standard return of MPI_Irecv
 */
/******************************************************************************/
int TIGLCommunicationRecvMPICommandNonblocking(uint8_t *rxBuf, int messageSize, int dev, int tag, MPI_Request *req)
{
#if defined(_MPI)
   return MPI_Irecv(rxBuf, messageSize, MPI_CHAR, dev, tag, MPI_COMM_WORLD, req);
#else
   return 0;
#endif
}

/******************************************************************************/
/*!
 * @ingroup comm_module comm_routines
 * @brief Receives a message with any tag from any source
 * @param rxBuf receiver buffer
 * @param tag the message tag
 * @return the message size in bytes
 */
/******************************************************************************/
int_fast32_t TIGLCommunicationRecvTaggedMPICommand(uint8_t *rxBuf, int tag)
{
#if defined(_MPI)
   MPI_Status status;
   MPI_Recv(rxBuf, MAX_MESSAGE_SIZE, MPI_CHAR, MPI_ANY_SOURCE, tag,
         MPI_COMM_WORLD, &status);
   //fromDevId = status.MPI_SOURCE;
   int_fast32_t numMessageBytes;
   int count;
   MPI_Get_count(&status, MPI_CHAR, &count);
   numMessageBytes = (int_fast32_t) count;
   return numMessageBytes;
#else
   return 0;
#endif
}

/******************************************************************************/
/*!
 * @ingroup comm_module comm_routines
 * @brief Receives a message with any tag from any source
 * @param rxBuf receiver buffer
 * @param tag the message tag
 * @param dev the rank of the tranmitting device
 * @param count maximum size of input message
 * @return the message size in bytes
 */
/******************************************************************************/
int_fast32_t TIGLCommunicationRecvTaggedMPICommandfromDev(uint8_t *rxBuf, int tag, int dev, int count =
MAX_MESSAGE_SIZE)
{
#if defined(_MPI)
   MPI_Status status;
   if(MPI_Recv(rxBuf, count, MPI_CHAR, dev, tag, MPI_COMM_WORLD, &status) == MPI_SUCCESS)
   {
      int_fast32_t numMessageBytes;
      int count;
      MPI_Get_count(&status, MPI_CHAR, &count);
      numMessageBytes = (int_fast32_t) count;
      return numMessageBytes;
   }
   else
   return 0;
#else
   return 0;
#endif
}
