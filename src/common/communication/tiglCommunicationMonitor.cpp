/*****************************************************************************/
/*!
 * @file tiglCommunicationMonitor.cpp
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/

/******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/
#include "tiglCommunication.hpp"
/******************************************************************************/

/******************************************************************************/
/*!
 * @ingroup comm_module comm_routines
 * @brief Check if there exist messages from any source with any tag
 * @return true if there are messages, false otherwise
 *
 */
/******************************************************************************/
bool TIGLCommunicationExistMutationMessages()
{
#if defined(_MPI)
   return TIGLCommunicationExistMessagesWithTag(MPI_MUTATION_MESSAGE_TAG);
#else
   return false;
#endif
}

/******************************************************************************/
/*!
 * @ingroup comm_module comm_routines
 * @brief Checks if there exist messages from any source with any tag
 * @return true if there are messages, false otherwise
 *
 */
/******************************************************************************/
bool TIGLCommunicationExistMessages()
{
#if defined(_MPI)
   MPI_Status stat;
   int gotMsg = 0;
   MPI_Iprobe(MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &gotMsg, &stat);
   return gotMsg? true:false;
#else
   return false;
#endif
}

/******************************************************************************/
/*!
 * @ingroup comm_module comm_routines
 * @brief Checks if there exist messages from any source with any tag
 * @return true if there are messages, false otherwise
 */
/******************************************************************************/
bool TIGLCommunicationExistMessages(int *tag, int *dev, int *messageSize)
{
#if defined(_MPI)
   MPI_Status stat;
   int gotMsg = 0;
   MPI_Iprobe(MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &gotMsg, &stat);
   if(gotMsg)
   {
      *tag = stat.MPI_TAG;
      *dev = stat.MPI_SOURCE;
      MPI_Get_count(&stat, MPI_CHAR, messageSize);
   }

   return gotMsg? true:false;
#else
   return false;
#endif
}
/******************************************************************************/
/*!
 * @ingroup comm_module comm_routines
 * @brief Checks if there exist messages with any tag from a given source device
 * @param <dev> source device
 * @return true if there are messages, false otherwise
 *
 */
/******************************************************************************/
bool TIGLCommunicationExistMessagesFromDev(uint_fast32_t dev)
{
#if defined(_MPI)
   MPI_Status stat;
   int gotMsg = 0;
   MPI_Iprobe(dev, MPI_ANY_TAG, MPI_COMM_WORLD, &gotMsg, &stat);
   return gotMsg? true:false;
#else
   return false;
#endif
}

/******************************************************************************/
/*!
 * @ingroup comm_module comm_routines
 * @brief Checks if there exist messages with a given tag from any source
 * @param <tag> message tag
 * @return true if there are messages, false otherwise
 *
 */
/******************************************************************************/
bool TIGLCommunicationExistMessagesWithTag(int tag)
{
#if defined(_MPI)
   MPI_Status stat;
   int gotMsg = 0;
   MPI_Iprobe(MPI_ANY_SOURCE, tag, MPI_COMM_WORLD, &gotMsg, &stat);
   return gotMsg? true:false;
#else
   return false;
#endif
}

/******************************************************************************/
/*!
 * @ingroup comm_module comm_routines
 * @brief checks if there exist messages with a given tag from a given source
 * @param tag message tag
 * @param dev rank source device
 * @param messageSize pointer to the size of the message in bytes (upon completion). It is not changed if the process is not completed
 * @return true if there are messages, false otherwise
 *
 */
/******************************************************************************/
bool TIGLCommunicationExistTaggedMessageFromDev(int tag, uint_fast32_t dev, int_fast32_t *messageSize)
{
#if defined(_MPI)
   MPI_Status stat;
   int gotMsg = 0;
   MPI_Iprobe(dev, tag, MPI_COMM_WORLD, &gotMsg, &stat);
   if(gotMsg)
   {
      int count ;
      MPI_Get_count(&stat, MPI_CHAR, &count);
      *messageSize = (int_fast32_t)count;
      return true;
   }
   else
   {
      *messageSize = 0;
      return false;
   }
#else
   return false;
#endif
}

/******************************************************************************/
/*!
 * @ingroup comm_module comm_routines
 * @brief Waits for nonblocking tx/rx
 * @param req pointer to MPI_request object that was filled with the tx/rx
 * @return true if there are messages, false otherwise
 *
 */
/******************************************************************************/
int TIGLCommunicationWaitMPINonblocking(MPI_Request *req)
{
#if defined(_MPI)
   MPI_Status stat;
   return MPI_Wait(req, &stat);
#else
   return 0;
#endif
}

/******************************************************************************/
/*!
 * @ingroup comm_module comm_routines
 * @brief Tests the status nonblocking tx/rx and return the tag and message size
 * @param req pointer to MPI_request object that was filled with the tx/rx
 * @param tag pointer to the the tag of the message (upon completion). It is not changed if the process is not completed
 * @param messageSize pointer to the size of the message in bytes (upon completion). It is not changed if the process is not completed
 * @return true if process is completed, false otherwise
 */
/******************************************************************************/
bool TIGLCommunicationTestMPINonblocking(MPI_Request *req, int *tag, int *messageSize)
{
#if defined(_MPI)
   MPI_Status stat;
   int test;
   MPI_Test(req, &test, &stat);
   if(test)
   {
      MPI_Get_count(&stat, MPI_CHAR, messageSize);
      *tag = stat.MPI_TAG;
      return true;
   }
   else
   return false;
#else
   return 0;
#endif
}

/******************************************************************************/
/*!
 * @ingroup comm_module comm_routines
 * @brief Simple test the status nonblocking tx/rx
 * @param req pointer to MPI_request object that was filled with the tx/rx
 * @return true if process is completed, false otherwise
 *
 */
/******************************************************************************/
bool TIGLCommunicationTestMPINonblocking(MPI_Request *req)
{
#if defined(_MPI)
   MPI_Status stat;
   int test;
   MPI_Test(req, &test, &stat);
   if(test)
   return true;
   else
   return false;
#else
   return false;
#endif
}

