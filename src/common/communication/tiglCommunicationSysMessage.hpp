#ifndef TIGLCOMMUNICATIONSYSMESSAGE_HPP_
#define TIGLCOMMUNICATIONSYSMESSAGE_HPP_

/*****************************************************************************/
/*!
 *@file tiglCommunicationSysMessage.hpp
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/

/******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/
#include  <stdint.h>
#include <vector>
/******************************************************************************/

/******************************************************************************
 *
 * SYS_MESSAGE_CODE enumeration
 *
 ******************************************************************************/
typedef enum
{
   ADD_VERTEX_INFO = 0,   // adding vertex pos information to the position hash table
   REMOVE_VERTEX_INFO,   // removing vertex information (vertex is deleted)
   UPDATE_VERTEX_INFO,   // changing the vertex pos info in the position hash table
//	ADD_EDGE, 
//	REMOVE_EDGE, 
   GET_VERTEX_POS,   // request a position of a given vertex
   INFO_VERTEX_POS,   // provides the device position of a given vertex
   VERTEX_NOT_EXIST,
//    ERR_RCVR_NOT_EXIST, 
   UNDEFINED_MESSAGE,
   AGGREGATE_INFO,
   DEV_IDLE,
   HALT
} SYS_MESSAGE_CODE;

inline SYS_MESSAGE_CODE getMessageCode(uint8_t *buf)
{
   SYS_MESSAGE_CODE code = (SYS_MESSAGE_CODE) *(buf++);
   return code;
}

/*!
 * @ingroup comm_module
 * @defgroup messages
 * @brief Contains the class definitions of inter-device messages
 */
/******************************************************************************
 *
 * TIGLCommunicationSysMessage class
 *
 ******************************************************************************/
/*!
 * @ingroup comm_module messages
 * @class TIGLCommunicationSysMessage
 * @brief Core system message class definition
 * @details It handles all the system message fields and processing
 */

using namespace std;

class TIGLCommunicationSysMessage
{
   /******************************************************************************
    *
    * Constructor/Destructor
    *
    ******************************************************************************/
public:
   /*****************************************************************************/
   /*!
    * @brief Constructor set the fields of the system message
    * @param code system message
    * @param dev destination device
    * @param message pointer to message content
    *
    */
   /*****************************************************************************/
   template<typename MESSAGE_TYPE>
   TIGLCommunicationSysMessage(SYS_MESSAGE_CODE code, uint_fast32_t dev, MESSAGE_TYPE *message)
   {
      messageCode = code;
      otherDevice = dev;

      messageBody.clear();

      uint8_t *messageBytes = (uint8_t *) message;
      messageBody.insert(messageBody.begin(), messageBytes, messageBytes + sizeof(MESSAGE_TYPE));
   }

   /*****************************************************************************/
   /*!
    * @brief Overloaded constructor for system messages that do not have a body
    * @param code system message
    * @param dev destination device
    *
    */
   /*****************************************************************************/
   TIGLCommunicationSysMessage(SYS_MESSAGE_CODE code, uint_fast32_t dev)
   {
      messageCode = code;
      otherDevice = dev;

      messageBody.clear();
   }

   /*****************************************************************************/
   /*!
    * @brief Overloaded constructor for an system message
    * @details empty message to be filled by the unpack procedure
    *
    */
   /*****************************************************************************/
   TIGLCommunicationSysMessage()
   {
      messageCode = UNDEFINED_MESSAGE;
      messageBody.clear();
   }

   ~TIGLCommunicationSysMessage()
   {
   }

   /******************************************************************************
    *
    * attributes
    *
    ******************************************************************************/
protected:
   /// @param code of the system message
   SYS_MESSAGE_CODE messageCode;
   /// @param message body in bytes
   vector<uint8_t> messageBody;
   /// @param the destination device
   uint_fast32_t otherDevice;

   /******************************************************************************
    *
    * methods
    *
    ******************************************************************************/
public:
   /*****************************************************************************/
   /*!
    * @brief Returns the system message code
    */
   /*****************************************************************************/
   inline SYS_MESSAGE_CODE getMessageCode()
   {
      return messageCode;
   }

   /*****************************************************************************/
   /*!
    * @brief Checks if the message has a body
    */
   /*****************************************************************************/
   inline bool hasBody()
   {
      return (messageBody.size() > 0);
   }

   /*****************************************************************************/
   /*!
    * @brief Reads the variable-size message body
    */
   /*****************************************************************************/
   inline uint_fast32_t getSizeInBytes()
   {
      return messageBody.size() + 1;
   }

   /*****************************************************************************/
   /*!
    * @brief Reads the destination device
    */
   /*****************************************************************************/
   inline uint_fast32_t getDst()
   {
      return otherDevice;
   }

   /*****************************************************************************/
   /*!
    * @brief Packs the system message into bytes
    * @details The packed message contains the message code and the message body
    * @param buf output buffer for packed message
    * @return the message size in bytes
    *
    */
   /*****************************************************************************/
   inline uint_fast32_t pack(uint8_t *buf)
   {
      // putting the message code and the system message flag
      uint8_t firstByte = (uint8_t) messageCode;	// | IS_SM_MASK;
      *(buf++) = firstByte;
      uint_fast32_t messageSize = getSizeInBytes();
      if (messageSize > 1)
      {
         std::copy(messageBody.begin(), messageBody.end(), buf);
      }
      return messageSize;
   }

   /*****************************************************************************/
   /*!
    * @brief Unpacks a system message to the object fields
    * @param buf input buffer for the packed message
    * @param numBytes the size in bytes of the packed message
    *
    */
   /*****************************************************************************/
   inline void unpack(uint8_t *buf, uint_fast32_t numBytes)
   {
      messageCode = (SYS_MESSAGE_CODE) (*(buf++));
      messageBody.clear();
      if (numBytes > 1)
      {
         messageBody.insert(messageBody.begin(), buf, buf + numBytes - 1);   //std::copy(buf, buf+numBytes-1, messageBody.begin());
      }
   }

   /*****************************************************************************/
   /*!
    * @brief Converts a packed format to a customized message format
    * @param message pointer to the output message buffer
    *  @return true if success, otherwise if failed
    *
    */
   /*****************************************************************************/
   template<typename MESSAGE_TYPE>
   inline bool readMessageBody(MESSAGE_TYPE *message)
   {
      if (messageBody.size() != sizeof(MESSAGE_TYPE))
      {
         cout << "XXX ERROR bodySize = " << messageBody.size() << "/" << sizeof(MESSAGE_TYPE) << endl;
         return false;
      }
      uint8_t *p = (uint8_t *) message;
      std::copy(messageBody.begin(), messageBody.end(), p);

      return true;
   }
};

#endif /* TIGLCOMMUNICATIONSYSMESSAGE_HPP_ */
