#ifndef TIGLCOMMUNICATIONAGGREGATESYSMESSAGE_HPP_
#define TIGLCOMMUNICATIONAGGREGATESYSMESSAGE_HPP_

/*****************************************************************************/
/*!
 * @file tiglCommunicationAggregateSysMessage.hpp
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/

/******************************************************************************
 *
 * TIGLCommunicationAggregateSysMessage class
 *
 ******************************************************************************/
/*!
 * @ingroup comm_module messages
 * @class  TIGLCommunicationAggregateSysMessage
 * @brief Base class for aggregate information messages
 * @details It holds aggregate information that is exchanged between devices
 *  it is a template class that is parameterized the algorithm data field of the graph algorithm
 *
 */
template<typename AlgData>
class TIGLCommunicationAggregateSysMessage
{
   /******************************************************************************
    *
    * attributes
    *
    ******************************************************************************/
protected:
   /// maximum aggregate value
   AlgData maxAgg;
   ///minimum aggregate value
   AlgData minAgg;
   /// sum of aggregate values
   AlgData sumAgg;

   /******************************************************************************
    *
    * public methods
    *
    ******************************************************************************/
public:
   /*****************************************************************************/
   /*!
    * @brief set the maximum value
    */
   /*****************************************************************************/
   inline void setMax(AlgData val)
   {
      maxAgg = val;
   }

   /*****************************************************************************/
   /*!
    * @brief set the minimum value of the algData
    */
   /*****************************************************************************/
   inline void setMin(AlgData val)
   {
      minAgg = val;
   }

   /*****************************************************************************/
   /*!
    * @brief set the sum value field
    */
   /*****************************************************************************/
   inline void setSum(AlgData val)
   {
      sumAgg = val;
   }

   /*****************************************************************************/
   /*!
    * @brief get the max value field
    */
   /*****************************************************************************/
   inline AlgData getMax()
   {
      return maxAgg;
   }

   /*****************************************************************************/
   /*!
    *  @brief get the min value field
    */
   /*****************************************************************************/
   inline AlgData getMin()
   {
      return minAgg;
   }

   /*****************************************************************************/
   /*!
    * @brief get the sum value field
    */
   /*****************************************************************************/
   inline AlgData getSum()
   {
      return sumAgg;
   }
};
#endif /* TIGLCOMMUNICATIONAGGREGATESYSMESSAGE_HPP_ */
