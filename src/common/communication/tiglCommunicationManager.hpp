#ifndef TIGLCOMMUNICATIONMANAGER_HPP_
#define TIGLCOMMUNICATIONMANAGER_HPP_

/*****************************************************************************/
/*!
 * @file tiglCommunicationManager.hpp
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/

/******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/
#include <iostream>
#include <vector>
#include <unistd.h>
#include  <stdint.h>
#include "tiglCommunication.hpp"
#include "tiglContainersSubgraph.hpp"
#include "tiglUtils.hpp"

/******************************************************************************
 *
 * NAMESPACES
 *
 ******************************************************************************/
using namespace std;
/******************************************************************************
 *
 * TIGLCommunicationManager class
 *
 ******************************************************************************/

/*****************************************************************************/
/*!
 * @ingroup comm_module
 * @defgroup comm_engine Communication Engine
 * @brief Contains the core class for handling all inter-device communication
 */
/*****************************************************************************/

/*****************************************************************************/
/*!
 * @ingroup comm_module comm_engine
 * @class TIGLCommunicationManager
 * @brief Core communicator class definition
 * @details It handles all the MPI communication between devices (including both system and data messages)
 * It is a template class that is parameterized by
 * @param SUBGRAPH_TYPE the class definition of the data manager object
 */
/*****************************************************************************/
template<typename SUBGRAPH_TYPE>
class TIGLCommunicationManager
{
   /******************************************************************************
    *
    * type definitions
    *
    ******************************************************************************/
private:
   typedef typename SUBGRAPH_TYPE::MESSAGE_TYPE DATA_MESSAGE_TYPE;
   typedef TIGLCommunicationSysMessage SYS_MESSAGE_TYPE;
   typedef vector<DATA_MESSAGE_TYPE> DATA_MESSAGE_BUF_TYPE;
   typedef typename DATA_MESSAGE_BUF_TYPE::iterator MESSAGE_BUF_ITERATOR;
   typedef vector<SYS_MESSAGE_TYPE> SYS_MESSAGE_BUF_TYPE;
   typedef typename SUBGRAPH_TYPE::ID_TYPE ID_TYPE;
   typedef typename SUBGRAPH_TYPE::INDEX_ITERATOR INDEX_ITERATOR;
   typedef typename SUBGRAPH_TYPE::MUTATION_MBUF MUTATION_MBUF;
   typedef vector<DATA_MESSAGE_BUF_TYPE>* MBUF_PTR;
   typedef SYS_MESSAGE_BUF_TYPE* SYS_MBUF_PTR;
   typedef MUTATION_MBUF* MUTATION_MBUF_PTR;
   typedef typename MUTATION_MBUF::iterator MUTATION_ITERATOR;
   //typedef vector<DATA_MESSAGE_BUF_TYPE>					EXTERNAL_MESSAGE_BUF;
   typedef DATA_MESSAGE_BUF_TYPE EXTERNAL_MESSAGE_BUF;

   /******************************************************************************
    *
    * constructor/destructor
    *
    ******************************************************************************/
public:
   TIGLCommunicationManager();
   TIGLCommunicationManager(SUBGRAPH_TYPE *dataPtr, uint_fast32_t devices, int rank = 0);
   ~TIGLCommunicationManager();

   /******************************************************************************
    *
    * attributes
    *
    ******************************************************************************/
protected:
   /// pointer to PregelDataContainer object that contains all message buffers
   SUBGRAPH_TYPE *dataPtr;
   /// initlialization flag
   bool initFlag;
   /// total number of devices
   int_fast32_t numDevices;
   /// total number of vertices in all devices
   uint64_t totalNumVertices;
   /// pointer to the ss counter
   uint_fast32_t *timeCountSS;
   /// MPI rank of the current device
   int_fast32_t rank;
   /// MPI_Request objectsfor nonBlocking TX
   MPI_Request txRequests[NONBLOCKING_TX_THREADS];
   /// flags nonBlocking TX
   bool txBusyRequestFlag[NONBLOCKING_TX_THREADS];
   /// output buffers for the different TX threads
   uint8_t txBuffers[NONBLOCKING_TX_THREADS][MAX_MESSAGE_SIZE];

   MPI_Request *txCountRequests;
   uint_fast32_t *numDevMessagesBuf;
   /// counter for data messages
   uint_fast32_t *dataTxCount;
   /// counter for mutation messages
   uint_fast32_t *mutationTxCount;
   /// counter for system messages
   uint_fast32_t sysTxCount;
   /// message counter for the device messages
   TIGLCommunicationMessageDev *devTxMessages;
   /// flag of the end of the SS execution (no more messages are generated)
   bool ssComputationEndFlag;
   /// flag of the end TX for current SS
   bool ssTxActiveFlag;
   /// flag of the end RX for current SS
   bool ssRxActiveFlag;
   /// holds the number of received messages from each device (incremented with each new message)
   int_fast32_t *rxDevMessageCount;
   /// holds the target number of messages from each device
   int_fast32_t *targetRxDevMessageCount;
   /// profiling information for the processing tasks
   uint_fast32_t txIdleTime, rxIdleTime, computeIdleTime;

   /******************************************************************************
    *
    * methods
    *
    ******************************************************************************/
protected:
   template<typename MESSAGE_TYPE>
   void bytes2Messages(uint8_t* rxBuf, uint_fast32_t messageSize, vector<MESSAGE_TYPE> &messageBuf);
   uint_fast32_t messages2Bytes(DATA_MESSAGE_BUF_TYPE *messagePtr, uint8_t *outBuf);
   void processOneMessage(uint8_t *rxBuf, int tag, uint_fast32_t messageSize, uint_fast32_t dev);
   void countMessages(uint_fast32_t *devMessagesCount);
   bool existDataMessages2Tx();
   bool existSysMessages2Tx();
   bool existMutationMessages2Tx();
   void sendDataMessages(bool flushBuffers = false);
   void sendMessagesSizes();
   void waitAllTx();
   int findTxSlot();
   bool RecvNewMessages();

public:
   inline void reset();
   inline void init(SUBGRAPH_TYPE *dPtr, uint_fast32_t devices = 1, uint_fast32_t devRank = 0);
   void initSS();
   bool isTxComplete();
   inline void endSS();
   void continuousSendMessages();
   void continuousRecvMessages();
   inline void printIdleTime();
   inline void setTimePtr(uint_fast32_t *timePtr);
   inline void setNumVertices(uint64_t numVertices);
   inline bool isMultipleDevices();
   void sendSysMessages();
   void sendMutationMessages();
   void sendInstMutationMessage(int thread);
   void taggedBcast(uint8_t *txBuf, uint_fast32_t bufSize, int tag);
   bool instRecvMutationMessages(int dev);
   inline int getRank();
};

#include "tiglCommunicationManagerImplementation.hpp"

#endif /* TIGLCOMMUNICATIONMANAGER_HPP_ */
