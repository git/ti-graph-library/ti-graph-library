#ifndef TIGLCOMMMUNICATIONMANAGERIMPLEMENTATION_HPP_
#define TIGLCOMMMUNICATIONMANAGERIMPLEMENTATION_HPP_

/*****************************************************************************/
/*!
 * @file  tiglCommunicationManagerImplementation.hpp
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/

/******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/
#include <string.h>
#include "tiglCommunicationManager.hpp"

/*****************************************************************************/
/*!
 * @brief Empty constructor
 */
/*****************************************************************************/
template<typename SUBGRAPH_TYPE>
TIGLCommunicationManager<SUBGRAPH_TYPE>::TIGLCommunicationManager()
{
   txCountRequests = 0;
   numDevMessagesBuf = 0;

   dataTxCount = 0;
   mutationTxCount = 0;

   devTxMessages = 0;
   rxDevMessageCount = 0;
   targetRxDevMessageCount = 0;

   txIdleTime = 0;
   rxIdleTime = 0;
   computeIdleTime = 0;

   reset();
}

/*****************************************************************************/
/*!
 * @brief Overloaded constructor constructor
 * @param dataPtr pointer to the data manager object
 * @param devices total number of devices
 * @param rank rank of current device
 */
/*****************************************************************************/
template<typename SUBGRAPH_TYPE>
TIGLCommunicationManager<SUBGRAPH_TYPE>::TIGLCommunicationManager(SUBGRAPH_TYPE *dataPtr, uint_fast32_t devices, int rank)
{
   init(dataPtr, devices, rank);
   txIdleTime = 0;
   rxIdleTime = 0;
   computeIdleTime = 0;

   totalNumVertices = 0;
}

/*****************************************************************************/
/*!
 * @brief Destructor
 */
/*****************************************************************************/
template<typename SUBGRAPH_TYPE>
TIGLCommunicationManager<SUBGRAPH_TYPE>::~TIGLCommunicationManager()
{
   if (txCountRequests)
      delete txCountRequests;
   if (numDevMessagesBuf)
      delete numDevMessagesBuf;

   if (dataTxCount)
      delete dataTxCount;
   if (mutationTxCount)
      delete mutationTxCount;

   if (devTxMessages)
      delete[] devTxMessages;

   if (rxDevMessageCount)
      delete rxDevMessageCount;
   if (targetRxDevMessageCount)
      delete targetRxDevMessageCount;
}

/*****************************************************************************/
/*!
 * @brief Reset procedure
 *
 */
/*****************************************************************************/
template<typename SUBGRAPH_TYPE>
inline void TIGLCommunicationManager<SUBGRAPH_TYPE>::reset()
{
   dataPtr = 0;
   initFlag = false;
   totalNumVertices = 0;

   dataTxCount = 0;
   mutationTxCount = 0;
}

/*****************************************************************************/
/*!
 * @brief Initializes the communicator object
 * @details It performs all necsssary memory allocations
 * @param dPtr pointer to the data container object
 * @param devices total number of devices
 * @param devRank rank of the current device
 */
/*****************************************************************************/
template<typename SUBGRAPH_TYPE>
inline void TIGLCommunicationManager<SUBGRAPH_TYPE>::init(SUBGRAPH_TYPE *dPtr, uint_fast32_t devices, uint_fast32_t devRank)
{
   dataPtr = dPtr;

   initFlag = true;
   numDevices = devices;
   rank = devRank;

   int numThreads = dataPtr->getNumAllocThreads();

   memset(txBusyRequestFlag, 0, NONBLOCKING_TX_THREADS * sizeof(bool));   // resetting use flag
   txCountRequests = new MPI_Request[numDevices];

   numDevMessagesBuf = new uint_fast32_t[numDevices];

   dataTxCount = new uint_fast32_t[numThreads];
   mutationTxCount = new uint_fast32_t[numThreads];

   devTxMessages = new TIGLCommunicationMessageDev[numDevices];

   rxDevMessageCount = new int_fast32_t[numDevices];
   targetRxDevMessageCount = new int_fast32_t[numDevices];

   ssTxActiveFlag = false;
   ssRxActiveFlag = false;

}

/*****************************************************************************/
/*!
 * @brief Convert a sequence of bytes to messages (can be one or more than one messages)
 * @param rxBuf: pointer to bytes to be converted
 * @param messageSize: number of bytes to convert
 * @param messageBuf: message buffer to stored converted messages
 */
/*****************************************************************************/
template<typename SUBGRAPH_TYPE>
template<typename MESSAGE_TYPE>
void TIGLCommunicationManager<SUBGRAPH_TYPE>::bytes2Messages(uint8_t* rxBuf, uint_fast32_t messageSize, vector<MESSAGE_TYPE> &messageBuf)
{
   uint_fast32_t numBytes = 0;
   uint_fast32_t totalBytes = 0;
   do
   {
      MESSAGE_TYPE newMessage;
      //currMsg = newMessage.fromBytes(currMsg, &messageSize);
      numBytes = newMessage.unpack(rxBuf + totalBytes);
      totalBytes += numBytes;
      if (numBytes)
         messageBuf.push_back(newMessage);
      else
         break;
   } while (totalBytes < messageSize);
}

/*****************************************************************************/
/*!
 * @brief Convert a group of data messages to a sequence of bytes to be sent
 * @param messagePtr: pointer to message buffer
 * @param outBuf: pointer to allocated uint8_t buffer
 * @return number of bytes consumed
 *
 */
/*****************************************************************************/
template<typename SUBGRAPH_TYPE>
uint_fast32_t TIGLCommunicationManager<SUBGRAPH_TYPE>::messages2Bytes(DATA_MESSAGE_BUF_TYPE *messagePtr, uint8_t *outBuf)
{
   uint_fast32_t numBytes = 0;

   for (MESSAGE_BUF_ITERATOR it = messagePtr->begin(); it != messagePtr->end(); ++it)
   {
      // Convert individual data message
      numBytes += it->pack(outBuf + numBytes);
//			if (numBytes > MAX_MESSAGE_SIZE){
//				cout << dataPtr->getDeviceId() << " ERROR: message2Bytes() outBuf overflow" << endl;
//				exit(-1);
//			}
   }

   return numBytes;
}

/*****************************************************************************/
/*!
 * @brief Processes a single received message
 * @param rxBuf message buffer
 * @param tag message tag
 * @param messageSize  messages size in bytes
 * @param dev  source device
 *
 */
/*****************************************************************************/
template<typename SUBGRAPH_TYPE>
void TIGLCommunicationManager<SUBGRAPH_TYPE>::processOneMessage(uint8_t *rxBuf, int tag, uint_fast32_t messageSize, uint_fast32_t dev)
{
   // inserting in the external slot

   /// \b Procedure

   int thread = dataPtr->getNumAllocThreads() - 1;   //the last thread is for external messages
   switch (tag) {
      case MPI_SYSTEM_MESSAGE_TAG: {   // system message
         /// 1. if system message, read and push it to system message buffer.
         /* Unpack system message */
         /* System messages do not get combined */
         /* Each MPI call sends one system message at a time */
         SYS_MBUF_PTR inSysMessagesPtr = dataPtr->getInSysMessageBuf((*timeCountSS + 1) % 2);
         SYS_MESSAGE_TYPE newSysMessage;
         /* Convert from bytes to system message */
         newSysMessage.unpack(rxBuf, messageSize);   //newSysMessage.fromBytes(rxBuf, count);

         /* Push onto buffer to be processed */
         inSysMessagesPtr->push_back(newSysMessage);
         break;
      }
      case MPI_DATA_MESSAGE_TAG: {
         /// 2. if data message, read and route to appropriate vertex message buffer.
         /// If the destination vertex does not exist send a system error message.
         DATA_MESSAGE_BUF_TYPE dataMessageBuf;
         // the message is a data message
         bytes2Messages(rxBuf, messageSize, dataMessageBuf);
         //
         for (MESSAGE_BUF_ITERATOR it = dataMessageBuf.begin(); it != dataMessageBuf.end(); ++it)
         {
            ID_TYPE rcvr = it->getRcvr();
            uint_fast32_t pos;
#if defined(DEBUG_MPI)
            cout << "dev" << rank << "(" << rcvr << ") <- " << it->getSender() << ": " << it-> getContent() << endl;
#endif

            if (dataPtr->findPos(rcvr, pos) == false)   // the vertex does not exist on the device
            {   // The target does not exist in the current core
               SYS_MESSAGE_TYPE errMessage(VERTEX_NOT_EXIST, it->getSender(), &rcvr);
               // adding the system message for the next time
               SYS_MBUF_PTR outSysMessagesPtr = dataPtr->getOutSysMessageBuf();
               outSysMessagesPtr->push_back(errMessage);
               continue;
            }
            // the rcvr exist at position "pos"
            // Route to the input message buffer of the following time

            // pushing the message to the appropriate buffer
            dataPtr->pushDataMessage(*it, pos, (*timeCountSS + 1) % 2, thread);
         }
         break;
      }
      case MPI_MUTATION_MESSAGE_TAG: {
         /// 3. If mutation message, read and route to mutation message buffer.
         MUTATION_MBUF mutationMessageBuf;
         bytes2Messages(rxBuf, messageSize, mutationMessageBuf);
         for (MUTATION_ITERATOR it = mutationMessageBuf.begin(); it != mutationMessageBuf.end(); ++it)
            dataPtr->pushLocalMutationMessage(*it, thread);
         break;
      }
      case MPI_DEV_MESSAGE_COUNT_TAG: {
         /// 4. If a count message, set the target number of message from the source device.

         targetRxDevMessageCount[dev] = *((int_fast32_t*) rxBuf);
         break;
      }
      default: {
         cout << "!!!! warning: unknown message tag ! : " << tag << endl;
         break;
      }
   }   // of the switch case

}

/*****************************************************************************/
/*!
 * @brief Counts the number of messages to each device
 * @param devMessagesCount pointer to the output buffer that contains the count for each device
 *
 */
/*****************************************************************************/
template<typename SUBGRAPH_TYPE>
void TIGLCommunicationManager<SUBGRAPH_TYPE>::countMessages(uint_fast32_t *devMessagesCount)
{
   int numThreads = dataPtr->getNumAllocThreads();

   uint_fast32_t *devCount = new uint_fast32_t[numDevices * numThreads];
   memset(devCount, 0, numDevices * numThreads * sizeof(uint_fast32_t));

   //OMP("omp parallel for")
   for (int thread = 0; thread < numThreads; thread++)
   {
      EXTERNAL_MESSAGE_BUF *messagePtr = dataPtr->getExternalMessageBuf(thread);

      for (typename EXTERNAL_MESSAGE_BUF::iterator it = messagePtr->begin(); it != messagePtr->end(); ++it)   //for(i=0;i<numMessages;i++)//
      {   // outer loop over all destinations
         uint_fast32_t dev = TIGLCommunicationFindDevice(it->getRcvr(), totalNumVertices, numDevices);
         uint_fast32_t ind = thread * numDevices + dev;
         uint_fast32_t messageSize = it->sizeInBytes();
         devCount[ind] += messageSize;
         if (devCount[ind] > MAX_MESSAGE_SIZE - messageSize)
         {   // packing the message
            OMP("omp atomic")
            devMessagesCount[dev]++;
            devCount[ind] = 0;
         }

      }
      for (int_fast32_t dev = 0; dev < numDevices; dev++)
      {
         if (dev != rank)
         {
            if (devCount[thread * numDevices + dev])
            {   // packing the message
               OMP("omp atomic")
               devMessagesCount[dev]++;
            }
         }
      }
   }
   delete devCount;
}

/*****************************************************************************/
/*!
 * @brief Initializes the communicator object
 * @param timePtr pointer to the time count
 *
 */
/*****************************************************************************/
template<typename SUBGRAPH_TYPE>
inline void TIGLCommunicationManager<SUBGRAPH_TYPE>::setTimePtr(uint_fast32_t *timePtr)
{
   timeCountSS = timePtr;
}

/*****************************************************************************/
/*!
 * @brief Passes the total number of vertices in the graph (used in finding the device for each vertex)
 *
 */
/*****************************************************************************/
template<typename SUBGRAPH_TYPE>
inline void TIGLCommunicationManager<SUBGRAPH_TYPE>::setNumVertices(uint64_t numVertices)
{
   totalNumVertices = numVertices;
}

/*****************************************************************************/
/*!
 * @brief Returns true if multiple devices are used, otherwise returns false
 */
/*****************************************************************************/
template<typename SUBGRAPH_TYPE>
inline bool TIGLCommunicationManager<SUBGRAPH_TYPE>::isMultipleDevices()
{
   return (numDevices > 1);
}

/*****************************************************************************/
/*!
 * @brief Sends all system messages.
 * This is a blocking function
 *
 */
/*****************************************************************************/
template<typename SUBGRAPH_TYPE>
void TIGLCommunicationManager<SUBGRAPH_TYPE>::sendSysMessages()
{
   uint8_t outBuf[MAX_MESSAGE_SIZE];

   SYS_MESSAGE_BUF_TYPE messBuf;

   /// \b Procedure

   /// 1. copying all system messages to  a local buffer.
   dataPtr->consumeSysMessages(&messBuf);
   SYS_MBUF_PTR outSysMessagesPtr = &messBuf;
   typename vector<SYS_MESSAGE_TYPE>::iterator itMessage;
   typename vector<SYS_MESSAGE_TYPE>::iterator itBegin = outSysMessagesPtr->begin();
   uint_fast32_t numMessages = outSysMessagesPtr->size();

   //OMP("omp parallel for")
   /// 2. Itarate over all message, if it has code AGGREGATE_INFO, DEV_IDLE, or HALT, broadcast to all devices. Otherwise, send only to target device.
   for (uint_fast32_t i = 0; i < numMessages; i++)
   {   // outer loop over all destinations
      itMessage = itBegin + i;
      // constructing the message body
      uint_fast32_t messageSize = itMessage->pack(outBuf);
      SYS_MESSAGE_CODE code = itMessage->getMessageCode();
      if (code == AGGREGATE_INFO || code == DEV_IDLE || code == HALT)
      {   // broadcast message
         taggedBcast(outBuf, messageSize, MPI_SYSTEM_MESSAGE_TAG);
         for (int_fast32_t dev = 0; dev < numDevices; dev++)
            if (dev != rank)
            {
               devTxMessages[dev].numSysTx++;
            }
      }
      else
      {
         uint_fast32_t deviceAddress = itMessage->getDst();
         OMP("omp critical (COMMUNICATION)")
         TIGLCommunicationSendMPICommand(outBuf, messageSize, deviceAddress,
         MPI_SYSTEM_MESSAGE_TAG);
         devTxMessages[deviceAddress].numSysTx++;
      }
   }
   sysTxCount += numMessages;
}

/*****************************************************************************/
/*!
 * @brief Sends all mutation messages.
 * This is a blocking function
 *
 */
/*****************************************************************************/
template<typename SUBGRAPH_TYPE>
void TIGLCommunicationManager<SUBGRAPH_TYPE>::sendMutationMessages()
{
   uint8_t outBuf[MAX_MESSAGE_SIZE];
   int numThreads = dataPtr->getNumAllocThreads();

   /// \b Procedure

   /// 1. Iterate over all devices
   for (int_fast32_t dev = 0; dev < numDevices; dev++)
   {
      if (dev == rank)
         continue;
      uint_fast32_t accMessageSize = 0;
      /// 2. iterate over all messages, and extract mutation messages going to the current device to a local buffer.
      for (int thread = 0; thread < numThreads; thread++)
      {
         MUTATION_MBUF_PTR outMutationMessagesPtr = dataPtr->getOutMutationMessageBuf(thread);
         if (outMutationMessagesPtr->size() == 0)
            continue;
         if (dev == 0 || (rank == 0 && dev == 1))
            mutationTxCount[thread] += outMutationMessagesPtr->size();

         MUTATION_ITERATOR itMessage;
         for (itMessage = outMutationMessagesPtr->begin(); itMessage != outMutationMessagesPtr->end(); ++itMessage)
         {
            int_fast32_t dstDev = itMessage->getDstDev();
            if (dstDev == UNKNOWN_DEV)
            {
               ID_TYPE targetVertex;
               if (itMessage->getDstVertex(&targetVertex) == false)   // unknown device and vertex
                  continue;
               dstDev = TIGLCommunicationFindDevice(targetVertex, totalNumVertices, numDevices);
            }
            if (dstDev != dev)
               continue;
            uint_fast32_t currMessageSize = itMessage->getSizeInBytes();

            /// 3. If the buffer is full, send its messages to the target device, and clear for refill
            if (accMessageSize + currMessageSize > MAX_MESSAGE_SIZE - 2)
            {   // enough room for the new message
               TIGLCommunicationSendMPICommand(outBuf, accMessageSize, dev,
               MPI_MUTATION_MESSAGE_TAG);
               accMessageSize = 0;
               devTxMessages[dev].numMutationTx++;
            }
            accMessageSize += itMessage->pack(outBuf + accMessageSize);
         }
      }
      if (accMessageSize)
      {
         TIGLCommunicationSendMPICommand(outBuf, accMessageSize, dev, MPI_MUTATION_MESSAGE_TAG);
         devTxMessages[dev].numMutationTx++;
      }
   }
}

/*****************************************************************************/
/*!
 * @brief Sends a single mutation messages.
 * This is a blocking function
 *
 */
/*****************************************************************************/
template<typename SUBGRAPH_TYPE>
void TIGLCommunicationManager<SUBGRAPH_TYPE>::sendInstMutationMessage(int thread)
{
   MUTATION_MBUF_PTR outMutationMessagesPtr = dataPtr->getOutMutationMessageBuf(thread);
#if NONBLOCKING_MUTATATION_BUILD_FLAG
   MPI_Request req;
#endif
   if (outMutationMessagesPtr->empty())
      return;
   uint8_t outBuf[MAX_MESSAGE_SIZE];

   for (int_fast32_t dev = 0; dev < numDevices; dev++)
   {   // gathering messages for a given device
      if (dev == rank)
         continue;
      uint_fast32_t accMessageSize = 0;
      uint_fast32_t accMessageCount = 0;
      MUTATION_ITERATOR itMessage;
      uint_fast32_t messIter;
      uint_fast32_t numMessages = outMutationMessagesPtr->size();
      MUTATION_ITERATOR itBegin = outMutationMessagesPtr->begin();
      for (messIter = 0; messIter < numMessages; ++messIter)
      {
         itMessage = itBegin + messIter;
         int_fast32_t dstDev = itMessage->getDstDev();
         if (dstDev == UNKNOWN_DEV)
         {
            ID_TYPE targetVertex;
            if (itMessage->getDstVertex(&targetVertex) == false)   // unknown device and vertex
               continue;
            dstDev = TIGLCommunicationFindDevice(targetVertex, totalNumVertices, numDevices);
         }
         if (dstDev != dev)
            continue;
         uint_fast32_t currMessageSize = itMessage->getSizeInBytes();
         if (currMessageSize > MAX_MESSAGE_SIZE)
         {
//					cout << "!!! warning in dev " << rank << " in sendInstMutationMessage: overflow message : " << currMessageSize << " bytest" << endl;
            uint8_t *overflowBuffer = new uint8_t[currMessageSize];
            itMessage->pack(overflowBuffer);
#if NONBLOCKING_MUTATATION_BUILD_FLAG
            if(accMessageCount)
            TIGLCommunicationWaitMPINonblocking(&req);

            TIGLCommunicationSendMPICommandNonblocking(overflowBuffer, currMessageSize, dev, MPI_MUTATION_MESSAGE_TAG, &req);
#else
            OMP("omp critical (COMMUNICATION)")
            TIGLCommunicationSendMPICommand(overflowBuffer, currMessageSize, dev,
            MPI_MUTATION_MESSAGE_TAG);
#endif
            delete overflowBuffer;
            accMessageCount++;
            continue;
         }

         if (accMessageSize + currMessageSize > MAX_MESSAGE_SIZE)
         {   // enough room for the new message
#if NONBLOCKING_MUTATATION_BUILD_FLAG
         if(accMessageCount)
         TIGLCommunicationWaitMPINonblocking(&req);
         TIGLCommunicationSendMPICommandNonblocking(outBuf, accMessageSize, dev, MPI_MUTATION_MESSAGE_TAG, &req);
#else
            OMP("omp critical (COMMUNICATION)")
            TIGLCommunicationSendMPICommand(outBuf, accMessageSize, dev,
            MPI_MUTATION_MESSAGE_TAG);
#endif
            //cout << ">>> sending " <<accMessageCount << "/"<< accMessageSize << " to dev " << dev << endl;
            accMessageSize = 0;
            accMessageCount = 0;
         }
         accMessageSize += itMessage->pack(outBuf + accMessageSize);
         accMessageCount++;
      }
      if (accMessageSize)
      {
#if NONBLOCKING_MUTATATION_BUILD_FLAG
         if(accMessageCount)
         TIGLCommunicationWaitMPINonblocking(&req);
//					TIGLCommunicationSendMPICommandNonblocking(outBuf, accMessageSize, dev, MPI_MUTATION_MESSAGE_TAG, &req);
#endif
         //last call always blocking
         OMP("omp critical (COMMUNICATION)")
         TIGLCommunicationSendMPICommand(outBuf, accMessageSize, dev, MPI_MUTATION_MESSAGE_TAG);
         //cout << ">>> sending " <<accMessageCount << "/"<< accMessageSize << " to dev " << dev << endl;
      }
   }
}

/*****************************************************************************/
/*!
 * @brief Overloaded function for sending a single message to a particular destination with a given tag
 * @param txBuf: tx buffer
 * @param bufSize: buffer size
 * @param tag: message tag
 *
 */
/*****************************************************************************/
template<typename SUBGRAPH_TYPE>
void TIGLCommunicationManager<SUBGRAPH_TYPE>::taggedBcast(uint8_t *txBuf, uint_fast32_t bufSize, int tag)
{
#if defined(_MPI)
   OMP("omp critical (COMMUNICATION)")
   {
      for(int_fast32_t dev = 0; dev < numDevices; dev ++)
      {
         if(dev != rank)
         {
            TIGLCommunicationSendMPICommand(txBuf, bufSize, dev, tag);
         }
      }
   }
#endif
}

/*****************************************************************************/
/*!
 * @brief Core function for receiving all mutation messages from a given device
 * @details It checks for input MPI messages and routes each mutation message to the local mutation message buffer
 * @param dev the source device
 * @return true if there exists messages, false otherwise
 *
 */
/*****************************************************************************/
template<typename SUBGRAPH_TYPE>
bool TIGLCommunicationManager<SUBGRAPH_TYPE>::instRecvMutationMessages(int dev)
{
   int_fast32_t tmpMessageSize;

   bool commStatus = TIGLCommunicationExistTaggedMessageFromDev(MPI_MUTATION_MESSAGE_TAG, dev, &tmpMessageSize);

   /// \b Procedure
   /// 1. Repeat until no more message exists
   while (commStatus == true)   //(gotMsg == true)
   {
      MUTATION_MBUF mutationMessageBuf;
      int_fast32_t messageSize;
      uint8_t rxBuf[MAX_MESSAGE_SIZE];
      uint8_t *overflowBuf = 0;

      bool localStat = true;
      //OMP("omp critical (COMMUNICATION_RX)")
      {
         /// 1. If a message exists, check its size
         if (TIGLCommunicationExistTaggedMessageFromDev(MPI_MUTATION_MESSAGE_TAG, dev, &messageSize) == true)
         {
            if (messageSize > MAX_MESSAGE_SIZE)
            {
               /// 2. If the size is bigger than the local buffer, allocate an overflow buffer and read the message to that buffer.
               overflowBuf = new uint8_t[messageSize];
               TIGLCommunicationRecvTaggedMPICommandfromDev(overflowBuf,
               MPI_MUTATION_MESSAGE_TAG, dev, messageSize);
            }
            else
            {
               /// 3. Else read the message to to the local buffer
               TIGLCommunicationRecvTaggedMPICommandfromDev(rxBuf, MPI_MUTATION_MESSAGE_TAG, dev, messageSize);
            }
         }
         else
            localStat = false;
      }
      if (localStat == true)
      {
         if (messageSize > MAX_MESSAGE_SIZE)
         {
            bytes2Messages(overflowBuf, messageSize, mutationMessageBuf);
            delete overflowBuf;
         }
         else
         {
            bytes2Messages(rxBuf, messageSize, mutationMessageBuf);
         }
         uint_fast32_t i;
         /// 4. unpack the messages, and push to the mutation message buffer
         OMP("omp parallel for")
         for (i = 0; i < mutationMessageBuf.size(); ++i)
         {
            MUTATION_ITERATOR it = mutationMessageBuf.begin() + i;
            dataPtr->pushLocalMutationMessage(*it, omp_get_thread_num());
         }
      }

      commStatus = TIGLCommunicationExistTaggedMessageFromDev(MPI_MUTATION_MESSAGE_TAG, dev, &messageSize);
   }
   return true;
}

/*****************************************************************************/
/*!
 @brief Returns the rank of the device
 *
 */
/*****************************************************************************/
template<typename SUBGRAPH_TYPE>
inline int TIGLCommunicationManager<SUBGRAPH_TYPE>::getRank()
{
   return rank;
}

/*****************************************************************************/
/*!
 * @details check if there is any new data message to transmit to other devices
 * @return true if there exists messages, false otherwise
 */
/*****************************************************************************/
template<typename SUBGRAPH_TYPE>
bool TIGLCommunicationManager<SUBGRAPH_TYPE>::existDataMessages2Tx()
{
   // checking data messages
   int numThreads = dataPtr->getNumAllocThreads() - 1;
   for (int thread = 0; thread < numThreads; thread++)
   {
      EXTERNAL_MESSAGE_BUF *messagePtr = dataPtr->getExternalMessageBuf(thread);
      if (messagePtr->size())
         return true;
   }
   return false;
}

/*****************************************************************************/
/*!
 * @brief Checks if there is any new system message to transmit to other devices
 * @return true if there exists system messages, false otherwise
 */
/*****************************************************************************/
template<typename SUBGRAPH_TYPE>
bool TIGLCommunicationManager<SUBGRAPH_TYPE>::existSysMessages2Tx()
{
   // checking system messages
   SYS_MBUF_PTR outSysMessagesPtr = dataPtr->getOutSysMessageBuf();
   if (outSysMessagesPtr->size())
      return true;
   else
      return false;
}

/*****************************************************************************/
/*!
 * @brief Checks if there is any new mutation message to transmit to other devices
 * @return true if there exists mutation messages, false otherwise
 */
/*****************************************************************************/
template<typename SUBGRAPH_TYPE>
bool TIGLCommunicationManager<SUBGRAPH_TYPE>::existMutationMessages2Tx()
{
   int numThreads = dataPtr->getNumAllocThreads() - 1;

   for (int thread = 0; thread < numThreads; thread++)
   {
      MUTATION_MBUF_PTR outMutationMessagesPtr = dataPtr->getOutMutationMessageBuf(thread);
      if (outMutationMessagesPtr->size())
         return true;
   }
   return false;
}

/*****************************************************************************/
/*!
 * @brief Checks if there is any new system message to transmit to other devices
 * @return true if there exists system messages, false otherwise
 */
/*****************************************************************************/
template<typename SUBGRAPH_TYPE>
void TIGLCommunicationManager<SUBGRAPH_TYPE>::sendDataMessages(bool flushBuffers)
{
   int numThreads = dataPtr->getNumAllocThreads() - 1;   // iterates only over local messages

   // sending the count messages at the start

   /// \b Procedure
   /// 1. Iterate over all data messages
   //OMP("omp parallel for")
   for (int thread = 0; thread < numThreads; thread++)
   {
      EXTERNAL_MESSAGE_BUF messBuf;
      dataPtr->consumeExternalDataMessageBuf(&messBuf, thread);
      if (messBuf.empty())
         continue;
      this->dataTxCount[thread] += messBuf.size();

      for (typename EXTERNAL_MESSAGE_BUF::iterator it = messBuf.begin(); it != messBuf.end(); ++it)
      {   // outer loop over all destinations
         ID_TYPE rcvr = it->getRcvr();
         /// 2. Find the destination device
         uint_fast32_t dev = TIGLCommunicationFindDevice(rcvr, totalNumVertices, numDevices);

         TIGLCommunicationMessageDev *pBufStruct = &devTxMessages[dev];

         /// 2. Convert the message to bytes and copy it to the local buffer of the target device.
         // constructing the message body
         uint_fast32_t messageSize = it->pack(pBufStruct->buf + pBufStruct->accMessageSize);
         pBufStruct->accMessageSize += messageSize;   //accMessageSize += messageSize;

         /// 3. If the buffer is full, find a transmission slot and send the message using nonblocking TX.
         if (pBufStruct->accMessageSize > MAX_MESSAGE_SIZE - messageSize)
         {
            //wait until getting an available TX slot
            int reqSlot = findTxSlot();
            while (reqSlot == -1)
            {   // loop until a slot is available
               usleep(10);
               txIdleTime++;

               reqSlot = findTxSlot();
            }
            memcpy(txBuffers[reqSlot], pBufStruct->buf, pBufStruct->accMessageSize);

            OMP("omp critical (COMMUNICATION)")
            {
               TIGLCommunicationSendMPICommandNonblocking(txBuffers[reqSlot], pBufStruct->accMessageSize, dev,
               MPI_DATA_MESSAGE_TAG, &txRequests[reqSlot]);   //&pBufStruct->req[pBufStruct->numTx%NONBLOCKING_THREADS]);//
            }
            txBusyRequestFlag[reqSlot] = true;   // buffer is flagged as used

            /// 4. update the communication statistics after message transmission
            pBufStruct->numTx++;
            pBufStruct->accMessageSize = 0;
         }
      }
   }

   if (flushBuffers == true)
   {   // sending all outstanding messages
      for (int_fast32_t dev = 0; dev < numDevices; dev++)
         if (dev != rank)
         {
            TIGLCommunicationMessageDev *pBufStruct = &devTxMessages[dev];

            if (pBufStruct->accMessageSize)
            {
               int reqSlot = findTxSlot();
               while (reqSlot == -1)
               {
                  usleep(10);
                  txIdleTime++;
                  reqSlot = findTxSlot();
               }

               OMP("omp critical (COMMUNICATION)")
               {
                  TIGLCommunicationSendMPICommandNonblocking(pBufStruct->buf, pBufStruct->accMessageSize, dev,
                  MPI_DATA_MESSAGE_TAG, &txRequests[reqSlot]);   //&pBufStruct->req[pBufStruct->numTx%NONBLOCKING_THREADS]);//
               }
               txBusyRequestFlag[reqSlot] = true;   // buffer is flagged as used

               pBufStruct->numTx++;
               pBufStruct->accMessageSize = 0;
            }
         }

      //OMP("omp flush")
   }
}

/*****************************************************************************/
/*!
 * @brief Sends the count of data messages to all devices
 */
/*****************************************************************************/
template<typename SUBGRAPH_TYPE>
void TIGLCommunicationManager<SUBGRAPH_TYPE>::sendMessagesSizes()
{
   for (int_fast32_t dev = 0; dev < numDevices; dev++)
   {
      if (dev != rank)
      {
         numDevMessagesBuf[dev] = devTxMessages[dev].getMessageCount();
         OMP("omp critical (COMMUNICATION)")
         {
            TIGLCommunicationSendMPICommandNonblocking((uint8_t*) &numDevMessagesBuf[dev], sizeof(uint_fast32_t), dev,
            MPI_DEV_MESSAGE_COUNT_TAG, &txCountRequests[dev]);
         }
      }
   }
}

/*****************************************************************************/
/*!
 * @brief Waits for all pending TX messages
 */
/*****************************************************************************/
template<typename SUBGRAPH_TYPE>
void TIGLCommunicationManager<SUBGRAPH_TYPE>::waitAllTx()
{

   /// \b Procedure
   /// 1. Wait for all data/system/mutation messages
   for (int reqCount = 0; reqCount < NONBLOCKING_TX_THREADS; reqCount++)
   {
      if (this->txBusyRequestFlag[reqCount] == true)
      {
         OMP("omp critical (COMMUNICATION)")
         TIGLCommunicationWaitMPINonblocking(&txRequests[reqCount]);
      }
   }
   /// 2. Wait for count messages to all devices
   // checking device count
   for (int_fast32_t dev = 0; dev < numDevices; dev++)
   {
      if (dev != rank)
      {
         OMP("omp critical (COMMUNICATION)")
         TIGLCommunicationWaitMPINonblocking(&txCountRequests[dev]);
      }
   }
}

/*****************************************************************************/
/*!
 * @brief Returns the index of an available TX slot to be used for nonblocking transmission
 *
 */
/*****************************************************************************/
template<typename SUBGRAPH_TYPE>
int TIGLCommunicationManager<SUBGRAPH_TYPE>::findTxSlot()
{
   int reqCount;
   /// \b Procedure
   /// 1. search for any slot that has not been used before, and if any return its index
   for (reqCount = 0; reqCount < NONBLOCKING_TX_THREADS; reqCount++)
      if (this->txBusyRequestFlag[reqCount] == false)
         return reqCount;

   int reqIndex = -1;
   /// 2. If all slots have been used before, find any TX slots that finished it nonblocking transmission.
   OMP("omp critical (COMMUNICATION)")
   {
      for (reqCount = 0; reqCount < NONBLOCKING_TX_THREADS; reqCount++)
      {
         if (TIGLCommunicationTestMPINonblocking(&txRequests[reqCount]) == true)
         {
            this->txBusyRequestFlag[reqCount] = false;
            reqIndex = reqCount;
         }
      }
   }
   return reqIndex;   //reqCount == NONBLOCKING_TX_THREADS ?-1:reqCount;
}

/*****************************************************************************/
/*!
 * @brief Core function for the receive communication task; it wait and receives all new messages
 *
 */
/*****************************************************************************/
template<typename SUBGRAPH_TYPE>
bool TIGLCommunicationManager<SUBGRAPH_TYPE>::RecvNewMessages()
{
   uint8_t rxBuf[NONBLOCKING_RX_THREADS][MAX_MESSAGE_SIZE];
   MPI_Request req[NONBLOCKING_RX_THREADS];
   int messageSize[NONBLOCKING_RX_THREADS];
   int tag[NONBLOCKING_RX_THREADS];
   int dev[NONBLOCKING_RX_THREADS];

   /// \b Procedure
   /// 1. Initialize all RX slots as available.
   bool busyReading[NONBLOCKING_RX_THREADS];
   for (int r = 0; r < NONBLOCKING_RX_THREADS; r++)
      busyReading[r] = false;

   //int numWaiting;
   bool terminationFlag = false;
   /// 2. While there are messages to receive, do the following steps.
   do
   {
      // reading loop
      /// 3. Find all available RX slots, and start a nonblocking receive if  a message is available to read.
      for (int index = 0; index < NONBLOCKING_RX_THREADS; index++)
      {   // loop over all available threads;
         if (busyReading[index] == false)
         {   // reading available to read new messages
            bool existFlag;
            OMP("omp critical (COMMUNICATION)")
            {
               existFlag = TIGLCommunicationExistMessages(&tag[index], &dev[index], &messageSize[index]);
               if (existFlag)
               {   // reading a message (nonblocking) if one is available
                  TIGLCommunicationRecvMPICommandNonblocking(rxBuf[index], messageSize[index], dev[index], tag[index], &req[index]);
               }
            }
            if (existFlag)
            {
               busyReading[index] = true;
            }
         }
      }
      int numWaiting = 0;
      //OMP("omp parallel for shared(numWaiting)")
      /// 4. Find all RX slots that finished receiving, and process the corresponding message using @link TIGLCommunicationManager::processOneMessage processOneMessage @endlink.
      /// 5. Mark all finished RX slots as available.
      for (int index = 0; index < NONBLOCKING_RX_THREADS; index++)
      {   // loop over all available threads;
         if (busyReading[index] == true)
         {   // execution
             // at this point the buffer has already been invoked for reading
            bool testFlag;
            OMP("omp critical (COMMUNICATION)")
            testFlag = TIGLCommunicationTestMPINonblocking(&req[index], &tag[index], &messageSize[index]);

            if (testFlag == true)
            {
               processOneMessage(rxBuf[index], tag[index], messageSize[index], dev[index]);
               if (tag[index] != MPI_DEV_MESSAGE_COUNT_TAG)
                  rxDevMessageCount[dev[index]]++;

               // buffer is ready to be refilled
               busyReading[index] = false;
            }
            else
            {
               //OMP("omp atomic")
               numWaiting++;
            }
         }
      }
      bool newMessagesFlag;

      OMP("omp critical (COMMUNICATION)")
      newMessagesFlag = TIGLCommunicationExistMessages();

      /// 6. If there is no new messages, and the target number of messages is not reached exit with error; otherwise return normally.
      if (numWaiting == 0 && newMessagesFlag == false)
      {
         terminationFlag = true;
         // checking of matching count
         for (int_fast32_t dev = 0; dev < numDevices; dev++)
         {
            if (dev != rank && rxDevMessageCount[dev] != targetRxDevMessageCount[dev])
            {
               if (targetRxDevMessageCount[dev] >= 0)
               {
                  cout << "XXXX dev " << rank << "<-" << dev << ": " << rxDevMessageCount[dev] << "/" << targetRxDevMessageCount[dev] << endl;

                  cout << "COMMUNICATION ERROR: forced exit dev " << rank << endl;
                  exit(1);
               }
               terminationFlag = false;
               usleep(10);
               rxIdleTime++;
               break;
            }
         }
      }
   } while (terminationFlag == false);   //(numWaiting > 0 || TIGLCommunicationExistMessages() == true);

   return true;
}

/*****************************************************************************/
/*!
 * @brief Initializes the flags of the TX/RX tasks at the start of the superstep
 * @details This method must be called at the beginning of each superstep
 *
 */
/*****************************************************************************/
template<typename SUBGRAPH_TYPE>
void TIGLCommunicationManager<SUBGRAPH_TYPE>::initSS()
{
#if defined(_MPI)
   int numThreads = dataPtr->getNumAllocThreads();

   /// \b Procedure
   /// 1. Reset all message counters
   sysTxCount = 0;
   memset(dataTxCount, 0, numThreads*sizeof(*dataTxCount));
   memset(mutationTxCount, 0, numThreads*sizeof(*mutationTxCount));
   memset(txBusyRequestFlag, 0, NONBLOCKING_TX_THREADS*sizeof(bool));// resetting use flag
   //memset(txDevCount, 0, numDevices*sizeof(*txDevCount));
   memset(rxDevMessageCount, 0, numDevices*sizeof(*rxDevMessageCount));// initializing to zero
   memset(targetRxDevMessageCount, -1, numDevices*sizeof(uint_fast32_t));// initializing to -1 (flag for not read)

   for (int_fast32_t dev = 0; dev < numDevices;dev++)
   devTxMessages[dev].reset();

   /// 2. Set TX and RX enable flags
   ssComputationEndFlag = false;
   ssTxActiveFlag = true;// activating trasmission
   ssRxActiveFlag = true;// activating receiving
   OMP("omp flush (ssComputationEndFlag, ssTxActiveFlag, ssRxActiveFlag) ")//OMP("omp flush (ssTxActiveFlag)")
#endif
}

/*****************************************************************************/
/*!
 * @brief Checks if all TX messages for the current SS is done
 * @return true if TX completed, false otherwise
 *
 */
/*****************************************************************************/
template<typename SUBGRAPH_TYPE>
bool TIGLCommunicationManager<SUBGRAPH_TYPE>::isTxComplete()
{
   uint_fast32_t sumTxMessages = sysTxCount;

   int numThreads = dataPtr->getNumAllocThreads() - 1;
   for (int thread = 0; thread < numThreads; thread++)
   {
      sumTxMessages += dataTxCount[thread];
      sumTxMessages += mutationTxCount[thread];
   }

   uint_fast32_t totalNumMessages = dataPtr->getTotalExternalMessageCount();
   return sumTxMessages == totalNumMessages ? true : false;
}

/*****************************************************************************/
/*!
 * @brief Called at the end of the superstep to flag the end of message generation
 * @details it waits for all TX/RX to complete before going to the superstep barrier
 *
 */
/*****************************************************************************/
template<typename SUBGRAPH_TYPE>
inline void TIGLCommunicationManager<SUBGRAPH_TYPE>::endSS()
{   // this is called by the computation thread
#if defined(_MPI)
//		cout << " dev " << rank << ": endSS" << endl;
ssComputationEndFlag = true;
//cout << "--- dev " << rank << " : "<< *ssComputationEndFlag << ", " << *ssTxActiveFlag << endl;
OMP("omp flush (ssComputationEndFlag)")

// now waiting until TX is complete
while(ssTxActiveFlag == true || ssRxActiveFlag == true)//while(isTxComplete(totalNumMessages) == false)
{
   //cout << "------ dev " << rank << " : "<< *ssComputationEndFlag << ", " << *ssTxActiveFlag << endl;
   usleep(10);// sleep for 100 us while waiting
   computeIdleTime++;
}
#endif
}

/*****************************************************************************/
/*!
 * @brief High-level message TX routine
 *
 */
/*****************************************************************************/
template<typename SUBGRAPH_TYPE>
void TIGLCommunicationManager<SUBGRAPH_TYPE>::continuousSendMessages()
{
   /// \b Procedure
   /// 1. Wait until TX is enabled.
   if (ssTxActiveFlag == false)
   {   // transmission is not allowed
       //cout << " dev " << rank << ", th " << omp_get_thread_num() << " : false" << endl;
      usleep(10);   // sleep for 100 us while waiting
      txIdleTime++;
      return;
   }
//   SYS_MBUF_PTR outSysMessagesPtr = dataPtr->getOutSysMessageBuf();

   /// 2. Continue check and send available messages as long as the superstep computation flag is active.
   do
   {
      //TerminationFlag = true;
      if (existSysMessages2Tx())
         sendSysMessages();

      if (existMutationMessages2Tx())
         sendMutationMessages();

      if (existDataMessages2Tx())
         sendDataMessages();
      if (ssComputationEndFlag == false)
      {
         usleep(10);
         txIdleTime++;
      }
   } while (ssComputationEndFlag == false);

   /// 3. If the superstep flag becomes inactive, check and send any residual messages.
   //OMP("omp flush") // updating the view of all buffers
   if (existSysMessages2Tx())
      sendSysMessages();
   if (existMutationMessages2Tx())
      sendMutationMessages();
   sendDataMessages(true);   // sending residual messages

   // this may be redundant
   while (isTxComplete() == false)
   {
      usleep(10);
      txIdleTime++;

      if (existSysMessages2Tx())
         sendSysMessages();
      if (existMutationMessages2Tx())
         sendMutationMessages();
      sendDataMessages(true);   // sending residual messages
   }

   /// 4. Send a count of the total number of message to all devices.
   sendMessagesSizes();

   /// 5. Deactivate TX flag.
   ssTxActiveFlag = false;
OMP("omp flush (ssTxActiveFlag)")
}

/*****************************************************************************/
/*!
 * @brief Core message receive routine
 *
 */
/*****************************************************************************/
template<typename SUBGRAPH_TYPE>
void TIGLCommunicationManager<SUBGRAPH_TYPE>::continuousRecvMessages()
{
/// \b Procedure
/// 1. Wait until RX is enabled.
if (ssRxActiveFlag == false)
{   // transmission is not allowed
   usleep(10);   // sleep for 100 us while waiting
   rxIdleTime++;
   return;
}
/// 2. Receive all new messages by calling @link TIGLCommunicationManager::RecvNewMessages RecvNewMessages @endlink.
RecvNewMessages();

/// 3. Deactivate RX flag
ssRxActiveFlag = false;
OMP("omp flush (ssRxActiveFlag)")
return;
}

/*****************************************************************************/
/*!
 * @brief Profiling function that prints the idle time for each task
 *
 */
/*****************************************************************************/
template<typename SUBGRAPH_TYPE>
inline void TIGLCommunicationManager<SUBGRAPH_TYPE>::printIdleTime()
{
cout << "******* dev " << rank << ":  computeIdleTime = " << computeIdleTime << ", txIdleTime = " << txIdleTime << ", rxIdleTime = " << rxIdleTime << endl;
}
#endif /* TIGLCOMMMUNICATIONMANAGERIMPLEMENTATION_HPP_ */
