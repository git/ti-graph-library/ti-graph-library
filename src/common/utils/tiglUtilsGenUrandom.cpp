/*****************************************************************************/
/*!
 * @file tiglUtilsGenUrandom.cpp
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/

/******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/
#include <stdio.h>
#include "tiglUtils.hpp"

/******************************************************************************
 *
 * MACROS
 *
 ******************************************************************************/
#define 	RAND_READING_STEP	100

/*****************************************************************************/
/*!
 * @ingroup misc_tools
 * @brief Generates random variable(s) that is uniformly distributed between [0, 1]
 * @param buf output buffer
 * @param numEntries number of generated random variables
 */
/*****************************************************************************/
void tiglUtilsGenUrandom(double *buf, unsigned long numEntries)
{
   unsigned long tmpBuf[RAND_READING_STEP];
   FILE *fp;

   // getting the inverse of the maximum ulong value
   double scale = 1.0;
   for (unsigned i = 0; i < sizeof(unsigned long); i++)   // iterating over number of bytes
      scale *= 256.0;   // maximum byte value
   scale = 1.0 / scale;

   while (numEntries >= RAND_READING_STEP)
   {
      fp = fopen("/dev/urandom", "rb");
      if(fread(tmpBuf, sizeof(unsigned long), RAND_READING_STEP, fp) != RAND_READING_STEP)
      {
         printf("!!! warning: error reading /dev/urandon\n");
      }
      fclose(fp);

      // generating the double equivalent
      for (int j = 0; j < RAND_READING_STEP; j++)
         *(buf++) = (double) tmpBuf[j] * scale;

      numEntries -= RAND_READING_STEP;
   }
   if (numEntries)
   {
      fp = fopen("/dev/urandom", "rb");
      if(fread(tmpBuf, sizeof(unsigned long), numEntries, fp) != numEntries)
      {
         printf("!!! warning: error reading /dev/urandon\n");
      }
      fclose(fp);

      // generating the double equivalent
      for (unsigned j = 0; j < numEntries; j++)
         *(buf++) = (double) tmpBuf[j] * scale;
   }
}

