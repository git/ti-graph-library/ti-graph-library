#ifndef TIGLUTILSCHECKSYMMETRY_HPP_
#define TIGLUTILSCHECKSYMMETRY_HPP_

/*****************************************************************************/
/*!
 * @file tiglUtilsCheckSymmetry.hpp
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/

/******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/
#include <vector>
#include "tiglContainersVertexBase.hpp"

/*****************************************************************************/
/*!
 * @ingroup misc_tools
 * @brief Checks consistency of undirected graphs
 * @return true if conistent, false otherwise
 */
/*****************************************************************************/
template<typename VertexClass>
bool tiglUtilsCheckSymmetry(vector<VertexClass> & verticesList)
{
   typedef typename VertexClass::ID_TYPE ID_TYPE;
   typedef typename VertexClass::OUT_EDGE_ITERATOR EDGE_ITERATOR;

   for (typename vector<VertexClass>::iterator it = verticesList.begin(); it != verticesList.end(); ++it)
   {
      for (EDGE_ITERATOR ite = it->getBeginOutEdgeIterator(); ite != it->getEndOutEdgeIterator(); ++ite)
      {
         ID_TYPE nbr = ite->getNbr();
         if (verticesList[nbr].isConnected(it->getId()) == false)
            return false;
      }
   }
   return true;
}

#endif /* TIGLUTILSCHECKSYMMETRY_HPP_ */
