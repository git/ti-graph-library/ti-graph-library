#ifndef TIGLUTILSCOUNTEDGES_HPP_
#define TIGLUTILSCOUNTEDGES_HPP_

/*****************************************************************************/
/*!
 * @file tiglUtilsCountEdges.hpp
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/

/******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/
#include <vector>
#include <math.h>
#include "tiglContainersVertexBase.hpp"
#include "tiglContainersEdgeBase.hpp"
//using namespace std;

/*****************************************************************************/
/*!
 * @ingroup misc_tools
 * @brief Counts all the edges of between a subset of vertices
 * @param vertexList list of vertices
 * @param startSrcID starting ID of source vertices
 * @param endSrcID end ID of source vertices
 * @param startDstID starting ID of target vertices
 * @param endDstID end ID of target vertices
 *
 * @return The number of edges originating from any of the source vertices within range to any of destination vertices within range
 */
/*****************************************************************************/
template<typename VertexClass>
unsigned long tiglUtilsCountEdges(vector<VertexClass> & vertexList, typename VertexClass::ID_TYPE startSrcID, typename VertexClass::ID_TYPE endSrcID,
      typename VertexClass::ID_TYPE startDstID, typename VertexClass::ID_TYPE endDstID)
{

   typedef typename VertexClass::OUT_EDGE_ITERATOR EDGE_ITERATOR;

   typename vector<VertexClass>::iterator it;
   unsigned long edgeCount = 0;

   for (it = vertexList.begin(); it != vertexList.end(); ++it)
   {
      // checking range of vertex id
      if (it->getId() < startSrcID || it->getId() > endSrcID)
         continue;   // out of range

      // iterating over the edge of this vertex
      for (EDGE_ITERATOR ite = it->getBeginOutEdgeIterator(); ite != it->getEndOutEdgeIterator(); ++ite)
         if (ite->getNbr() >= startDstID && ite->getNbr() <= endDstID)
            edgeCount++;

   }
   return edgeCount;
}

/*****************************************************************************/
/*!
 * @ingroup misc_tools
 * @brief Counts all the edges in a graph
 * @param vertexList list of vertices
 *
 * @return The total number of edges in the graph
 */
/*****************************************************************************/
template<typename VertexClass>
unsigned long tiglUtilsCountEdges(vector<VertexClass> & vertexList)
{
   typedef typename VertexClass::OUT_EDGE_ITERATOR EDGE_ITERATOR;

   typename vector<VertexClass>::iterator it;
   unsigned long edgeCount = 0;

   for (it = vertexList.begin(); it != vertexList.end(); ++it)
   {

      // iterating over the edge of this vertex
      for (EDGE_ITERATOR ite = it->get_out_edge_iterator(); ite != it->get_end_out_edge_iterator(); ++ite)
         edgeCount++;
   }
   return edgeCount;
}

/*****************************************************************************/
/*!
 * @ingroup misc_tools
 * @brief Overloaded version of counting edges using iterators
 * @param itStart start iterator of the vertex list
 * @param itEnd end iterator of the vertex list
 * @param startSrcID starting ID of source vertices
 * @param endSrcID end ID of source vertices
 * @param startDstID starting ID of target vertices
 * @param endDstID end ID of target vertices
 *
 * @return The number of edges originating from any of the source vertices within range to any of destination vertices within range
 */
/*****************************************************************************/
template<typename VERTEX_ITERATOR, typename ID_TYPE>
unsigned long tiglUtilsCountEdges(VERTEX_ITERATOR itStart, VERTEX_ITERATOR itEnd, ID_TYPE startSrcID, ID_TYPE endSrcID, ID_TYPE startDstID, ID_TYPE endDstID)
/*
 * Inputs:
 * Return value
 * The number of edges originating from any of the source vertices within range to any of destination vertices within range
 */
{
   typedef typename std::vector<TIGLContainersEdgeBase<ID_TYPE> >::iterator EDGE_ITERATOR;
   unsigned long edgeCount = 0;

   for (VERTEX_ITERATOR it = itStart; it != itEnd; ++it)
   {
      // checking range of vertex id
      if (it->getId() < startSrcID || it->getId() > endSrcID)
         continue;   // out of range

      // iterating over the edge of this vertex
      for (EDGE_ITERATOR ite = it->get_out_edge_iterator(); ite != it->get_end_out_edge_iterator(); ++ite)

         if (ite->getNbr() >= startDstID && ite->getNbr() <= endDstID)
            edgeCount++;

   }
   return edgeCount;
}

#endif /* TIGLUTILSCOUNTEDGES_HPP_ */
