#ifndef TIGLUTILSMACROS_H_
#define TIGLUTILSMACROS_H_

/*****************************************************************************/
/*!
 *@file tiglUtilsMacros.h
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/

#define DEFAULT_MAXIMUM_SS	100 // default value of the maximum number of supersteps
#define LIST_MEM_OVERHEAD   (double)1.1

#if defined(_OPENMP)//#ifdef	 OPENMP_ENABLE
#define		NUM_OMP_THREADS		4
#define		EXTRA_OMP_THREADS	5
#else
#define		NUM_OMP_THREADS		1
#endif

#define	SMALL_NUMBER	0
#define	BIG_NUMBER	2147483647L // 2^31 -1

#define	BIG_DBL_NUMBER 1e8

#define	UNDEFINED_NODE BIG_NUMBER

#define	AGGREGATE_HISTORY	2 // history storage of the aggregate information

typedef enum
{
   SS_SUCCESS = 0, NOT_INITIALIZED, VERTEX_NOT_FOUND, ALGORITHM_COMPLETED, MAX_SS_REACHED,
} SS_STATUS;

#define     ILLEGAL_NODE        -1

#define MAX_LOCAL_MUTATION_MESSAGE     1000
#define MAX_EXTERNAL_MUTATION_MESSAGE  100
#define MAX_PROCESSED_LINES      100

#define     MPI_DISTRIBUTED_GRAPH_CONSTRUCT_TAG    1001
#define     MPI_DISTRIBUTED_GRAPH_COUNT_TAG        1002

#define     GRAPH_CONSTRUCT_SUCCESS             1
#define     GRAPH_CONSTRUCT_FAIL                0
#define     MAX_WAIT_TIME                    (double)5e8 // time threshold for weighting = 10 sec

#define     MAX_MUTATION_MESSAGE  1000
#define     MAX_PROCESSED_LINES      100

#endif /* TIGLUTILSMACROS_H_ */

