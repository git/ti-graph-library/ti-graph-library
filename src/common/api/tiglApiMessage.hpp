#ifndef TIGLAPIMESSAGE_HPP_
#define TIGLAPIMESSAGE_HPP_

/*****************************************************************************/
/*!
 *@file tiglApiMessage.hpp
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/

/******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/
#include  <stdint.h>

/******************************************************************************
 *
 * TIGLApiMessage class
 *
 ******************************************************************************/

/*****************************************************************************/
/*!
 * @ingroup interface_engine
 * @class TIGLApiMessage
 * @brief Core data message class definition
 * @details It handles all the data message fields and processing
 * It is a template class that is parameterized by
 * @param DATA_TYPE the data field of the message
 * @param ID_TYPE the type of the rx/tx vertex id
 *
 */
/*****************************************************************************/
template<typename DATA_TYPE, typename ID_TYPE = uint_fast32_t>
class TIGLApiMessage
{
public:
   TIGLApiMessage();
   TIGLApiMessage(ID_TYPE & tx, ID_TYPE & rx, DATA_TYPE & messContent);
   ~TIGLApiMessage();

protected:
   ///@param the sender vertex
   ID_TYPE sender;
   ///@param the receiver vertex
   ID_TYPE rcvr;
   ///@param the message body
   DATA_TYPE content;

public:
   inline ID_TYPE & getSender();
   inline ID_TYPE & getRcvr();
   inline DATA_TYPE & getContent();
   inline void setMessage(ID_TYPE & tx, ID_TYPE & rx, DATA_TYPE & messContent);
   template<typename CMessages> inline void setMessage(CMessages & newMessage);
   inline void setContent(DATA_TYPE & newContent);
   inline void sumContent(DATA_TYPE & newContent);
   template<typename CMessages> inline void maxContent(CMessages & newMessage);
   template<typename CMessages> inline void minContent(CMessages & newMessage);
   inline uint_fast32_t pack(uint8_t * buf);
   inline uint_fast32_t unpack(uint8_t * buf);
   inline uint_fast32_t sizeInBytes();
};

#include "tiglImplementationMessage.hpp"
#endif /* TIGLAPIMESSAGE_HPP_ */
