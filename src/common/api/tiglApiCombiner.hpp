#ifndef TIGLAPICOMBINER_HPP_
#define TIGLAPICOMBINER_HPP_

/*****************************************************************************/
/*!
 * @file tiglApiCombiner.hpp
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/

/******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/
#include <vector>
#include  <stdint.h>

/******************************************************************************
 *
 * TIGLApiCombiner class
 *
 ******************************************************************************/

/*****************************************************************************/
/*!
 * @ingroup interface_engine
 * @class TIGLApiCombiner
 * @brief Pure combiner class for the %TIGL engine
 * @details It contains the definitions of the combine function and has few template implementations
 * It is a template class that is parameterized by
 * @param ALG_MESSAGE_TYPE the data field of pregel messages
 *
 */
/*****************************************************************************/
template<typename ALG_MESSAGE_TYPE>
class TIGLApiCombiner
{
protected:
   typedef typename std::vector<ALG_MESSAGE_TYPE>::iterator MESSAGE_ITERATOR;
public:
   // constructor/destructor
   TIGLApiCombiner();
   virtual ~TIGLApiCombiner();

public:
   uint_fast32_t combineMessages(MESSAGE_ITERATOR itIn, uint_fast32_t numMessages, MESSAGE_ITERATOR itOut);
   uint_fast32_t combineMessages(ALG_MESSAGE_TYPE & newMessage, MESSAGE_ITERATOR itOut);
   uint_fast32_t sumCombineMessages(MESSAGE_ITERATOR itIn, uint_fast32_t numMessages, MESSAGE_ITERATOR itOut);
   uint_fast32_t sumCombineMessages(ALG_MESSAGE_TYPE & newMessage, MESSAGE_ITERATOR itOut);
   uint_fast32_t maxCombineMessages(MESSAGE_ITERATOR itIn, uint_fast32_t numMessages, MESSAGE_ITERATOR itOut);
   uint_fast32_t maxCombineMessages(ALG_MESSAGE_TYPE & newMessage, MESSAGE_ITERATOR itOut);
   uint_fast32_t minCombineMessages(MESSAGE_ITERATOR itIn, uint_fast32_t numMessages, MESSAGE_ITERATOR itOut);
   uint_fast32_t minCombineMessages(ALG_MESSAGE_TYPE & newMessage, MESSAGE_ITERATOR itOut);
   uint_fast32_t bypassCombineMessages(MESSAGE_ITERATOR itIn, uint_fast32_t numMessages, MESSAGE_ITERATOR itOut);
};
#include "tiglImplementationCombiner.hpp"

#endif /* TIGLAPICOMBINER_HPP_ */
