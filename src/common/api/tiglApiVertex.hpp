#ifndef TIGLAPIVERTEX_HPP_
#define TIGLAPIVERTEX_HPP_

/*****************************************************************************/
/*!
 * @file tiglApiVertex.hpp
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/

/******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/
#include <vector>
#include "tiglContainersVertexBase.hpp"
#include "tiglApiMessage.hpp"
#ifdef CMEM_ENABLE
#include "tiglMemDdrAllocator.hpp"
#endif
#include "tiglContainersAggregate.hpp"
#include "tiglUtilsMacros.h"

/******************************************************************************
 *
 * NAMESPACES
 *
 ******************************************************************************/
using namespace std;

/******************************************************************************
 *
 * TIGLApiVertex class
 *
 ******************************************************************************/

/*****************************************************************************/
/*!
 * @ingroup interface_engine
 * @class TIGLApiVertex
 * @brief Core vertex class for the %TIGL Engine (with only output edge buffer).
 * @details It inherits the core vertex class and add messaging capabilities. This class is inherited by customized classes for different graph algorithms.
 * It is a template class that is parameterized by the following parameters
 * @param DataType the vertex data field, 
 * @param AlgDataType the algorithm data field, 
 * @param EdgeType the edge type, 
 * @param InitType the type of initial data for the graph algorithm, 
 * @param IdType the type of id for the vertices
 * @param the message type.
 */
/*****************************************************************************/
template<typename DataType, typename AlgDataType, typename EdgeType, typename InitType, typename IdType = uint_fast32_t, typename MessageType = DblMessage>
class TIGLApiVertex: public TIGLContainersVertexBase<DataType, AlgDataType, EdgeType, IdType>
{
   /******************************************************************************
    *
    * type definitions
    *
    ******************************************************************************/
public:
   typedef MessageType MESSAGE_TYPE;
   typedef vector<MESSAGE_TYPE> MESSAGE_BUF_TYPE;
   typedef InitType INIT_TYPE;
   typedef TIGLContainersAggregate<AlgDataType> AGGREGATE_TYPE;

   /******************************************************************************
    *
    * class attributes
    *
    ******************************************************************************/
protected:
   /// active flag
   bool flagActive;
   /// pointer to the aggregate information
   static AGGREGATE_TYPE *aggregateInfoPtr;
   /// a flag when true the vertex is processed even with no message, if false it is processed only if there are messages
   static bool activeWithNoMessage;
   /// a flag when true the vertex can be reactivated by new messages, otherwise it never wake up once becomes inactive
   static bool reactivateFlag;

   /******************************************************************************
    *
    * constructor/destructor
    *
    ******************************************************************************/
public:
   TIGLApiVertex(IdType key, bool noMessageFlag, bool inFlagReactivate);
   TIGLApiVertex(IdType key, DataType data, AlgDataType alg, bool noMessageFlag = false, bool inFlagReactivate = true);
   TIGLApiVertex(IdType key);
   TIGLApiVertex();
   TIGLApiVertex(IdType key, DataType data, AlgDataType alg);
   ~TIGLApiVertex();

   /******************************************************************************
    *
    * class methods
    *
    ******************************************************************************/
public:
   void setParams(IdType key, DataType data, AlgDataType alg);
   inline void vote_to_halt();
   inline bool isActive();
   inline bool isActiveWithNoMessage();
   void clear();
   inline bool canReactivate();
   inline void activate();
   template<typename ALLOCATOR_TYPE>
   uint_fast32_t compute(MESSAGE_BUF_TYPE *pIn, vector<MESSAGE_TYPE, ALLOCATOR_TYPE> *pOut, uint_fast32_t numMessages);
   template<typename ALLOCATOR_TYPE>
   uint_fast32_t init(INIT_TYPE *initParams, vector<MESSAGE_TYPE, ALLOCATOR_TYPE> *pOut);
   uint_fast32_t getMessageBufSize();
   inline void setAggPtr(AGGREGATE_TYPE *infoPtr);
};

#include "tiglImplementationVertex.hpp"
#endif /* TIGLAPIVERTEX_HPP_ */
