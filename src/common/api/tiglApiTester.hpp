#ifndef TIGLAPITESTER_HPP_
#define TIGLAPITESTER_HPP_

/*****************************************************************************/
/*!
 * @file tiglApiTester.hpp
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/

/******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/
#include "tigl.hpp"

/******************************************************************************
 *
 * TIGLApiTester class
 *
 ******************************************************************************/

/*****************************************************************************/
/*!
 * @ingroup interface_engine
 * @class TIGLApiTester
 * @brief Interface class for testing the different modules in the %TIGL \n
 * @details It inherits the core interface class tigl, and adds special procedures for modules testing
 */
/*****************************************************************************/
template<typename ALG_VERTEX_TYPE, typename ALG_COMBINER>
class TIGLApiTester: public TIGL<ALG_VERTEX_TYPE, ALG_COMBINER>
{
   /******************************************************************************
    *
    * type definitions
    *
    ******************************************************************************/
protected:
   typedef typename ALG_VERTEX_TYPE::MESSAGE_TYPE ALG_MESSAGE_TYPE;
   typedef typename ALG_VERTEX_TYPE::EDGE_TYPE EDGE_TYPE;
   typedef typename ALG_VERTEX_TYPE::INIT_TYPE ALG_INIT;
   typedef TIGLContainersSubgraph<ALG_VERTEX_TYPE, ALG_MESSAGE_TYPE> SUBGRAPH_TYPE;
   typedef TIGLCommunicationManager<SUBGRAPH_TYPE> COMMUNICATOR_TYPE;
   typedef TIGLComputationManager<SUBGRAPH_TYPE, ALG_COMBINER, ALG_INIT> COMPUTATION_TYPE;
   typedef TIGLMutationManager<SUBGRAPH_TYPE> MUTATION_TYPE;
   typedef typename ALG_VERTEX_TYPE::ID_TYPE ID_TYPE;
   typedef typename SUBGRAPH_TYPE::MESSAGE_BUF_TYPE MESSAGE_BUF_TYPE;
   typedef typename SUBGRAPH_TYPE::VERTEX_ITERATOR VERTEX_ITERATOR;
   typedef typename ALG_VERTEX_TYPE::ALG_DATA_TYPE ALG_DATA_TYPE;

   /******************************************************************************
    *
    * constructor/destructor
    *
    ******************************************************************************/
public:
   TIGLApiTester(bool isDirected, uint64_t numTotalVertices, uint_fast32_t idDevice = 0, uint_fast32_t totalDevices = 1, int allocThreads = NUM_OMP_THREADS);

   /******************************************************************************
    *
    * Methods
    *
    ******************************************************************************/
public:
   bool getVertex(ID_TYPE id, ALG_VERTEX_TYPE *pVertex);
   bool testMutationManagerModule(char *fileName);
   bool testMpiMutationManagerModule(int_fast32_t readDev, char *fileName);
};

#include "tiglImplementationTester.hpp"
#endif /* TIGLAPITESTER_HPP_ */
