#ifndef TIGL_HPP_
#define TIGL_HPP_

/*****************************************************************************/
/*!
 * @file tigl.hpp
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/

/******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/
#include <time.h>
#include <math.h>
#include <fstream>
#include  <stdint.h>

#include "tiglUtils.hpp"
#include "tiglRW.hpp"
#include "tiglUtilsMacros.h"
#include "tiglContainersSubgraph.hpp"
#include "tiglCommunication.hpp"
#include "tiglComputationManager.hpp"
#include "tiglMutationManager.hpp"
#include "tiglUtilsMpiTools.h"
#include "tiglUtilsOmpTools.h"

#include <vector>
#include <set>
#include <unistd.h>

/******************************************************************************
 *
 * namespaces
 *
 ******************************************************************************/
using namespace std;
/******************************************************************************
 *
 * TIGL class
 *
 ******************************************************************************/

/*****************************************************************************/
/*!
 * @ingroup interface_engine
 * @class TIGL
 * @brief Top level class for the %TIGL engine \n
 * @details The class provides the high-level interface to executing the TIGL engine.
 *  It allocates objects of the core components of the engine, including the data container, the compute engine, and the communication engine.
 * It is has a generic template structure that is prototyped to the particular algorithm vertex definition and data combiner.
 *
 */
/*****************************************************************************/
template<typename ALG_VERTEX_TYPE, typename ALG_COMBINER>
class TIGL
{
   /******************************************************************************
    *
    * type definitions
    *
    ******************************************************************************/
protected:
   typedef typename ALG_VERTEX_TYPE::MESSAGE_TYPE ALG_MESSAGE_TYPE;
   typedef typename ALG_VERTEX_TYPE::EDGE_TYPE EDGE_TYPE;
   typedef typename ALG_VERTEX_TYPE::INIT_TYPE ALG_INIT;
   typedef TIGLContainersSubgraph<ALG_VERTEX_TYPE, ALG_MESSAGE_TYPE> SUBGRAPH_TYPE;
   typedef TIGLCommunicationManager<SUBGRAPH_TYPE> COMMUNICATOR_TYPE;
   typedef TIGLComputationManager<SUBGRAPH_TYPE, ALG_COMBINER, ALG_INIT> COMPUTATION_TYPE;
   typedef TIGLMutationManager<SUBGRAPH_TYPE> MUTATION_TYPE;
   typedef typename ALG_VERTEX_TYPE::ID_TYPE ID_TYPE;
   typedef typename SUBGRAPH_TYPE::MESSAGE_BUF_TYPE MESSAGE_BUF_TYPE;
   typedef typename SUBGRAPH_TYPE::VERTEX_ITERATOR VERTEX_ITERATOR;
   typedef typename ALG_VERTEX_TYPE::ALG_DATA_TYPE ALG_DATA_TYPE;
   typedef MESSAGE_BUF_TYPE EXTERNAL_MESSAGE_BUF;

   /******************************************************************************
    *
    * Attributes
    *
    ******************************************************************************/
protected:
   // params
   /// ID of the current device
   uint_fast32_t rank;
   // engine managers
   /// pointer to TIGLContainersSubgraph object (allocated in the constructor)
   SUBGRAPH_TYPE *dataManager;
   /// pointer to TIGLCommunicationManager object (allocated in the constructor)
   COMMUNICATOR_TYPE *communicationManager;
   /// pointer to TIGLComputationManager object (allocated in the constructor)
   COMPUTATION_TYPE *computationManager;
   /// pointer to user-defined combiner object (allocated in the constructor)
   ALG_COMBINER *combiner;
   /// pointer to TIGLMutationManager object (allocated in the constructor)
   MUTATION_TYPE *mutationManager;
   /// total number of devices (1 if MPI is not used)
   uint_fast32_t numDevices;
   /// number of local vertices
   uint_fast32_t numLocalVertices;

   /// total number of vertices in the graph
   uint_fast32_t numTotalVertices;
   /// directed/undirected graph
   bool flagDirected;
   /// true if the algorithm is running, otherwise false
   bool algorithmRunning;
   /// the superstep counter
   uint_fast32_t countSS;

   /******************************************************************************
    *
    * constructor/destructor
    *
    ******************************************************************************/
public:
   TIGL(bool isDirected, uint64_t totalVertices, uint_fast32_t idDevice = 0, uint_fast32_t totalDevices = 1, int allocThreads = NUM_OMP_THREADS);
   ~TIGL();

   /******************************************************************************
    *
    * class internal methods
    *
    ******************************************************************************/
protected:
   uint_fast32_t mutateProcessOneLine(string localLine, uint_fast32_t *numMessages);
   bool mutateDistributedGraphFromFile(char *fileName);
   bool remoteMutateDistributedGraph(uint_fast32_t readDev);
   bool computeTask(uint_fast32_t maxSS, ALG_INIT *initParams);
   void txTask();
   void rxTask();

   /******************************************************************************
    *
    * class public methods
    *
    ******************************************************************************/
public:
   void reset();
   bool networkReadGraph(uint_fast32_t readDev, char *fileName = NULL);
   bool mutateLocalSubgraphFromFile(char *fileName);
   bool readSubgraphFromFile(char *fileName);
   void initGraph(vector<ALG_VERTEX_TYPE> & vertexList);
   bool run(uint_fast32_t maxSS = DEFAULT_MAXIMUM_SS, ALG_INIT *initParams = 0);
   template<typename CVertex> double computeSNR(vector<CVertex> & refVect);
   template<typename CVertex> double computeNormalizedSNR(vector<CVertex> & refVect);
   template<typename CVertex> double computeMSE(vector<CVertex> & refVect);
   uint_fast32_t memoryUsage();
   uint_fast32_t getNumVertices();
   ID_TYPE getNumEdges();
   uint_fast32_t MaxCollision();
   inline uint_fast32_t numSS();
#ifdef DEBUG_ENABLE
   void printBuffers();
   void printAlgData();
   bool printAlgData2File(char *fileName);
#endif

#ifdef	PROFILE_ENABLE
   void PrintProfilingInfo();
#endif
};

#include "tiglImplementation.hpp"
#endif /* TIGL_HPP_ */

