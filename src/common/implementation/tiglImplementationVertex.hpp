#ifndef TIGLIMPLEMENTATIONVERTEX_HPP_
#define TIGLIMPLEMENTATIONVERTEX_HPP_

/*****************************************************************************/
/*!
 * @file tiglImplementationVertex.hpp
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/

/******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/
#include "tiglApiVertex.hpp"

/*****************************************************************************/
/*!
 * @brief Constructor setting parameters
 * @details The data and algorithm attributes are not assigned
 * @param key vertex id
 * @param noMessageFlag flag to set activeWithNoMessage
 * @param inFlagReactivate flag to set reactivateFlag
 */
/*****************************************************************************/
template<typename DataType, typename AlgDataType, typename EdgeType, typename InitType, typename IdType, typename MessageType>
TIGLApiVertex<DataType, AlgDataType, EdgeType, InitType, IdType, MessageType>::TIGLApiVertex(IdType key, bool noMessageFlag, bool inFlagReactivate) :
      TIGLContainersVertexBase<DataType, AlgDataType, EdgeType, IdType>(key)
{
   //disableListedActive();
   activeWithNoMessage = noMessageFlag;
   reactivateFlag = inFlagReactivate;
}

/*****************************************************************************/
/*!
 * @brief Overloaded constructor with more detailed setting parameters
 * @param key vertex id
 * @param data initial value of @link TIGLContainersVertexBase::vertexData vertexData @endlink field
 * @param alg initial value for @link TIGLContainersVertexBase::algData algData @endlink field
 * @param noMessageFlag flag to set activeWithNoMessage
 * @param inFlagReactivate flag to set reactivateFlag
 */
/*****************************************************************************/
template<typename DataType, typename AlgDataType, typename EdgeType, typename InitType, typename IdType, typename MessageType>
TIGLApiVertex<DataType, AlgDataType, EdgeType, InitType, IdType, MessageType>::TIGLApiVertex(IdType key, DataType data, AlgDataType alg, bool noMessageFlag,
      bool inFlagReactivate) :
      TIGLContainersVertexBase<DataType, AlgDataType, EdgeType, IdType>(key, data, alg)
{
   //disableListedActive();
   activeWithNoMessage = noMessageFlag;
   reactivateFlag = inFlagReactivate;
}

/*****************************************************************************/
/*!
 * @brief Overloaded constructor with less detailed setting parameters
 * @param key vertex id
 */
/*****************************************************************************/
template<typename DataType, typename AlgDataType, typename EdgeType, typename InitType, typename IdType, typename MessageType>
TIGLApiVertex<DataType, AlgDataType, EdgeType, InitType, IdType, MessageType>::TIGLApiVertex(IdType key) :
      TIGLContainersVertexBase<DataType, AlgDataType, EdgeType, IdType>(key)
{

}

/*****************************************************************************/
/*!
 * @brief Empty constructor
 */
/*****************************************************************************/
template<typename DataType, typename AlgDataType, typename EdgeType, typename InitType, typename IdType, typename MessageType>
TIGLApiVertex<DataType, AlgDataType, EdgeType, InitType, IdType, MessageType>::TIGLApiVertex() :
      TIGLContainersVertexBase<DataType, AlgDataType, EdgeType, IdType>()
{
}

/*****************************************************************************/
/*!
 * @brief Overloaded constructor
 * @param key vertex id
 * @param data initial value of @link TIGLContainersVertexBase::vertexData vertexData @endlink field
 * @param alg initial value for @link TIGLContainersVertexBase::algData algData @endlink field
 */
/*****************************************************************************/
template<typename DataType, typename AlgDataType, typename EdgeType, typename InitType, typename IdType, typename MessageType>
TIGLApiVertex<DataType, AlgDataType, EdgeType, InitType, IdType, MessageType>::TIGLApiVertex(IdType key, DataType data, AlgDataType alg) :
      TIGLContainersVertexBase<DataType, AlgDataType, EdgeType, IdType>(key, data, alg)
{
}

/*****************************************************************************/
/*!
 * @brief Destructor
 */
/*****************************************************************************/
template<typename DataType, typename AlgDataType, typename EdgeType, typename InitType, typename IdType, typename MessageType>
TIGLApiVertex<DataType, AlgDataType, EdgeType, InitType, IdType, MessageType>::~TIGLApiVertex()
{
}

/*****************************************************************************/
/*!
 * @brief Sets the fields of the vertex
 * @param key vertex id
 * @param data value for @link TIGLContainersVertexBase::vertexData vertexData @endlink field
 * @param alg  value for @link TIGLContainersVertexBase::algData algData @endlink field
 */
/*****************************************************************************/
template<typename DataType, typename AlgDataType, typename EdgeType, typename InitType, typename IdType, typename MessageType>
void TIGLApiVertex<DataType, AlgDataType, EdgeType, InitType, IdType, MessageType>::setParams(IdType key, DataType data, AlgDataType alg)
{
   this->id = key;
   this->algData = alg;
   this->vertexData = data;
}

/*****************************************************************************/
/*!
 * @brief Deactivates the vertex
 */
/*****************************************************************************/
template<typename DataType, typename AlgDataType, typename EdgeType, typename InitType, typename IdType, typename MessageType>
inline void TIGLApiVertex<DataType, AlgDataType, EdgeType, InitType, IdType, MessageType>::vote_to_halt()
{
   flagActive = false;
}

/*****************************************************************************/
/*!
 * @brief Returns active status
 * @return true if active, false if inactive
 */
/*****************************************************************************/
template<typename DataType, typename AlgDataType, typename EdgeType, typename InitType, typename IdType, typename MessageType>
inline bool TIGLApiVertex<DataType, AlgDataType, EdgeType, InitType, IdType, MessageType>::isActive()
{
   return flagActive;
}

/*****************************************************************************/
/*!
 * @brief	Checks if the vertex stays active even without messages
 */
/*****************************************************************************/
template<typename DataType, typename AlgDataType, typename EdgeType, typename InitType, typename IdType, typename MessageType>
inline bool TIGLApiVertex<DataType, AlgDataType, EdgeType, InitType, IdType, MessageType>::isActiveWithNoMessage()
{
   return activeWithNoMessage;
}

/*****************************************************************************/
/*!
 * @brief Clears all edges and deactivates the vertex
 */
/*****************************************************************************/
template<typename DataType, typename AlgDataType, typename EdgeType, typename InitType, typename IdType, typename MessageType>
void TIGLApiVertex<DataType, AlgDataType, EdgeType, InitType, IdType, MessageType>::clear()
{
   TIGLContainersVertexBase<DataType, AlgDataType, EdgeType, IdType>::clear();
   flagActive = false;
}

/*****************************************************************************/
/*!
 * @brief Checks if the vertex can be reactivated by new messages
 */
/*****************************************************************************/
template<typename DataType, typename AlgDataType, typename EdgeType, typename InitType, typename IdType, typename MessageType>
inline bool TIGLApiVertex<DataType, AlgDataType, EdgeType, InitType, IdType, MessageType>::canReactivate()
{
   return reactivateFlag;
}

/*****************************************************************************/
/*!
 * @brief Activates the vertex
 */
/*****************************************************************************/
template<typename DataType, typename AlgDataType, typename EdgeType, typename InitType, typename IdType, typename MessageType>
inline void TIGLApiVertex<DataType, AlgDataType, EdgeType, InitType, IdType, MessageType>::activate()
{
   flagActive = true;
}

/*****************************************************************************/
/*!
 * @brief Virtual core compute function.
 * @details It is an empty function that must be overridden by customized vertex definition for the different graph algorithms
 * @param pIn pointer to the input message buffer
 * @param pOut pointer to the output message buffer
 * @param numMessages the number of input data messages
 * @return the number of output data messages
 */
/*****************************************************************************/
template<typename DataType, typename AlgDataType, typename EdgeType, typename InitType, typename IdType, typename MessageType>
template<typename ALLOCATOR_TYPE> uint_fast32_t TIGLApiVertex<DataType, AlgDataType, EdgeType, InitType, IdType, MessageType>::compute(MESSAGE_BUF_TYPE *pIn,
      vector<MESSAGE_TYPE, ALLOCATOR_TYPE> *pOut, uint_fast32_t numMessages)
{
   return 0;
}

/*****************************************************************************/
/*!
 *@brief virtual core init function. Here it is an empty function that must be overridden by customized vertex definition for the different graph algorithms
 * @param initParams pointer to the initialization parameters
 * @param pOut pointer to the output message buffer
 * @return the number of output data messages
 */
/*****************************************************************************/
template<typename DataType, typename AlgDataType, typename EdgeType, typename InitType, typename IdType, typename MessageType>
template<typename ALLOCATOR_TYPE> uint_fast32_t TIGLApiVertex<DataType, AlgDataType, EdgeType, InitType, IdType, MessageType>::init(INIT_TYPE *initParams,
      vector<MESSAGE_TYPE, ALLOCATOR_TYPE> *pOut)
{
   return 0;
}

/*****************************************************************************/
/*!
 * @brief Returns the size of message buffers
 */
/*****************************************************************************/
template<typename DataType, typename AlgDataType, typename EdgeType, typename InitType, typename IdType, typename MessageType>
uint_fast32_t TIGLApiVertex<DataType, AlgDataType, EdgeType, InitType, IdType, MessageType>::getMessageBufSize()
{
   return (this->outEdges).capacity() * sizeof(MESSAGE_TYPE);
}

/*****************************************************************************/
/*!
 * @brief Sets a pointer to the aggregate information object
 */
/*****************************************************************************/
template<typename DataType, typename AlgDataType, typename EdgeType, typename InitType, typename IdType, typename MessageType>
inline void TIGLApiVertex<DataType, AlgDataType, EdgeType, InitType, IdType, MessageType>::setAggPtr(AGGREGATE_TYPE * infoPtr)
{
   TIGLApiVertex<DataType, AlgDataType, EdgeType, InitType, IdType, MessageType>::aggregateInfoPtr = infoPtr;
   //aggregateInfoPtr = infoPtr;
}

/*****************************************************************************
 *
 * defining the static members
 *
 ******************************************************************************/
template<typename DataType, typename AlgDataType, typename EdgeType, typename InitType, typename IdType, typename MessageType>
TIGLContainersAggregate<AlgDataType>* TIGLApiVertex<DataType, AlgDataType, EdgeType, InitType, IdType, MessageType>::aggregateInfoPtr;

template<typename DataType, typename AlgDataType, typename EdgeType, typename InitType, typename IdType, typename MessageType>
bool TIGLApiVertex<DataType, AlgDataType, EdgeType, InitType, IdType, MessageType>::activeWithNoMessage;

template<typename DataType, typename AlgDataType, typename EdgeType, typename InitType, typename IdType, typename MessageType>
bool TIGLApiVertex<DataType, AlgDataType, EdgeType, InitType, IdType, MessageType>::reactivateFlag;

#endif /* TIGLIMPLEMENTATIONVERTEX_HPP_ */
