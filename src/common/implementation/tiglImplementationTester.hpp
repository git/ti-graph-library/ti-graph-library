#ifndef TIGLIMPLEMENTATIONTESTER_HPP_
#define TIGLIMPLEMENTATIONTESTER_HPP_

/*****************************************************************************/
/*!
 * @file tiglImplementationTester.hpp
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/

/******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/
#include "tiglApiTester.hpp"

/*****************************************************************************/
/*!
 * @brief Destructor performs necessary memory release
 */
/*****************************************************************************/
template<typename ALG_VERTEX_TYPE, typename ALG_COMBINER>
TIGLApiTester<ALG_VERTEX_TYPE, ALG_COMBINER>::TIGLApiTester(bool isDirected, uint64_t numTotalVertices, uint_fast32_t idDevice, uint_fast32_t totalDevices,
      int allocThreads) :
      TIGL<ALG_VERTEX_TYPE, ALG_COMBINER>(isDirected, numTotalVertices, idDevice, totalDevices, allocThreads)
{
}

/*****************************************************************************/
/*!
 * @brief Retrieves a vertex
 * @param id the ID of the requested vertex
 * @param pVertex pointer to the vertex object to which the vertex is retrieved
 * @return true if the vertex exists, false otherwise
 */
/*****************************************************************************/
template<typename ALG_VERTEX_TYPE, typename ALG_COMBINER>
bool TIGLApiTester<ALG_VERTEX_TYPE, ALG_COMBINER>::getVertex(ID_TYPE id, ALG_VERTEX_TYPE *pVertex)
{
   VERTEX_ITERATOR it;
   if (this->dataManager->getVertex(id, &it) == false)
      return false;
   *pVertex = *it;
   return true;
}

/*****************************************************************************/
/*!
 * @brief Test procedure for mutation manager with single device
 * @details It processes basic mutation messages and compares the output with reference implementation
 * @param fileName the name of the file that contains the adjacency list of the graph
 * @return true if success, otherwise false
 */
/*****************************************************************************/
template<typename ALG_VERTEX_TYPE, typename ALG_COMBINER>
bool TIGLApiTester<ALG_VERTEX_TYPE, ALG_COMBINER>::testMutationManagerModule(char *fileName)
{
   /// \b Procedure
   /// 1. Read the graph file using @link tigl::mutateLocalSubgraphFromFile mutateLocalSubgraphFromFile @endlink
   if (this->mutateLocalSubgraphFromFile(fileName) == false)
   {
      cout << "ERROR: cannot read graph file " << fileName << endl;
      return false;
   }

   /// 2. Read the graph file plainly to construct simple adjacency list
   vector<ALG_VERTEX_TYPE> refVerticesList;
   if (TIGLRwReadAdjList(refVerticesList, fileName) == false)
   {
      cout << "ERROR: cannot read graph file " << fileName << endl;
      return false;
   }
   /// 3. Check the vertex consistency
   // now checking the graph
   uint_fast32_t i;
   bool stat = true;
   OMP("omp parallel for")
   for (i = 0; i < refVerticesList.size(); ++i)
   {
      VERTEX_ITERATOR itRef = refVerticesList.begin() + i;
      uint_fast32_t id = itRef->getId();

      VERTEX_ITERATOR it2;
      if (this->dataManager->getVertex(id, &it2) == false)
      {
         OMP("omp critical (PRINT)")
         cout << "ERROR vertex " << id << " does not exist " << endl;
         stat = false;
      }

      if (stat == true)
      {
         // now comparing the edges
         if (it2->getNumOutEdges() != itRef->getNumOutEdges())
         {
            OMP("omp critical (PRINT)")
            cout << "ERROR: vertex " << id << ": edges mismatch" << endl;
            stat = false;
         }
      }
      // comparing the edges themeselves
   }
   if (stat == false)
      return false;

   cout << "-- ADD_EMPTY_VERTEX, ADD_EDGE verified successfully" << endl;

   /// 4. Remove selected vertices and edges and recheck consistency
   // second test REMOVE_VERTEX, REMOVE_EDGE : removing all vertices that are multiple of 10
   uint_fast32_t originalNumVertices = this->dataManager->size();
   OMP("omp parallel for")
   for (uint_fast32_t i = 0; i < originalNumVertices; i += 10)
   {
      int thread = omp_get_thread_num();
      TIGLMutationMessage mess(this->rank);
      mess.writeSimpleVertexMessage(REMOVE_VERTEX, &i);
      this->dataManager->pushLocalMutationMessage(mess, thread);
      if (this->dataManager->numLocalMutationMessage(thread) >= MAX_MUTATION_MESSAGE)
         this->mutationManager->processMutationMessages(thread);
   }
   uint_fast32_t numRemovedVertices = originalNumVertices % 10 ? originalNumVertices / 10 + 1 : originalNumVertices / 10;
   // now executing all threads
   OMP("omp parallel for")
   for (int thread = 0; thread < NUM_OMP_THREADS; thread++)
      this->mutationManager->processMutationMessages(thread);

   // checking the vertices size
   if (this->dataManager->size() != originalNumVertices - numRemovedVertices)
   {
      cout << "ERROR REMOVE_VERTEX: size mismatch after remove : (" << this->dataManager->size() << ", " << originalNumVertices - numRemovedVertices << ")"
            << endl;
      return false;
   }

   OMP("omp parallel for")
   for (i = 0; i < originalNumVertices; i += 10)
   {
      VERTEX_ITERATOR itRef = refVerticesList.begin() + i;
      uint_fast32_t id = itRef->getId();

      if (id % 10 == 0)
      {
         if (this->dataManager->exist(id))
         {
            OMP("omp critical (PRINT)")
            cout << "ERROR REMOVE_VERTEX: vertex " << id << "is not removed" << endl;
            stat = false;
         }
      }
      else
      {
         VERTEX_ITERATOR it2;
         if (this->dataManager->getVertex(id, &it2) == false)
         {
            OMP("omp critical (PRINT)")
            cout << "ERROR REMOVE_VERTEX: vertex " << id << "is removed" << endl;
            stat = false;
         }

         if (stat == true)
         {     // now comparing the edges
            vector<ID_TYPE> nbrs;
            itRef->getChildren(nbrs);
            uint_fast32_t numNbrs = 0;
            for (typename vector<ID_TYPE>::iterator itNbr = nbrs.begin(); itNbr != nbrs.end(); ++itNbr)
               if ((*itNbr) % 10)
                  numNbrs++;
            if (it2->getNumOutEdges() != numNbrs)
            {
               OMP("omp critical (PRINT)")
               cout << "ERROR: vertex " << id << ": edges mismatch after removal" << endl;
               stat = false;
            }
         }
      }
   }
   cout << "-- REMOVE_VERTEX, REMOVE_EDGE verified successfully" << endl;
   ///////////////////////////////////////////////////////
   /// 5. Add back the removed vertices and edges and recheck consistency
   // now returning ADD_VERTEX, MIGRATE_VERTEX
   OMP("omp parallel for")
   for (i = 0; i < originalNumVertices; i += 10)
   {
      int thread = omp_get_thread_num();
      VERTEX_ITERATOR itRef = refVerticesList.begin() + i;

      TIGLMutationMessage mess(this->rank);
      mess.writeVertexMessage(ADD_VERTEX, &(*itRef));
      this->dataManager->pushLocalMutationMessage(mess, thread);

      if (this->dataManager->numLocalMutationMessage(thread) >= MAX_MUTATION_MESSAGE)
         this->mutationManager->processMutationMessages(thread);
   }
   // now executing all threads for writing vertices
   OMP("omp parallel for")
   for (int thread = 0; thread < NUM_OMP_THREADS; thread++)
      this->mutationManager->processMutationMessages(thread);

   // reconnecting edges between recovered vertices and their neigbors
   OMP("omp parallel for")
   for (i = 0; i < originalNumVertices; i += 10)
   {
      int thread = omp_get_thread_num();
      VERTEX_ITERATOR itRef = refVerticesList.begin() + i;
      if (itRef->getId() % 10 == 0)
      {
         TIGLMutationMessage mess(this->rank);

         vector<ID_TYPE> nbrs;
         itRef->getChildren(nbrs);
         for (typename vector<ID_TYPE>::iterator itNbr = nbrs.begin(); itNbr != nbrs.end(); ++itNbr)
         {
            EDGE_TYPE newEdge(itRef->getId());
            ID_TYPE nbrId = *itNbr;
            //newVertex.addEdge(newEdge);
            TIGLMutationMessage message(this->rank);   //(ADD_OUT_EDGE, rank, &newEdge , &id);

            message.writeEdgeMessage(ADD_OUT_EDGE, &newEdge, &nbrId);
            this->dataManager->pushLocalMutationMessage(message, thread);
         }
         if (this->dataManager->numLocalMutationMessage(thread) >= MAX_MUTATION_MESSAGE)
            this->mutationManager->processMutationMessages(thread);
      }
   }
   // now executing all threads for writing vertices
   OMP("omp parallel for")
   for (int thread = 0; thread < NUM_OMP_THREADS; thread++)
      this->mutationManager->processMutationMessages(thread);

   // now checking the graph
   OMP("omp parallel for")
   for (i = 0; i < refVerticesList.size(); ++i)
   {
      VERTEX_ITERATOR itRef = refVerticesList.begin() + i;
      uint_fast32_t id = itRef->getId();

      VERTEX_ITERATOR it2;
      if (this->dataManager->getVertex(id, &it2) == false)
      {
         cout << "ERROR vertex " << id << " does not exist " << endl;
         stat = false;
      }
      if (stat == true)
      {
         // now comparing the edges
         if (it2->getNumOutEdges() != itRef->getNumOutEdges())
         {
            OMP("omp critical (PRINT)")
            cout << "ERROR: vertex " << id << ": edges mismatch: (" << it2->getNumOutEdges() << ", " << itRef->getNumOutEdges() << ") " << endl;
            stat = false;
         }
      }
      // comparing the edges themeselves
   }
   if (stat == false)
      return false;

   cout << "-- ADD_VERTEX verified successfully" << endl;
   cout << "-- mutation manager test DONE !" << endl;

   return true;
}

/*****************************************************************************/
/*!
 * @brief Test procedure for mutation manager with multiple devices
 * @details It processes basic mutation messages and compares the output with reference implementation
 * @param fileName the name of the file that contains the adjacency list of the graph
 * @param readDev the rank of the device that reads the graph from its file system
 * @return true if success, otherwise false
 */
/*****************************************************************************/
template<typename ALG_VERTEX_TYPE, typename ALG_COMBINER>
bool TIGLApiTester<ALG_VERTEX_TYPE, ALG_COMBINER>::testMpiMutationManagerModule(int_fast32_t readDev, char *fileName)
{
   /// \b Procedure
   /// 1. Read the graph file using @link tigl::networkReadGraph networkReadGraph @endlink
   if (this->networkReadGraph(readDev, fileName) == false)
   {
      cout << "ERROR: in  networkReadGraph" << endl;
      return false;
   }

   /// 2. Read the graph file plainly to construct simple adjacency list
   // now checking the graph consistency
   vector<ALG_VERTEX_TYPE> refVerticesList;
   if (TIGLRwReadAdjList(refVerticesList, fileName) == false)
   {
      cout << "ERROR: cannot read graph file " << fileName << endl;
      return false;
   }
   /// 3. Check the graph consistency by comparing the local subgrah to the corresponding subgraph of the reference grahp
   // now checking the graph
   uint_fast32_t i;
   bool stat = true;
   uint_fast32_t totalVertices = refVerticesList.size();
   OMP("omp parallel for")
   for (i = 0; i < totalVertices; ++i)
   {
      VERTEX_ITERATOR itRef = refVerticesList.begin() + i;
      uint_fast32_t id = itRef->getId();

      uint_fast32_t dev = TIGLCommunicationFindDevice(id, totalVertices, this->numDevices);
      if (dev != this->rank)
         continue;
      VERTEX_ITERATOR it2;
      if (this->dataManager->getVertex(id, &it2) == false)
      {
         OMP("omp critical (PRINT)")
         cout << "ERROR vertex " << id << " does not exist " << endl;
         stat = false;
      }

      if (stat == true)
      {
         // now comparing the edges
         if (it2->getNumOutEdges() != itRef->getNumOutEdges())
         {
            OMP("omp critical (PRINT)")
            cout << "ERROR: vertex " << id << ": edges mismatch" << endl;
            stat = false;
         }
      }
      // comparing the edges themeselves
   }
   if (stat == false)
      return false;

   return true;
}

#endif /* TIGLIMPLEMENTATIONTESTER_HPP_ */
