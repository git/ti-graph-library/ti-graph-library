#ifndef TIGLIMPLEMENTATION_HPP_
#define TIGLIMPLEMENTATION_HPP_

/*****************************************************************************/
/*!
 *@file tiglImplementation.hpp
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/

/******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/
#include "tigl.hpp"

/*****************************************************************************/
/*!
 * @brief Constructor performs necessary memory allocations
 * @param isDirected true if the graph is directed, false if the graph is undirected
 * @param totalVertices total number of vertices on all devices of the whole graph
 * @param idDevice the rank of the current device (set to zero for single device)
 * @param totalDevices the total number of devices (set to one for single device)
 * @param allocThreads the total number of parallel allocated buffer (special setting that is not needed for general use).
 */
/*****************************************************************************/
template<typename ALG_VERTEX_TYPE, typename ALG_COMBINER>
TIGL<ALG_VERTEX_TYPE, ALG_COMBINER>::TIGL(bool isDirected, uint64_t totalVertices, uint_fast32_t idDevice, uint_fast32_t totalDevices, int allocThreads)
{
   flagDirected = isDirected;
   rank = idDevice;
   numDevices = totalDevices;
   numTotalVertices = totalVertices;

#if !defined(_OPENMP)
   if (allocThreads != 1)
      allocThreads = 1;
#endif

   dataManager = new SUBGRAPH_TYPE(flagDirected, allocThreads);

   computationManager = new COMPUTATION_TYPE;
   communicationManager = new COMMUNICATOR_TYPE(dataManager, numDevices, rank);
   combiner = new ALG_COMBINER;
   mutationManager = new MUTATION_TYPE(dataManager);

   computationManager->setDataManager(dataManager);
   computationManager->setCombiner(combiner);
   communicationManager->setNumVertices(numTotalVertices);
}

/*****************************************************************************/
/*!
 * @brief Destructor performs necessary memory release
 */
/*****************************************************************************/
template<typename ALG_VERTEX_TYPE, typename ALG_COMBINER>
TIGL<ALG_VERTEX_TYPE, ALG_COMBINER>::~TIGL()
{
   delete dataManager;
   delete computationManager;
   delete communicationManager;
   delete combiner;
   delete mutationManager;
}

/*****************************************************************************/
/*!
 * @brief Processes a single line (in string format) of the graph file
 * @param localLine the line to be processed. It has the format <srcVtx>: <nbrVtx1>, <nbrVtx2>, ..., <nbrVtxN>
 * @param numMessages pointer to the number of mutation messages after processing the line
 * @return the rank of the destination device that has the corresponding vertex
 */
/*****************************************************************************/
template<typename ALG_VERTEX_TYPE, typename ALG_COMBINER>
uint_fast32_t TIGL<ALG_VERTEX_TYPE, ALG_COMBINER>::mutateProcessOneLine(string localLine, uint_fast32_t *numMessages)
{
   /// \b Procedure

   /// 1. reading the source vertex id
   int thread = omp_get_thread_num();
   unsigned pos = localLine.find(":");
   string tmpStr = localLine.substr(0, pos);
   ID_TYPE id = atol(tmpStr.c_str());
   localLine = localLine.substr(pos + 1);
   ALG_VERTEX_TYPE newVertex(id);

   /// 2. reading all the neighbors
   while (localLine.size() > 1)
   {   // adding all out edges to the current vertex
      pos = localLine.find(", ");
      tmpStr = localLine.substr(0, pos);
      ID_TYPE target = atol(tmpStr.c_str());
      if (newVertex.isConnected(target) == false)
      {
         EDGE_TYPE newEdge(target);
         newVertex.addEdge(newEdge);
      }

      localLine = localLine.substr(pos + 1);
   }
   // end of reading a vertex

   /// 3. find the vertex device
   uint_fast32_t dstDev = TIGLCommunicationFindDevice(id, numTotalVertices, numDevices);

   /// 4. construct the ADD_VERTEX message
   TIGLMutationMessage message(dstDev);
   message.writeVertexMessage(ADD_VERTEX, &newVertex);
   *numMessages = 1;

   /// 5. If the vertex is local, push to the local mutation message buffer; otherwise push to external mutation buffer
   if (dstDev == rank)   // local message
   {
      dataManager->pushLocalMutationMessage(message, thread);
      if (dataManager->numLocalMutationMessage(thread) >= MAX_LOCAL_MUTATION_MESSAGE)
         mutationManager->processMutationMessages(thread);
   }
   else   // external message
   {
      dataManager->pushExternalMutationMessage(message, thread);
      if (dataManager->numExternalMutationMessage(thread) >= MAX_EXTERNAL_MUTATION_MESSAGE)
      {
         communicationManager->sendInstMutationMessage(thread);
         dataManager->clearExternalMutationMessage(thread);   // cleared after tx
      }
   }
   return dstDev;
}

/*****************************************************************************/
/*!
 * @brief  Constructs a graph from a distributed file
 * @details reading subgraph in the from of adjacency list file and communicating the vertices to other devices. This function is called by only a single device that has access to the file.
 * @param fileName file name of the subgraph for the device
 * @return true if no error, false if in error
 */
/*****************************************************************************/
template<typename ALG_VERTEX_TYPE, typename ALG_COMBINER>
bool TIGL<ALG_VERTEX_TYPE, ALG_COMBINER>::mutateDistributedGraphFromFile(char *fileName)
{

   /// \b Procedure

   /// 1. Check graph and file consistency
   if (dataManager->isBiVertex())
   {   // TIGLContainersBiVertex Graph is not supported
      cout << "TIGLContainersBiVertex Distributed graph not supported !" << endl;
      return false;
   }

   if (EDGE_TYPE::hasVal())
   {   // weighted graph
      cout << "!!!!!! ERROR : weighted edges are not supported in tigl::readSubgraphFromFile" << endl << endl;
      return false;
   }
   ifstream file(fileName);
   string line;

   if (!file)
   {
      cout << "Error: cannot open " << fileName << " to read !" << endl << endl;
      return false;
   }

   ID_TYPE *deviceMessageCount = new ID_TYPE[numDevices];
   memset(deviceMessageCount, 0, numDevices * sizeof(ID_TYPE));
   // reading the file and filling the local/external mutation messages
   //MPI_BARRIER();

   vector < string > allLines;
   uint_fast32_t numLines = 0;
   ID_TYPE totalVerticesinFile = 0;
   /// 2. Read each line of the file, and push the corresponding messages to local/external mutation messages buffer
   while (getline(file, line))
   {
      allLines.push_back(line);
      numLines++;
      if (numLines == MAX_PROCESSED_LINES)
      {
         OMP("omp parallel for")
         for (uint_fast32_t lineIndex = 0; lineIndex < numLines; lineIndex++)
         {
            // reading the vertex
            uint_fast32_t numMessages;
            uint_fast32_t dev = mutateProcessOneLine(allLines[lineIndex], &numMessages);
            OMP("omp atomic")
            deviceMessageCount[dev] += numMessages;
         }
         totalVerticesinFile += numLines;
         numLines = 0;
         allLines.clear();
      }
   }
   file.close();
   if (numLines)
   {
      OMP("omp parallel for")
      for (uint_fast32_t lineIndex = 0; lineIndex < numLines; lineIndex++)
      {
         uint_fast32_t numMessages;
         uint_fast32_t dev = mutateProcessOneLine(allLines[lineIndex], &numMessages);
         OMP("omp atomic")
         deviceMessageCount[dev] += numMessages;
      }
      totalVerticesinFile += numLines;
   }

   /// 3. For external mutation messages, invoke the communication manager to send them to corresponding devices
   /// 4. Process all local mutation messages to construct the local subgraph
   OMP("omp parallel for")
   for (int thread = 0; thread < NUM_OMP_THREADS; thread++)
   {
      communicationManager->sendInstMutationMessage(thread);
      dataManager->clearExternalMutationMessage(thread);
      mutationManager->processMutationMessages(thread);
   }

   /// 5. Send to each device the total number of mutation messages that has been transmitted to it.
   for (uint_fast32_t dev = 0; dev < numDevices; dev++)
   {
      if (dev != rank)
      {
         TIGLCommunicationSendMPICommand((uint8_t*) &deviceMessageCount[dev], sizeof(ID_TYPE), dev,
         MPI_DISTRIBUTED_GRAPH_COUNT_TAG);
         //cout << "******* dev " << rank << " sent " << deviceMessageCount[dev] << " to dev" << dev << endl;
      }
   }
   // waiting for confirmaiton from other devices
   //MPI_BARRIER();

   ID_TYPE totalVerticesConstructed = dataManager->size();   // local vertices

   /// 6. Wait for confirmation from all devices
   // now waiting for confirmation for confirmation from other devices
   bool commStatus = true;
   //cout << "rank = " << rank << " waiting for size messages" << endl;
   while (totalVerticesConstructed != totalVerticesinFile && commStatus == true)
   {
      double startTime = tiglUtilsGetTimeUs();
      while (TIGLCommunicationExistMessagesWithTag(MPI_DISTRIBUTED_GRAPH_COUNT_TAG) == false)
      {
         double currTime = tiglUtilsGetTimeUs();
         if (fabs(currTime - startTime) > MAX_WAIT_TIME)
         {
            commStatus = false;
            break;
         }
      }
      if (commStatus == false)
         break;

      ID_TYPE numRemoteVertices;
      while (TIGLCommunicationExistMessagesWithTag(MPI_DISTRIBUTED_GRAPH_COUNT_TAG) == true)
      {
         if (TIGLCommunicationRecvTaggedMPICommand((uint8_t*) &numRemoteVertices,
         MPI_DISTRIBUTED_GRAPH_COUNT_TAG) != sizeof(ID_TYPE))
            break;
         totalVerticesConstructed += numRemoteVertices;
         if (totalVerticesConstructed == totalVerticesinFile)
            break;
      }
   }

   /// 7. Broadcast the final status to all devices
   uint8_t mutationStatus = totalVerticesConstructed == totalVerticesinFile ?
   GRAPH_CONSTRUCT_SUCCESS :
                                                                              GRAPH_CONSTRUCT_FAIL;

   communicationManager->taggedBcast(&mutationStatus, sizeof(uint8_t),
   MPI_DISTRIBUTED_GRAPH_CONSTRUCT_TAG);

   delete deviceMessageCount;
   return totalVerticesConstructed == totalVerticesinFile ? true : false;
}

/*****************************************************************************/
/*!
 * @brief  Constructs a graph from a distributed file
 * @details receiving mutation messages from reading device and processing them. This function is called by all devices except the reading device. This function is the complement to mutateDistributedGraphFromFile.
 * @param readDev the rank of the reading device
 *  @return true if no error, false if in error
 */
/*****************************************************************************/
template<typename ALG_VERTEX_TYPE, typename ALG_COMBINER>
bool TIGL<ALG_VERTEX_TYPE, ALG_COMBINER>::remoteMutateDistributedGraph(uint_fast32_t readDev)
{
   /// \b Procedure

   /// 1. Check graph and file consistency
   if (dataManager->isBiVertex())
   {   // TIGLContainersBiVertex Graph is not supported
      cout << "TIGLContainersBiVertex Distributed graph not supported !" << endl;
      return false;
   }

   if (EDGE_TYPE::hasVal())
   {   // weighted graph
      cout << "!!!!!! ERROR : weighted edges are not supported in tigl::readSubgraphFromFile" << endl << endl;
      return false;
   }

   // reading and executing the mutation messages from the read device
   //MPI_BARRIER();
   //cout << "rank = " << rank << " crossed barrier 1" << endl;

   double initTime = tiglUtilsGetTimeUs();
   bool commStatus = true;
   int_fast32_t messageSize;
   /// 2. Wait to receive all messages from the reading device.  Whenever, the mutation message buffer is full, invoke the mutation manager to process all mutation messages
   while (TIGLCommunicationExistTaggedMessageFromDev(MPI_MUTATION_MESSAGE_TAG, readDev, &messageSize) == false)
   {
      double CurrTime = tiglUtilsGetTimeUs();
      if (fabs(CurrTime - initTime) > MAX_WAIT_TIME)
      {
         commStatus = false;
         break;
      }
      //cout << "rank = " << rank << "  no message after " << (CurrTime-initTime)/1e3 << " ms" << endl;
   }
   if (commStatus == false)
      return false;

   initTime = tiglUtilsGetTimeUs();
   bool endStat = TIGLCommunicationExistTaggedMessageFromDev(MPI_DISTRIBUTED_GRAPH_COUNT_TAG, readDev, &messageSize);
//		OMP("omp parallel shared(endStat, commStatus)")
   while (endStat == false)
   {   // continue reading until receiving the end of reading message with the supposed number of vertices for this device
      while (TIGLCommunicationExistTaggedMessageFromDev(MPI_MUTATION_MESSAGE_TAG, readDev, &messageSize) == true)
      {
         communicationManager->instRecvMutationMessages(readDev);
//				cout << "rank = " << rank << " has " <<
//							dataManager->numAllLocalMutationMessage() <<" mutation messages" << endl;

//				cout<<"++++ numMutations : ";
//				for(int thread = 0; thread < NUM_OMP_THREADS; thread++)
//					cout << dataManager->numLocalMutationMessage(thread) << ", " ;
//				cout << endl;
         OMP("omp parallel for")
         for (int thread = 0; thread < NUM_OMP_THREADS; thread++)
            mutationManager->processMutationMessages(thread);

         initTime = tiglUtilsGetTimeUs();
      }

      double CurrTime = tiglUtilsGetTimeUs();
      if (fabs(CurrTime - initTime) > MAX_WAIT_TIME)
      {
         commStatus = false;
         break;
      }
      endStat = TIGLCommunicationExistTaggedMessageFromDev(MPI_DISTRIBUTED_GRAPH_COUNT_TAG, readDev, &messageSize);
   }
   if (commStatus == false)
      return false;

   /// 3. Wait and receive the count message from the reading device.
   // reading the message
   ID_TYPE targetVertexSize;   // = numTotalVertices/4;
   if (TIGLCommunicationRecvMPICommand((uint8_t*) &targetVertexSize, sizeof(ID_TYPE), readDev,
   MPI_DISTRIBUTED_GRAPH_COUNT_TAG) != sizeof(ID_TYPE))
      return false;

   ID_TYPE totalVerticesConstructed = dataManager->size();   // local vertices

   /// 4. If the number received mutation messages is less than the target, wait until receiving the remaining messages or until a maximum time is reaced
   initTime = tiglUtilsGetTimeUs();
   while (totalVerticesConstructed != targetVertexSize)
   {   // reading the remaining mutation messages
      while (TIGLCommunicationExistTaggedMessageFromDev(MPI_MUTATION_MESSAGE_TAG, readDev, &messageSize) == true)
      {
         communicationManager->instRecvMutationMessages(readDev);
         //cout << "rank = " << rank << " has " << dataManager->numAllLocalMutationMessage() <<" mutation messages" << endl;

//				cout<<"---- numMutations : ";
//				for(int thread = 0; thread < NUM_OMP_THREADS; thread++)
//					cout << dataManager->numLocalMutationMessage(thread) << ", " ;
//				cout << endl;
         OMP("omp parallel for")
         for (int thread = 0; thread < NUM_OMP_THREADS; thread++)
            mutationManager->processMutationMessages(thread);
         initTime = tiglUtilsGetTimeUs();
         totalVerticesConstructed = dataManager->size();   // local vertices
//				if(rank == 0)
//					cout << "------- dev " << rank << " rcved " << totalVerticesConstructed << " messages" << endl;
      }

      double CurrTime = tiglUtilsGetTimeUs();
      if (fabs(CurrTime - initTime) > MAX_WAIT_TIME)
      {
         commStatus = false;
         break;
      }
   }
   if (commStatus == false)
      return false;

   //MPI_BARRIER();

   // now sending this information for confirmaiton from other devices
   //cout << "|||||||| dev " << rank << " sends " <<totalVerticesConstructed << " count message" << endl;
   /// 5. Send a confirmation message to the reading device with the  number of local vertices
   TIGLCommunicationSendMPICommand((uint8_t*) &totalVerticesConstructed, sizeof(ID_TYPE), readDev,
   MPI_DISTRIBUTED_GRAPH_COUNT_TAG);

   // now waiting for confirmation from the read device
   double startTime = tiglUtilsGetTimeUs();
   //cout << "|||||||| dev " << rank << " waits for confirmation" << endl;
   /// 6. Wait to receive the confirmation message from the reading device
   while (TIGLCommunicationExistTaggedMessageFromDev(MPI_DISTRIBUTED_GRAPH_CONSTRUCT_TAG, readDev, &messageSize) == false)
   {
      double currTime = tiglUtilsGetTimeUs();
      if (fabs(currTime - startTime) > MAX_WAIT_TIME)
      {
         commStatus = false;
         break;
      }
   }
   if (commStatus == false)
      return false;

   uint8_t mutationStatus;
   //cout << "|||||||| dev " << rank << "reads the confirmation message" << endl;
   TIGLCommunicationRecvMPICommand(&mutationStatus, sizeof(uint8_t), readDev,
   MPI_DISTRIBUTED_GRAPH_CONSTRUCT_TAG);

   //MPI_BARRIER();
   return mutationStatus == GRAPH_CONSTRUCT_SUCCESS ? true : false;
}

/*****************************************************************************/
/*!
 * @brief Clears the memory allocation for the graph
 */
/*****************************************************************************/
template<typename ALG_VERTEX_TYPE, typename ALG_COMBINER>
void TIGL<ALG_VERTEX_TYPE, ALG_COMBINER>::reset()
{
   dataManager->reset();
}

/*****************************************************************************/
/*!
 * @brief  Newtwork reading subgraph from file.
 * @details The whole graph is read by an assigned device and communicated to other devices. It calls either @link tigl::mutateDistributedGraphFromFile mutateDistributedGraphFromFile @endlink if the device is the reading device or @link tigl::remoteMutateDistributedGraph remoteMutateDistributedGraph@endlink for all other devices
 * @param readDev the rank of the device that reads the whole subgraph from the file
 * @param fileName file name of the subgraph for the device
 * @return true if no error, false if in error
 * */
/*****************************************************************************/
template<typename ALG_VERTEX_TYPE, typename ALG_COMBINER>
bool TIGL<ALG_VERTEX_TYPE, ALG_COMBINER>::networkReadGraph(uint_fast32_t readDev, char *fileName)
{
   if (readDev == rank)
      return mutateDistributedGraphFromFile(fileName);
   else
      return remoteMutateDistributedGraph(readDev);
}

/*****************************************************************************/
/*!
 * @brief  Reads subgraph in the form of adjacency list
 * @details Each device reads its subgraph from the locak file system
 //@param fileName file name of the subgraph for the device
 * @return true if no error, false if in error
 */
/*****************************************************************************/
template<typename ALG_VERTEX_TYPE, typename ALG_COMBINER>
bool TIGL<ALG_VERTEX_TYPE, ALG_COMBINER>::mutateLocalSubgraphFromFile(char *fileName)
{
   if (EDGE_TYPE::hasVal())
   {   // weighted graph
      cout << "!!!!!! ERROR : weighted edges are not supported in tigl::readSubgraphFromFile" << endl << endl;
      return false;
   }
   ifstream file(fileName);
   string line;

   if (!file)
   {
      cout << "Error: cannot open " << fileName << " to read !" << endl << endl;
      return false;
   }
   /// \b Procedure

   /// 1. read all lines from the graph file:
   vector < string > allLines;
   uint_fast32_t numLines = 0;
   while (getline(file, line))
   {
      allLines.push_back(line);
      numLines++;
      if (numLines == MAX_PROCESSED_LINES)
      {
         OMP("omp parallel for")
         for (uint_fast32_t lineIndex = 0; lineIndex < numLines; lineIndex++)
         {
            string localLine = allLines[lineIndex];
            int thread = omp_get_thread_num();
            unsigned pos = localLine.find(":");
            string tmpStr = localLine.substr(0, pos);
            ID_TYPE id = atol(tmpStr.c_str());
            localLine = localLine.substr(pos + 1);
            TIGLMutationMessage message(rank);
            /// 2. For each new line construct a new vertex mutation messages
            message.writeSimpleVertexMessage(ADD_EMPTY_VERTEX, &id);
            dataManager->pushLocalMutationMessage(message, thread);

            /// 3. for each new line construct a number of edge mutation messages that equals the total number of edges

            while (localLine.size() > 1)
            {   // adding all out edges to the current vertex
               pos = localLine.find(", ");
               tmpStr = localLine.substr(0, pos);
               ID_TYPE target = atol(tmpStr.c_str());
               EDGE_TYPE newEdge(target);
               //newVertex.addEdge(newEdge);
               TIGLMutationMessage message(rank);   //(ADD_OUT_EDGE, rank, &newEdge , &id);
               message.writeEdgeMessage(ADD_OUT_EDGE, &newEdge, &id);
               dataManager->pushLocalMutationMessage(message, thread);
               localLine = localLine.substr(pos + 1);   // cutting the localLine prefix
            }
            /// 4. Execute the mutation messages, when the message buffer is full
            if (dataManager->numLocalMutationMessage(thread) >= MAX_MUTATION_MESSAGE)
               mutationManager->processMutationMessages(thread);
         }
         numLines = 0;
         allLines.clear();
      }
   }
   file.close();

   if (numLines)
   {
      OMP("omp parallel for")
      for (uint_fast32_t lineIndex = 0; lineIndex < numLines; lineIndex++)
      {
         string localLine = allLines[lineIndex];
         int thread = omp_get_thread_num();
         unsigned pos = localLine.find(":");
         string tmpStr = localLine.substr(0, pos);
         ID_TYPE id = atol(tmpStr.c_str());
         localLine = localLine.substr(pos + 1);
         TIGLMutationMessage message(rank);   //(ADD_EMPTY_VERTEX, rank, &id);
         message.writeSimpleVertexMessage(ADD_EMPTY_VERTEX, &id);
         dataManager->pushLocalMutationMessage(message, thread);

         while (localLine.size() > 1)
         {   // adding all out edges to the current vertex
            pos = localLine.find(", ");
            tmpStr = localLine.substr(0, pos);
            ID_TYPE target = atol(tmpStr.c_str());
            EDGE_TYPE newEdge(target);
            //newVertex.addEdge(newEdge);
            TIGLMutationMessage message(rank);   //(ADD_OUT_EDGE, rank, &newEdge , &id);
            message.writeEdgeMessage(ADD_OUT_EDGE, &newEdge, &id);
            dataManager->pushLocalMutationMessage(message, thread);

            localLine = localLine.substr(pos + 1);   // cutting the localLine prefix
         }
         if (dataManager->numLocalMutationMessage(thread) >= MAX_MUTATION_MESSAGE)
            mutationManager->processMutationMessages(thread);
      }
   }

   OMP("omp parallel for")
   for (int thread = 0; thread < NUM_OMP_THREADS; thread++)   //for(int thread = 3; thread >= 0; thread--)//
   {
      if (dataManager->numLocalMutationMessage(thread))
         mutationManager->processMutationMessages(thread);
   }
   // now taking care of the input edges in case a buffer exist for them
   if (dataManager->isBiVertex())
   {   // reading again to add the input edges
      allLines.clear();
      numLines = 0;
      file.open(fileName);
      if (!file)
      {
         dataManager->reset();   // clear previous allocations
         cout << "Error: cannot open " << fileName << " to read !" << endl << endl;
         return false;
      }
      while (getline(file, line))
      {
         allLines.push_back(line);
         numLines++;
         if (numLines == MAX_PROCESSED_LINES)
         {
            OMP(
                  "omp parallel for")for (uint_fast32_t lineIndex = 0; lineIndex < numLines; lineIndex++)
            {
               string localLine = allLines[lineIndex];
               int thread = omp_get_thread_num();
               unsigned pos = localLine.find(":");
               string tmpStr = localLine.substr(0, pos);
               ID_TYPE id = atol(tmpStr.c_str());
               localLine = localLine.substr(pos + 1);

               while (localLine.size() > 1)
               {   // adding all out edges to the current vertex
                  pos = localLine.find(", ");
                  tmpStr = localLine.substr(0, pos);
                  ID_TYPE target = atol(tmpStr.c_str());
                  EDGE_TYPE newEdge(id);
                  //dataManager-> addInEdge(id, target);
                  //TIGLMutationMessage message(ADD_IN_EDGE, rank, &id , &target);
                  TIGLMutationMessage message(rank);
                  message.writeEdgeMessage(ADD_IN_EDGE, &newEdge, &target);
                  dataManager->pushLocalMutationMessage(message, thread);
                  localLine = localLine.substr(pos + 1);
               }
            }
            //OMP("omp parallel for")
            for (int thread = 0; thread < omp_get_num_threads(); thread++)
               mutationManager->processMutationMessages(thread);

            numLines = 0;
            allLines.clear();
         }
      }
      file.close();

      if (numLines)
      {
         OMP("omp parallel for")
         for (uint_fast32_t lineIndex = 0; lineIndex < numLines; lineIndex++)
         {
            string localLine = allLines[lineIndex];
            int thread = omp_get_thread_num();
            unsigned pos = localLine.find(":");
            string tmpStr = localLine.substr(0, pos);
            ID_TYPE id = atol(tmpStr.c_str());
            localLine = localLine.substr(pos + 1);

            while (localLine.size() > 1)
            {   // adding all out edges to the current vertex
               pos = localLine.find(", ");
               tmpStr = localLine.substr(0, pos);
               ID_TYPE target = atol(tmpStr.c_str());
               EDGE_TYPE newEdge(id);
               //dataManager-> addInEdge(id, target);
               //TIGLMutationMessage message(ADD_IN_EDGE, rank, &id , &target);
               TIGLMutationMessage message(rank);
               message.writeEdgeMessage(ADD_IN_EDGE, &newEdge, &target);
               dataManager->pushLocalMutationMessage(message, thread);
               localLine = localLine.substr(pos + 1);
            }
         }
//OMP("omp parallel for")
         for (int thread = 0; thread < omp_get_num_threads(); thread++)
            mutationManager->processMutationMessages(thread);

         allLines.clear();
      }
   }
   return true;
}

/*****************************************************************************/
/*!
 * @brief Reads subgraph in the form of adjacency list
 * @details This is similar to mutateLocalSubgraphFromFile but construct the graph directly without using the mutation manager.
 * @param fileName file name of the subgraph for the device
 * @return true if no error, false if in error
 */
/*****************************************************************************/
template<typename ALG_VERTEX_TYPE, typename ALG_COMBINER>
bool TIGL<ALG_VERTEX_TYPE, ALG_COMBINER>::readSubgraphFromFile(char *fileName)
{
   if (EDGE_TYPE::hasVal())
   {   // weighted graph
      cout << "!!!!!! ERROR : weighted edges are not supported in tigl::readSubgraphFromFile" << endl << endl;
      return false;
   }
// unweighted Graph
   ifstream file(fileName);
   string line;

   if (!file)
   {
      cout << "Error: cannot open " << fileName << " to read !" << endl << endl;
      return false;
   }

   while (getline(file, line))
   {
      unsigned pos = line.find(":");
      string tmpStr = line.substr(0, pos);
      ID_TYPE id = atol(tmpStr.c_str());
      line = line.substr(pos + 1);
      ALG_VERTEX_TYPE newVertex(id);

      while (line.size() > 1)
      {   // adding all out edges to the current vertex
         pos = line.find(", ");
         tmpStr = line.substr(0, pos);
         ID_TYPE target = atol(tmpStr.c_str());
         if (newVertex.isConnected(target) == false)
         {
            EDGE_TYPE newEdge(target);
            newVertex.addEdge(newEdge);
         }

         line = line.substr(pos + 1);
      }
      // adding the vertex to the list
      dataManager->addVertex(newVertex);
   }
   file.close();

   if (dataManager->isBiVertex())
   {   // reading again to add the input edges
      file.open(fileName);
      if (!file)
      {
         dataManager->reset();   // clear previous allocations
         cout << "Error: cannot open " << fileName << " to read !" << endl << endl;
         return false;
      }
      while (getline(file, line))
      {
         unsigned pos = line.find(":");
         string tmpStr = line.substr(0, pos);
         ID_TYPE id = atol(tmpStr.c_str());
         line = line.substr(pos + 1);

         while (line.size() > 1)
         {   // adding all out edges to the current vertex
            pos = line.find(", ");
            tmpStr = line.substr(0, pos);
            ID_TYPE target = atol(tmpStr.c_str());   // get from "live" to the end
            dataManager->addInEdge(id, target);
            line = line.substr(pos + 1);
         }
      }
      file.close();
   }

   return true;
}

/*****************************************************************************/
/*!
 * @brief Creates a graph from a vertex list
 * @param vertex list that contains the subgraph for the current device
 */
/*****************************************************************************/
template<typename ALG_VERTEX_TYPE, typename ALG_COMBINER>
void TIGL<ALG_VERTEX_TYPE, ALG_COMBINER>::initGraph(vector<ALG_VERTEX_TYPE> & vertexList)
{
   numLocalVertices = vertexList.size();
   for (typename vector<ALG_VERTEX_TYPE>::iterator it = vertexList.begin(); it != vertexList.end(); ++it)
      dataManager->addVertex(*it);
}

/*****************************************************************************/
/*!
 * @brief The highest level function for %TIGL engine execution.
 * @details It launches all the communication and computation tasks for algorithm execution
 * @param maxSS the maximum number of supersteps for the graph algorithm
 * @param initParams pointer to the intialization parameters for the graph algorithm
 * @return true if not error, false if an error takes place
 */
/*****************************************************************************/
template<typename ALG_VERTEX_TYPE, typename ALG_COMBINER>
bool TIGL<ALG_VERTEX_TYPE, ALG_COMBINER>::run(uint_fast32_t maxSS, ALG_INIT *initParams)
{
   countSS = 0;
   dataManager->init();
   communicationManager->setTimePtr(&countSS);
   computationManager->setMaxSS(maxSS);
   algorithmRunning = true;
   bool status;

   /// \b Procedure

   if (numDevices > 1)
   {
      omp_set_nested(1);
      /// 1. Synchronize all devices
      MPI_BARRIER();
      OMP("omp parallel")
      {
         /// 2. If multiple devices: launch in parallel the TX, RX, and compute tasks
         OMP("omp single nowait")
         {
            OMP("omp task")
            {
               txTask();
//						cout << "dev" << rank << "------------------- end of txTask" << endl;
            }
         }
         OMP("omp single nowait")
         {
            OMP("omp task")
            {
               rxTask();
//						cout << "dev" << rank << "------------------- end of rxTask" << endl;
            }
         }
         OMP("omp single nowait")
         {
            OMP("omp task")
            {
               status = computeTask(maxSS, initParams);
//						cout << "dev" << rank <<"------------------- end of computeTask" << endl;
            }
         }
      OMP("omp barrier")
// probably redundant
   }
}
else
{
   ///  3. else if one device, launch the compute task
   status = computeTask(maxSS, initParams);
}

#if PROFILE_ENABLE
communicationManager->printIdleTime();
#endif
return status;
}

/*****************************************************************************/
/*!
 * @brief The core computation task
 * @details It performs the engine initialization, handles the vertex processing at each superstep,
 * It also handles the algorithm termination procedure
 * @param maxSS the maximum number of supersteps for the graph algorithm
 * @param initParams pointer to the intialization parameters for the graph algorithm
 * @return true if not error, false if an error takes place
 */
/*****************************************************************************/
template<typename ALG_VERTEX_TYPE, typename ALG_COMBINER>
bool TIGL<ALG_VERTEX_TYPE, ALG_COMBINER>::computeTask(uint_fast32_t maxSS, ALG_INIT *initParams)
{

// running the initialization procedure for all vertices
#ifdef	PROFILE_ENABLE
tiglUtilsRegisterTime();
//double txTime = 0;
//		double rxTime = 0;
double processTime = 0;
double barrierTime = 0;
uint_fast32_t avExternalEdges = 0;
#endif

#ifdef	DEBUG_ENABLE
cout<< "Dev " << rank << ": Initialization procedure" << endl;
#endif
/// \b Procedure

/// 1. Synchronize all devices.
MPI_BARRIER();
/// 2. Initialize communication manager of the superstep.
communicationManager->initSS();
/// 3. Execute the init procedure of all local vertices.
computationManager->init(initParams);
/// 4. Wait for the communication manager to TX/RX all messages.
communicationManager->endSS();   // returns only when TX is complete

#ifdef	DEBUG_ENABLE
cout<< "Dev " << rank << ": Initialization Done" << endl;
#endif

#ifdef	DEBUG_ENABLE
uint_fast32_t initTime = tiglUtilsRegisterTime();
cout<< "init profiling: " << initTime << " ns" << endl;
#endif

SS_STATUS ssStatus = SS_SUCCESS;
/// 5. Repeat the following steps for each superstep until the algorithm is completed or the maximum number of supersteps is reached.
countSS = 1;
#if defined(_MPI)
bool haltMessageSent = false;
uint_fast32_t numDevicesIdle = 0;
uint_fast32_t numDevicesCompleted = 0;

while(countSS < maxSS)
#else
while (ssStatus != ALGORITHM_COMPLETED && countSS < maxSS)
#endif
{
   //				cout << "-------------------------------------------------- dev " << rank << " SS " << countSS<< endl;
#ifdef	PROFILE_ENABLE

   double barrierStart = tiglUtilsGetTimeUs();
#endif

   //		cout << " (((((((((((( dev " << rank << "at barrier" << endl;
   /// 6. Apply global barrier for all devices.
   MPI_BARRIER();
   //		cout << " )))))))))))) dev " << rank << "crossed barrier" << endl;
#ifdef	PROFILE_ENABLE
   barrierTime += tiglUtilsGetTimeUs()-barrierStart;
#endif
   // running the core superstep procedure
   /// 7. Execute all mutation messages (if any).
   mutationManager->processAllMutationMessages();

   /// 8. Enable the communication manager to restart TX/RX.
   communicationManager->initSS();
#ifdef	PROFILE_ENABLE
   double processStart = tiglUtilsGetTimeUs();
#endif
   /// 9. Execute the superstep in the computation manager.
   ssStatus = computationManager->superstep(countSS);
#ifdef	PROFILE_ENABLE
   processTime += tiglUtilsGetTimeUs()-processStart;
#endif

#if defined(_MPI)
   /// 10. If there is no more active local nodes, send idle message to all devices.

   /// 11. If device is idle and received idle message from all other device, send halt message to all devices.

   /// 12. If all devices are in halt, algorithm is terminated.

   if(ssStatus == ALGORITHM_COMPLETED)
   {
      numDevicesIdle = computationManager-> getNumIdleSysMessages();
      numDevicesIdle ++;
      numDevicesCompleted = computationManager-> getNumHaltSysMessages();
      if (haltMessageSent == true)
      numDevicesCompleted ++;
   }
   if(numDevicesIdle == numDevices && numDevicesCompleted < numDevices)
   {   // sending completed messages
      computationManager->pushHaltMessage();
      haltMessageSent = true;
   }
#endif

   /// 13. Elseif the algorithm is still running, wait to TX/RX all pending messages, then disable communication.
   communicationManager->endSS();

#if defined(_MPI)
   if(numDevicesCompleted == numDevices)
   {
      break;
   }
#endif

   //		 uint_fast32_t totalMessages = dataManager->getTotalExternalMessageCount();
#ifdef	PROFILE_ENABLE
   avExternalEdges += dataManager->getTotalExternalMessageCount();;
#endif
   countSS++;
}   // end of the message while loop

//	cout << "******************************************************* dev" << rank << ": done!" << endl;
algorithmRunning = false;
OMP("omp flush (algorithmRunning)")
//	OMP("omp barrier")
//	cout << "^^^^^^^^^^ dev " << rank << " completed computation" << endl;
/// 14. After the algorithm is completed, apply a global barrier.
MPI_BARRIER();
//	cout << "^^^^^^^^^^ dev " << rank << " crossed MPI barrier" << endl;
#if defined(PROFILE_ENABLE) && defined(_MPI)

cout << "$$$$$$$ dev " << rank << " processTime = " << (uint_fast32_t)(processTime/1000) <<
", barrierTime = " << (uint_fast32_t)(barrierTime/1000)<<
", avEx = " << avExternalEdges/countSS << endl;
#endif

if (ssStatus == ALGORITHM_COMPLETED)
   return true;

return false;
}

/*****************************************************************************/
/*!
 * @brief Starts the TX task that is handled by the communication manager
 */
/*****************************************************************************/
template<typename ALG_VERTEX_TYPE, typename ALG_COMBINER>
void TIGL<ALG_VERTEX_TYPE, ALG_COMBINER>::txTask()
{
//		cout << "****** txTask started " << endl;
while (algorithmRunning == true)
{
   communicationManager->continuousSendMessages();
   usleep(2);
}
}
/*****************************************************************************/
/*!
 * @brief Starts the RX task that is handled by the communication manager
 */
/*****************************************************************************/
template<typename ALG_VERTEX_TYPE, typename ALG_COMBINER>
void TIGL<ALG_VERTEX_TYPE, ALG_COMBINER>::rxTask()
{
while (algorithmRunning == true)
{
   communicationManager->continuousRecvMessages();
   usleep(2);
}
}

/*****************************************************************************/
/*!
 * @brief Computes the SNR (in dB) between the computed values and values from a benchmark implementation
 * @param refVect vertex list that has the reference implementation results
 * @return the snr in dB between the computed output and the reference output. In MPI environment device with rank 0 collects the SNR from all other devices
 */
/*****************************************************************************/
template<typename ALG_VERTEX_TYPE, typename ALG_COMBINER>
template<typename CVertex>
double TIGL<ALG_VERTEX_TYPE, ALG_COMBINER>::computeSNR(vector<CVertex> & refVect)
{
double mse = 0;
double sig = 0;

for (typename vector<CVertex>::iterator refIt = refVect.begin(); refIt != refVect.end(); ++refIt)
{
   uint_fast32_t pos;
   ID_TYPE refId = refIt->getId();
   if (dataManager->findPos(refId, pos) == false)
      continue;
   VERTEX_ITERATOR it = dataManager->begin() + pos;

   ALG_DATA_TYPE algVal = it->getAlgData();
   double diff = algVal - refIt->getAlgData();
   //cout<<"(" << algVal << ", " << refIt -> getAlgData() << ")" << endl;
   mse += diff * diff;
   sig += algVal * algVal;
}

#if defined(_MPI)
#define			SNR_MESSAGE_TAG			20
double remoteErr[2], globalErr[2];
remoteErr[0] = mse; remoteErr[1] = sig;

MPI_Reduce(remoteErr, globalErr, 2,
      MPI_DOUBLE, MPI_SUM,
      0,
      MPI_COMM_WORLD);

if(rank == 0)
{
   mse = globalErr[0];
   sig = globalErr[1];
}

#endif

return 10 * log10(sig / mse);
}

/*****************************************************************************/
/*!
 * @brief Compares the output results with benchmark algorithm (similar to computeSNR but normalization takes place)
 * @param refVect vertex list that has the reference implementation results
 * @return the snr in dB between the computed output and the reference output. In MPI environment device with rank 0 collects the SNR from all other devices
 */
/*****************************************************************************/
template<typename ALG_VERTEX_TYPE, typename ALG_COMBINER>
template<typename CVertex>
double TIGL<ALG_VERTEX_TYPE, ALG_COMBINER>::computeNormalizedSNR(vector<CVertex> & refVect)
{
double mse = 0;
double sig = 0;

ALG_DATA_TYPE refFactor = 0;
ALG_DATA_TYPE currFactor = 0;

typename vector<CVertex>::iterator refIt = refVect.begin();
for (VERTEX_ITERATOR it = dataManager->begin(); it != dataManager->end(); ++it)
{
   currFactor += it->getAlgData();
   refFactor += refIt->getAlgData();
   refIt++;
}

refIt = refVect.begin();
for (VERTEX_ITERATOR it = dataManager->begin(); it != dataManager->end(); ++it)
{
   ALG_DATA_TYPE algVal = it->getAlgData() / currFactor;
   ALG_DATA_TYPE diff = algVal - refIt->getAlgData() / refFactor;
   //cout<<"(" << algVal << ", " << refIt -> getAlgData()/refFactor << ")" << endl;
   mse += diff * diff;
   sig += algVal * algVal;
   refIt++;
}
return 10 * log10(sig / mse);
}

/*****************************************************************************/
/*!
 * @brief Computes the Mean Square Error (MSE) between the computed value and a reference value
 * @param refVect vertex list that has the reference implementation results
 * @return the MSE between the computed output and the reference output. In MPI environment device with rank 0 collects the SNR from all other devices
 */
/*****************************************************************************/
template<typename ALG_VERTEX_TYPE, typename ALG_COMBINER>
template<typename CVertex>
double TIGL<ALG_VERTEX_TYPE, ALG_COMBINER>::computeMSE(vector<CVertex> & refVect)
{
double mse = 0;

for (typename vector<CVertex>::iterator refIt = refVect.begin(); refIt != refVect.end(); ++refIt)
{
   uint_fast32_t pos;
   ID_TYPE refId = refIt->getId();
   if (dataManager->findPos(refId, pos) == false)
      continue;
   VERTEX_ITERATOR it = dataManager->begin() + pos;

   ALG_DATA_TYPE algVal = it->getAlgData();
   double diff = algVal - refIt->getAlgData();
   mse += diff * diff;
}

#if defined(_MPI)
#define			MSE_MESSAGE_TAG			21
double globalMse = 0;
MPI_Reduce(&mse, &globalMse, 1,
      MPI_DOUBLE, MPI_SUM,
      0,
      MPI_COMM_WORLD);

if(rank == 0)
mse = globalMse;
#endif
return mse;
}

/*****************************************************************************/
/*!
 * @brief Computes the approximate overall memory usage
 */
/*****************************************************************************/
template<typename ALG_VERTEX_TYPE, typename ALG_COMBINER>
uint_fast32_t TIGL<ALG_VERTEX_TYPE, ALG_COMBINER>::memoryUsage()
{
return dataManager->memoryUsage();   // not including outMessage buffer and activeBuffers
}

/*****************************************************************************/
/*!
 * @brief Returns number of local vertices
 * */
/*****************************************************************************/
template<typename ALG_VERTEX_TYPE, typename ALG_COMBINER>
uint_fast32_t TIGL<ALG_VERTEX_TYPE, ALG_COMBINER>::getNumVertices()
{
return dataManager->getNumVertices();
}

/*****************************************************************************/
/*!
 * @brief Return the total number of edges in the graph
 */
/*****************************************************************************/
template<typename ALG_VERTEX_TYPE, typename ALG_COMBINER>
typename ALG_VERTEX_TYPE::ID_TYPE TIGL<ALG_VERTEX_TYPE, ALG_COMBINER>::getNumEdges()
{
return dataManager->getNumEdges();
}

/*****************************************************************************/
/*!
 * @brief Returns the maximum number of hash collisions in the position buffer
 */
/*****************************************************************************/
template<typename ALG_VERTEX_TYPE, typename ALG_COMBINER>
uint_fast32_t TIGL<ALG_VERTEX_TYPE, ALG_COMBINER>::MaxCollision()
{
return dataManager->MaxCollision();
}

#ifdef	PROFILE_ENABLE
/*****************************************************************************/
/*!
 * @brief Prints detailed profiling information
 */
/*****************************************************************************/
template <typename ALG_VERTEX_TYPE, typename ALG_COMBINER>
void TIGL<ALG_VERTEX_TYPE, ALG_COMBINER> :: PrintProfilingInfo()
{
computationManager->printProfilingInfo();
}
#endif

/*****************************************************************************/
/*!
 * @brief Returns the number of supersteps
 */
/*****************************************************************************/
template<typename ALG_VERTEX_TYPE, typename ALG_COMBINER>
inline uint_fast32_t TIGL<ALG_VERTEX_TYPE, ALG_COMBINER>::numSS()
{
return countSS;
}

#ifdef DEBUG_ENABLE
/*****************************************************************************/
/*!
 * @brief Prints the algData field of local vertices to stdout
 */
/*****************************************************************************/
template<typename ALG_VERTEX_TYPE, typename ALG_COMBINER>
void TIGL<ALG_VERTEX_TYPE, ALG_COMBINER>::printAlgData()
{
for (VERTEX_ITERATOR it = dataManager->begin(); it != dataManager->end(); ++it)
cout << "TIGLApiVertex " << it->getId() << ": " << it->getAlgData() << endl;
}

/*****************************************************************************/
/*!
 * @brief Prints the algData field of local vertices to a file
 * @param fileName the destination file name
 */
/*****************************************************************************/
template<typename ALG_VERTEX_TYPE, typename ALG_COMBINER>
bool TIGL<ALG_VERTEX_TYPE, ALG_COMBINER>::printAlgData2File(char *fileName)
{
ofstream file(fileName);

if (!file)
{
   cout << "Error: cannot open " << fileName << " to write !" << endl << endl;
   return false;
}

for (VERTEX_ITERATOR it = dataManager->begin(); it != dataManager->end(); ++it)
file << it->getAlgData() << endl;
file.close();
return true;
}

/*****************************************************************************/
/*!
 * @brief Prints the contents of vertex buffer
 */
/*****************************************************************************/
template<typename ALG_VERTEX_TYPE, typename ALG_COMBINER>
void TIGL<ALG_VERTEX_TYPE, ALG_COMBINER>::printBuffers()
{
dataManager->printBuffers();
}
#endif
#endif /* TIGLIMPLEMENTATION_HPP_ */
