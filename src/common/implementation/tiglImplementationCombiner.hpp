#ifndef TIGLIMPLEMENTATIONCOMBINER_HPP_
#define TIGLIMPLEMENTATIONCOMBINER_HPP_

/*****************************************************************************/
/*!
 * @file tiglImplementationCombiner.hpp
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/

/******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/
#include "tiglApiCombiner.hpp"

/******************************************************************************
 *
 * global definitions
 *
 ******************************************************************************/
using namespace std;

/*****************************************************************************/
/*!
 * @brief Contsructor
 */
/*****************************************************************************/
template<typename ALG_MESSAGE_TYPE>
TIGLApiCombiner<ALG_MESSAGE_TYPE>::TIGLApiCombiner()
{
}

/*****************************************************************************/
/*!
 * @brief Destructor
 */
/*****************************************************************************/
template<typename ALG_MESSAGE_TYPE>
TIGLApiCombiner<ALG_MESSAGE_TYPE>::~TIGLApiCombiner()
{
}

/*****************************************************************************/
/*!
 * @brief Core combine function for multiple messages.
 * @details It is an empty function that must be overridden by customized vertex definition for the different graph algorithms.
 * @param itIn Iterator to input messages.
 * @param numMessages number of input messages.
 *@param itOut iterator to the start of the ouput message.
 *@return the number of combined output data messages.
 */
/*****************************************************************************/
template<typename ALG_MESSAGE_TYPE>
uint_fast32_t TIGLApiCombiner<ALG_MESSAGE_TYPE>::combineMessages(MESSAGE_ITERATOR itIn, uint_fast32_t numMessages, MESSAGE_ITERATOR itOut)
{
   return 0;
}

/*****************************************************************************/
/*!
 *@brief core combine function from combining a single message.
 *@details It is an empty function that must be overridden by customized vertex definition for the different graph algorithms.
 *@param newMessage the new message.
 *@param itOut iterator to the start of the ouput message.
 *@return the number of messages after combining.
 */
/*****************************************************************************/
template<typename ALG_MESSAGE_TYPE>
uint_fast32_t TIGLApiCombiner<ALG_MESSAGE_TYPE>::combineMessages(ALG_MESSAGE_TYPE & newMessage, MESSAGE_ITERATOR itOut)
{
   // reduces the onverall number of messages for each node
   // returns the output number of messages
   return 0;
}

/*****************************************************************************/
/*!
 *@brief Sum combine function.
 *@details it sums the data fields of all messages into a single message
 *@param itIn Iterator to input messages
 *@param numMessages number of input messages
 *@param itOut iterator to the start of the ouput message
 *@return the number of combined output data messages (1 if there are messages, 0 if no messages)
 */
/*****************************************************************************/
template<typename ALG_MESSAGE_TYPE>
uint_fast32_t TIGLApiCombiner<ALG_MESSAGE_TYPE>::sumCombineMessages(MESSAGE_ITERATOR itIn, uint_fast32_t numMessages, MESSAGE_ITERATOR itOut)
{

   if (numMessages == 0)
      return 0;

   if (itOut != itIn)
   {
      itOut->setMessage(*itIn);
   }
   if (numMessages == 1)
      return 1;

   itIn++;
   for (uint_fast32_t loopCount = 1; loopCount < numMessages; loopCount++)   //for(; itm !=  itIn->end(); ++itm)//
   {
      itOut->sumContent(itIn->getContent());
      ++itIn;
   }
   return 1;
}

/*****************************************************************************/
/*!
 *@brief implementation of sum combine function for adding a single message.
 *@param newMessage reference to the new message
 *@param itOut iterator to the start of the ouput message
 *@return the number of combined output data messages
 */
/*****************************************************************************/
template<typename ALG_MESSAGE_TYPE>
uint_fast32_t TIGLApiCombiner<ALG_MESSAGE_TYPE>::sumCombineMessages(ALG_MESSAGE_TYPE & newMessage, MESSAGE_ITERATOR itOut)
{
   itOut->sumContent(newMessage.getContent());
   return 1;
}

/*****************************************************************************/
/*!
 *@brief Maximum combine function.
 *@details It computes the maximum the data fields of all messages into a single message.
 *@param itIn Iterator to input messages.
 *@param numMessages number of input messages.
 *@param itOut iterator to the start of the ouput message.
 *@return the number of combined output data messages (1 if there are messages, 0 if no messages).
 */
/*****************************************************************************/
template<typename ALG_MESSAGE_TYPE>
uint_fast32_t TIGLApiCombiner<ALG_MESSAGE_TYPE>::maxCombineMessages(MESSAGE_ITERATOR itIn, uint_fast32_t numMessages, MESSAGE_ITERATOR itOut)
{
   if (numMessages == 0)
      return 0;

   if (itOut != itIn)
   {
      itOut->setMessage(*itIn);
   }
   if (numMessages == 1)
      return 1;

   itIn++;
   for (uint_fast32_t loopCount = 1; loopCount < numMessages; loopCount++)   //for(; itm !=  itIn->end(); ++itm)
   {
      itOut->maxContent(*itIn);
      ++itIn;
   }
   return 1;
}

/*****************************************************************************/
/*!
 *@brief Max combine function for adding a single message.
 *@param newMessage reference to the new message.
 *@param itOut iterator to the start of the ouput message.
 *@return the number of combined output data messages.
 */
/*****************************************************************************/
template<typename ALG_MESSAGE_TYPE>
uint_fast32_t TIGLApiCombiner<ALG_MESSAGE_TYPE>::maxCombineMessages(ALG_MESSAGE_TYPE & newMessage, MESSAGE_ITERATOR itOut)
{
   itOut->maxContent(newMessage);

   return 1;
}

/*****************************************************************************/
/*!
 *@brief bypass combine function.
 *@details it copies the input to the output
 *@param itIn Iterator to input messages.
 *@param numMessages number of input messages.
 *@param itOut iterator to the start of the ouput message.
 *@return the number of combined output data messages (which equals numMessages).
 */
/*****************************************************************************/
template<typename ALG_MESSAGE_TYPE>
uint_fast32_t TIGLApiCombiner<ALG_MESSAGE_TYPE>::bypassCombineMessages(MESSAGE_ITERATOR itIn, uint_fast32_t numMessages, MESSAGE_ITERATOR itOut)
{
   if (numMessages == 0)
      return 0;

   if (itOut == itIn)
   {
      return numMessages;
   }
   //itOut->setMessage(*itIn);
   std::copy(itIn, itIn+numMessages, itOut);
   return numMessages;
}


/*****************************************************************************/
/*!
 *@brief Minimum combine function.
 *@details it computes the minimum the data fields of all messages into a single message.
 *@param itIn Iterator to input messages.
 *@param numMessages number of input messages.
 *@param itOut iterator to the start of the ouput message.
 *@return the number of combined output data messages (1 if there are messages, 0 if no messages).
 */
/*****************************************************************************/
template<typename ALG_MESSAGE_TYPE>
uint_fast32_t TIGLApiCombiner<ALG_MESSAGE_TYPE>::minCombineMessages(MESSAGE_ITERATOR itIn, uint_fast32_t numMessages, MESSAGE_ITERATOR itOut)
{
   if (numMessages == 0)
      return 0;

   if (itOut != itIn)
   {
      itOut->setMessage(*itIn);
   }
   if (numMessages == 1)
      return 1;

   itIn++;
   for (uint_fast32_t loopCount = 1; loopCount < numMessages; loopCount++)
   {
      itOut->minContent(*itIn);
      ++itIn;
   }
   return 1;
}

/*****************************************************************************/
/*!
 *@brief Minimum combine function for a single message.
 *@param newMessage reference to the new message.
 *@param itOut iterator to the start of the ouput message.
 */
/*****************************************************************************/
template<typename ALG_MESSAGE_TYPE>
uint_fast32_t TIGLApiCombiner<ALG_MESSAGE_TYPE>::minCombineMessages(ALG_MESSAGE_TYPE & newMessage, MESSAGE_ITERATOR itOut)
{
   itOut->minContent(newMessage);

   return 1;
}
#endif // of TIGLIMPLEMENTATIONCOMBINER_HPP_
