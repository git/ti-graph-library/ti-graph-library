#ifndef TIGLIMPLEMENTATIONMESSAGE_HPP_
#define TIGLIMPLEMENTATIONMESSAGE_HPP_

/*****************************************************************************/
/*!
 * @file tiglImplementationMessage.hpp
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/

/******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/
#include "tiglApiMessage.hpp"

/*****************************************************************************/
/*!
 * @brief Empty constructor
 * @details Message fields have to be filled using public methods
 */
/*****************************************************************************/
template<typename DATA_TYPE, typename ID_TYPE>
TIGLApiMessage<DATA_TYPE, ID_TYPE>::TIGLApiMessage()
{
}

/*****************************************************************************/
/*!
 * @brief Constructor of a message with all fields
 * @param tx  the sender vertex
 * @param rx  the receiver vertex
 * @param messContent  the message body
 */
/*****************************************************************************/
template<typename DATA_TYPE, typename ID_TYPE>
TIGLApiMessage<DATA_TYPE, ID_TYPE>::TIGLApiMessage(ID_TYPE & tx, ID_TYPE & rx, DATA_TYPE & messContent)
{
   setMessage(tx, rx, messContent);
}

/*****************************************************************************/
/*!
 * @brief Destructor
 */
/*****************************************************************************/
template<typename DATA_TYPE, typename ID_TYPE>
TIGLApiMessage<DATA_TYPE, ID_TYPE>::~TIGLApiMessage()
{
}

/*****************************************************************************/
/*!
 * @brief Returns the sender vertex
 */
/*****************************************************************************/
template<typename DATA_TYPE, typename ID_TYPE>
inline ID_TYPE & TIGLApiMessage<DATA_TYPE, ID_TYPE>::getSender()
{
   return sender;
}

/*****************************************************************************/
/*!
 * @brief Returns the destination vertex
 */
/*****************************************************************************/
template<typename DATA_TYPE, typename ID_TYPE>
inline ID_TYPE & TIGLApiMessage<DATA_TYPE, ID_TYPE>::getRcvr()
{
   return rcvr;
}

/*****************************************************************************/
/*!
 *@brief returns the message content
 */
/*****************************************************************************/
template<typename DATA_TYPE, typename ID_TYPE>
inline DATA_TYPE & TIGLApiMessage<DATA_TYPE, ID_TYPE>::getContent()
{
   return content;
}

/*****************************************************************************/
/*!
 * @brief Prepares the full message fields
 * @param tx  the sender vertex
 * @param rx  the receiver vertex
 * @param messContent  the message body
 */
/*****************************************************************************/
template<typename DATA_TYPE, typename ID_TYPE>
inline void TIGLApiMessage<DATA_TYPE, ID_TYPE>::setMessage(ID_TYPE & tx, ID_TYPE & rx, DATA_TYPE & messContent)
{
   sender = tx;
   rcvr = rx;
   content = messContent;
}

/*****************************************************************************/
/*!
 * @brief Copies from another message
 * @param newMessage  reference to the other messages
 */
/*****************************************************************************/
template<typename DATA_TYPE, typename ID_TYPE>
template<typename CMessages>
inline void TIGLApiMessage<DATA_TYPE, ID_TYPE>::setMessage(CMessages & newMessage)
{
   content = newMessage.getContent();
   rcvr = newMessage.getRcvr();
   sender = newMessage.getSender();
}

/*****************************************************************************/
/*!
 *@brief set the message content
 * @param newContent  the new message content
 */
/*****************************************************************************/
template<typename DATA_TYPE, typename ID_TYPE>
inline void TIGLApiMessage<DATA_TYPE, ID_TYPE>::setContent(DATA_TYPE & newContent)
{
   content = newContent;
}

/*****************************************************************************/
/*!
 * @brief Sums new content to the current message content
 * @param newContent  the new message content (to be summed with the current message value)
 */
/*****************************************************************************/
template<typename DATA_TYPE, typename ID_TYPE>
inline void TIGLApiMessage<DATA_TYPE, ID_TYPE>::sumContent(DATA_TYPE & newContent)
{
   content += newContent;
}

/*****************************************************************************/
/*!
 * @brief Copies a new message content if its value is bigger than the current message content
 * @param newMessage  reference to the other messages
 */
/*****************************************************************************/
template<typename DATA_TYPE, typename ID_TYPE>
template<typename CMessages>
inline void TIGLApiMessage<DATA_TYPE, ID_TYPE>::maxContent(CMessages & newMessage)
{
   DATA_TYPE newContent = newMessage.getContent();
   if (newContent > content)
   {
      content = newContent;
      sender = newMessage.getSender();
   }
}

/*****************************************************************************/
/*!
 *@brief copy a new message content if its value is smaller than the current message content
 *@param newMessage  reference to the other messages
 */
/*****************************************************************************/
template<typename DATA_TYPE, typename ID_TYPE>
template<typename CMessages>
inline void TIGLApiMessage<DATA_TYPE, ID_TYPE>::minContent(CMessages & newMessage)
{
   DATA_TYPE newContent = newMessage.getContent();
   if (newContent < content)
   {
      content = newContent;
      sender = newMessage.getSender();
   }
}

/*****************************************************************************/
/*!
 * @brief Packs the content of the message to a uint8_t buffer. It is organized as : rcvr, sender, content
 * @param buf  a uint8_t buffer that will have the packed message
 * @return the size of the packed message in bytes.
 *
 */
/*****************************************************************************/
template<typename DATA_TYPE, typename ID_TYPE>
inline uint_fast32_t TIGLApiMessage<DATA_TYPE, ID_TYPE>::pack(uint8_t *buf)
{
//		*(buf++) = 0; // data message flag
   memcpy(buf, &rcvr, sizeof(ID_TYPE));
   buf += sizeof(ID_TYPE);
   memcpy(buf, &sender, sizeof(ID_TYPE));
   buf += sizeof(ID_TYPE);
   memcpy(buf, &content, sizeof(DATA_TYPE));
   return 2 * sizeof(ID_TYPE) + sizeof(DATA_TYPE);
}

/*****************************************************************************/
/*!
 * @brief Unpacks a packed message to get the message
 * @param buf  a uint8_t buffer that has the packed message
 * @return the size of the packed message in bytes.
 */
/*****************************************************************************/
template<typename DATA_TYPE, typename ID_TYPE>
inline uint_fast32_t TIGLApiMessage<DATA_TYPE, ID_TYPE>::unpack(uint8_t *buf)
{
   memcpy(&rcvr, buf, sizeof(ID_TYPE));
   buf += sizeof(ID_TYPE);
   memcpy(&sender, buf, sizeof(ID_TYPE));
   buf += sizeof(ID_TYPE);
   memcpy(&content, buf, sizeof(DATA_TYPE));
   return 2 * sizeof(ID_TYPE) + sizeof(DATA_TYPE);
}

/*****************************************************************************/
/*!
 *@brief returns message size in bytes
 */
/*****************************************************************************/
template<typename DATA_TYPE, typename ID_TYPE>
inline uint_fast32_t TIGLApiMessage<DATA_TYPE, ID_TYPE>::sizeInBytes()
{
   return 2 * sizeof(ID_TYPE) + sizeof(DATA_TYPE);
}

/******************************************************************************
 *
 * GLOBAL DEFINITIONS
 *
 ******************************************************************************/
typedef TIGLApiMessage<double, uint_fast32_t> DblMessage;
#endif /* TIGLIMPLEMENTATIONMESSAGE_HPP_ */
