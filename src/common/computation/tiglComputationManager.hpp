#ifndef TIGLCOMPUTATIONMANAGER_HPP_
#define TIGLCOMPUTATIONMANAGER_HPP_

/*****************************************************************************/
/*!
 * @file tiglComputationManager.hpp
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/

/******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/
#include "tiglContainersAggregate.hpp"

/******************************************************************************
 *
 * TIGLComputationManager class
 *
 ******************************************************************************/

/*****************************************************************************/
/*!
 *   @ingroup comp_engine
 * @class  TIGLComputationManager
 * @brief Core Computation class for the %TIGL
 * @details It handles all the superstep processing including: vertex processing, message combining, aggregate information updating, and message distribution.
 * it also handles the initialization procedure for all vertices.
 * It is a template class that is parameterized by the following parameters
 * @param SUBGRAPH_CLASS 		the data manager class definition,
 * @param COMBINER_TYPE 				the data combiner class definition,
 * @param INIT_TYPE					the definition of the init parameters of the graph algorithm
 *
 * @ date October 2, 2014
 * @ author Texas Instruments
 *
 *
 * Revision History:\n
 * 05/1/2014: created for single device support \n
 * 08/20/2014: added basic MPI functionality \n
 * 10/02/2014: completed MPI support including system messaging, and message buffers \n
 */
/*****************************************************************************/
template<typename SUBGRAPH_CLASS, typename COMBINER_TYPE, typename INIT_TYPE>
class TIGLComputationManager
{
   /******************************************************************************
    *
    * type definitions
    *
    ******************************************************************************/
public:
   typedef typename SUBGRAPH_CLASS::ID_TYPE ID_TYPE;
   typedef typename SUBGRAPH_CLASS::EDGE_TYPE EDGE_TYPE;
   typedef typename SUBGRAPH_CLASS::VERTEX_TYPE VERTEX_TYPE;
   typedef typename SUBGRAPH_CLASS::MESSAGE_TYPE MESSAGE_TYPE;
   typedef typename SUBGRAPH_CLASS::VERTEX_ITERATOR VERTEX_ITERATOR;
   typedef typename SUBGRAPH_CLASS::MESSAGE_BUF_TYPE MESSAGE_BUF_TYPE;
   typedef typename SUBGRAPH_CLASS::VERTEX_LIST_PTR VERTEX_LIST_PTR;

   typedef typename vector<MESSAGE_BUF_TYPE>::iterator EXTERNAL_ITERATOR;
   typedef typename VERTEX_TYPE::ALG_DATA_TYPE ALG_DATA_TYPE;
   typedef TIGLCommunicationAggregateSysMessage<ALG_DATA_TYPE> AGGREGATE_MESSAGE_TYPE;

   /******************************************************************************
    *
    * constructor/destructor
    *
    ******************************************************************************/
public:
   TIGLComputationManager();
   ~TIGLComputationManager();

   /******************************************************************************
    *
    * attributes
    *
    ******************************************************************************/
protected:
   ///  holds the number of supersteps and the maximum number
   uint_fast32_t maxSS;
   ///  holds the ID of the active vertex list
   vector<uint_fast32_t> activeList;
   ///  number of active vertices on the current device
   uint_fast32_t numLocalActive;
   ///  if enabled: all vertices are active all the time (terminated by max count)
   bool allActive;
   ///  the aggregate information object
   TIGLContainersAggregate<ALG_DATA_TYPE> aggregateInfo;
   ///  pointer to the data manager object
   SUBGRAPH_CLASS *dataManager;
   ///  pointer to the data combiner object
   COMBINER_TYPE *combiner;
   ///  The number of idle system messages that are received
   uint_fast32_t numIdleSysMessages;
   ///  The number of halt system messages that are received
   uint_fast32_t numHaltSysMessages;
#ifdef	PROFILE_ENABLE
   double computeTime;
   double distributeTime;
   double combineTime;
   double searchTime;
   double initTime;
   double readTime;   //double insertTime;
   double ssTime;
   double preprocessTime;
#endif

   /******************************************************************************
    *
    * Protected Methods
    *
    ******************************************************************************/
protected:
   template<typename ALLOCATOR_TYPE>
   void distributeOutMessages(vector<MESSAGE_TYPE, ALLOCATOR_TYPE> *outMessages, uint_fast32_t countSS, int thread);
   void combineMessages(bool oddTimeFlag);
   void clearActiveBuffers();
   void InsertActive(uint_fast32_t *allPos, uint_fast16_t count);
   template<typename ALLOCATOR_TYPE>
   inline uint_fast32_t executeVertex(VERTEX_ITERATOR itVertex, uint_fast32_t pos, bool oddTimeFlag, vector<MESSAGE_TYPE, ALLOCATOR_TYPE> *outMessages);

   /******************************************************************************
    *
    * Public Methods
    *
    ******************************************************************************/
public:
   void reset();
   inline void setDataManager(SUBGRAPH_CLASS *dm);
   inline void setCombiner(COMBINER_TYPE *cmb);
   bool isInitialized();
   inline void setMaxSS(uint_fast32_t maxSSVal);
   void processSystemMessages(uint_fast32_t countSS);
   void init(INIT_TYPE *initParams);
   SS_STATUS superstep(uint_fast32_t countSS);
   inline uint_fast32_t getNumExternalMessages();
   inline uint_fast32_t getPastNumExternalMessages();
   inline uint_fast32_t numActive();
   uint_fast32_t getNumIdleSysMessages();
   void pushHaltMessage();
   uint_fast32_t getNumHaltSysMessages();
#ifdef	PROFILE_ENABLE
   /// prints the profiling information of the algorithm execution
   void printProfilingInfo();
#endif
};
#include "tiglComputationManagerImplementation.hpp"
#endif /* TIGLCOMPUTATIONMANAGER_HPP_ */
