#ifndef TIGLCOMPUTATIONMANAGERIMPLEMENTATION_HPP_
#define TIGLCOMPUTATIONMANAGERIMPLEMENTATION_HPP_

/*****************************************************************************/
/*!
 * @file tiglComputationManagerImplementation.hpp
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/

/******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/
#include <vector>
#include  <stdint.h>
#include "tiglUtilsMacros.h"
#include "tiglContainersSubgraph.hpp"
#include "tiglCommunicationManager.hpp"
#include "tiglCommunicationSysMessage.hpp"
#include "tiglUtilsOmpTools.h"
#ifdef CMEM_ENABLE
#include "tiglMemMsmcAllocator.hpp"
#include "tiglMemDdrAllocator.hpp"
#endif
#include "tiglComputationManager.hpp"

/******************************************************************************
 *
 * MACROS
 *
 ******************************************************************************/
#define OMP_CHUNK_SIZE        1024
#define MAX_OUT_MESSAGE_SIZE  128

/*****************************************************************************/
/*!
 * @brief Constructor
 * @details Resets counters and flags
 */
/*****************************************************************************/
template<typename SUBGRAPH_CLASS, typename COMBINER_TYPE, typename INIT_TYPE>
TIGLComputationManager<SUBGRAPH_CLASS, COMBINER_TYPE, INIT_TYPE>::TIGLComputationManager()
{
   combiner = 0;
   dataManager = 0;
   //reset();
   maxSS = DEFAULT_MAXIMUM_SS;
   allActive = false;

}

/*****************************************************************************/
/*!
 * @brief Destructor
 */
/*****************************************************************************/
template<typename SUBGRAPH_CLASS, typename COMBINER_TYPE, typename INIT_TYPE>
TIGLComputationManager<SUBGRAPH_CLASS, COMBINER_TYPE, INIT_TYPE>::~TIGLComputationManager()
{
}

/*****************************************************************************/
/*!
 * @brief Distributes messages after node execution
 * @ details It processes the output messages after each node processing
 * and route them to appropriate internal or external message buffer
 * @param outMessages a buffer of messeages after a single vertex execution
 * @param countSS the Superstep count
 * @param thread current omp thread
 *
 */
/*****************************************************************************/
template<typename SUBGRAPH_CLASS, typename COMBINER_TYPE, typename INIT_TYPE>
template<typename ALLOCATOR_TYPE>
void TIGLComputationManager<SUBGRAPH_CLASS, COMBINER_TYPE, INIT_TYPE>::distributeOutMessages(vector<MESSAGE_TYPE, ALLOCATOR_TYPE> *outMessages,
      uint_fast32_t countSS, int thread)
{
   typedef vector<MESSAGE_TYPE, ALLOCATOR_TYPE> LOCAL_MESSAGE_BUF;
   bool nextTimeFlag = (countSS + 1) % 2;   // buffer flag for next time

   /// \b Procedure
   /// 1. Iterate over all new messages.
   for (typename LOCAL_MESSAGE_BUF::iterator itm = outMessages->begin(); itm != outMessages->end(); ++itm)
   {   // iteration for processing output message
       // each message is put either in the local buffer or in the external message buffer
       /// 2. Get the ID of the destination vertex.
      ID_TYPE rcvr = itm->getRcvr();
      uint_fast32_t pos;
      if (dataManager->findPos(rcvr, pos) == false)
      {
         /// 3. If the destination is an external vertex, push the message to the external message buffer.
         dataManager->pushExternalDataMessage(*itm, thread);
      }
      else
      {   // local message -> route directly to the input message buffer of the following time
          /// 4. Else push the message to the local message buffer of the destination vertex.
         dataManager->pushDataMessage(*itm, pos, nextTimeFlag, thread, combiner);
      }
   }   // end of the loop of the output messages of the current node
}

/*****************************************************************************/
/*!
 * @brief Combines all the messages
 * @details This is called at the beginning of the superstep to combine all local and external messages for each vertex
 * Each omp thread has its own message buffer, while external messages has its own buffer. They are all combined to thread 0 buffer.
 * The function also sets all the local active nodes (depending on their messages).
 * @param oddTimeFlag  flag for the ping-poing buffer: true for odd superstep, false for even superstep
 *
 */
/*****************************************************************************/
template<typename SUBGRAPH_CLASS, typename COMBINER_TYPE, typename INIT_TYPE>
void TIGLComputationManager<SUBGRAPH_CLASS, COMBINER_TYPE, INIT_TYPE>::combineMessages(bool oddTimeFlag)
{
   // combining local messages
   vector<MESSAGE_BUF_TYPE> *localMessagePtr = dataManager->getDataMessageBuf(oddTimeFlag, 0);

   uint_fast32_t loopCount;
   uint_fast32_t messSize = localMessagePtr->size();

   // Emptying the active list
   numLocalActive = 0;

   VERTEX_ITERATOR itBegin = dataManager->begin();
   bool noMessageActive = itBegin->isActiveWithNoMessage();
   bool reactivateFlag = itBegin->canReactivate();

#define		MAX_THREAD_MESSAGE_SIZE		1024

   uint_fast32_t threadActiveList[NUM_OMP_THREADS][MAX_THREAD_MESSAGE_SIZE];
   uint_fast16_t threadActiveCount[NUM_OMP_THREADS];
   for (int i = 0; i < NUM_OMP_THREADS; i++)
   {
      threadActiveCount[i] = 0;
   }

   /// \b Procedure
   /// 1. Iterate over all local vertices (with multiple threads)
   OMP("omp parallel for num_threads(NUM_OMP_THREADS) schedule(dynamic, OMP_CHUNK_SIZE)")
   for (loopCount = 0; loopCount < messSize; loopCount++)
   {
      VERTEX_ITERATOR it = itBegin + loopCount;
      int thread = omp_get_thread_num();

      /// 2. If the vertex is not active and cannot be reactivated, move to the next vertex.
      bool activeFlag = it->isActive();
      if (activeFlag == false && reactivateFlag == false)
      {   // if the node is not active discard the input messages
         dataManager->setAllNumMessages(loopCount, 0, oddTimeFlag);
         continue;
      }
      // aggregating messages from the four threadCount to first one
      typename vector<MESSAGE_BUF_TYPE>::iterator itv;
      uint_fast32_t totalMessages = 0;

      typename vector<MESSAGE_BUF_TYPE>::iterator ito = localMessagePtr[0].begin() + loopCount;
      int numAllocThreads = dataManager->getNumAllocThreads();
      /// 3. Iterate over all message buffers for the current vertex (which equals the number of omp threads + 1 (if MPI is enabled) ).
      if (numAllocThreads > 1)
      {
         for (int threadCount = 0; threadCount < numAllocThreads; threadCount++)
         {
            /// 4. Combine messages in the current message buffer of the current vertex.
            uint_fast32_t numLocalMessages = dataManager->getNumMessages(loopCount, oddTimeFlag, threadCount);
            if (numLocalMessages == 0)
               continue;
            else
            {
               itv = localMessagePtr[threadCount].begin() + loopCount;
               if (ito->size() <= totalMessages && threadCount > 0)
               {
                  uint_fast32_t tempCombinedMessages = combiner->combineMessages(itv->begin(), numLocalMessages, itv->begin());
                  /// 5. Add the combined messages to the end of the message buffer 0 for the current vertex.
                  ito->insert(ito->end(), itv->begin(), itv->begin() + tempCombinedMessages);
                  totalMessages += tempCombinedMessages;
               }
               else
               {
                  totalMessages += combiner->combineMessages(itv->begin(), numLocalMessages, ito->begin() + totalMessages);
               }
               /// 6. Clear the current message buffer (if it is not buffer 0).
            }
            if (threadCount > 0)
               dataManager->setNumMessages(loopCount, 0, oddTimeFlag, threadCount);   // resetting of all buffer
         }
      }
      else
      {
         totalMessages = dataManager->getNumMessages(loopCount, oddTimeFlag, 0);
      }

      /// 7. Combine all messages at message buffer 0 for the current vertex.
      if (totalMessages > 0)
      {
         uint_fast32_t numCombinedMessages;
         if (totalMessages > 1)
         {
            itv = localMessagePtr[0].begin() + loopCount;
            numCombinedMessages = combiner->combineMessages(itv->begin(), totalMessages, itv->begin());
         }
         else
            numCombinedMessages = 1;

         /// 8. If the current vertex has messages, add to active vertex list.
         dataManager->setNumMessages(loopCount, numCombinedMessages, oddTimeFlag, 0);   // resetting of all buffer
         threadActiveList[thread][threadActiveCount[thread]] = loopCount;
         threadActiveCount[thread]++;
         if (threadActiveCount[thread] == MAX_THREAD_MESSAGE_SIZE)
         {
            InsertActive(threadActiveList[thread], MAX_THREAD_MESSAGE_SIZE);
            threadActiveCount[thread] = 0;
         }
         if (activeFlag == false)
            it->activate();
      }
      else
      {
         dataManager->setNumMessages(loopCount, 0, oddTimeFlag, 0);   // resetting of all buffer
         if (it->isActive() && noMessageActive == true)
         {
            threadActiveList[thread][threadActiveCount[thread]] = loopCount;
            threadActiveCount[thread]++;
            if (threadActiveCount[thread] == MAX_THREAD_MESSAGE_SIZE)
            {
               InsertActive(threadActiveList[thread], MAX_THREAD_MESSAGE_SIZE);
               threadActiveCount[thread] = 0;
            }
         }
      }
   }
   for (int i = 0; i < NUM_OMP_THREADS; i++)
      if (threadActiveCount[i])
      {
         InsertActive(threadActiveList[i], threadActiveCount[i]);
      }

#ifdef	DEBUG_ENABLE
   cout << "combiner loop ends !! " << endl;
#endif
}

/*****************************************************************************/
/*!
 * @brief Clears the active buffer (this is called for each new superstep)
 *
 */
/*****************************************************************************/
template<typename SUBGRAPH_CLASS, typename COMBINER_TYPE, typename INIT_TYPE>
void TIGLComputationManager<SUBGRAPH_CLASS, COMBINER_TYPE, INIT_TYPE>::clearActiveBuffers()
{
   activeList.clear();
   allActive = false;
}

/*****************************************************************************/
/*!
 * @brief Inserts new vertices in the local active list
 * @param allPos pointer to the buffer that contains the new vertices
 * @param count number of vertices in the input buffer
 *
 */
/*****************************************************************************/
template<typename SUBGRAPH_CLASS, typename COMBINER_TYPE, typename INIT_TYPE>
void TIGLComputationManager<SUBGRAPH_CLASS, COMBINER_TYPE, INIT_TYPE>::InsertActive(uint_fast32_t *allPos, uint_fast16_t count)
{
   OMP("omp critical(INSERT_ACTIVE)")
   {
      if (activeList.size() > numLocalActive + count)
         std::copy(allPos, allPos + count, activeList.begin() + numLocalActive);   //activeList[numLocalActive] = pos;
      else
         activeList.insert(activeList.begin() + numLocalActive, allPos, allPos + count);   //	activeList.push_back(pos);

      numLocalActive += count;
   }
}

/*****************************************************************************/
/*!
 * @brief Executes a single vertex during a super step
 * @details It reads the input messages and calls the compute function of the vertex
 * @param itVertex iterator to the current vertex in the vertex list
 * @param pos position of the vartex (used for messages retrieval)
 * @param oddTimeFlag  flag for the ping-poing buffer: true for odd SS, false for even S
 * @param  outMessages  buffers for the output messages after vertex execution
 *
 * @return the number of output messages
 *
 */
/*****************************************************************************/
template<typename SUBGRAPH_CLASS, typename COMBINER_TYPE, typename INIT_TYPE>
template<typename ALLOCATOR_TYPE>
inline uint_fast32_t TIGLComputationManager<SUBGRAPH_CLASS, COMBINER_TYPE, INIT_TYPE>::executeVertex(VERTEX_ITERATOR itVertex, uint_fast32_t pos,
      bool oddTimeFlag, vector<MESSAGE_TYPE, ALLOCATOR_TYPE> *outMessages)
{
   MESSAGE_BUF_TYPE *messagePtr;
   uint_fast32_t currNumOutMessages;

   /// \b Procedure
   /// 1. Get message handler.
   messagePtr = dataManager->getIndexedMessageBuf(pos, oddTimeFlag, 0);
   /// 2. Call the customized @link TIGLApiVertex::compute compute@endlink function for the graph algorithm
   currNumOutMessages = itVertex->compute(messagePtr, outMessages, dataManager->getNumMessages(pos, oddTimeFlag, 0));
   /// 3. Clear input message buffer
   dataManager->clearNumMessages(pos, oddTimeFlag, 0);	//

   return currNumOutMessages;
}

/*****************************************************************************/
/*!
 * @brief Resets the compute engine to return to the state before algorithm execution
 */
/*****************************************************************************/
template<typename SUBGRAPH_CLASS, typename COMBINER_TYPE, typename INIT_TYPE>
void TIGLComputationManager<SUBGRAPH_CLASS, COMBINER_TYPE, INIT_TYPE>::reset()
{
   if (dataManager)
      dataManager->reset();
   if (activeList)
      clearActiveBuffers();
}

/*****************************************************************************/
/*!
 * @brief Sets the data manager pointer
 * @details This function has to be called before running graph algorithms
 *
 * @param dm pointer to the data manager object
 *
 */
/*****************************************************************************/
template<typename SUBGRAPH_CLASS, typename COMBINER_TYPE, typename INIT_TYPE>
inline void TIGLComputationManager<SUBGRAPH_CLASS, COMBINER_TYPE, INIT_TYPE>::setDataManager(SUBGRAPH_CLASS *dm)
{
   dataManager = dm;
   dataManager->begin()->setAggPtr(&aggregateInfo);
}

/*****************************************************************************/
/*!
 * @brief Sets the combiner pointer
 * @details This function has to be called before running graph algorithms
 *  @param cmb pointer to the data combiner object
 *
 */
/*****************************************************************************/
template<typename SUBGRAPH_CLASS, typename COMBINER_TYPE, typename INIT_TYPE>
inline void TIGLComputationManager<SUBGRAPH_CLASS, COMBINER_TYPE, INIT_TYPE>::setCombiner(COMBINER_TYPE *cmb)
{
   combiner = cmb;
}

/*****************************************************************************/
/*!
 * @brief Checks if the object is properly initialized
 * @return true if initialized, false otherwise
 *
 */
/*****************************************************************************/
template<typename SUBGRAPH_CLASS, typename COMBINER_TYPE, typename INIT_TYPE>
bool TIGLComputationManager<SUBGRAPH_CLASS, COMBINER_TYPE, INIT_TYPE>::isInitialized()
{
   if (dataManager == 0)
      return false;

   if (combiner == 0)
      return false;

   return true;
}

/*****************************************************************************/
/*!
 * @brief Sets the maximum number of supersteps
 * @param maxSSVal maximum possible number of supersteps
 *
 */
/*****************************************************************************/
template<typename SUBGRAPH_CLASS, typename COMBINER_TYPE, typename INIT_TYPE>
inline void TIGLComputationManager<SUBGRAPH_CLASS, COMBINER_TYPE, INIT_TYPE>::setMaxSS(uint_fast32_t maxSSVal)
{
   maxSS = maxSSVal;
}

/*****************************************************************************/
/*!
 * @brief Processes all system messages.
 * @details This function is executed at each superstep before processing data messages.
 * @param countSS the current SS index.
 */
/*****************************************************************************/
template<typename SUBGRAPH_CLASS, typename COMBINER_TYPE, typename INIT_TYPE>
void TIGLComputationManager<SUBGRAPH_CLASS, COMBINER_TYPE, INIT_TYPE>::processSystemMessages(uint_fast32_t countSS)
{
   numIdleSysMessages = 0;
   numHaltSysMessages = 0;
   vector<TIGLCommunicationSysMessage> *pActiveMessages = dataManager->getInSysMessageBuf(countSS % 2);

   if (pActiveMessages->empty() == true)
      return;

   for (vector<TIGLCommunicationSysMessage>::iterator it = pActiveMessages->begin(); it != pActiveMessages->end(); ++it)
   {
      switch (it->getMessageCode()) {
         case ADD_VERTEX_INFO:
            cout << "!!!!!!! warning ADD_VERTEX_INFO message not supported" << endl << endl;
            break;

         case UPDATE_VERTEX_INFO:
            cout << "!!!!!!! warning UPDATE_VERTEX_INFO message not supported" << endl << endl;
            break;

         case REMOVE_VERTEX_INFO:
            cout << "!!!!!!! warning REMOVE_VERTEX_INFO message not supported" << endl << endl;
            break;

            // TODO : define recovery procedure for these errors
         case VERTEX_NOT_EXIST:
            cout << "Warning in PregelCommunicator::processSystemMessages: VERTEX_NOT_EXIST proc. not defined " << endl << endl;
            break;

//			case ERR_RCVR_NOT_EXIST:
//				cout << "Warning in PregelCommunicator::processSystemMessages: ERR_RCVR_NOT_EXIST proc. not defined "<< endl << endl;
//				break;

         case GET_VERTEX_POS:
            cout << "Warning in PregelCommunicator::processSystemMessages: GET_VERTEX_POS proc. not defined " << endl << endl;
            break;

         case INFO_VERTEX_POS:
            cout << "Warning in PregelCommunicator::processSystemMessages: INFO_VERTEX_POS proc. not defined " << endl << endl;
            break;

         case AGGREGATE_INFO:
            AGGREGATE_MESSAGE_TYPE aggMessage;
//				cout << "++++++ reading aggregate_info " << endl;
            //cout << "aggregate info message" << endl;
            if (it->readMessageBody(&aggMessage) == false)
            {
               cout << "!!!!!!! ERROR reading AGGREGATE_INFO message" << endl;
               continue;
            }
            aggregateInfo.externalAggregateUpdate(&aggMessage);
            break;
         case DEV_IDLE:
            numIdleSysMessages++;
            break;

         case HALT:
            numHaltSysMessages++;
            break;

         case UNDEFINED_MESSAGE:
            cout << "Warning in PregelCommunicator::processSystemMessages: UNDEFINED_MESSAGE proc. not defined " << endl << endl;
            break;
      }
   }
   dataManager->clearInSysMessage(countSS % 2);
}

/*****************************************************************************/
/*!
 * @brief Initialization procedure
 * @details It is called once for each algorithm before executing the supersteps
 * and it calls the customized @link TIGLApiVertex::init init@endlink function for all vertices on the device.
 * @param initParams pointer to the initialization parameter of the graph algorithm
 */
/*****************************************************************************/
template<typename SUBGRAPH_CLASS, typename COMBINER_TYPE, typename INIT_TYPE>
void TIGLComputationManager<SUBGRAPH_CLASS, COMBINER_TYPE, INIT_TYPE>::init(INIT_TYPE *initParams)
{
   // reseting message buffers
#ifdef	PROFILE_ENABLE
   tiglUtilsRegisterTime();
#endif
   /// \b Procedure
   /// 1. Clear active buffers.
   clearActiveBuffers();

   /// 2. Clear external message buffers.
#if defined(_MPI)
   dataManager->clearAllExternalMessageBuf();
#endif

#ifdef	PROFILE_ENABLE
   computeTime = 0;
   distributeTime = 0;
   combineTime = 0;
   searchTime = 0;
   initTime = 0;
   readTime = 0;
   ssTime = 0;
   preprocessTime = 0;
#endif

   // running the initialization procedure for all vertices

   uint_fast32_t loopCount;
   uint_fast32_t dataSize = dataManager->size();
   VERTEX_ITERATOR itBegin = dataManager->begin();

#ifdef	DEBUG_ENABLE
   cout << "init loop starts !!" << endl;
#endif
   vector<MESSAGE_TYPE> currOutMessages;   //LOCAL_MESSAGE_BUF currOutMessages; // initialized at each iteration
   aggregateInfo.localAggregateInit(0, dataSize);

   /// 3. Iterate over all local vertices.
   OMP("omp parallel for private(currOutMessages) num_threads(NUM_OMP_THREADS) schedule(dynamic, OMP_CHUNK_SIZE)")
   for (loopCount = 0; loopCount < dataSize; loopCount++)   //for(it = dataManager->begin(); it != dataManager->end(); ++it)
   {   // main computation loop
      VERTEX_ITERATOR it = itBegin + loopCount;

      currOutMessages.clear();
      /// 4. For each vertex, call the customized @link TIGLApiVertex::init init@endlink function for the graph algorithm.
      uint_fast32_t currNumOutMessages = it->init(initParams, &currOutMessages);

      /// 5. Distribute the output messages to local and external message buffers by calling @link TIGLComputationManager::distributeOutMessages distributeOutMessages@endlink.
      if (currNumOutMessages)
      {
         distributeOutMessages(&currOutMessages, 0, omp_get_thread_num());
      }

      /// 6. update local aggregate information.
      aggregateInfo.localAggregateUpdate(it, currNumOutMessages);
   }
   /// 7. end of vertex iteration.
#ifdef	DEBUG_ENABLE
   cout << "init loop done !!" << endl;
#endif
   /// 8. Update global aggregate information.
   uint_fast32_t numExternalMessages = dataManager->getTotalExternalMessageCount();
   if (numExternalMessages)
   {
      aggregateInfo.incrementExternalMessagesCount(numExternalMessages);
   }

#ifdef	DEBUG_ENABLE
   cout << "local combining done !!" << endl;
#endif

   /// 9. Prepare aggregate information system messages and push to external message buffer.
#if defined(_MPI)
   dataManager->flushExternalDataMessage();

   AGGREGATE_MESSAGE_TYPE aggMessage;
   aggregateInfo.prepareAggregateSysMessage(&aggMessage);

   TIGLCommunicationSysMessage aggSysMessage(AGGREGATE_INFO, 0, &aggMessage);   // device field is left empty because it is broadcasted
   dataManager->pushSysMessage(aggSysMessage);
#endif

#ifdef	PROFILE_ENABLE
   initTime = tiglUtilsRegisterTime();
#endif
   /// 10. If all nodes are not active, send a system IDLE message
#if defined(_MPI)
   if(numLocalActive == 0 || aggregateInfo.getNumTotalMessages() == 0)
   {
      TIGLCommunicationSysMessage haltMessage(DEV_IDLE, 0);
      dataManager->pushSysMessage(haltMessage);
   }
#endif
}

/*****************************************************************************/
/*!
 * @brief Core superstep computation procedure
 * @details It processes both system and data messages and route the output message to the appropriate buffers
 * it also call the (algorithm-dependent) combiner function after processing all input messages
 *
 * @param countSS the current SS index
 @return The execution status, possible values are: 	SS_SUCCESS, 	NOT_INITIALIZED, VERTEX_NOT_FOUND, 	ALGORITHM_COMPLETED, 	MAX_SS_REACHED,
 *
 */
/*****************************************************************************/
template<typename SUBGRAPH_CLASS, typename COMBINER_TYPE, typename INIT_TYPE>
SS_STATUS TIGLComputationManager<SUBGRAPH_CLASS, COMBINER_TYPE, INIT_TYPE>::superstep(uint_fast32_t countSS)
{

#ifdef	PROFILE_ENABLE
   double localComputeTime = 0;
   double localDistributeTime = 0;
   double localSearchTime = 0;
   double localReadTime = 0;
   double startTime = tiglUtilsGetTimeUs();
#endif

   if (countSS >= maxSS)
      return MAX_SS_REACHED;
   if (isInitialized() == false)
      return NOT_INITIALIZED;

   bool oddTimeFlag = countSS % 2 ? true : false;

#ifdef	PROFILE_ENABLE
   tiglUtilsRegisterTime();
#endif
#if defined(_MPI)
   dataManager->clearAllExternalMessageBuf();
#endif
   /// \b Procedure
   /// 1. Combine all local and external message for each vertx using @link TIGLComputationManager::combineMessages combineMessages @endlink
   combineMessages(oddTimeFlag);

#ifdef	PROFILE_ENABLE
   combineTime += (double)tiglUtilsRegisterTime();
#endif

   /// 2. Reset aggregate information
   aggregateInfo.localAggregateInit(countSS, numLocalActive);

   /// 3. Process system messages
   processSystemMessages(countSS);

   // This is the main processing loop that needs to be parallalized in the openMP implemenation
   SS_STATUS status = SS_SUCCESS;

   uint_fast32_t numLocalVertices = dataManager->size();

   uint_fast32_t loopCount;
   vector<MESSAGE_TYPE> currOutMessages;   //vector <MESSAGE_TYPE, TIGLMemMsmcAllocator<MESSAGE_TYPE> > currOutMessages; // initialized at each iteration
#ifdef	DEBUG_ENABLE
   cout << "processing loop begins " << endl;
#endif

   /// 4. Iterate over all active nodes (mutlithread execution); execute the following steps for each active vertex
#ifdef	PROFILE_ENABLE
   preprocessTime += tiglUtilsGetTimeUs() - startTime;
#endif
   if (numLocalActive)
   {
#ifdef	PROFILE_ENABLE
      OMP("omp parallel for num_threads(NUM_OMP_THREADS) private(currOutMessages) schedule(dynamic, OMP_CHUNK_SIZE)")   // reduction(+:localComputeTime, localDistributeTime, localSearchTime, localReadTime)
#else
      OMP("omp parallel for num_threads(NUM_OMP_THREADS) private(currOutMessages) schedule(dynamic, OMP_CHUNK_SIZE)")
#endif // of PROFILE_ENABLE
      for (loopCount = 0; loopCount < numLocalActive; loopCount++)
      {
         int thread = omp_get_thread_num();
#ifdef	PROFILE_ENABLE
         if(thread == 0)
         tiglUtilsRegisterTime();
#endif

         /// 5. Retrieve the vertex position
         uint_fast32_t pos = activeList[loopCount];   //localActiveList[loopCount-bufStart];// *(activeBegin+loopCount);//;//ID_TYPE id;
         VERTEX_ITERATOR it = dataManager->getIndexedVertex(pos);

#ifdef	PROFILE_ENABLE
         if(thread == 0)
         localReadTime += tiglUtilsRegisterTime();
#endif
         if (pos >= numLocalVertices)
         {   // error takes place
            status = VERTEX_NOT_FOUND;
            continue;
         }

#ifdef	PROFILE_ENABLE
         if(thread == 0)
         tiglUtilsRegisterTime();
#endif

         /// 6. Execute the vertex (using @link executeVertex executeVertex@endlink).
         uint_fast32_t currNumOutMessages = executeVertex(it, pos, oddTimeFlag, &currOutMessages);

         /// 7. Update the aggregate information for the current vertex.
         aggregateInfo.localAggregateUpdate(it, currNumOutMessages);

         // could be handleD more efficiently
#ifdef	PROFILE_ENABLE
         if(thread == 0)
         localComputeTime += (double)tiglUtilsRegisterTime();
#endif

         /// 8. Distribute the output messages from executing the current vertex to appropriate message buffers using @link distributeOutMessages distributeOutMessages@endlink
         if (currOutMessages.size() >= MAX_OUT_MESSAGE_SIZE || loopCount % OMP_CHUNK_SIZE == OMP_CHUNK_SIZE - 1 || loopCount == numLocalActive - 1)   //(currNumOutMessages)
         {
            distributeOutMessages(&currOutMessages, countSS, thread);
            currOutMessages.clear();
         }

#ifdef	PROFILE_ENABLE
         if(thread == 0)
         localDistributeTime += tiglUtilsRegisterTime();
#endif

      }   // end of the big loop over active nodes

      /// 9. End of the active vertex loop

      /// 10. Update global aggregate information, and prepare system aggregate message.
#if defined(_MPI)
      uint_fast32_t numExternalMessages = dataManager->getTotalExternalMessageCount();
      if(numExternalMessages)
      {
         aggregateInfo.incrementExternalMessagesCount(numExternalMessages);
      }

      dataManager->flushExternalDataMessage();

      AGGREGATE_MESSAGE_TYPE aggMessage;
      aggregateInfo.prepareAggregateSysMessage(&aggMessage);

      TIGLCommunicationSysMessage aggSysMessage(AGGREGATE_INFO, 0, &aggMessage);   // device field is left empty because it is broadcasted
      dataManager->pushSysMessage(aggSysMessage);
#endif
   }   // of if(localActive)

   /// 11. If there is no active nodes, send system IDLE message to all devices.
   if (numLocalActive == 0 || aggregateInfo.getNumTotalMessages() == 0)
   {
#if defined(_MPI)
      TIGLCommunicationSysMessage idleMessage(DEV_IDLE, 0);
      dataManager->pushSysMessage(idleMessage);
#endif
      status = ALGORITHM_COMPLETED;
   }
#ifdef	PROFILE_ENABLE
   computeTime += localComputeTime;
   distributeTime += localDistributeTime;
   searchTime += localSearchTime;
   //insertTime += localInsertTime;
   readTime += localReadTime;
   ssTime += tiglUtilsGetTimeUs() - startTime;
#endif

   return status;
}

/*****************************************************************************/
/*!
 * @brief Returns the number of output message
 *
 */
/*****************************************************************************/
template<typename SUBGRAPH_CLASS, typename COMBINER_TYPE, typename INIT_TYPE>
inline uint_fast32_t TIGLComputationManager<SUBGRAPH_CLASS, COMBINER_TYPE, INIT_TYPE>::getNumExternalMessages()
{
   return aggregateInfo.getNumExternalMessages();
}

/*****************************************************************************/
/*!
 * @brief Returns the number of output message in the previous superstep
 *
 */
/*****************************************************************************/
template<typename SUBGRAPH_CLASS, typename COMBINER_TYPE, typename INIT_TYPE>
inline uint_fast32_t TIGLComputationManager<SUBGRAPH_CLASS, COMBINER_TYPE, INIT_TYPE>::getPastNumExternalMessages()
{
   return aggregateInfo.getPastNumExternalMessages();
}

/*****************************************************************************/
/*!
 * @brief  Returns the number of active nodes for the device
 */
/*****************************************************************************/
template<typename SUBGRAPH_CLASS, typename COMBINER_TYPE, typename INIT_TYPE>
inline uint_fast32_t TIGLComputationManager<SUBGRAPH_CLASS, COMBINER_TYPE, INIT_TYPE>::numActive()
{
   return numLocalActive;   //activeList.size();//oddTimeFlag?oddActiveList.size():activeList.size();
}

/*****************************************************************************/
/*!
 * @brief  Returns the number of received idle system messages
 *
 */
/*****************************************************************************/
template<typename SUBGRAPH_CLASS, typename COMBINER_TYPE, typename INIT_TYPE>
inline uint_fast32_t TIGLComputationManager<SUBGRAPH_CLASS, COMBINER_TYPE, INIT_TYPE>::getNumIdleSysMessages()
{
   return numIdleSysMessages;
}

/*****************************************************************************/
/*!
 * @brief  Pushes a halt message into the sys message buffer
 *
 */
/*****************************************************************************/
template<typename SUBGRAPH_CLASS, typename COMBINER_TYPE, typename INIT_TYPE>
void TIGLComputationManager<SUBGRAPH_CLASS, COMBINER_TYPE, INIT_TYPE>::pushHaltMessage()
{
   TIGLCommunicationSysMessage haltMessage(HALT, 0);
   dataManager->pushSysMessage(haltMessage);
}

/*****************************************************************************/
/*!
 * @brief  Returns the number of received halt system messages
 *
 */
/*****************************************************************************/
template<typename SUBGRAPH_CLASS, typename COMBINER_TYPE, typename INIT_TYPE>
inline uint_fast32_t TIGLComputationManager<SUBGRAPH_CLASS, COMBINER_TYPE, INIT_TYPE>::getNumHaltSysMessages()
{
   return numHaltSysMessages;
}

#ifdef	PROFILE_ENABLE
/*****************************************************************************/
/*!
 @brief Prints the profiling information of the algorithm execution
 */
/*****************************************************************************/
template <typename SUBGRAPH_CLASS, typename COMBINER_TYPE, typename INIT_TYPE>
void TIGLComputationManager<SUBGRAPH_CLASS, COMBINER_TYPE, INIT_TYPE>::
printProfilingInfo()
{
   cout<< "compTime(ms) = " << computeTime/1e6 <<
   ", readTime = " << readTime/1e6 <<
   ", distTime = " << distributeTime/1e6 <<
   ", combTime = " << combineTime/1e6 <<
   ", searchTime = " << searchTime/1e6 <<
   ", initTime = " << initTime/1e6 <<
   ", preprocessTime = " << preprocessTime/1e3 <<
   ", ssTime = " << ssTime/1e3 <<endl;

   double allTimes = (double)computeTime/1e6 + (double)readTime/1e6 +
   (double)distributeTime/1e6 + (double)combineTime/1e6 +
   (double)searchTime/1e6 + (double)initTime/1e6 + preprocessTime/1e3;
   cout << "allTimes = " << allTimes << endl;
   cout.precision(3);
   cout<< "compTime = " << (double)computeTime/allTimes/1e6 <<
   ", readTime = " << (double)readTime/allTimes/1e6 <<
   ", distTime = " << (double)distributeTime/allTimes/1e6 <<
   ", combTime = " << (double)combineTime/allTimes/1e6 <<
   ", searchTime = " << (double)searchTime/allTimes/1e6 <<
   ", initTime = " << (double)initTime/allTimes/1e6 <<
   ", preprocessTime = " << (double)preprocessTime/allTimes/1e6 << endl;

}
#endif

#endif /* TIGLCOMPUTATIONMANAGER_IMPLEMENTATION_HPP_ */
