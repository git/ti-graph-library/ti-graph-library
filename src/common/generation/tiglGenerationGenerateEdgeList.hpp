#ifndef TIGLGENERATIONGENERATEEDGELIST_HPP
#define TIGLGENERATIONGENERATEEDGELIST_HPP

/*****************************************************************************/
/*!
 * @file tiglGenerationGenerateEdgeList.hpp
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/

/******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/
#include <vector>
#include <algorithm>

/******************************************************************************
 *
 * customized sort function
 *
 ******************************************************************************/

template<typename T>
bool sortEdgeFnc(T x, T y)
{
   return x < y;
}

/*****************************************************************************/
/*!
 * @ingroup graph_gen
 * @brief Generating edge list from vertices list
 */
/*****************************************************************************/
template<typename VertexClass>
void tiglGenerationGenerateEdgeList(vector<VertexClass> & verticesList, vector<typename VertexClass::EDGE_TYPE> & edgeList)
{
   typedef typename VertexClass::EDGE_TYPE EDGE_TYPE;
   typedef typename VertexClass::OUT_EDGE_ITERATOR OUT_EDGE_ITERATOR;

   OUT_EDGE_ITERATOR edgePtr;
   typename vector<VertexClass>::iterator it;

   if (edgeList.empty() == false)
   {
      cout << "Warning ! edge list is not empty " << endl;
   }

   /// \b Procedure
   /// 1. Reading the output edges from each vertex and pushing it to the edge list
   for (it = verticesList.begin(); it != verticesList.end(); ++it)
   {
      for (edgePtr = it->getBeginOutEdgeIterator(); edgePtr != it->getEndOutEdgeIterator(); ++edgePtr)
         edgeList.push_back(*edgePtr);
   }

   /// 2. sorting the the edges based on the ID
   sort(edgeList.begin(), edgeList.end(), sortEdgeFnc<EDGE_TYPE>);

}

#endif
