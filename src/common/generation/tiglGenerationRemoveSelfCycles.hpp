#ifndef TIGLGENERATIONREMOVESELFCYCLES_HPP_
#define TIGLGENERATIONREMOVESELFCYCLES_HPP_

/*****************************************************************************/
/*!
 * @file tiglGenerationRemoveSelfCycles.hpp
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/

/******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/

#include <vector>

/*****************************************************************************/
/*!
 * @ingroup graph_gen
 * @brief Removes from the graph all length-1 cycles
 * @details Removes all edges that results in cycles of length 1 (i.e., source and target nodes are the same)
 * This is in-place procedure, 
 */
/*****************************************************************************/
template<typename CVertex>
void tiglGenerationRemoveSelfCycles(vector<CVertex> & verticesList)
{
   typedef typename CVertex::ID_TYPE ID_TYPE;

   typename vector<CVertex>::iterator itv = verticesList.begin();
   bool isBivertex = itv->isBiVertex();

   for (; itv != verticesList.end(); ++itv) /*itv already initialized*/
   {
      ID_TYPE vertexId = itv->getId();

      for (typename CVertex::OUT_EDGE_ITERATOR ite = itv->getBeginOutEdgeIterator(); ite != itv->getEndOutEdgeIterator(); ++ite)
      { /*loop for checking the target*/
         if (ite->getNbr() == vertexId)
         {
            if (isBivertex) /*removing link to the input*/
               itv->removeInEdge(vertexId);

            itv->removeEdge(vertexId);/*removing the edge itself from output edges*/

            break; /* no more search at the current vertex is needed if a self-cycle is found*/
         }
      }
   }
}
#endif /* TIGLGENERATIONREMOVESELFCYCLES_HPP_ */
