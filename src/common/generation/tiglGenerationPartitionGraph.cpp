/*****************************************************************************/
/*!
 * @file tiglGenerationPartitionGraph.cpp
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/

/******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/
#include <fstream>
#include <iostream>
#include  <stdint.h>
#include <inttypes.h>
#include "tiglUtils.hpp"

/*****************************************************************************/
/*!
 * @ingroup graph_gen
 * @brief Partitions a graph to subgraphs with equal number of vertices
 * @details The original graph is stored in a file, and the partitioned filenames are derived from the original file name.
 * If the original file is name is <name>.<ext>, the partititoned file names are: <name>_0.<ext>, <name>_1.<ext>, ...
 * @param fileName file name of the file that contains the adjacency list of the graph
 * @param numPartitions number of partitions
 * @return true if success, false otherwise
 */
bool tiglGenerationPartitionGraph(char *fileName, uint_fast32_t numPartitions)
{
   ifstream file(fileName);
   string line;

   if (!file)
   {
      cout << "Error: cannot open " << fileName << " to read !" << endl << endl;
      return false;
   }

   uint_fast32_t numVertices = 0;
   while (getline(file, line))
   {
      numVertices++;
   }
   if (numVertices < numPartitions)
   {
      file.close();
      return false;
   }

   uint_fast32_t numLinesPerPartition = numVertices / numPartitions;
   printf("*** numLinesPerPartition = %ld\n", (unsigned long)numLinesPerPartition);

   string inputFileName(fileName);
   uint_fast32_t off = inputFileName.find_last_of(".");
   string str1 = inputFileName.substr(0, off);
   string str2 = inputFileName.substr(off + 1);

   uint_fast32_t part = 0;

   char partFileName[1000];
   sprintf(partFileName, "%s_%ld_0.%s", str1.c_str(), (unsigned long)numPartitions, str2.c_str());

   ofstream pfile(partFileName);

   uint_fast32_t numLocalLines = 0;
   uint_fast32_t numLines = 0;

   // returning to the starting point
   file.close();
   file.open(fileName);
   while (getline(file, line))
   {
      if (numLocalLines == numLinesPerPartition)
      {
         part++;
         pfile.close();
         numLocalLines = 0;
         sprintf(partFileName, "%s_%ld_%ld.%s", str1.c_str(), (unsigned long)numPartitions, (unsigned long)part, str2.c_str());
         printf("### part %ld, numLines %ld, file %s\n", (unsigned long)part, (unsigned long)numLines, partFileName);
         pfile.open(partFileName);
      }
      pfile << line << endl;
      numLocalLines++;
      numLines++;
   }
   file.close();

   return true;
}

