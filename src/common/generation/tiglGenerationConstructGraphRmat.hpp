#ifndef TIGLGENERATIONCONSTRUCTGRAPHRMAT_HPP
#define TIGLGENERATIONCONSTRUCTGRAPHRMAT_HPP

/*****************************************************************************/
/*!
 * @file tiglGenerationConstructGraphRmat.hpp
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/

/******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/
#include <vector>
#include <math.h>
#include "tiglUtils.hpp"

/******************************************************************************
 *
 * MACROS
 *
 ******************************************************************************/
#define		MAX_PROB_COUNT		1000
#define		MAX_STEPS			30 // equivalent to 1B  edges
#define		SMOOTH_SCALE		0.02

/*****************************************************************************/
/*!
 * @ingroup graph_gen
 * @brief Graph construction using the R-mat procedure
 * @details The procedure built a template graph using the R-mat procedure as outlined in the paper:
 * D. Chakrabarti, Y. Zhan, C. Faloutsos, "R-MAT: a recursive model for graph Mining"
 *
 * Inputs:
 * @param numVertices total number of vertices in the output graph
 * @param meanNumEdges the mean of the number of output edges per vertex
 * @param isDirected  a flag which is set to "true" if the graph is directed, otherwise it is set to "false"
 * @param vertexList output vector of all the vertices in the graph
 * @param a, b, c, d: probability parameters that governs the graph generation algorithm
 * @param startID: starting value of the key of the graph vertices
 */
/*****************************************************************************/
template<typename VertexClass>
void tiglGenerationConstructGraphRmat(typename VertexClass::ID_TYPE numVertices, long meanNumEdges, vector<VertexClass> & vertexList, double a = 0.25,
      double b = 0.25, double c = 0.25, double d = 0.25, bool isDirected = true, typename VertexClass::ID_TYPE startID = 0)
{
   typedef typename VertexClass::EDGE_TYPE EDGE_TYPE;
   typedef typename VertexClass::ID_TYPE ID_TYPE;

   double bufProb[MAX_PROB_COUNT];
   double modProb[MAX_STEPS * 4];

   /// \b Procedure

   unsigned long numSteps = (unsigned long) ceil(log((double) numVertices) / log(2.0));
   ID_TYPE N = ((ID_TYPE) 1) << numSteps;
   ID_TYPE targetTotalEdges = meanNumEdges * numVertices;

   if (isDirected == false)
   {   // if undirected graph, each input edge is also considered an output edge
      targetTotalEdges /= 2;
   }

   if (vertexList.empty() == false)
   {
      cout << "Warning ! input vertex list is not empty" << endl;
   }

   double prob[3];   // original abcd values
   //double currProb[3]; // values that are perturbed at each iteration

   double sumABCD = a + b + c + d;

   prob[0] = a / sumABCD;
   prob[1] = prob[0] + b / sumABCD;
   prob[2] = prob[1] + c / sumABCD;
   //prob[3] = prob[2]+d;

   /// 1. Allocating enough memory to the vertex list and assigning default keys to them
   for (ID_TYPE loopCount = 0; loopCount < numVertices; loopCount++)
   {
      VertexClass tmpClass(loopCount + startID);   // assigning loop count as vertex id
      vertexList.push_back(tmpClass);
   }

   ID_TYPE edgeCount = 0;

   // generating the prabability in bulks to increase the efficiency
   ID_TYPE probCount = 0;
   tiglUtilsGenUrandom(bufProb, MAX_PROB_COUNT);

   /// 2. Loop for generating edge, one at a time until the target number of edges is reached. Execute the following for each new edge
   while (edgeCount < targetTotalEdges)
   {
      ID_TYPE src = 0;
      ID_TYPE target = 0;

      ID_TYPE shift = N / 2;

      if (probCount >= MAX_PROB_COUNT - numSteps)
      {   // if near end of probability buffer refill it
         tiglUtilsGenUrandom(bufProb, MAX_PROB_COUNT);
         probCount = 0;
      }
      tiglUtilsGenUrandom(modProb, numSteps * 4);

      for (unsigned j = 0; j < numSteps * 4; j++)
         modProb[j] *= SMOOTH_SCALE;

      /// 3. Loop to compute the x-y position of the edge in the incidence matrix

      for (unsigned step = 0; step < numSteps; step++)
      {
         double r = bufProb[probCount++];
         unsigned ind = step << 2;
         if ((r >= prob[0] + modProb[ind]) && (r < prob[1] + modProb[ind + 1]))   // second quadrant
            target += shift;
         else if ((r >= prob[1] + modProb[ind + 1]) && (r < prob[2] + modProb[ind + 2]))   // third quadrant
            src += shift;
         else if (r >= prob[2] + modProb[ind + 2])   // fourth quadrant
         {
            src += shift;
            target += shift;
         }
         // if r < prob[0] -> it falls in first quadrant and no action is needed
         shift /= 2;
      }
      // mapping to new edge to both the vertex list and edge list
      if (src >= numVertices || target >= numVertices)
         continue;

      /// 4. if an edge already exists ignore this edge
      if (vertexList[src].isConnected(target))
         continue;

      if (isDirected == 0 && target > src)   // skipping the upper half of the adjacency matrix (because of the clip and flip that is used later)
         continue;

      EDGE_TYPE newEdge(target);
      //edgesList.push_back(newEdge);

      /// 5. Attach the new edge to the corresponding vertex (or vertices)
      vertexList[src].addEdge(newEdge);
      if (isDirected == 0)
      {
         EDGE_TYPE newInEdge(src);
         vertexList[target].addEdge(newInEdge);
      }
      else
      {   // adding to the input edges list
          /// 6. If the vertex has input edge buffer, add the new edge to the input buffer of the target vertex
         if (vertexList[target].isBiVertex())
         {
            vertexList[target].addInEdge(src);
         }
      }

      edgeCount++;
   }   // end of the edge loop
}

/*****************************************************************************/
/*!
 * @ingroup graph_gen
 * @brief Overloaed version of graph construction using the R-mat procedure
 *
 * Inputs:
 * @param numVertices total number of vertices in the output graph
 * @param meanNumEdges the mean of the number of output edges per vertex
 * @param graph a subgraph class that contains the vertexList
 * @param a, b, c, d: probability parameters that governs the graph generation algorithm
 */
/*****************************************************************************/
template<typename GraphClass>
void tiglGenerationConstructGraphRmat(typename GraphClass::ID_TYPE numVertices, long meanNumEdges, GraphClass & graph, double a = 0.25, double b = 0.25,
      double c = 0.25, double d = 0.25)
{
   typedef typename GraphClass::EDGE_TYPE EDGE_TYPE;
   typedef typename GraphClass::ID_TYPE ID_TYPE;

   double bufProb[MAX_PROB_COUNT];
   unsigned long numSteps = (unsigned long) ceil(log((double) numVertices) / log(2.0));
   ID_TYPE N = ((ID_TYPE) 1) << numSteps;
   ID_TYPE targetTotalEdges = meanNumEdges * numVertices;

   if (graph.get_num_vertices() < numVertices && graph.isDirected() == true)
   {
      graph.createVerticesList(numVertices);
   }

   double prob[3];

   double sumABCD = a + b + c + d;

   prob[0] = a / sumABCD;
   prob[1] = prob[0] + b / sumABCD;
   prob[2] = prob[1] + c / sumABCD;

   ID_TYPE edgeCount = 0;

   // generating the prabability in bulks to increase the efficiency
   ID_TYPE probCount = 0;
   tiglUtilsGenUrandom(bufProb, MAX_PROB_COUNT);

   while (edgeCount < targetTotalEdges)   //for(loopCount = 0; loopCount < totalEdges; loopCount++)
   {
      ID_TYPE src = 0;
      ID_TYPE target = 0;

      ID_TYPE shift = N / 2;

      if (probCount >= MAX_PROB_COUNT - numSteps)
      {   // if near end of probability buffer refill it
         tiglUtilsGenUrandom(bufProb, MAX_PROB_COUNT);
         probCount = 0;
      }
      // loop for computing the x-y position of the edge in the incidence matrix
      for (unsigned step = 0; step < numSteps; step++)
      {
         double r = bufProb[probCount++];
         if (r >= prob[0] && r < prob[1])   // second quadrant
            target += shift;
         else if (r >= prob[1] && r < prob[2])   // third quadrant
            src += shift;
         else if (r >= prob[2])   // fourth quadrant
         {
            src += shift;
            target += shift;
         }
         // if r < prob[0] -> it falls in first quadrant and no action is needed
         shift /= 2;
      }
      // mapping to new edge to both the vertex list and edge list
      if (src >= numVertices || target >= numVertices)
         continue;

      // if an edge already exists ignore this edge
      if (graph.isConnected(src, target))
         continue;

      EDGE_TYPE newEdge(target);
      graph.addOutEdge(src, newEdge);

      EDGE_TYPE newInEdge(src);
      graph.addInEdge(target, newInEdge);

      edgeCount++;
   }   // end of the edge loop
}
#endif

