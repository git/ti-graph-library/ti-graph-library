#ifndef TIGLVERTEXBASE_HPP_
#define TIGLVERTEXBASE_HPP_

/*****************************************************************************/
/*!
 * @file tiglContainersVertexBase.hpp
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/

/******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/
#include <vector>
#include <set>
#include <stdio.h>
#include  <stdint.h>

#include "tiglContainersVertexTraits.hpp"
#include "tiglContainersEdgeBase.hpp"

/******************************************************************************
 *
 * NAMESPACE
 *
 ******************************************************************************/
using namespace std;

/*****************************************************************************/
/*!
 * @ingroup data_structures
 * @defgroup core Core Classes
 * \brief define the core classes for edge and vertex definitions
 */
/*****************************************************************************/

/******************************************************************************
 *
 * TIGLContainersVertexBase class
 *
 ******************************************************************************/

/*****************************************************************************/
/*!
 * @ingroup data_structures core
 *
 * @class TIGLContainersVertexBase
 * @brief Core vertex class
 * @details It contains all the vertex data and defines all vertex mutations
 * It is a template class that is parameterized by the following parameters
 * @param DataType the type of the data field in the vertex
 * @param AlgDataType the type of algorithm data filed in the vertex
 * @param  EdgeType the edge type
 * @param IdType the data type of the ID field of each vertex
 */
/*****************************************************************************/
template<typename DataType, typename AlgDataType, typename EdgeType, typename IdType = uint_fast32_t>
class TIGLContainersVertexBase
{
   /******************************************************************************
    *
    * type definitions
    *
    ******************************************************************************/
public:
   typedef TIGLContainersVertexTraits<DataType, AlgDataType, EdgeType, IdType> trait_type;
   typedef typename trait_type::ID_TYPE ID_TYPE;
   typedef typename trait_type::OUT_EDGE_ITERATOR OUT_EDGE_ITERATOR;
   typedef typename trait_type::OUT_EDGE_BUF OUT_EDGE_BUF;
   typedef typename trait_type::EDGE_TYPE EDGE_TYPE;
   typedef typename trait_type::OUT_EDGE_ITERATOR EDGE_ITERATOR;

   typedef AlgDataType ALG_DATA_TYPE;
   typedef DataType DATA_TYPE;

   /******************************************************************************
    *
    * constructor/destructor
    *
    ******************************************************************************/
public:
   /*****************************************************************************/
   /*!
    * @brief Constructor
    * @param key the vertex ID
    */
   /*****************************************************************************/
   TIGLContainersVertexBase(IdType key)
   {
      id = key;
   }

   /*****************************************************************************/
   /*!
    * @brief Overloaded constructor
    * @param key the vertex ID
    * @param data initial value of vertexData field
    * @param alg initial value of algData field
    */
   /*****************************************************************************/
   TIGLContainersVertexBase(IdType key, DataType data, AlgDataType alg)
   {
      id = key;
      vertexData = data;
      algData = alg;
   }

   /*****************************************************************************/
   /*!
    * @brief Overloaded empty constructor
    */
   /*****************************************************************************/
   TIGLContainersVertexBase()
   {
   }

   ~TIGLContainersVertexBase()
   {
   }

   /******************************************************************************
    *
    * attributes
    *
    ******************************************************************************/
protected:
   /// unique vertex ID
   IdType id;
   ///  associated generic data
   DataType vertexData;
   /// Algorithm-specific data
   AlgDataType algData;
   ///output edge buffer
   OUT_EDGE_BUF outEdges;

   /******************************************************************************
    *
    * methods
    *
    ******************************************************************************/
public:
   /*****************************************************************************/
   /*!
    * @brief Overloaded operator for list ordering
    */
   /*****************************************************************************/
   bool operator<(const TIGLContainersVertexBase<DataType, AlgDataType, EdgeType, IdType> & rhs) const
   {
      return (id < rhs.id);
   }

   /*****************************************************************************/
   /*!
    * @brief Overloaded equal operator for list ordering
    */
   /*****************************************************************************/
   bool operator==(const TIGLContainersVertexBase<DataType, AlgDataType, EdgeType, IdType> & rhs) const
   {
      return (id == rhs.id);
   }

   /*****************************************************************************/
   /*!
    * @brief Adds output edge
    * @param x the edge object
    */
   /*****************************************************************************/
   inline void addEdge(EdgeType & x)
   {
      outEdges.push_back(x);
   }

   /*****************************************************************************/
   /*!
    * @brief Adds output edge (overloaded function)
    * @param target the other side of the edge
    */
   /*****************************************************************************/
   inline void addEdge(ID_TYPE & target)
   {
      EDGE_TYPE newEdge(target);
      outEdges.push_back(newEdge);
   }

   /*****************************************************************************/
   /*!
    * @brief Adds multiple ouput edges
    * @param x a buffer of the output edges
    */
   /*****************************************************************************/
   inline void addEdge(OUT_EDGE_BUF & x)
   {
      for (OUT_EDGE_ITERATOR it = x.begin(); it != x.end(); ++it)
         outEdges.push_back(*it);
   }

   /*****************************************************************************/
   /*!
    * @brief Removes an output edge identified by target node
    * @param target the other side of the edge
    * @return true if successful, false if the edge does not exit
    */
   /*****************************************************************************/
   bool removeEdge(IdType & target)
   {
      for (OUT_EDGE_ITERATOR it = outEdges.begin(); it != outEdges.end(); ++it)
         if (it->getNbr() == target)
         {
            outEdges.erase(it);
            return true;
         }
      return false;
   }

   /*****************************************************************************/
   /*!
    * @brief Clears all output edges
    */
   /*****************************************************************************/
   inline void clearEdges()
   {
      outEdges.clear();
   }

   /*****************************************************************************/
   /*!
    * @brief Returns the total number of output edges
    */
   /*****************************************************************************/
   inline uint_fast32_t getNumOutEdges()
   {
      return outEdges.size();
   }

   /*****************************************************************************/
   /*!
    * @brief Returns the number of neigbors excluding self (i.e., ignoring self cycles)
    */
   /*****************************************************************************/
   uint_fast32_t getNumDifferentNbr()
   {
      uint_fast32_t numNbr = 0;
      for (OUT_EDGE_ITERATOR it = outEdges.begin(); it != outEdges.end(); ++it)
         if (it->getNbr() != id)
            numNbr++;
      return numNbr;
   }

   /*****************************************************************************/
   /*!
    * @brief Returns the vertex id of all children
    * @param vectChildren the output vector
    */
   /*****************************************************************************/
   void getChildren(vector<IdType> & vectChildren)
   {
      for (OUT_EDGE_ITERATOR it = outEdges.begin(); it != outEdges.end(); ++it)
         vectChildren.push_back(it->getNbr());
   }

   /*****************************************************************************/
   /*!
    * @brief Checks if a given node is connected to the current node
    * @param node ID of the other node
    * @return true if connected, false if not connected
    */
   /*****************************************************************************/
   bool isConnected(IdType node)
   {
      for (OUT_EDGE_ITERATOR it = outEdges.begin(); it != outEdges.end(); ++it)
         if (it->getNbr() == node)
            return true;
      return false;
   }

   /*****************************************************************************/
   /*!
    * @brief Returns the vertex data field
    */
   /*****************************************************************************/
   inline DataType & get_value()
   {
      return vertexData;
   }

   /*****************************************************************************/
   /*!
    * @brief Returns the algorithm data
    *
    */
   /*****************************************************************************/
   inline AlgDataType & getAlgData()
   {
      return algData;
   }

   /*****************************************************************************/
   /*!
    * @brief Returns the vertex id
    *
    */
   /*****************************************************************************/
   inline IdType getId()
   {
      return id;
   }

   /*****************************************************************************/
   /*!
    * @brief Returns an iterator to the start of the outEdges buffer
    *
    */
   /*****************************************************************************/
   inline OUT_EDGE_ITERATOR getBeginOutEdgeIterator()
   {
      return outEdges.begin();
   }

   /*****************************************************************************/
   /*!
    * @brief Returns an iterator to the last edge of the outEdges buffer
    *
    */
   /*****************************************************************************/
   inline OUT_EDGE_ITERATOR getEndOutEdgeIterator()
   {
      return outEdges.end();
   }

   /*****************************************************************************/
   /*!
    * @brief Returns an iterator to the output edge with a given target
    * @param target the ID of the other side of the edge
    * @return iterator to the output edge if the edge exists, and end of buffer if the edge does not exist
    *
    */
   /*****************************************************************************/
   OUT_EDGE_ITERATOR get_edge(ID_TYPE target)
   {
      for (OUT_EDGE_ITERATOR it = outEdges.begin(); it != outEdges.end(); ++it)
         if (it->get_target() == target)
            return it;
      return outEdges.end();
   }

   /*****************************************************************************/
   /*!
    * @brief Checks if the vertex type is TIGLContainersBiVertex
    * @retval false
    */
   /*****************************************************************************/
   bool isBiVertex()
   {
      return false;
   }

   /*****************************************************************************/
   /*!
    * @brief Core compute function: trivial realization in the core class. This function is overriden by the algorithm class
    *
    */
   /*****************************************************************************/
   virtual bool compute(void *pIn = 0, void *pOut = 0)
   {
      return false;
   }

   /*****************************************************************************/
   /*!
    * @brief Core init function: trivial realization in the core class. This function is overriden by the algorithm class
    */
   /*****************************************************************************/
   virtual void init()
   {
   }

   /*****************************************************************************/
   /*!
    * @brief Returns memory usage for edges
    */
   /*****************************************************************************/
   uint_fast32_t getEdgesSize()
   {
      return outEdges.capacity() * sizeof(EdgeType);
   }

   // dummy functions (added for consistency but not needed)
   void addInEdge(EdgeType & x)
   {
   }
   void addInEdge(IdType & src)
   {
   }
   void addInEdge(vector<EdgeType> & x)
   {
   }
   bool removeInEdge(IdType & src)
   {
      return false;
   }
   EDGE_ITERATOR getBeginInEdgeIterator()
   {
      return (EDGE_ITERATOR) NULL;
   }
   EDGE_ITERATOR getEndInEdgeIterator()
   {
      return (EDGE_ITERATOR) NULL;
   }
   inline uint_fast32_t get_num_in_edges()
   {
      return 0;
   }
   inline void clear()
   {
      outEdges.clear();
   }
};
#endif
