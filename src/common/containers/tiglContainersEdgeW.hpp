#ifndef TIGLCONTAINERSEDGEW_HPP_
#define TIGLCONTAINERSEDGEW_HPP_

/*****************************************************************************/
/*!
 * @file: tiglContainersEdgeW.hpp
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/

/******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/
#include <vector>
#include  <stdint.h>
#include "tiglContainersEdgeBase.hpp"

/******************************************************************************
 *
 * TIGLContainersEdgeW class
 *
 ******************************************************************************/

/*****************************************************************************/
/*!
 * @ingroup data_structures core
 * @class TIGLContainersEdgeW
 * @brief Weighted edge class
 * @details This is derived from TIGLContainersEdgeBase and had the extra parameters of "edgeData" that carries information about the edge weight
 */
/*****************************************************************************/
template<typename idType = unsigned long, typename dataType = double>
class TIGLContainersEdgeW: public TIGLContainersEdgeBase<idType>
{
   /******************************************************************************
    *
    * type definitions
    *
    ******************************************************************************/
public:
   typedef idType ID_TYPE;
   typedef dataType DATA_TYPE;

   /******************************************************************************
    *
    * constructor/destructor
    *
    ******************************************************************************/
public:
   TIGLContainersEdgeW(ID_TYPE eNode) :
         TIGLContainersEdgeBase<idType>(eNode)
   {
   }
   TIGLContainersEdgeW(ID_TYPE eNode, DATA_TYPE newVal) :
         TIGLContainersEdgeBase<idType>(eNode)
   {
      edgeData = newVal;
   }
   ~TIGLContainersEdgeW()
   {
   }

   /******************************************************************************
    *
    * attributes
    *
    ******************************************************************************/
protected:
   /// edge data field
   dataType edgeData;

   /******************************************************************************
    *
    * public methods
    *
    ******************************************************************************/
public:
   /*****************************************************************************/
   /*!
    *  @brief Returns the value of data
    */
   /*****************************************************************************/
   inline dataType getData()
   {
      return edgeData;
   }

   /*****************************************************************************/
   /*!
    *  @brief Returns true (overloaded from base class)
    */
   /*****************************************************************************/
   inline static bool hasVal()
   {
      return true;
   }
};

#endif /* TIGLCONTAINERSEDGEW_HPP_ */
