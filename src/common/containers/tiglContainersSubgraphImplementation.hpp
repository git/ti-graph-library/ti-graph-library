#ifndef TIGLCONTAINERSSUBGRAPHIMPLEMENTATION_HPP_
#define TIGLCONTAINERSSUBGRAPHIMPLEMENTATION_HPP_

/*****************************************************************************/
/*!
 * @file tiglContainersSubgraphImplementation.hpp
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/

/******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/

#include "tiglContainersSubgraph.hpp"

/*****************************************************************************/
/*!
 * @brief Constructor
 * @details It performs the basic memory allocation for messages and initializes the graph buffers to be filled with a graph read
 * @param flagDirected true if the graph is directed, false if the graph is undirected
 * @param threads total number of threads on the device (default: 1 for single core, 4 for openMP)
 * @param assignedHashSize: the hashing size for the position buffer
 *
 */
/*****************************************************************************/
template<typename CVertex, typename MessageType>
TIGLContainersSubgraph<CVertex, MessageType>::TIGLContainersSubgraph(bool flagDirected, int threads, uint_fast32_t assignedHashSize) :
      TIGLContainersSubgraphBase<CVertex>(flagDirected, assignedHashSize)
{
   numAllocThreads = threads;
#if defined(_MPI)
   // one extra thread for the external messages
   numAllocThreads++;
#endif
   evenTimeMessages = new vector<MESSAGE_BUF_TYPE> [numAllocThreads];
   oddTimeMessages = new vector<MESSAGE_BUF_TYPE> [numAllocThreads];
   oddMessageCount = new vector<uint_fast32_t> [numAllocThreads];
   evenMessageCount = new vector<uint_fast32_t> [numAllocThreads];
   externalOutMessages = new EXTERNAL_MESSAGE_BUF[numAllocThreads];
   localExternalOutMessages = new EXTERNAL_MESSAGE_BUF[numAllocThreads];
   externalOutMessagesCount = new uint_fast32_t[numAllocThreads];
   //externalOutMessagesPos = new EXTERNAL_POS_BUF [numAllocThreads];

   outMutationMessages = new MUTATION_MBUF[numAllocThreads];
   inMutationMessages = new MUTATION_MBUF[numAllocThreads];
   reset();
}

/*****************************************************************************/
/*!
 * @brief Overloaded constructor
 * @details It has the initial adjacency list of the graph as an input parameter
 * @param init_adj_list the input adjacencty list of the graph (as vector of vertices)
 * @param flagDirected true if the graph is directed, false if the graph is undirected
 * @param threads total number of threads on the device (default: 1 for single core, 4 for openMP)
 * @param assignedHashSize: the hashing size for the position buffer
 */
/*****************************************************************************/
template<typename CVertex, typename MessageType>
TIGLContainersSubgraph<CVertex, MessageType>::TIGLContainersSubgraph(VERTEX_LIST_TYPE & init_adj_list, bool flagDirected, int threads,
      uint_fast32_t assignedHashSize) :
      TIGLContainersSubgraphBase<CVertex>(init_adj_list, flagDirected, assignedHashSize)
{
   numAllocThreads = threads;
#if defined(_MPI)
   // one extra thread for the external messages
   numAllocThreads++;
#endif
   evenTimeMessages = new vector<MESSAGE_BUF_TYPE> [numAllocThreads];   // one extra for external messages
   oddTimeMessages = new vector<MESSAGE_BUF_TYPE> [numAllocThreads];   // on extra for external messages
   oddMessageCount = new vector<uint_fast32_t> [numAllocThreads];
   evenMessageCount = new vector<uint_fast32_t> [numAllocThreads];
//		externalOutMessages = new map<ID_TYPE, MESSAGE_BUF_TYPE> [numAllocThreads];
   externalOutMessages = new EXTERNAL_MESSAGE_BUF[numAllocThreads];
   localExternalOutMessages = new EXTERNAL_MESSAGE_BUF[numAllocThreads];
   externalOutMessagesCount = new uint_fast32_t[numAllocThreads];

//		externalOutMessagesPos = new EXTERNAL_POS_BUF [numAllocThreads];

   outMutationMessages = new MUTATION_MBUF[numAllocThreads];
   inMutationMessages = new MUTATION_MBUF[numAllocThreads];
   reset();
}

/*****************************************************************************/
/*!
 * @brief Destructor
 * @details It performs memory cleanup
 */
/*****************************************************************************/
template<typename CVertex, typename MessageType>
TIGLContainersSubgraph<CVertex, MessageType>::~TIGLContainersSubgraph()
{
   delete[] evenTimeMessages;
   delete[] oddTimeMessages;
   delete[] oddMessageCount;
   delete[] evenMessageCount;

   delete[] externalOutMessages;
   delete[] localExternalOutMessages;
   delete[] externalOutMessagesCount;

   delete[] outMutationMessages;
   delete[] inMutationMessages;
}

/*****************************************************************************/
/*!
 * @brief Initialization procedure
 */
/*****************************************************************************/
template<typename CVertex, typename MessageType>
void TIGLContainersSubgraph<CVertex, MessageType>::reset()
{
   TIGLContainersSubgraphBase<VERTEX_TYPE>::reset();
   resetMessageBuf();
}

/*****************************************************************************/
/*!
 * @brief Clears all the message buffers
 */
/*****************************************************************************/
template<typename CVertex, typename MessageType>
void TIGLContainersSubgraph<CVertex, MessageType>::resetMessageBuf()
{
   for (int thread = 0; thread < numAllocThreads; thread++)
   {
      evenTimeMessages[thread].clear();
      oddTimeMessages[thread].clear();
      oddMessageCount[thread].clear();
      evenMessageCount[thread].clear();

      outMutationMessages[thread].clear();
      inMutationMessages[thread].clear();
   }
   outSysMessages.clear();
   evenInSysMessages.clear();
   oddInSysMessages.clear();
}

/*****************************************************************************/
/*!
 * @brief Clears the contents of the message buffers
 */
/*****************************************************************************/
template<typename CVertex, typename MessageType>
void TIGLContainersSubgraph<CVertex, MessageType>::init()
{
   for (int thread = 0; thread < numAllocThreads; thread++)
   {
      for (uint_fast32_t vertexCount = 0; vertexCount < evenTimeMessages[thread].size(); vertexCount++)
      {
         (evenTimeMessages[thread])[vertexCount].clear();
         (oddTimeMessages[thread])[vertexCount].clear();
      }
      oddMessageCount[thread].assign(oddMessageCount[thread].size(), 0);   // resetting the number of messages
      evenMessageCount[thread].assign(evenMessageCount[thread].size(), 0);   // resetting the number of messages

      outMutationMessages[thread].clear();
      inMutationMessages[thread].clear();
   }
//#if defined(_MPI)
//		flagExtMessages.assign(flagExtMessages.size(), 0);
//#endif
   outSysMessages.clear();
   evenInSysMessages.clear();
   oddInSysMessages.clear();
}

/*****************************************************************************/
/*!
 * @brief Returns the number of parallel allocated messaging buffers
 */
/*****************************************************************************/
template<typename CVertex, typename MessageType>
inline int TIGLContainersSubgraph<CVertex, MessageType>::getNumAllocThreads()
{
   return numAllocThreads;
}

/*****************************************************************************/
/*!
 * @brief Adds a single fully-specified vertex
 * @details this overrides TIGLContainersSubgraphBase::addVertex and it allocates corresponding space in the message buffers
 * @param x the new vertex (passed by reference for efficiency)
 */
/*****************************************************************************/
template<typename CVertex, typename MessageType>
void TIGLContainersSubgraph<CVertex, MessageType>::addVertex(CVertex & x)
{
   if (TIGLContainersSubgraphBase<CVertex>::addVertex(x) == ADD_VERTEX_OLDPOS)
   {
      uint_fast32_t pos = 0;
      ID_TYPE id = x.getId();
      this->findPos(id, pos);

      for (int thread = 0; thread < numAllocThreads; thread++)
      {
         evenTimeMessages[thread][pos].clear();
         oddTimeMessages[thread][pos].clear();
         oddMessageCount[thread][pos] = 0;   //
         evenMessageCount[thread][pos] = 0;   //
      }
      return;   // no need for new allocation since it reused old positions
   }

   // adding to the message buffers
   MESSAGE_BUF_TYPE y;
   if (omp_get_num_threads() == 1)
   {
      for (int thread = 0; thread < numAllocThreads; thread++)
      {
         evenTimeMessages[thread].push_back(y);
         oddTimeMessages[thread].push_back(y);
         oddMessageCount[thread].push_back(0);   //
         evenMessageCount[thread].push_back(0);   //
      }
   }
   else
   {
      OMP("omp critical(ADD_MESSAGE_BUF)")
      {
         for (int thread = 0; thread < numAllocThreads; thread++)
         {
            evenTimeMessages[thread].push_back(y);
            oddTimeMessages[thread].push_back(y);
            oddMessageCount[thread].push_back(0);   //
            evenMessageCount[thread].push_back(0);   //
         }
      }
   }
}

/*****************************************************************************/
/*!
 * @brief Adds a single empty vertex by id
 * @param id the ID of the new vertex
 */
/*****************************************************************************/
template<typename CVertex, typename MessageType>
void TIGLContainersSubgraph<CVertex, MessageType>::addVertex(ID_TYPE id)
{
   CVertex x(id);
   addVertex(x);
}

/*****************************************************************************/
/*!
 * @brief Consistent vertex insertion that avoids duplication (this may not be used in most common cases)
 * @param x the new vertex (passed by reference for efficiency)
 */
/*****************************************************************************/
template<typename CVertex, typename MessageType>
bool TIGLContainersSubgraph<CVertex, MessageType>::addVertexConsistent(CVertex & x)
{
   if (TIGLContainersSubgraphBase<CVertex>::addVertex_consistent(x) == false)
      return false;

   // adding to the message buffers
   MESSAGE_BUF_TYPE y;
   for (int thread = 0; thread < numAllocThreads; thread++)
   {
      evenTimeMessages[thread].push_back(y);
      oddTimeMessages[thread].push_back(y);
      oddMessageCount[thread].push_back(0);   //
      evenMessageCount[thread].push_back(0);   //
   }

   return true;
}

/*****************************************************************************/
/*!
 * @brief Clears the message buffer
 * @param oddTimeFlag true for odd SS, false for even SS
 * @param thread thread index
 */
/*****************************************************************************/
template<typename CVertex, typename MessageType>
void TIGLContainersSubgraph<CVertex, MessageType>::clearMessages(bool oddTimeFlag, int thread)
{
   thread = thread % numAllocThreads;
   for (MESSAGE_BUF_ITERATOR it = oddTimeFlag ? oddTimeMessages[thread].begin() : evenTimeMessages[thread].begin();
         it != (oddTimeFlag ? oddTimeMessages[thread].end() : evenTimeMessages[thread].end()); ++it)
      it->clear();
   if (oddTimeFlag == true)
      oddMessageCount[thread].assign(oddMessageCount[thread].size(), 0);
   else
      evenMessageCount[thread].assign(evenMessageCount[thread].size(), 0);
}

/*****************************************************************************/
/*!
 * @brief Clears all data messages in
 * @param oddTimeFlag true for odd SS, false for even SS
 */
/*****************************************************************************/
template<typename CVertex, typename MessageType>
void TIGLContainersSubgraph<CVertex, MessageType>::clearAllMessages(bool oddTimeFlag)
{
   for (int thread = 0; thread < numAllocThreads; thread++)
   {
      for (MESSAGE_BUF_ITERATOR it = oddTimeFlag ? oddTimeMessages[thread].begin() : evenTimeMessages[thread].begin();
            it != (oddTimeFlag ? oddTimeMessages[thread].end() : evenTimeMessages[thread].end()); ++it)
         it->clear();
      if (oddTimeFlag == true)
         oddMessageCount[thread].assign(oddMessageCount[thread].size(), 0);
      else
         evenMessageCount[thread].assign(evenMessageCount[thread].size(), 0);
   }
}

/*****************************************************************************/
/*!
 * @brief Gets the number of messages or a given vertex for a given thread
 * @param index index of the vertex
 * @param oddTimeFlag true for odd SS, false for even SS
 * @param thread index of the threads (relevant in openMP)
 * @return the total number of messages for the vertex at the current thread
 */
/*****************************************************************************/
template<typename CVertex, typename MessageType>
inline uint_fast32_t TIGLContainersSubgraph<CVertex, MessageType>::getNumMessages(uint_fast32_t index, bool oddTimeFlag, int thread)
{
   thread = thread % numAllocThreads;
   return oddTimeFlag == true ? (oddMessageCount[thread])[index] : evenMessageCount[thread][index];
}

/*****************************************************************************/
/*!
 * @brief Returns a pointer to the buffer that contains the number of messages
 * @param index index of the vertex
 * @param oddTimeFlag true for odd SS, false for even SS
 * @param thread index of the threads (relevant in openMP)
 */
/*****************************************************************************/
template<typename CVertex, typename MessageType>
inline uint_fast32_t *TIGLContainersSubgraph<CVertex, MessageType>::getNumMessagesPtr(uint_fast32_t index, bool oddTimeFlag, int thread)
{
   thread = thread % numAllocThreads;
   return oddTimeFlag == true ? &((oddMessageCount[thread])[index]) : &((evenMessageCount[thread])[index]);
}

/*****************************************************************************/
/*!
 * @brief Returns the total number of data messages for a given thread
 * @param oddTimeFlag true for odd SS, false for even SS
 * @param thread index of the threads (relevant in openMP)
 * @return the total number of messages for all vertices at the current thread
 */
/*****************************************************************************/
template<typename CVertex, typename MessageType>
uint_fast32_t TIGLContainersSubgraph<CVertex, MessageType>::getTotalMessageCount(bool oddTimeFlag, int thread)
{
   thread = thread % numAllocThreads;
   uint_fast32_t totalMessages = 0;
   typename vector<uint_fast32_t>::iterator itStart = oddTimeFlag == true ? oddMessageCount[thread].begin() : evenMessageCount[thread].begin();
   typename vector<uint_fast32_t>::iterator itEnd = oddTimeFlag == true ? oddMessageCount[thread].end() : evenMessageCount[thread].end();
   for (typename vector<uint_fast32_t>::iterator it = itStart; it != itEnd; ++it)
      totalMessages += *it;   // of any has nonzero messages

   return totalMessages;
}

/*****************************************************************************/
/*!
 * @brief Sets the number of messages of a given vertex and thread to zero
 * @param index index of the vertex
 * @param oddTimeFlag true for odd SS, false for even SS
 * @param thread index of the threads (relevant in openMP)
 */
/*****************************************************************************/
template<typename CVertex, typename MessageType>
inline void TIGLContainersSubgraph<CVertex, MessageType>::clearNumMessages(uint_fast32_t index, bool oddTimeFlag, int thread)
{
   thread = thread % numAllocThreads;
   if (oddTimeFlag == true)
      (oddMessageCount[thread])[index] = 0;
   else
      (evenMessageCount[thread])[index] = 0;
}

/*****************************************************************************/
/*!
 * @brief Increments the number of messages of a given vertex and thread by one
 * @param index index of the vertex
 * @param oddTimeFlag true for odd SS, false for even SS
 * @param thread index of the threads (relevant in openMP)
 */
/*****************************************************************************/
template<typename CVertex, typename MessageType>
inline void TIGLContainersSubgraph<CVertex, MessageType>::incrementNumMessages(uint_fast32_t index, bool oddTimeFlag, int thread)
{
   thread = thread % numAllocThreads;
   if (oddTimeFlag == true)
      (oddMessageCount[thread])[index]++;
   else
      (evenMessageCount[thread])[index]++;
}

/*****************************************************************************/
/*!
 * @brief Sets the number of messages of a given vertex and thread to a given value
 * @param index index of the vertex
 * @param numMessages the value set for the number of messages
 * @param oddTimeFlag true for odd SS, false for even SS
 * @param thread index of the threads (relevant in openMP)
 */
/*****************************************************************************/
template<typename CVertex, typename MessageType>
inline void TIGLContainersSubgraph<CVertex, MessageType>::setNumMessages(uint_fast32_t index, uint_fast32_t numMessages, bool oddTimeFlag, int thread)
{
   thread = thread % numAllocThreads;
   if (oddTimeFlag == true)
      (oddMessageCount[thread])[index] = numMessages;
   else
      (evenMessageCount[thread])[index] = numMessages;
}

/*****************************************************************************/
/*!
 * @brief Sets the number of messages of a given vertex at all threads to a given value
 * @param index index of the vertex
 * @param numMessages the value set for the number of messages
 * @param oddTimeFlag true for odd SS, false for even SS
 */
/*****************************************************************************/
template<typename CVertex, typename MessageType>
inline void TIGLContainersSubgraph<CVertex, MessageType>::setAllNumMessages(uint_fast32_t index, uint_fast32_t numMessages, bool oddTimeFlag)
{
   if (oddTimeFlag == true)
      for (int thread = 0; thread < numAllocThreads; thread++)
         (oddMessageCount[thread])[index] = numMessages;
   else
      for (int thread = 0; thread < numAllocThreads; thread++)
         (evenMessageCount[thread])[index] = numMessages;
}

/*****************************************************************************/
/*!
 * @brief Gets the a pointer to the message buffer of an indexed vertex in a given thread
 * @param index index of the vertex
 * @param oddTimeFlag true for odd SS, false for even SS
 * @param thread index of the threads (relevant in openMP)
 * @return a pointer to the ping/pong data message buffer of the vertex
 */
/*****************************************************************************/
template<typename CVertex, typename MessageType>
inline typename TIGLContainersSubgraph<CVertex, MessageType>::MESSAGE_BUF_TYPE *TIGLContainersSubgraph<CVertex, MessageType>::getIndexedMessageBuf(
      ID_TYPE index, bool oddTimeFlag, int thread)
{
   thread = thread % numAllocThreads;
   return oddTimeFlag == true ? &((oddTimeMessages[thread])[index]) : &((evenTimeMessages[thread])[index]);
}

/*****************************************************************************/
/*!
 * @brief Gets the a pointer to the big data message buffer of all vertices
 * @param oddTimeFlag true for odd SS, false for even SS
 * @param thread index of the threads (relevant in openMP)
 * @return a pointer to the ping/pong data message buffer of message buffers
 */
/*****************************************************************************/
template<typename CVertex, typename MessageType>
inline vector<typename TIGLContainersSubgraph<CVertex, MessageType>::MESSAGE_BUF_TYPE> *TIGLContainersSubgraph<CVertex, MessageType>::getDataMessageBuf(
      bool oddTimeFlag, int thread)
{
   thread = thread % numAllocThreads;
   return oddTimeFlag == true ? &oddTimeMessages[thread] : &evenTimeMessages[thread];
}

/*****************************************************************************/
/*!
 * @brief Pushes a single mutation message to the local message buffer
 * @param message the new mutation message
 * @param workingThread the current OMP thread (0 for single device)
 */
/*****************************************************************************/
template<typename CVertex, typename MessageType>
inline void TIGLContainersSubgraph<CVertex, MessageType>::pushLocalMutationMessage(TIGLMutationMessage & message, int workingThread)
{
   workingThread = workingThread % numAllocThreads;
   inMutationMessages[workingThread].push_back(message);
}

/*****************************************************************************/
/*!
 * @brief Gets the number of mutation messages in the local buffer
 * @param workingThread the current OMP thread (0 for single device)
 * @return number of mutation messages in the local mutation buffer of the thread
 */
/*****************************************************************************/
template<typename CVertex, typename MessageType>
inline uint_fast32_t TIGLContainersSubgraph<CVertex, MessageType>::numLocalMutationMessage(int workingThread)
{
   workingThread = workingThread % numAllocThreads;

   return inMutationMessages[workingThread].size();
}

/*****************************************************************************/
/*!
 * @brief Pushes a single mutation message to the external message buffer
 * @param message the new mutation message
 * @param workingThread the current OMP thread (0 for single device)
 */
/*****************************************************************************/
template<typename CVertex, typename MessageType>
inline void TIGLContainersSubgraph<CVertex, MessageType>::pushExternalMutationMessage(TIGLMutationMessage & message, int workingThread)
{
   workingThread = workingThread % numAllocThreads;

   outMutationMessages[workingThread].push_back(message);
}

/*****************************************************************************/
/*!
 * @brief Gets the number of mutation messages in the local buffer
 * @param workingThread the current OMP thread (0 for single device)
 * @return number of mutation messages in the local mutation buffer of the thread
 */
/*****************************************************************************/
template<typename CVertex, typename MessageType>
inline uint_fast32_t TIGLContainersSubgraph<CVertex, MessageType>::numExternalMutationMessage(int workingThread)
{
   workingThread = workingThread % numAllocThreads;
   return outMutationMessages[workingThread].size();
}

/*****************************************************************************/
/*!
 * @brief Gets the number of mutation messages in all local buffers
 * @return number of mutation messages in all local mutation buffers
 */
/*****************************************************************************/
template<typename CVertex, typename MessageType>
inline uint_fast32_t TIGLContainersSubgraph<CVertex, MessageType>::numAllLocalMutationMessage()
{
   uint_fast32_t numMessages = numLocalMutationMessage(0);
   for (int i = 1; i < numAllocThreads; i++)
      numMessages += numLocalMutationMessage(i);

   return numMessages;
}

/*****************************************************************************/
/*!
 * @brief Clears the external mutation message buffer
 * @param workingThread the current OMP thread (0 for single device)
 */
/*****************************************************************************/
template<typename CVertex, typename MessageType>
inline void TIGLContainersSubgraph<CVertex, MessageType>::clearExternalMutationMessage(int workingThread)
{
   workingThread = workingThread % numAllocThreads;
   outMutationMessages[workingThread].clear();
}

/*****************************************************************************/
/*!
 * @brief Pushes a single system message to the output message buffer
 * @param message the new system message
 */
/*****************************************************************************/
template<typename CVertex, typename MessageType>
inline void TIGLContainersSubgraph<CVertex, MessageType>::pushSysMessage(TIGLCommunicationSysMessage & message)
{
   OMP("omp critical (PUSH_SYS_MESSAGE)")
   {
      outSysMessages.push_back(message);
      outSysMessagesCount++;
   }
}

/*****************************************************************************/
/*!
 * @brief Pushes a single system message to the output message buffer
 * @param message the new system message
 */
/*****************************************************************************/
template<typename CVertex, typename MessageType>
inline void TIGLContainersSubgraph<CVertex, MessageType>::consumeSysMessages(vector<TIGLCommunicationSysMessage> *messBuf)
{
   OMP("omp critical (PUSH_SYS_MESSAGE)")
   {
      messBuf->insert(messBuf->end(), outSysMessages.begin(), outSysMessages.end());
      outSysMessages.clear();
   }
}

/*****************************************************************************/
/*!
 * @brief Clears the output system message buffer
 // this may be called after transmitting all system messages
 */
/*****************************************************************************/
template<typename CVertex, typename MessageType>
inline void TIGLContainersSubgraph<CVertex, MessageType>::clearOutSysMessage()
{
   outSysMessages.clear();
   outSysMessagesCount = 0;
}

/*****************************************************************************/
/*!
 * @brief Clears the input system message buffer
 // this may be called after processing all incoming messages
 */
/*****************************************************************************/
template<typename CVertex, typename MessageType>
inline void TIGLContainersSubgraph<CVertex, MessageType>::clearInSysMessage(bool oddTimeFlag)
{
   if (oddTimeFlag)
      oddInSysMessages.clear();
   else
      evenInSysMessages.clear();
}

/*****************************************************************************/
/*!
 * @brief Pushes and combine a single data message to the one of the local data message buffer.
 * @details This is one of the critical functions for performance if openMP is used. In this version, new messages are combined with old ones rather than pushing.
 * @param message the new data message
 * @param index local index of the target vertex
 * @param oddTimeFlag true for odd SS, false for even SS
 * @param workingThread current thread
 * @param combiner pointer to the combiner class
 */
/*****************************************************************************/
template<typename CVertex, typename MessageType>
template<typename COMBINER_CLASS>
void TIGLContainersSubgraph<CVertex, MessageType>::pushDataMessage(MESSAGE_TYPE & message, uint_fast32_t index, bool oddTimeFlag, int workingThread,
      COMBINER_CLASS combiner)
{
   int thread = workingThread % numAllocThreads;

   /// \b Procedure
   /// 1. Retrieving a pointer to the message buffer
   typename vector<MESSAGE_BUF_TYPE>::iterator activeMessageIterator =
         oddTimeFlag == true ? oddTimeMessages[thread].begin() + index : evenTimeMessages[thread].begin() + index;

   /// 2. Retrieving a pointer to the message count buffer
   uint_fast32_t *numMessagesPtr = oddTimeFlag == true ? &((oddMessageCount[thread])[index]) : &((evenMessageCount[thread])[index]);   //getNumMessagesPtr(index, oddTimeFlag);//uint_fast32_t numMessages = getNumMessages(index, oddTimeFlag);

   if (numAllocThreads < NUM_OMP_THREADS)
   {   // this is inefficient implementation that reduces memory usage
      switch (numAllocThreads % NUM_OMP_THREADS) {
         case 0:OMP("omp critical (PushMessage0)")
         {
            if (activeMessageIterator->size() <= *numMessagesPtr)
            {   // buffer size needs to be increased
               activeMessageIterator->push_back(message);
            }

            else
            {   // rewrite in allocated area (no buffer enlargement)
               activeMessageIterator->at(*numMessagesPtr) = message;   // assigning to an existing position
            }
            (*numMessagesPtr)++;   //incrementNumMessages(index, oddTimeFlag);
         }
            break;
         default:OMP("omp critical (PushMessage1)")
         {
            if (activeMessageIterator->size() <= *numMessagesPtr)
            {   // buffer size needs to be increased
               activeMessageIterator->push_back(message);
            }

            else
            {   // rewrite in allocated area (no buffer enlargement)
               activeMessageIterator->at(*numMessagesPtr) = message;   // assigning to an existing position
            }
            (*numMessagesPtr)++;   //incrementNumMessages(index, oddTimeFlag);
         }
      }
   }
   else
   {
      /// 3. If first message, push to the message buffer, and increment counter
      if (*numMessagesPtr == 0)
      {
         if (activeMessageIterator->size() <= *numMessagesPtr)
         {   // buffer size needs to be increased
            activeMessageIterator->push_back(message);
         }

         else
         {   // rewrite in allocated area (no buffer enlargement)
            activeMessageIterator->at(*numMessagesPtr) = message;   // assigning to an existing position
         }
         (*numMessagesPtr)++;   //incrementNumMessages(index, oddTimeFlag);
      }
      else
      {
         /// 4. Else, combine the new message with messages already in the buffer
         combiner->combineMessages(message, activeMessageIterator->begin());
      }
   }
}

/*****************************************************************************/
/*!
 * @brief Pushes a single data message to the one of the local data message buffer.
 // @details This is one of the critical functions for performance if openMP is used. This version push messages directly without combining
 * @param message the new data message
 * @param index local index of the target vertex
 * @param oddTimeFlag true for odd SS, false for even SS
 * @param workingThread current thread
 */
/*****************************************************************************/
template<typename CVertex, typename MessageType>
void TIGLContainersSubgraph<CVertex, MessageType>::pushDataMessage(MESSAGE_TYPE & message, uint_fast32_t index, bool oddTimeFlag, int workingThread)
{
   int thread = workingThread % numAllocThreads;

   /// \b Procedure
   /// 1. Retrieving a pointer to the message buffer
   typename vector<MESSAGE_BUF_TYPE>::iterator activeMessageIterator =
         oddTimeFlag == true ? oddTimeMessages[thread].begin() + index : evenTimeMessages[thread].begin() + index;

   /// 2. Retrieving a pointer to the message count buffer
   uint_fast32_t *numMessagesPtr = oddTimeFlag == true ? &((oddMessageCount[thread])[index]) : &((evenMessageCount[thread])[index]);   //getNumMessagesPtr(index, oddTimeFlag);//uint_fast32_t numMessages = getNumMessages(index, oddTimeFlag);

   if (numAllocThreads < NUM_OMP_THREADS)
   {
      switch (numAllocThreads % NUM_OMP_THREADS) {
         case 0:OMP("omp critical (PushMessage0)")
         {
            if (activeMessageIterator->size() <= *numMessagesPtr)
            {   // buffer size needs to be increased
               activeMessageIterator->push_back(message);
            }

            else
            {   // rewrite in allocated area (no buffer enlargement)
               activeMessageIterator->at(*numMessagesPtr) = message;   // assigning to an existing position
            }
            (*numMessagesPtr)++;   //incrementNumMessages(index, oddTimeFlag);
         }
            break;
         default:OMP("omp critical (PushMessage1)")
         {
            if (activeMessageIterator->size() <= *numMessagesPtr)
            {   // buffer size needs to be increased
               activeMessageIterator->push_back(message);
            }

            else
            {   // rewrite in allocated area (no buffer enlargement)
               activeMessageIterator->at(*numMessagesPtr) = message;   // assigning to an existing position
            }
            (*numMessagesPtr)++;   //incrementNumMessages(index, oddTimeFlag);
         }
      }
   }
   else
   {
      /// 3. Push new message to the message buffer directly, and increment counter
      if (activeMessageIterator->size() <= *numMessagesPtr)
      {   // buffer size needs to be increased
         activeMessageIterator->push_back(message);
      }

      else
      {   // rewrite in allocated area (no buffer enlargement)
         activeMessageIterator->at(*numMessagesPtr) = message;   // assigning to an existing position
      }
      (*numMessagesPtr)++;   //incrementNumMessages(index, oddTimeFlag);
   }
}

/*****************************************************************************/
/*!
 * @brief Adds a message to the message buffer of an external vertex
 * @param thread working thread
 * @return pointer to the corresponding output message buffer.
 */
/*****************************************************************************/
template<typename CVertex, typename MessageType>
inline void TIGLContainersSubgraph<CVertex, MessageType>::pushExternalDataMessage(MESSAGE_TYPE & message, uint_fast32_t thread)
{
   thread = thread % numAllocThreads;
   /// \b Procedure
   /// 1. Push the new external message to the intermediate external buffer
   localExternalOutMessages[thread].push_back(message);
   externalOutMessagesCount[thread]++;
   /// 2. If the buffer is full, push to the final external message buffer (which is consumed by the communication manager)
   if (localExternalOutMessages[thread].size() == LOCAL_EXTERNAL_MESSAGE_SIZE)
   {
      switch (thread) {
         case 0:
            OMP("omp critical (PUSH_EXTERNAL0)")
            externalOutMessages[thread].insert(externalOutMessages[thread].end(), localExternalOutMessages[thread].begin(),
                  localExternalOutMessages[thread].end());
            break;
         case 1:
            OMP("omp critical (PUSH_EXTERNAL1)")
            externalOutMessages[thread].insert(externalOutMessages[thread].end(), localExternalOutMessages[thread].begin(),
                  localExternalOutMessages[thread].end());
            break;
         case 2:
            OMP("omp critical (PUSH_EXTERNAL2)")
            externalOutMessages[thread].insert(externalOutMessages[thread].end(), localExternalOutMessages[thread].begin(),
                  localExternalOutMessages[thread].end());
            break;
         case 3:
            OMP("omp critical (PUSH_EXTERNAL3)")
            externalOutMessages[thread].insert(externalOutMessages[thread].end(), localExternalOutMessages[thread].begin(),
                  localExternalOutMessages[thread].end());
            break;
         default:
            cout << "XXXX ERROR IN pushExternalDataMessage" << endl;
      }
      /// 3. After push to the final external message buffer, clear the intermediate buffer
      localExternalOutMessages[thread].clear();
   }
}

/*****************************************************************************/
/*!
 @brief Moves all messages from temporary local buffer to external buffer
 */
/*****************************************************************************/
template<typename CVertex, typename MessageType>
inline void TIGLContainersSubgraph<CVertex, MessageType>::flushExternalDataMessage()
{
   for (int thread = 0; thread < numAllocThreads - 1; thread++)
   {
      switch (thread) {
         case 0:
            OMP("omp critical (PUSH_EXTERNAL0)")
            externalOutMessages[thread].insert(externalOutMessages[thread].end(), localExternalOutMessages[thread].begin(),
                  localExternalOutMessages[thread].end());
            break;
         case 1:
            OMP("omp critical (PUSH_EXTERNAL1)")
            externalOutMessages[thread].insert(externalOutMessages[thread].end(), localExternalOutMessages[thread].begin(),
                  localExternalOutMessages[thread].end());
            break;
         case 2:
            OMP("omp critical (PUSH_EXTERNAL2)")
            externalOutMessages[thread].insert(externalOutMessages[thread].end(), localExternalOutMessages[thread].begin(),
                  localExternalOutMessages[thread].end());
            break;
         case 3:
            OMP("omp critical (PUSH_EXTERNAL3)")
            externalOutMessages[thread].insert(externalOutMessages[thread].end(), localExternalOutMessages[thread].begin(),
                  localExternalOutMessages[thread].end());
            break;
         default:
            cout << "XXXX ERROR IN flushExternalDataMessage" << endl;

      }
      localExternalOutMessages[thread].clear();
   }
}

/*****************************************************************************/
/*!
 * @brief Copies external data messages to external buffer.
 * @details The content of the external message buffer is copied to an auxiliary buffer for TX. After copying the external buffer is cleared.
 * @param commBuf buffer to be filled with external messages
 * @param thread working thread
 */
/*****************************************************************************/
template<typename CVertex, typename MessageType>
void TIGLContainersSubgraph<CVertex, MessageType>::consumeExternalDataMessageBuf(EXTERNAL_MESSAGE_BUF *commBuf, int thread)
{
   thread = thread % numAllocThreads;

   EXTERNAL_MESSAGE_BUF *messagePtr = &externalOutMessages[thread];
   switch (thread) {
      case 0:OMP("omp critical (PUSH_EXTERNAL0)")
      {
         commBuf->insert(commBuf->end(), messagePtr->begin(), messagePtr->end());
         externalOutMessages[thread].clear();
      }
         break;
      case 1:OMP("omp critical (PUSH_EXTERNAL1)")
      {
         commBuf->insert(commBuf->end(), messagePtr->begin(), messagePtr->end());
         externalOutMessages[thread].clear();
      }
         break;
      case 2:OMP("omp critical (PUSH_EXTERNAL2)")
      {
         commBuf->insert(commBuf->end(), messagePtr->begin(), messagePtr->end());
         externalOutMessages[thread].clear();
      }
         break;
      case 3:OMP("omp critical (PUSH_EXTERNAL3)")
      {
         commBuf->insert(commBuf->end(), messagePtr->begin(), messagePtr->end());
         externalOutMessages[thread].clear();
      }
         break;
      default:
         cout << "XXXX ERROR IN consumeExternalDataMessageBuf" << endl;
   }
}

/*****************************************************************************/
/*!
 * @brief Clears the buffers for external messages of a particular thread
 */
/*****************************************************************************/
template<typename CVertex, typename MessageType>
inline void TIGLContainersSubgraph<CVertex, MessageType>::clearAllExternalMessageBuf()
{
   clearOutSysMessage();
   for (int thread = 0; thread < numAllocThreads; thread++)
   {
//			clearExternalDataMessageBuf(thread);
      externalOutMessages[thread].clear();
      localExternalOutMessages[thread].clear();
      externalOutMessagesCount[thread] = 0;

      outMutationMessages[thread].clear();
   }
}

/*****************************************************************************/
/*!
 * @brief Returns pointer to output system message buffers
 */
/*****************************************************************************/
template<typename CVertex, typename MessageType>
inline vector<TIGLCommunicationSysMessage> * TIGLContainersSubgraph<CVertex, MessageType>::getOutSysMessageBuf()
{
   return &outSysMessages;
}

/*****************************************************************************/
/*!
 * @brief Returns pointer to input system message buffers
 */
/*****************************************************************************/
template<typename CVertex, typename MessageType>
inline vector<TIGLCommunicationSysMessage> * TIGLContainersSubgraph<CVertex, MessageType>::getInSysMessageBuf(bool oddTimeFlag)
{
   return oddTimeFlag ? &oddInSysMessages : &evenInSysMessages;
}

/*****************************************************************************/
/*!
 * @brief Returns true if the vertices has a buffer of input edges, otherwise Returns false
 */
/*****************************************************************************/
template<typename CVertex, typename MessageType>
inline bool TIGLContainersSubgraph<CVertex, MessageType>::isBiVertex()
{
   return ((this->vertexList).begin())->isBiVertex();
}

/*****************************************************************************/
/*!
 * @brief Returns the total number of external messages of all types
 */
/*****************************************************************************/
template<typename CVertex, typename MessageType>
uint_fast32_t TIGLContainersSubgraph<CVertex, MessageType>::getTotalExternalMessageCount()
{
   uint_fast32_t totalMessageCount = outSysMessagesCount;   // system messages
#if defined(_MPI)
   for(int thread = 0; thread < numAllocThreads-1; thread++)
#else
   for (int thread = 0; thread < numAllocThreads; thread++)
#endif
   {
      totalMessageCount += externalOutMessagesCount[thread];
      totalMessageCount += outMutationMessages[thread].size();
   }
   return totalMessageCount;
}

/*****************************************************************************/
/*!
 * @brief Returns a pointer to the external message buf
 */
/*****************************************************************************/
template<typename CVertex, typename MessageType>
inline typename TIGLContainersSubgraph<CVertex, MessageType>::EXTERNAL_MESSAGE_BUF * TIGLContainersSubgraph<CVertex, MessageType>::getExternalMessageBuf(
      uint_fast32_t thread)
{
   thread = thread % numAllocThreads;
   return &externalOutMessages[thread];
}

/*****************************************************************************/
/*!
 * @brief Returns the external number of vertices that have messages for this particular thread
 */
/*****************************************************************************/
template<typename CVertex, typename MessageType>
inline typename TIGLContainersSubgraph<CVertex, MessageType>::ID_TYPE TIGLContainersSubgraph<CVertex, MessageType>::getExternalVertexSize(uint_fast32_t thread)
{
   thread = thread % numAllocThreads;
   return externalOutMessages[thread].size();
}

/*****************************************************************************/
/*!
 * @brief Returns the overall external number of vertices that have messages
 */
/*****************************************************************************/
template<typename CVertex, typename MessageType>
inline typename TIGLContainersSubgraph<CVertex, MessageType>::ID_TYPE TIGLContainersSubgraph<CVertex, MessageType>::getExternalVertexSize()
{
   ID_TYPE vertexSize = 0;
   for (uint_fast32_t thread = 0; thread < numAllocThreads; thread++)
      vertexSize += getExternalVertexSize(thread);

   return vertexSize;
}

/*****************************************************************************/
/*!
 * @brief Gets a pointer to the output mutation message buffer of a given thread
 * @param thread working thread
 * @return pointer to the corresponding output message buffer.
 */
/*****************************************************************************/
template<typename CVertex, typename MessageType>
inline typename TIGLContainersSubgraph<CVertex, MessageType>::MUTATION_MBUF * TIGLContainersSubgraph<CVertex, MessageType>::getOutMutationMessageBuf(
      uint_fast32_t thread)
{
   thread = thread % numAllocThreads;
   return &outMutationMessages[thread];
}

/*****************************************************************************/
/*!
 * @brief Gets a pointer to the input mutation message buffer of a given thread
 * @param thread working thread
 * @return pointer to the corresponding output message buffer.
 *
 */
/*****************************************************************************/
template<typename CVertex, typename MessageType>
inline typename TIGLContainersSubgraph<CVertex, MessageType>::MUTATION_MBUF * TIGLContainersSubgraph<CVertex, MessageType>::getInMutationMessageBuf(
      uint_fast32_t thread)
{
   thread = thread % numAllocThreads;
   return &inMutationMessages[thread];
}

/*****************************************************************************/
/*!
 * @brief Returns the "approximate" memory usage in bytes
 */
/*****************************************************************************/
template<typename CVertex, typename MessageType>
uint_fast32_t TIGLContainersSubgraph<CVertex, MessageType>::memory_usage()
{
   uint_fast32_t totalSize = TIGLContainersSubgraphBase<VERTEX_TYPE>::memory_usage();

   // computing memory for the vertex list
   uint_fast32_t sizeMessageBuff = sizeof(vector<MESSAGE_BUF_TYPE> );
   for (VERTEX_ITERATOR it = this->begin(); it != this->end(); ++it)
   {
      uint_fast32_t vertexSize = sizeof(MESSAGE_BUF_TYPE) * 2;   // for the two buffers
      vertexSize += it->getMessageBufSize();   // for the input and output messages
      sizeMessageBuff += vertexSize;
   }
   // adding the overhead
   sizeMessageBuff = (uint_fast32_t) ((double) sizeMessageBuff * LIST_MEM_OVERHEAD );

   totalSize += sizeMessageBuff;

   return totalSize;

}
#endif /* TIGLCONTAINERSSUBGRAPHIMPLEMENTATION_HPP_ */
