#ifndef TIGLCONTAINERSCOMPUTEEDGE_HPP_
#define TIGLCONTAINERSCOMPUTEEDGE_HPP_

/*****************************************************************************/
/*!
 * @file: tiglContainersComputeEdge.hpp
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/

/******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/
#include <vector>
#include  <stdint.h>
#include "tiglContainersEdgeW.hpp"

/******************************************************************************
 *
 * TIGLContainersComputeEdge class
 *
 ******************************************************************************/

/*****************************************************************************/
/*!
 *
 * @ingroup data_structures core
 *
 * @class TIGLContainersComputeEdge
 * @brief Compute edge class
 * @details This is derived from TIGLContainersEdgeW and had the extra parameters of edgeData and algData along with their methods
 */
/*****************************************************************************/
template<typename idType = unsigned long, typename dataType = double, typename algDataType = double>
class TIGLContainersComputeEdge: public TIGLContainersEdgeW<idType, dataType>
{

   /******************************************************************************
    *
    * type definitions
    *
    ******************************************************************************/
public:
   typedef idType ID_TYPE;
   typedef dataType DATA_TYPE;
   typedef algDataType ALG_DATA_TYPE;

   /******************************************************************************
    *
    * constructor/destructor
    *
    ******************************************************************************/
public:
   TIGLContainersComputeEdge(ID_TYPE eNode) :
         TIGLContainersEdgeW<idType>(eNode)
   {
   }

   TIGLContainersComputeEdge(ID_TYPE eNode, DATA_TYPE newVal) :
         TIGLContainersEdgeW<idType>(eNode, newVal)
   {
   }

   ~TIGLContainersComputeEdge()
   {
   }

   /******************************************************************************
    *
    * attributes
    *
    ******************************************************************************/
protected:
   /// the edge-centric algorithm data
   algDataType algData;

   /******************************************************************************
    *
    * methods
    *
    ******************************************************************************/
public:

   /*****************************************************************************/
   /*!
    * @brief Returns algData field
    */
   /*****************************************************************************/
   inline algDataType getAlgData()
   {
      return algData;
   }

   /*****************************************************************************/
   /*!
    * @brief Edge compute function
    * @details It is an empty implementation that must be customized by algorithm class
    */
   /*****************************************************************************/
   bool compute(void * pIn = 0, void * pOut = 0)
   {
      algData = *((algDataType*) pIn);
      return true;
   }
};

#endif /* TIGLCONTAINERSCOMPUTEEDGE_HPP_ */
