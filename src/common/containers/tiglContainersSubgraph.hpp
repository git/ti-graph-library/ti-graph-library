#ifndef TIGLCONTAINERSSUBGRAPH_HPP_
#define TIGLCONTAINERSSUBGRAPH_HPP_

/*****************************************************************************/
/*!
 * @file tiglContainersSubgraph.hpp
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/

/******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/

#include "tiglContainersGraphTraits.hpp"
#include "tiglContainersSubgraphBase.hpp"
#include "tiglCommunicationSysMessage.hpp"
#include "tiglMutationMessage.hpp"
#include "tiglUtilsMacros.h"
#include "tiglUtilsOmpTools.h"

/******************************************************************************
 *
 * TIGLContainersSubgraph class
 *
 ******************************************************************************/

/*****************************************************************************/
/*!
 * @ingroup data_structures containers
 * @class TIGLContainersSubgraph
 * @brief Graph container for each device in the %TIGL.
 * @details It inherits the TIGLContainersSubgraphBase class and adds the messaging capability to the core subgraph class.
 * It is a template class that is parameterized by the following parameters
 * @param CVertex 		the vertex class definition, 
 * @param MessageType the data message definition,
 */
/*****************************************************************************/
using namespace std;
class TIGLMutationMessage;

#define LOCAL_EXTERNAL_MESSAGE_SIZE		1024

template<typename CVertex, typename MessageType>
class TIGLContainersSubgraph: public TIGLContainersSubgraphBase<CVertex>
{
   /******************************************************************************
    *
    * type definitions
    *
    ******************************************************************************/
public:
   typedef TIGLContainersGraphTraits<CVertex> trait_type;
   typedef typename trait_type::ID_TYPE ID_TYPE;
   typedef typename trait_type::EDGE_TYPE EDGE_TYPE;
   typedef typename trait_type::FAST_LIST_TYPE VERTEX_LIST_TYPE;
   typedef typename trait_type::FAST_VERTEX_ITERATOR VERTEX_ITERATOR;
   typedef CVertex VERTEX_TYPE;
   typedef typename TIGLContainersSubgraphBase<CVertex>::INDEX_ITERATOR INDEX_ITERATOR;
   typedef vector<MessageType> MESSAGE_BUF_TYPE;
   typedef typename vector<MESSAGE_BUF_TYPE>::iterator MESSAGE_BUF_ITERATOR;
   typedef MessageType MESSAGE_TYPE;
   typedef TIGLMutationMessage MUTATION_MESSAGE;
   typedef vector<MUTATION_MESSAGE> MUTATION_MBUF;
   typedef typename vector<EDGE_TYPE>::iterator EDGE_ITERATOR;

   typedef MESSAGE_BUF_TYPE EXTERNAL_MESSAGE_BUF;
   typedef map<ID_TYPE, uint_fast32_t> EXTERNAL_POS_BUF;

   /******************************************************************************
    *
    * constructor/destructor
    *
    ******************************************************************************/
public:
   TIGLContainersSubgraph(bool flagDirected, int threads = NUM_OMP_THREADS, uint_fast32_t assignedHashSize = 0);
   TIGLContainersSubgraph(VERTEX_LIST_TYPE & init_adj_list, bool flagDirected, int threads = NUM_OMP_THREADS, uint_fast32_t assignedHashSize = 0);

   ~TIGLContainersSubgraph();

   /******************************************************************************
    *
    * attributes
    *
    ******************************************************************************/
protected:
   ///  ping message buffer for the local vertices
   vector<MESSAGE_BUF_TYPE> *evenTimeMessages;
   ///  pong message buffer for the local vertices
   vector<MESSAGE_BUF_TYPE> *oddTimeMessages;
   ///  ping message count buffer for the local vertices
   vector<uint_fast32_t> *oddMessageCount;
   ///  pong message count buffer for the local vertices
   vector<uint_fast32_t> *evenMessageCount;
   ///  External data messages buffer. Each entry is indexed by a vertex id and contains a message buffer tha has all the messages to this vertex
   EXTERNAL_MESSAGE_BUF *externalOutMessages;
   /// temporary buffer for external messages
   EXTERNAL_MESSAGE_BUF *localExternalOutMessages;
   /// counter for external data messages for each thread
   uint_fast32_t *externalOutMessagesCount;
   ///  external system message buffer for the current device
   vector<TIGLCommunicationSysMessage> outSysMessages;
   ///  counter of overall external system messages during a superstep
   uint_fast32_t outSysMessagesCount;
   ///  ping Local system message buffer for the current device
   vector<TIGLCommunicationSysMessage> evenInSysMessages;
   ///  pong Local system message buffer for the current device
   vector<TIGLCommunicationSysMessage> oddInSysMessages;
   ///  buffer of external mutation messages
   MUTATION_MBUF *outMutationMessages;
   ///  Buffer for local mutation messages  (no ping-pong is needed as they are processed seperately at the beginning)
   MUTATION_MBUF *inMutationMessages;
   ///  number of allocated message buffers that are access simultaneously. The default value is 1 for single core and 4 for 4-ARM cores (using openMP)
   int numAllocThreads;

   /******************************************************************************
    *
    * methods
    *
    ******************************************************************************/
public:
   void reset();
   void resetMessageBuf();
   void init();
   inline int getNumAllocThreads();
   void addVertex(CVertex & x);
   void addVertex(ID_TYPE id);
   bool addVertexConsistent(CVertex & x);
   void clearMessages(bool oddTimeFlag, int thread = 0);
   void clearAllMessages(bool oddTimeFlag);
   inline uint_fast32_t getNumMessages(uint_fast32_t index, bool oddTimeFlag, int thread = 0);
   inline uint_fast32_t * getNumMessagesPtr(uint_fast32_t index, bool oddTimeFlag, int thread = 0);
   uint_fast32_t getTotalMessageCount(bool oddTimeFlag, int thread = 0);
   inline void clearNumMessages(uint_fast32_t index, bool oddTimeFlag, int thread = 0);
   inline void incrementNumMessages(uint_fast32_t index, bool oddTimeFlag, int thread = 0);
   inline void setNumMessages(uint_fast32_t index, uint_fast32_t numMessages, bool oddTimeFlag, int thread = 0);
   inline void setAllNumMessages(uint_fast32_t index, uint_fast32_t numMessages, bool oddTimeFlag);
   inline MESSAGE_BUF_TYPE * getIndexedMessageBuf(ID_TYPE index, bool oddTimeFlag, int thread = 0);
   inline vector<MESSAGE_BUF_TYPE> * getDataMessageBuf(bool oddTimeFlag, int thread = 0);
   inline void pushLocalMutationMessage(TIGLMutationMessage & message, int workingThread = 0);
   inline uint_fast32_t numLocalMutationMessage(int workingThread = 0);
   inline void pushExternalMutationMessage(TIGLMutationMessage & message, int workingThread = 0);
   inline uint_fast32_t numExternalMutationMessage(int workingThread = 0);
   inline uint_fast32_t numAllLocalMutationMessage();
   inline void clearExternalMutationMessage(int workingThread = 0);
   inline void pushSysMessage(TIGLCommunicationSysMessage & message);
   void consumeSysMessages(vector<TIGLCommunicationSysMessage> *messBuf);
   inline void clearOutSysMessage();
   inline void clearInSysMessage(bool oddTimeFlag);
   template<typename COMBINER_CLASS>
   void pushDataMessage(MESSAGE_TYPE & message, uint_fast32_t index, bool oddTimeFlag, int workingThread, COMBINER_CLASS combiner);
   void pushDataMessage(MESSAGE_TYPE & message, uint_fast32_t index, bool oddTimeFlag, int workingThread);
   inline void pushExternalDataMessage(MESSAGE_TYPE & message, uint_fast32_t thread);
   inline void flushExternalDataMessage();
   void consumeExternalDataMessageBuf(EXTERNAL_MESSAGE_BUF *commBuf, int thread);
   inline void clearAllExternalMessageBuf();
   inline vector<TIGLCommunicationSysMessage> * getOutSysMessageBuf();
   inline vector<TIGLCommunicationSysMessage> * getInSysMessageBuf(bool oddTimeFlag);
   inline bool isBiVertex();
   uint_fast32_t getTotalExternalMessageCount();
   inline EXTERNAL_MESSAGE_BUF * getExternalMessageBuf(uint_fast32_t thread);
   inline ID_TYPE getExternalVertexSize(uint_fast32_t thread);
   ID_TYPE getExternalVertexSize();
   inline MUTATION_MBUF * getOutMutationMessageBuf(uint_fast32_t thread);
   inline MUTATION_MBUF * getInMutationMessageBuf(uint_fast32_t thread);
   uint_fast32_t memory_usage();
};

#include "tiglContainersSubgraphImplementation.hpp"
#endif /* TIGLCONTAINERSSUBGRAPH_HPP_ */
