#ifndef TIGLSUBGRAPHBASE_HPP_
#define TIGLSUBGRAPHBASE_HPP_

/*****************************************************************************/
/*!
 * @file tiglContainersSubgraphBase.hpp
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/

/******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/
#ifdef CMEM_ENABLE
#include "tiglMemMsmcAllocator.hpp"
#include "tiglMemDdrAllocator.hpp"
#endif


/*****************************************************************************/
/*!
 * @ingroup data_structures
 * @defgroup containers Graph Containers
 *
 * @brief Defines the container classes that hold the graph/subgraph information and handle all memory allocations
 * and queries for graph information. All graph mutation are handled within graph containers
 */
/*****************************************************************************/

/******************************************************************************
 *
 * TIGLContainersPosInfo class
 *
 ******************************************************************************/

/*****************************************************************************/
/*!
 * @ingroup data_structures Containers
 * @class TIGLContainersPosInfo
 * @brief Contains position information for each vertex which is stored in the position hash table.
 */
/*****************************************************************************/
template<typename ID_TYPE>
class TIGLContainersPosInfo
{
   /******************************************************************************
    *
    * type definitions
    *
    ******************************************************************************/
public:
   typedef TIGLContainersPosInfo<ID_TYPE> POS_INFO_TYPE;

   /******************************************************************************
    *
    * constructor/destructor
    *
    ******************************************************************************/
public:
   TIGLContainersPosInfo(ID_TYPE inId, uint_fast32_t inPos);
   template<typename T> TIGLContainersPosInfo(TIGLContainersPosInfo<T> & inPos);
   ~TIGLContainersPosInfo();

   /******************************************************************************
    *
    * attributes
    *
    ******************************************************************************/
protected:
   /// vertex id
   ID_TYPE id;
   /// vertex position in vertex list
   uint_fast32_t pos;

   /******************************************************************************
    *
    * methods
    *
    ******************************************************************************/
public:
   inline ID_TYPE getId();
   inline uint_fast32_t getPos();

   // overloading operator
   inline bool operator<(POS_INFO_TYPE & rhs)
   {
      return id < rhs.getId();
   }
   inline bool operator<=(POS_INFO_TYPE & rhs)
   {
      return id <= rhs.getId();
   }
   inline bool operator==(POS_INFO_TYPE & rhs)
   {
      return id == rhs.getId();
   }
   inline bool operator!=(POS_INFO_TYPE & rhs)
   {
      return id != rhs.getId();
   }
   inline bool operator>(POS_INFO_TYPE & rhs)
   {
      return id > rhs.getId();
   }
   inline bool operator>=(POS_INFO_TYPE & rhs)
   {
      return id >= rhs.getId();
   }

   inline bool operator<(ID_TYPE idRhs)
   {
#ifdef CMEM_ENABLE
//		cout << "$$$ id = " << id << ", idRhs = " << idRhs << endl;
#endif
      return id < idRhs;
   }
   inline bool operator<=(ID_TYPE idRhs)
   {
      return id <= idRhs;
   }
   inline bool operator==(ID_TYPE idRhs)
   {
      return id == idRhs;
   }
   inline bool operator!=(ID_TYPE idRhs)
   {
      return id != idRhs;
   }
   inline bool operator>(ID_TYPE idRhs)
   {
      return id > idRhs;
   }
   inline bool operator>=(ID_TYPE idRhs)
   {
      return id >= idRhs;
   }

};

template<typename ID_TYPE>
bool posCompare(TIGLContainersPosInfo<ID_TYPE> lhs, TIGLContainersPosInfo<ID_TYPE> rhs);

/******************************************************************************
 *
 * TIGLContainersSubgraphBase class
 *
 ******************************************************************************/

/*****************************************************************************/
/*!
 * @ingroup data_structures containers
 * @class TIGLContainersSubgraphBase
 * @brief Core data container for a subgraph in each device.
 * @details It contains the vertex list and the corresponding hashed position buffer. It also handles all graph mutations.
 * */
/*****************************************************************************/
template<typename CVertex>
class TIGLContainersSubgraphBase
{
   /******************************************************************************
    *
    * type definitions
    *
    ******************************************************************************/
public:
   typedef TIGLContainersGraphTraits<CVertex> trait_type;
   typedef typename trait_type::ID_TYPE ID_TYPE;
   typedef typename trait_type::EDGE_TYPE EDGE_TYPE;
   typedef typename trait_type::FAST_LIST_TYPE VERTEX_LIST_TYPE;
   typedef VERTEX_LIST_TYPE * VERTEX_LIST_PTR;
   typedef typename trait_type::FAST_VERTEX_ITERATOR VERTEX_ITERATOR;
   typedef CVertex VERTEX_TYPE;
#ifdef CMEM_ENABLE
   typedef TIGLMemDdrAllocator<TIGLContainersPosInfo<ID_TYPE> > ALLOCATOR_TYPE;
#else
   typedef allocator<TIGLContainersPosInfo<ID_TYPE> > ALLOCATOR_TYPE;
#endif

   typedef vector<TIGLContainersPosInfo<ID_TYPE>, ALLOCATOR_TYPE> INDEX_ELEMENT;   // //typedef map<ID_TYPE, uint_fast32_t, less<ID_TYPE>, ALLOCATOR_TYPE>	INDEX_ELEMENT; // typedef map<ID_TYPE, uint_fast32_t, less<ID_TYPE> >	INDEX_ELEMENT;//
   typedef typename INDEX_ELEMENT::iterator INDEX_ITERATOR;
   typedef vector<INDEX_ELEMENT, ALLOCATOR_TYPE> POS_LIST_TYPE;

   /******************************************************************************
    *
    * constructor/destructor
    *
    ******************************************************************************/
public:
   TIGLContainersSubgraphBase(bool isDireted = true, ID_TYPE assignedHashSize = 0);
   TIGLContainersSubgraphBase(VERTEX_LIST_TYPE & init_adj_list, bool isDireted, ID_TYPE assignedHashSize = 0);
   ~TIGLContainersSubgraphBase();

   /******************************************************************************
    *
    * attributes
    *
    ******************************************************************************/
protected:
   /// vertexList: holds the physical storage of the subgraph
   VERTEX_LIST_TYPE vertexList;
   /// vertexPos: holds the position of each vertex in the adjacancy list
   POS_LIST_TYPE vertexPos;   //INDEX_ELEMENT * vertexPos;//
   /// posHoles: holes in the vertex list that should be exploited first in adding vertex
   vector<uint_fast32_t> posHoles;
   /// flagDirected: true if directed graph, false if undirected graph
   bool flagDirected;
   /// the size of the position hash table
   uint_fast32_t posHashSize;

   /******************************************************************************
    *
    * methods
    *
    ******************************************************************************/
protected:
   void AllocPosTable();
   void DeallocPosTable();
   inline ID_TYPE hash(ID_TYPE & id);

public:
   inline bool exist(ID_TYPE id);
   void reset();
   inline bool findPos(ID_TYPE & id, uint_fast32_t & pos);
   int addVertex(CVertex & x);
   void createVerticesList(ID_TYPE & numVertices);
   void addVertex(ID_TYPE & id);
   bool addVertex_consistent(CVertex & x);
   bool removeVertex(ID_TYPE & id);
   bool addOutEdge(ID_TYPE src, ID_TYPE target);
   bool addOutEdge(ID_TYPE src, EDGE_TYPE edge);
   bool addInEdge(ID_TYPE src, ID_TYPE target);
   bool addInEdge(ID_TYPE target, EDGE_TYPE edge);
   bool removeOutEdge(ID_TYPE src, ID_TYPE target);
   bool removeInEdge(ID_TYPE src, ID_TYPE target);
   inline uint_fast32_t getNumVertices();
   ID_TYPE getNumEdges();
   bool getVertex(ID_TYPE & id, VERTEX_ITERATOR *it);
   inline uint_fast32_t getVertexPos(ID_TYPE & id);
   inline VERTEX_ITERATOR getIndexedVertex(ID_TYPE index);
   inline VERTEX_ITERATOR begin();
   inline VERTEX_ITERATOR end();
   inline uint_fast32_t size();
   inline VERTEX_LIST_PTR getVerticesPtr();
   bool isConnected(ID_TYPE & src, ID_TYPE & target);
   inline bool isDirected();
   uint_fast32_t memoryUsage();
   void memory_usage(uint_fast32_t & sizeVerticesList, uint_fast32_t & sizePos);
   uint_fast32_t MaxCollision();

#ifdef DEBUG_ENABLE
   void printBuffers();
   bool checkConsistency();
#endif
};

#include "tiglContainersSubgraphBaseImplementation.hpp"
#endif /* SUBGRAPH_HPP_ */
