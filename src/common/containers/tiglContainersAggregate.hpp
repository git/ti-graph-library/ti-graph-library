#ifndef TIGLCONTAINERSAGGREGATE_HPP_
#define TIGLCONTAINERSAGGREGATE_HPP_

/*****************************************************************************/
/*!
 * @file tiglContainersAggregate.hpp
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/

/******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/
#include "tiglUtilsOmpTools.h"
#include "tiglUtilsMacros.h"
#include "tiglCommunicationAggregateSysMessage.hpp"

/******************************************************************************
 *
 * TIGLContainersAggregate class
 *
 ******************************************************************************/

/*****************************************************************************/
/*!
 *  @ingroup data_structures core
 * @class  TIGLContainersAggregate
 * @brief Base class for aggregate information
 * @details It holds important aggregate information and methods for updating it.
 * it could be inherited by another more customized class if needed
 * it is a template class that is parameterized the algorithm data field of the graph algorithm
 */
/*****************************************************************************/
template<typename AlgData>
class TIGLContainersAggregate
{
   /******************************************************************************
    *
    * type definitions
    *
    ******************************************************************************/
protected:
   typedef AlgData ALG_DATA_TYPE;
   typedef TIGLCommunicationAggregateSysMessage<AlgData> AGG_SYS_MESSAGE_TYPE;

   /******************************************************************************
    *
    * constructor/destructor
    *
    ******************************************************************************/
public:
   TIGLContainersAggregate()
   {
#if defined(_OPENMP)
      omp_init_lock(&aggLock);
#endif
   }

   ~TIGLContainersAggregate()
   {
#if defined(_OPENMP)
      omp_destroy_lock(&aggLock);
#endif
   }

   /******************************************************************************
    *
    * attributes
    *
    ******************************************************************************/
protected:
   // static info
   ///total number of vertices in the graph
   uint_fast32_t numVertices;
   ///number of local vertices for this device
   uint_fast32_t numLocalVertices;

   // runtime statistics/info
   ///superstep counter
   uint_fast32_t countSS;
   ///total number of output messages over a superstep history
   uint_fast32_t numTotalMessages[AGGREGATE_HISTORY];
   ///total number of external output messages over a superstep history
   uint_fast32_t numExternalMessages[AGGREGATE_HISTORY];
   ///number of local active nodes over a superstep history
   uint_fast32_t numLocalActiveNodes[AGGREGATE_HISTORY];
   ///total number of active nodes over a superstep history
   uint_fast32_t numTotalActiveNodes[AGGREGATE_HISTORY];

   // algorithm specific information
   ///maximum value of algorithm data of all active vertices over a superstep history
   AlgData maxActiveAlgData[AGGREGATE_HISTORY];
   ///minimum value of algorithm data of all active vertices over a superstep history
   AlgData minActiveAlgData[AGGREGATE_HISTORY];
   ///the sum of the values of algorithm data of all active vertices over a superstep history
   AlgData sumActiveAlgData[AGGREGATE_HISTORY];
   /// update flag
   bool isUpdated;
private:
   // openMP lock for updating aggregate info
#if defined(_OPENMP)
   omp_lock_t aggLock;
#endif

   /******************************************************************************
    *
    * public methods
    *
    ******************************************************************************/
public:
   /*****************************************************************************/
   /*!
    * @brief Resets the aggregate parameters before the new superstep
    * @param ssIndex the superstep index
    * @param numLocActiveNodes number of local active nodes
    *
    */
   /*****************************************************************************/
   inline void localAggregateInit(uint_fast32_t ssIndex, uint_fast32_t numLocActiveNodes)
   {
      countSS = ssIndex;
      int t = countSS % AGGREGATE_HISTORY;

      numTotalMessages[t] = 0;
      numExternalMessages[t] = 0;

      numLocalActiveNodes[t] = numLocActiveNodes;
      isUpdated = false;
   }

   /*****************************************************************************/
   /*!
    * @brief Updates the aggregate information after a single node execution during a superstep
    * @param vertexIt iterator to the executed vertex
    * @param currNumMessages the output number of messages after vertex execution
    */
   /*****************************************************************************/
   template<typename VERTEX_ITERATOR>
   inline void localAggregateUpdate(VERTEX_ITERATOR vertexIt, uint_fast32_t currNumMessages)
   {
      int t = countSS % AGGREGATE_HISTORY;

      if (vertexIt->isActive())
      {
         //lock();
         ALG_DATA_TYPE newData = vertexIt->getAlgData();
         if (maxActiveAlgData[t] < newData || isUpdated == false)
         {
            OMP("omp critical (AGGREGATE_MAX)")
            {
               if (maxActiveAlgData[t] < newData)
               {
                  maxActiveAlgData[t] = newData;
//						ALG_DATA_TYPE * p = &minActiveAlgData[t];
//						OMP("omp flush (p)")
               }
            }
         }

         if (minActiveAlgData[t] > newData || isUpdated == false)
         {
            OMP("omp critical(AGGREGATE_MIN)")
            {
               if (minActiveAlgData[t] > newData)
               {
                  minActiveAlgData[t] = newData;
//						ALG_DATA_TYPE * p = &minActiveAlgData[t];
//						OMP("omp flush (p)")
               }
            }
         }

         OMP("omp critical(AGGREGATE_SUM)")
         {
            if (isUpdated == true)
               sumActiveAlgData[t] += newData;
            else
               sumActiveAlgData[t] = newData;
         }
      }
      //lock();
      OMP("omp atomic")
      numTotalMessages[t] += currNumMessages;
      //unlock();
      if (isUpdated == false)
      {
         isUpdated = true;
      OMP("omp flush(isUpdated)")
   }
}

/*****************************************************************************/
/*!
 * @brief Updates the aggregate information after receiving an update from other devices
 */
/*****************************************************************************/
inline void externalAggregateUpdate(AGG_SYS_MESSAGE_TYPE * aggSysInfo)
{
   int t = (countSS - 1) % AGGREGATE_HISTORY;

   ALG_DATA_TYPE newData = aggSysInfo->getMax();
   if (maxActiveAlgData[t] < newData)
   {
      maxActiveAlgData[t] = newData;
   }
   newData = aggSysInfo->getMin();
   if (minActiveAlgData[t] > newData)
   {
      minActiveAlgData[t] = newData;
   }
   newData = aggSysInfo->getSum();
   sumActiveAlgData[t] += newData;
}

/*****************************************************************************/
/*!
 * @brief Prepares the aggregate information system message (at the end of the superstep)
 * @param aggSysInfo pointer to the received external message
 *
 */
/*****************************************************************************/
void prepareAggregateSysMessage(AGG_SYS_MESSAGE_TYPE * aggSysInfo)
{
   int t = countSS % AGGREGATE_HISTORY;
   aggSysInfo->setMax(maxActiveAlgData[t]);
   aggSysInfo->setMin(minActiveAlgData[t]);
   aggSysInfo->setSum(sumActiveAlgData[t]);

//		cout << "countSS " << countSS << ": max "<< maxActiveAlgData[t] << endl;
}

/*****************************************************************************/
/*!
 * @brief Increments the number of external Messages after a single node compute in a superstep
 * @param currNumMessages the number of external output messages
 */
/*****************************************************************************/
inline void incrementExternalMessagesCount(uint_fast32_t currNumMessages)
{
   lock();
   numExternalMessages[countSS % AGGREGATE_HISTORY] += currNumMessages;
   unlock();
}

/*****************************************************************************/
/*!
 *@brief set the number of local active nodes at the end of a superstep
 * @param numActive the number of local active nodes
 */
/*****************************************************************************/
inline void setNumLocalActiveNoes(uint_fast32_t numActive)
{
   numLocalActiveNodes[countSS % AGGREGATE_HISTORY] = numActive;
}

/*****************************************************************************/
/*!
 * @brief Returns the number of external messages in the current superstep
 */
/*****************************************************************************/
inline uint_fast32_t getNumExternalMessages()
{
   return numExternalMessages[countSS % AGGREGATE_HISTORY];
}

/*****************************************************************************/
/*!
 * @brief Returns the number of external messages in the previous superstep
 */
/*****************************************************************************/
inline uint_fast32_t getPastNumExternalMessages()
{
   return numExternalMessages[(countSS - 1) % AGGREGATE_HISTORY];
}

/*****************************************************************************/
/*!
 * @brief Returns the total number of output messages of the  device in the current superstep
 */
/*****************************************************************************/
inline uint_fast32_t getNumTotalMessages()
{
   return numTotalMessages[countSS % AGGREGATE_HISTORY];
}

/*****************************************************************************/
/*!
 * @brief Returns the total number of output messages of the  device in the previous superstep
 */
/*****************************************************************************/
inline uint_fast32_t getPastNumTotalMessages()
{
   return numTotalMessages[(countSS - 1) % AGGREGATE_HISTORY];
}

/*****************************************************************************/
/*!
 * @brief Returns the total number of vertices in the whole graph
 */
/*****************************************************************************/
inline uint_fast32_t getNumVertices()
{
   return numVertices;
}

/*****************************************************************************/
/*!
 * @brief Sets the total number of vertices in the whole graph
 */
/*****************************************************************************/
inline void setNumVertices(uint_fast32_t num)
{
   numVertices = num;
}

/*****************************************************************************/
/*!
 * @brief Returns the number of local vertices
 */
/*****************************************************************************/
inline uint_fast32_t getNumLocalVertices()
{
   return numLocalVertices;
}

/*****************************************************************************/
/*!
 * @brief Sets the number of local vertices
 */
/*****************************************************************************/
inline void setNumLocalVertices(uint_fast32_t num)
{
   numLocalVertices = num;
}

/*****************************************************************************/
/*!
 * @brief Returns the superstep count
 */
/*****************************************************************************/

inline uint_fast32_t getNumSS()
{
   return countSS;
}

/*****************************************************************************/
/*!
 * @brief Returns the maximum value of the algorithm data of all active nodes for the current superstep
 */
/*****************************************************************************/
inline AlgData & getMaxActiveAlgData()
{
   return maxActiveAlgData[countSS % AGGREGATE_HISTORY];
}

/*****************************************************************************/
/*!
 * @brief Returns the maximum value of the algorithm data of all active nodes for the previous superstep
 */
/*****************************************************************************/
inline AlgData & getPastMaxActiveAlgData()
{
   return maxActiveAlgData[(countSS - 1) % AGGREGATE_HISTORY];
}

/*****************************************************************************/
/*!
 * @brief Returns the minimum value of the algorithm data of all active nodes for the current superstep
 */
/*****************************************************************************/
inline AlgData & getMinActiveAlgData()
{
   return minActiveAlgData[countSS % AGGREGATE_HISTORY];
}

/*****************************************************************************/
/*!
 * @brief Returns the minimum value of the algorithm data of all active nodes for the previous superstep
 */
/*****************************************************************************/
inline AlgData & getPastMinActiveAlgData()
{
   return minActiveAlgData[(countSS - 1) % AGGREGATE_HISTORY];
}

/*****************************************************************************/
/*!
 * @brief Returns the sum of the algorithm data of all active nodes for the current superstep
 */
/*****************************************************************************/
inline AlgData & getSumActiveAlgData()
{
   return sumActiveAlgData[countSS % AGGREGATE_HISTORY];
}

/*****************************************************************************/
/*!
 * @brief Returns the sum of the algorithm data of all active nodes for the previous superstep
 */
/*****************************************************************************/
inline AlgData & getPastSumActiveAlgData()
{
   return sumActiveAlgData[(countSS - 1) % AGGREGATE_HISTORY];
}

#if defined(_OPENMP)
inline void lock()
{  omp_set_lock(&aggLock);}
inline void unlock()
{  omp_unset_lock(&aggLock);}
#else
inline void lock()
{
}
inline void unlock()
{
}
#endif
};

#endif /* TIGLCONTAINERSAGGREGATE_HPP_ */
