#ifndef TIGLCONTAINERSGRAPHTRAITS_HPP_
#define TIGLCONTAINERSGRAPHTRAITS_HPP_

/*****************************************************************************/
/*!
 * @file tiglContainersGraphTraits.hpp
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/

/******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/
#include <map>
#include <vector>

/******************************************************************************
 *
 * namespace
 *
 ******************************************************************************/
using namespace std;

/*****************************************************************************/
/*!
 * @ingroup data_structures containers
 *
 * @struct TIGLContainersGraphTraits
 * @brief Basic type definitions for TIGLContainersSubgraphBase class
 */
/*****************************************************************************/
template<typename CVertex>
struct TIGLContainersGraphTraits
{
   typedef CVertex VERTEX_TYPE;
   typedef typename CVertex::EDGE_TYPE EDGE_TYPE;
   typedef typename CVertex::ID_TYPE ID_TYPE;
   typedef typename std::vector<CVertex> FAST_LIST_TYPE;
   typedef typename std::vector<EDGE_TYPE> FAST_EDGE_LIST_TYPE;
   typedef typename FAST_LIST_TYPE::iterator FAST_VERTEX_ITERATOR;
};

#endif /* TIGLCONTAINERSGRAPHTRAITS_HPP_ */
