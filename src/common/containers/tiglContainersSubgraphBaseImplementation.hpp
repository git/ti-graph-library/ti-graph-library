#ifndef TIGLCONTAINERSSUBGRAPHBASEIMPLEMENTATION_HPP_
#define TIGLCONTAINERSSUBGRAPHBASEIMPLEMENTATION_HPP_

/*****************************************************************************/
/*!
 * @file tiglContainersSubgraphBaseImplementation.hpp
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/

/******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/
#include <iostream>
#include <algorithm>
#include "tiglContainersGraphTraits.hpp"
#include "tiglUtils.hpp"
#ifdef CMEM_ENABLE
#include "tiglMemMsmcAllocator.hpp"
#include "tiglMemDdrAllocator.hpp"
#include <functional>
#include <cmem.h>
#include "tiglMemDdrAllocator.hpp"
#else
#include <memory>
#endif
#include "tiglContainersSubgraphBase.hpp"

/******************************************************************************
 *
 * MACROS
 *
 ******************************************************************************/
#define  DEFAULT_POS_HASH_SIZE      8192//2048//
#define  DEFAULT_POS_HASH_MASK      DEFAULT_POS_HASH_SIZE-1
#define ADD_VERTEX_FAILURE    0
#define ADD_VERTEX_NEWPOS  1
#define ADD_VERTEX_OLDPOS  2

template<typename ID_TYPE>
TIGLContainersPosInfo<ID_TYPE>::TIGLContainersPosInfo(ID_TYPE inId, uint_fast32_t inPos)
{
   pos = inPos;
   id = inId;
}
template<typename ID_TYPE>
template<typename T>
TIGLContainersPosInfo<ID_TYPE>::TIGLContainersPosInfo(TIGLContainersPosInfo<T> & inPos)
{
   pos = inPos.getPos();
   id = inPos.getId();
}
template<typename ID_TYPE>
TIGLContainersPosInfo<ID_TYPE>::~TIGLContainersPosInfo()
{
}

template<typename ID_TYPE>
inline ID_TYPE TIGLContainersPosInfo<ID_TYPE>::getId()
{
   return id;
}

template<typename ID_TYPE>
inline uint_fast32_t TIGLContainersPosInfo<ID_TYPE>::getPos()
{
   return pos;
}

template<typename ID_TYPE>
bool posCompare(TIGLContainersPosInfo<ID_TYPE> lhs, TIGLContainersPosInfo<ID_TYPE> rhs)
{
   return lhs.getId() < rhs.getId();
}

/*****************************************************************************/
/*!
 * @brief Constructor of TIGLContainersSubgraphBase
 * @param isDireted true if directed graph, false if undirected graph
 * @param assignedHashSize hash size of the position list (set to the default value if unassigned)
 *
 */
/*****************************************************************************/
template<typename CVertex>
TIGLContainersSubgraphBase<CVertex>::TIGLContainersSubgraphBase(bool isDireted, ID_TYPE assignedHashSize)
{
   flagDirected = isDireted;
   if (assignedHashSize == 0)
      posHashSize = DEFAULT_POS_HASH_SIZE;
   else
      posHashSize = assignedHashSize;
   //cout <<"posHashSize = " << posHashSize << endl;
//#ifdef CMEM_ENABLE
//   CMEM_init();
//#endif
#ifdef DEBUG_ENABLE
   cout<< "subgraph constructor 1: posHashSize " << posHashSize << endl;
#endif
   AllocPosTable();   //vertexPos = new INDEX_ELEMENT [posHashSize];
   posHoles.clear();
}

/*****************************************************************************/
/*!
 * @brief Overloaded constructor of TIGLContainersSubgraphBase
 * @param init_adj_list the initial adjacent list of the subgraph (in the form of vector of vertices)
 * @param  isDireted true if directed graph, false if undirected graph
 * @param  assignedHashSize hash size of the position list (set to the default value if unassigned)
 *
 */
/*****************************************************************************/
template<typename CVertex>
TIGLContainersSubgraphBase<CVertex>::TIGLContainersSubgraphBase(VERTEX_LIST_TYPE & init_adj_list, bool isDireted, ID_TYPE assignedHashSize)
{
   flagDirected = isDireted;
   vertexList = init_adj_list;

   if (assignedHashSize == 0)
      posHashSize = DEFAULT_POS_HASH_SIZE;
   else
      posHashSize = assignedHashSize;

#ifdef DEBUG_ENABLE
   cout<< "subgraph constructor 2: posHashSize" << posHashSize << endl;
#endif

//#ifdef CMEM_ENABLE
//   CMEM_init();
//#endif
   AllocPosTable();   //vertexPos = new INDEX_ELEMENT [posHashSize];
   uint_fast32_t pos = 0;
   for (VERTEX_ITERATOR it = vertexList.begin(); it != vertexList.end(); ++it)
   {
      ID_TYPE id = it->getID();
      vertexPos[hash(id)].insert(std::pair<ID_TYPE, uint_fast32_t>(id, pos));
      pos++;
   }
   posHoles.clear();
}

template<typename CVertex>
TIGLContainersSubgraphBase<CVertex>::~TIGLContainersSubgraphBase()
{
   // cleaning all possible assignments
   for (uint_fast32_t i = 0; i < posHashSize; i++)
      vertexPos[i].clear();

   DeallocPosTable();   //delete [] vertexPos;
}
template<typename CVertex>
void TIGLContainersSubgraphBase<CVertex>::AllocPosTable()
{
#ifdef DEBUG_ENABLE
   cout << "total size = " << posHashSize << "x" << sizeof(INDEX_ELEMENT) << " = " << (double)posHashSize * sizeof(INDEX_ELEMENT)/(1024.0*1024.0) << " MB" << endl;
#endif
   INDEX_ELEMENT emptyMap;
   vertexPos.insert(vertexPos.begin(), posHashSize, emptyMap);   // = vector<INDEX_ELEMENT, ALLOCATOR_TYPE>(posHashSize, emptyMap);//vertexPos.insert(vertexPos.begin(), posHashSize, emptyMap);//vertexPos = new INDEX_ELEMENT [posHashSize];
   //cout<< "posHashSize = " << posHashSize << endl;
#ifdef DEBUG_ENABLE
   cout << "--------------------------------------------------------------------" << endl;
#endif
}

/*****************************************************************************/
/*!
 * @brief Deallocates the position list
 *
 */
/*****************************************************************************/
template<typename CVertex>
void TIGLContainersSubgraphBase<CVertex>::DeallocPosTable()
{
   vertexPos.clear();
}

/*****************************************************************************/
/*!
 * @brief Gets the hashed position of a given id
 * @param id the id of the requested vertex
 * @return the hashed position of the vertex
 *
 */
/*****************************************************************************/
template<typename CVertex>
inline typename TIGLContainersSubgraphBase<CVertex>::ID_TYPE TIGLContainersSubgraphBase<CVertex>::hash(ID_TYPE & id)
{
   return (id) & (DEFAULT_POS_HASH_MASK);   // return id%posHashSize;
}

/*****************************************************************************/
/*!
 * @brief Checks the existence of a vertex
 * @param id the vertex id
 * @return true if the vertex exists in the local subgraph, otherwise false
 *
 */
/*****************************************************************************/
template<typename CVertex>
inline bool TIGLContainersSubgraphBase<CVertex>::exist(ID_TYPE id)
{
   return (find(vertexPos[hash(id)].begin(), vertexPos[hash(id)].end(), id) == vertexPos[hash(id)].end()) ? false : true;
}

/*****************************************************************************/
/*!
 * @brief Initialization procedure
 *
 */
/*****************************************************************************/
template<typename CVertex>
void TIGLContainersSubgraphBase<CVertex>::reset()
{
   vertexList.clear();
   for (uint_fast32_t i = 0; i < posHashSize; i++)
      vertexPos[i].clear();
}

/*****************************************************************************/
/*!
 * @brief Finds the position of a given vertex (from its id)
 * @details For core graph algorithms the performance of this function is critical
 * @param id TIGLApiVertex id
 * @param pos a reference in which the position is written
 * @return true if the vertex exist, false if the vertex does not exist
 */
/*****************************************************************************/
template<typename CVertex>
inline bool TIGLContainersSubgraphBase<CVertex>::findPos(ID_TYPE & id, uint_fast32_t & pos)
{
   INDEX_ELEMENT *posIt = &(vertexPos[hash(id)]);   //typename POS_LIST_TYPE::iterator posIt = vertexPos.begin()+hash(id);
   INDEX_ITERATOR itEnd = posIt->end();
   INDEX_ITERATOR it = lower_bound(posIt->begin(), itEnd, id);
   if (it == itEnd)
   {
      return false;
   }
   if (it->getId() != id)
   {
      return false;
   }

   pos = it->getPos();
//    pos = id;
   return true;
}

/*****************************************************************************/
/*!
 * @brief Adds a single vertex to the vertex list
 * @param x the new vertex (by reference)
 *
 */
/*****************************************************************************/
template<typename CVertex>
int TIGLContainersSubgraphBase<CVertex>::addVertex(CVertex & x)
{
   ID_TYPE id = x.getId();
   uint_fast32_t newPos;
   int stat;
   if (omp_get_num_threads() == 1)
   {
      if (posHoles.empty())
      {
         vertexList.push_back(x);
         newPos = vertexList.size() - 1;
         stat = ADD_VERTEX_NEWPOS;
      }
      else
      {
         newPos = posHoles.back();
         vertexList[newPos] = x;   // inserting in the old position
         posHoles.pop_back();
         stat = ADD_VERTEX_OLDPOS;
      }

      TIGLContainersPosInfo<ID_TYPE> newInfo(id, newPos);
      INDEX_ELEMENT *posIt = &(vertexPos[hash(id)]);   //typename POS_LIST_TYPE::iterator posIt = vertexPos.begin()+hash(id);
      if (posIt->empty())
         posIt->push_back(newInfo);
      else
      {
         INDEX_ITERATOR it = lower_bound(posIt->begin(), posIt->end(), id);

         if (it != posIt->end() && it->getId() == id)
         {
            cout << "XXXXXXXXXXX Error inserting: aborting" << id << endl;
            for (it = posIt->begin(); it != posIt->end(); ++it)
               cout << it->getId() << ", ";
            cout << endl;
            exit(1);
         }

         if (it == posIt->end())
            posIt->push_back(newInfo);
         else
            posIt->insert(it, newInfo);
      }
   }
   else
   {
      OMP("omp critical(VERTEX_MUTATE)")
      {
         if (posHoles.empty())
         {
            vertexList.push_back(x);
            newPos = vertexList.size() - 1;
            stat = ADD_VERTEX_NEWPOS;
         }
         else
         {
            newPos = posHoles.back();
            posHoles.pop_back();

            vertexList[newPos] = x;   // inserting in the old position

            stat = ADD_VERTEX_OLDPOS;
         }
      }
      TIGLContainersPosInfo<ID_TYPE> newInfo(id, newPos);
      INDEX_ELEMENT *posIt = &(vertexPos[hash(id)]);   //typename POS_LIST_TYPE::iterator posIt = vertexPos.begin()+hash(id);

      OMP("omp critical(POS_LOCK)")
      {
         if (posIt->empty())
            posIt->push_back(newInfo);
         else
         {
            INDEX_ITERATOR it = lower_bound(posIt->begin(), posIt->end(), id);

            if (it != posIt->end() && it->getId() == id)
            {
               cout << "XXXXXXXXXXX Error inserting: aborting" << id << endl;
               for (it = posIt->begin(); it != posIt->end(); ++it)
                  cout << it->getId() << ", ";
               cout << endl;
               exit(1);
            }

            if (it == posIt->end())
               posIt->push_back(newInfo);
            else
               posIt->insert(it, newInfo);
         }
      }
   }
   return stat;
}

/*****************************************************************************/
/*!
 * @brief Creates vertex list with trivial vertices
 * @param numVertices total number of vertices in the list
 *
 */
/*****************************************************************************/
template<typename CVertex>
void TIGLContainersSubgraphBase<CVertex>::createVerticesList(ID_TYPE & numVertices)
{
   for (ID_TYPE id = 0; id < numVertices; id++)
   {
      addVertex(id);
   }
}

/*****************************************************************************/
/*!
 * @brief Adds an empty vertex (by id)
 * @param id vertex id
 */
/*****************************************************************************/
template<typename CVertex>
void TIGLContainersSubgraphBase<CVertex>::addVertex(ID_TYPE & id)
{
   CVertex x(id);
   addVertex(x);
}

/*****************************************************************************/
/*!
 * @brief Adds a single vertex after checking its existence
 * @param x the new vertex (by reference)
 * @return true if the vertex is added propoerly, false if it already exists
 */
/*****************************************************************************/
template<typename CVertex>
bool TIGLContainersSubgraphBase<CVertex>::addVertex_consistent(CVertex & x)
{
   if (exist(x.getID()) == false)
   {
      addVertex(x);
      return true;
   }
   return false;
}

/*****************************************************************************/
/*!
 * @brief Removes a single vertex (by id) from the subgraph
 * @param id vertex id
 * @return true if successful, false if the vertex does not exist
 */
/*****************************************************************************/
template<typename CVertex>
bool TIGLContainersSubgraphBase<CVertex>::removeVertex(ID_TYPE & id)
{
   INDEX_ELEMENT *posIt = &(vertexPos[hash(id)]);   //typename POS_LIST_TYPE::iterator posIt = vertexPos.begin()+hash(id);
   INDEX_ITERATOR it;
   uint_fast32_t pos;

   if (omp_get_num_threads() == 1)
   {
      it = lower_bound(posIt->begin(), posIt->end(), id);   //posIt->find(id);
      if (it == posIt->end())
         return false;
      if (it->getId() != id)
         return false;
      pos = it->getPos();
      posIt->erase(it);
      vertexList[pos].clear();
      posHoles.push_back(pos);
      return true;
   }

   else
   {
      bool stat = true;
      OMP("omp critical(POS_LOCK)")
      {
         it = lower_bound(posIt->begin(), posIt->end(), id);   //posIt->find(id);
         if (it == posIt->end())
            stat = false;
         if (it->getId() != id)
            stat = false;
         if (stat == true)
         {
            pos = it->getPos();
            posIt->erase(it);
         }
      }
      if (stat == true)
      {   // updating the poshole
         vertexList[pos].clear();
         OMP("omp critical (VERTEX_MUTATE)")
         {
            posHoles.push_back(pos);
         }
      }
   }
   return true;
}

/*****************************************************************************/
/*!
 * @brief Adds a single unweighted edge to the output edge buffer of the source vertex
 * @param src source vertex
 * @param target destination vertex
 * @return true if successful, false if either vertex does not exist
 */
/*****************************************************************************/
template<typename CVertex>
bool TIGLContainersSubgraphBase<CVertex>::addOutEdge(ID_TYPE src, ID_TYPE target)
{
   uint_fast32_t pos;
   if (omp_get_num_threads() == 1)
   {
      if (findPos(src, pos) == false)
         return false;
   }
   else
   {
      bool stat;
      OMP("omp critical (POS_LOCK)")
      {
         stat = findPos(src, pos);
      }
      if (stat == false)
         return false;
   }

   VERTEX_ITERATOR itSrc = vertexList.begin() + pos;

   if (itSrc->isConnected(target))
      return false;

   itSrc->addEdge(target);
   return true;
}

/*****************************************************************************/
/*!
 * @brief (Overloaded function) Adds a single edge to the output edge buffer of the source vertex
 * @param src source vertex
 * @param edge edge object
 * @return true if successful, false if either vertex does not exist
 */
/*****************************************************************************/
template<typename CVertex>
bool TIGLContainersSubgraphBase<CVertex>::addOutEdge(ID_TYPE src, EDGE_TYPE edge)
{
   uint_fast32_t pos;
   if (omp_get_num_threads() == 1)
   {
      if (findPos(src, pos) == false)
         return false;
      VERTEX_ITERATOR itSrc = vertexList.begin() + pos;
      if (itSrc->isConnected(edge.getNbr()))
         return false;
      //
      itSrc->addEdge(edge);
      return true;
   }

   // at this point we have multiple threads
   bool stat;
   OMP("omp critical (POS_LOCK)")
   {
      stat = findPos(src, pos);
   }
   if (stat == false)
      return false;

   OMP("omp critical (VERTEX_MUTATE)")
   {
      VERTEX_ITERATOR itSrc = vertexList.begin() + pos;
      stat = itSrc->isConnected(edge.getNbr());
      if (stat == false)
         itSrc->addEdge(edge);
   }
   return stat == false ? true : false;   // Returns true if the two vertices are not originally connected
}

/*****************************************************************************/
/*!
 * @brief Adds a single edge to the input edge buffer (if exists) of the target vertex
 * @param src source vertex
 * @param target destination vertex
 * @return true if successful, false if either vertex does not exist
 */
/*****************************************************************************/
template<typename CVertex>
bool TIGLContainersSubgraphBase<CVertex>::addInEdge(ID_TYPE src, ID_TYPE target)
{

   uint_fast32_t pos;
   if (omp_get_num_threads() == 1)
   {
      if (findPos(target, pos) == false)
         return false;
      VERTEX_ITERATOR itTarget = vertexList.begin() + pos;

      itTarget->addInEdge(src);
      return true;
   }
   bool stat;
   OMP("omp critical (POS_LOCK)")
   {
      stat = findPos(target, pos);
   }
   if (stat == false)
      return false;

   OMP("omp critical (VERTEX_MUTATE)")
   {
      VERTEX_ITERATOR itTarget = vertexList.begin() + pos;
      itTarget->addInEdge(src);
   }
   return true;

}

/*****************************************************************************/
/*!
 * @brief (Overloaded function) Adds a single edge to the input edge buffer of the target vertex
 * @param target destination vertx vertex
 * @param edge edge object
 * @return true if successful, false if either vertex does not exist
 */
/*****************************************************************************/
template<typename CVertex>
bool TIGLContainersSubgraphBase<CVertex>::addInEdge(ID_TYPE target, EDGE_TYPE edge)
{
   uint_fast32_t pos;
   if (omp_get_num_threads() == 1)
   {
      if (findPos(target, pos) == false)
         return false;
      VERTEX_ITERATOR itTarget = vertexList.begin() + pos;

      itTarget->addInEdge(edge);
      return true;
   }
   bool stat;
   OMP("omp critical (POS_LOCK)")
   {
      stat = findPos(target, pos);
   }
   if (stat == false)
      return false;

   OMP("omp critical (VERTEX_MUTATE)")
   {
      VERTEX_ITERATOR itTarget = vertexList.begin() + pos;
      itTarget->addInEdge(edge);
   }
   return true;

}

/*****************************************************************************/
/*!
 * @brief Removes a single edge from the output edge buffer of the source vertex
 * @param src source vertex
 * @param target destination vertx vertex
 * @return true if successful, false if the edge or the vertex does not exist
 */
/*****************************************************************************/
template<typename CVertex>
bool TIGLContainersSubgraphBase<CVertex>::removeOutEdge(ID_TYPE src, ID_TYPE target)
{
   uint_fast32_t pos;
   if (omp_get_num_threads() == 1)
   {
      if (findPos(src, pos) == false)
         return false;

      VERTEX_ITERATOR itSrc = vertexList.begin() + pos;
      return itSrc->removeEdge(target);
   }

   bool stat;
   OMP("omp critical (POS_LOCK)")
   {
      stat = findPos(target, pos);
   }
   if (stat == false)
      return false;
   OMP("omp critical (VERTEX_MUTATE)")
   {
      VERTEX_ITERATOR itSrc = vertexList.begin() + pos;
      stat = itSrc->removeEdge(target);
   }
   return stat;

}

/*****************************************************************************/
/*!
 * @brief Removes a single edge from the input edge buffer of the target vertex
 * @param src source vertex
 * @param target destination vertx vertex
 * @return true if successful, false if the edge does not exist
 */
/*****************************************************************************/
template<typename CVertex>
bool TIGLContainersSubgraphBase<CVertex>::removeInEdge(ID_TYPE src, ID_TYPE target)
{
   uint_fast32_t pos;
   if (omp_get_num_threads() == 1)
   {
      if (findPos(target, pos) == false)
         return false;

      VERTEX_ITERATOR itTarget = vertexList.begin() + pos;
      return itTarget->removeInEdge(src);
   }
   bool stat;
   OMP("omp critical (POS_LOCK)")
   {
      stat = findPos(target, pos);
   }
   if (stat == false)
      return false;
   OMP("omp critical (VERTEX_MUTATE)")
   {
      VERTEX_ITERATOR itTarget = vertexList.begin() + pos;
      stat = itTarget->removeInEdge(src);
   }
   return stat;
}

/*****************************************************************************/
/*!
 * @brief Returns the total number of vertices in the subgraph
 */
/*****************************************************************************/
template<typename CVertex>
inline uint_fast32_t TIGLContainersSubgraphBase<CVertex>::getNumVertices()
{
   return vertexList.size();
}

/*****************************************************************************/
/*!
 * @brief Returns the total number of edges of local vertices
 */
/*****************************************************************************/
template<typename CVertex>
typename TIGLContainersSubgraphBase<CVertex>::ID_TYPE TIGLContainersSubgraphBase<CVertex>::getNumEdges()
{
   ID_TYPE numEdges = 0;
   for (VERTEX_ITERATOR it = begin(); it != end(); ++it)
      numEdges += it->getNumOutEdges();
   return isDirected() ? numEdges : numEdges / 2;
}

/*****************************************************************************/
/*!
 * @brief Returns an iterator to a vertex from its id
 */
/*****************************************************************************/
template<typename CVertex>
bool TIGLContainersSubgraphBase<CVertex>::getVertex(ID_TYPE & id, VERTEX_ITERATOR *it)
{
   uint_fast32_t pos;
   if (findPos(id, pos) == false)
      return false;

   *it = vertexList.begin() + pos;
   return true;
}

/*****************************************************************************/
/*!
 * @brief Returns the position of the vertex in the vertex list from its id
 *
 */
/*****************************************************************************/
template<typename CVertex>
inline uint_fast32_t TIGLContainersSubgraphBase<CVertex>::getVertexPos(ID_TYPE & id)
{
   uint_fast32_t pos;
   if (findPos(id, pos) == false)
      return vertexList.size();   // errror could not be found

   return pos;
}

/*****************************************************************************/
/*!
 * @brief Returns an iterator to a vertex from its position
 *
 */
/*****************************************************************************/
template<typename CVertex>
inline typename TIGLContainersSubgraphBase<CVertex>::VERTEX_ITERATOR TIGLContainersSubgraphBase<CVertex>::getIndexedVertex(ID_TYPE index)
{
   return vertexList.begin() + index;
}

/*****************************************************************************/
/*!
 * @brief Returns the start iterator of the vertex list
 *
 */
/*****************************************************************************/
template<typename CVertex>
inline typename TIGLContainersSubgraphBase<CVertex>::VERTEX_ITERATOR TIGLContainersSubgraphBase<CVertex>::begin()
{
   return vertexList.begin();
}

/*****************************************************************************/
/*!
 * @brief Returns the end iterator of the vertex list
 *
 */
/*****************************************************************************/
template<typename CVertex>
inline typename TIGLContainersSubgraphBase<CVertex>::VERTEX_ITERATOR TIGLContainersSubgraphBase<CVertex>::end()
{
   return vertexList.end();
}

/*****************************************************************************/
/*!
 * @brief Returns the local number of vertices
 *
 */
/*****************************************************************************/
template<typename CVertex>
inline uint_fast32_t TIGLContainersSubgraphBase<CVertex>::size()
{
//    cout << "posHoles : " ;
//    for(typename vector<uint_fast32_t>::iterator it = posHoles.begin(); it != posHoles.end();++it)
//       cout << *it << ", ";
//    cout << endl;
   return vertexList.size() - posHoles.size();
}

/*****************************************************************************/
/*!
 * @brief Returns a pointer to the vertex list
 */
/*****************************************************************************/
template<typename CVertex>
inline typename TIGLContainersSubgraphBase<CVertex>::VERTEX_LIST_PTR TIGLContainersSubgraphBase<CVertex>::getVerticesPtr()
{
   return &vertexList;
}

/*****************************************************************************/
/*!
 * @brief Checks whether two vertices are connected
 * @param src source vertex
 * @param target destination vertex
 * @return true if connected, false if not connected
 *
 */
/*****************************************************************************/
template<typename CVertex>
bool TIGLContainersSubgraphBase<CVertex>::isConnected(ID_TYPE & src, ID_TYPE & target)
{
   uint_fast32_t pos;
   if (findPos(src, pos) == false)
      return false;
   VERTEX_ITERATOR itSrc = vertexList.begin() + pos;
   return itSrc->isConnected(target);
}

/*****************************************************************************/
/*!
 * @brief Returns true if the graph is directed, false otherwise
 *
 */
/*****************************************************************************/
template<typename CVertex>
inline bool TIGLContainersSubgraphBase<CVertex>::isDirected()
{
   return flagDirected;
}

/*****************************************************************************/
/*!
 * @brief Returns the approximate overall memory usage of the graph in MB
 */
/*****************************************************************************/
template<typename CVertex>
uint_fast32_t TIGLContainersSubgraphBase<CVertex>::memoryUsage()
{
   // computing memory for the vertex list
   uint_fast32_t sizeVerticesList = sizeof(VERTEX_LIST_TYPE);
   for (VERTEX_ITERATOR it = begin(); it != end(); ++it)
   {
      uint_fast32_t vertexSize = sizeof(CVertex);
      vertexSize += it->getEdgesSize();
      sizeVerticesList += vertexSize;
   }
   // adding the overhead
   sizeVerticesList = (uint_fast32_t)((double) sizeVerticesList * LIST_MEM_OVERHEAD);

   uint_fast32_t sizePos = sizeof(INDEX_ELEMENT*);
   uint_fast32_t overallCapacity = 0;
   for (uint_fast32_t i = 0; i < posHashSize; i++)
      overallCapacity += vertexPos[i].size();

   sizePos += (sizeof(ID_TYPE) + sizeof(uint_fast32_t)) * overallCapacity;

   return sizeVerticesList + sizePos;
}

/*****************************************************************************/
/*!
 * @brief  Returns the detailed memory usage for the vertices and position buffers
 *
 */
/*****************************************************************************/
template<typename CVertex>
void TIGLContainersSubgraphBase<CVertex>::memory_usage(uint_fast32_t & sizeVerticesList, uint_fast32_t & sizePos)
{
   // computing memory for the vertex list
   sizeVerticesList = sizeof(VERTEX_LIST_TYPE);
   for (VERTEX_ITERATOR it = begin(); it != end(); ++it)
   {
      uint_fast32_t vertexSize = sizeof(CVertex);
      vertexSize += it->getEdgesSize();
      sizeVerticesList += vertexSize;
   }
   // adding the overhead
   sizeVerticesList = (uint_fast32_t)((double) sizeVerticesList * LIST_MEM_OVERHEAD);

   sizePos = sizeof(INDEX_ELEMENT*);
   uint_fast32_t overallCapacity = 0;
   for (uint_fast32_t i = 0; i < posHashSize; i++)
      overallCapacity += vertexPos[i].size();   //(sizeof(ID_TYPE) + sizeof(uint_fast32_t))*vertexPos.size();

   sizePos += (sizeof(ID_TYPE) + sizeof(uint_fast32_t)) * overallCapacity;

}

/*****************************************************************************/
/*!
 * @brief  Returns the maximum collision in the hash table of the position
 *
 */
/*****************************************************************************/
template<typename CVertex>
uint_fast32_t TIGLContainersSubgraphBase<CVertex>::MaxCollision()
{
   uint_fast32_t maxCollision = 0;
   for (uint_fast32_t i = 0; i < posHashSize; i++)
      if (vertexPos[i].size() > maxCollision)
         maxCollision = vertexPos[i].size();
   return maxCollision;
}

#ifdef DEBUG_ENABLE
void printBuffers()
{
   for(uint_fast32_t i=0;i<posHashSize;i++)
   {
      cout << i << ": ";
      for(INDEX_ITERATOR it = vertexPos[i].begin(); it !=vertexPos[i].end(); ++it)
      {
         cout << "(" << it->getId() << ", "<< it->getPos() <<"), ";
      }
      cout<<endl;
   }
}
bool checkConsistency()
{
   for(uint_fast32_t i=0;i<posHashSize;i++)
   {
      uint_fast32_t count = 0;
      for(INDEX_ITERATOR it = vertexPos[i].begin(); it !=vertexPos[i].end(); ++it)
      {
         if(it->getId() != i+count)
         return false;
         count += posHashSize;
      }
   }
   return true;
}
#endif

#endif /* TIGLCONTAINERSSUBGRAPHBASEIMPLEMENTATION_HPP_ */
