#ifndef TIGLCONTAINERSEDGE2SIDED_HPP_
#define TIGLCONTAINERSEDGE2SIDED_HPP_

/*****************************************************************************/
/*!
 * @file: tiglContainersEdge2Sided.hpp
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/

/******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/
#include <vector>
#include  <stdint.h>
#include "tiglContainersEdgeBase.hpp"

/******************************************************************************
 *
 * TIGLContainersEdge2Sided class
 *
 ******************************************************************************/

/*****************************************************************************/
/*!
 * @ingroup data_structures core
 * @class TIGLContainersEdge2Sided:
 * @brief Core edge class with information about the two sides
 * @details It has primitive edge information with both src and target information
 */
/*****************************************************************************/
template<typename idType = unsigned long>
class TIGLContainersEdge2Sided: public TIGLContainersEdgeBase<idType>
{
   /******************************************************************************
    *
    * constructor/destructor
    *
    ******************************************************************************/
public:
   TIGLContainersEdge2Sided(idType eSrc, idType eTarget)
   {
      src = eSrc;
      this->node = eTarget;
   }
   ~TIGLContainersEdge2Sided()
   {
   }

   /******************************************************************************
    *
    * attributes
    *
    ******************************************************************************/
protected:
   /// holds the id of the source vertex
   idType src;

   /******************************************************************************
    *
    * public methods
    *
    ******************************************************************************/
public:
   /*****************************************************************************/
   /*!
    * returns the id of the src vertex
    */
   /*****************************************************************************/
   inline idType getSrc()
   {
      return src;
   }

   /*****************************************************************************/
   /*!
    * returns the id of the target vertex
    */
   /*****************************************************************************/
   inline idType getTarget()
   {
      return this->node;
   }

   /*****************************************************************************/
   /*!
    * @brief Defines the "<"  operator definition for edge list comparisons
    * @details It compares the ID of the source vertex, and if equal it compares the ID of the target vertex
    */
   /*****************************************************************************/
   bool operator<(TIGLContainersEdge2Sided<idType> & rhs)
   {
      idType rhs_src = rhs.get_src();
      if (src < rhs_src)
         return true;
      else if (src == rhs_src)
         return this->node < rhs.get_target();
      return false;
   }

   /*****************************************************************************/
   /*!
    * @brief Defines the "=="  operator definition for edge list comparisons
    * @return true if both sides are equal, false otherwise
    */
   /*****************************************************************************/
   bool operator==(TIGLContainersEdge2Sided<idType> & rhs)
   {
      return (src == rhs.get_src() && this->node == rhs.get_target());
   }
};
#endif /* TIGLCONTAINERSEDGE2SIDED_HPP_ */
