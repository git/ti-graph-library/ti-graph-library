#ifndef TIGLCONTAINERSBIVERTEX_HPP_
#define TIGLCONTAINERSBIVERTEX_HPP_

/*****************************************************************************/
/*!
 *@file tiglContainersBivertex.hpp
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/

/******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/
#include <vector>
#include "tiglApiVertex.hpp"
#include "tiglApiMessage.hpp"
#ifdef CMEM_ENABLE
#include "tiglMemCmemAllocator.hpp"
#endif
#include "tiglContainersAggregate.hpp"
#include "tiglUtilsMacros.h"

/******************************************************************************
 *
 * NAMESPACES
 *
 ******************************************************************************/
using namespace std;

/******************************************************************************
 *
 * TIGLContainersBiVertex class
 *
 ******************************************************************************/

/*****************************************************************************/
/*!
 * @ingroup data_structures
 * @class TIGLContainersBiVertex
 * @brief Core vertex class with input and output edge buffers
 * @warning This class has not been used in the current release
 */
/*****************************************************************************/
template<typename DataType, typename AlgDataType, typename EdgeType, typename IdType = uint_fast32_t>
class TIGLContainersBiVertex: public TIGLContainersBiVertexBase<DataType, AlgDataType, EdgeType, IdType>
{
protected:
   bool flagActive;

public:
   /*****************************************************************************/
   /*!
    * @brief Deactivate the vertex
    */
   /*****************************************************************************/
   inline void vote_to_halt()
   {
      flagActive = false;
   }

   /*****************************************************************************/
   /*!
    * @brief Checks if vertex is active
    */
   /*****************************************************************************/
   inline bool isActive()
   {
      return flagActive;
   }

   /*****************************************************************************/
   /*!
    * @brief Activate the vertex
    */
   /*****************************************************************************/
   inline void activate()
   {
      flagActive = true;
   }
};

#endif /* TIGLCONTAINERSBIVERTEX_HPP_ */
