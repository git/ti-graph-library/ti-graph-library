#ifndef TIGLCONTAINERSBIVERTEXBASE_HPP_
#define TIGLCONTAINERSBIVERTEXBASE_HPP_

/*****************************************************************************/
/*!
 * @file tiglContainersBiVertexBase.hpp
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/

/******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/
#include "tiglContainersVertexBase.hpp"

/******************************************************************************
 *
 * NAMESPACE
 *
 ******************************************************************************/
using namespace std;

/******************************************************************************
 *
 * TIGLContainersBiVertexBase class
 *
 ******************************************************************************/

/*****************************************************************************/
/*!
 * @ingroup data_structures core
 *
 * @class TIGLContainersBiVertexBase
 * @brief A vertex class with both input and output edges lists
 * @details It inherits all the properties of regular vertex and has reference to input edges
 * @warning This class has not been used in the current release
 * @param DataType the type of the data field in the vertex
 * @param AlgDataType the type of algorithm data filed in the vertex
 * @param  EdgeType the edge type
 * @param IdType the data type of the ID field of each vertex
 */
template<typename DataType, typename AlgDataType, typename EdgeType, typename IdType = uint_fast32_t>
class TIGLContainersBiVertexBase: public TIGLContainersVertexBase<DataType, AlgDataType, EdgeType, IdType>
{

   /******************************************************************************
    *
    * type definitions
    *
    ******************************************************************************/
public:
   typedef TIGLContainersVertexBase<DataType, AlgDataType, EdgeType, IdType> VERTEX_TYPE;
   typedef typename VERTEX_TYPE::OUT_EDGE_ITERATOR EDGE_ITERATOR;
   typedef EDGE_ITERATOR IN_EDGE_ITERATOR;

   /******************************************************************************
    *
    * constructor/destructor
    *
    ******************************************************************************/
public:
   TIGLContainersBiVertexBase(IdType key) :
         TIGLContainersVertexBase<DataType, AlgDataType, EdgeType, IdType>(key)
   {
   }
   ~TIGLContainersBiVertexBase()
   {
   }

   /******************************************************************************
    *
    * attributes
    *
    ******************************************************************************/
protected:
   /// holds the id's of the input edges and the src vertices
   vector<EdgeType> inEdges;

   /******************************************************************************
    *
    * methods
    *
    ******************************************************************************/
public:
   /*****************************************************************************/
   /*!
    * @brief Adds an input edge
    */
   /*****************************************************************************/
   void addInEdge(EdgeType & x)
   {
      inEdges.push_back(x);
   }

   /*****************************************************************************/
   /*!
    * @ adding input edge
    */
   /*****************************************************************************/
   void addInEdge(IdType & src)
   {
      EdgeType newEdge(src);
      inEdges.push_back(newEdge);
   }

   /*****************************************************************************/
   /*!
    * @ adding input/outout edge(s)
    */
   /*****************************************************************************/
   void addInEdge(vector<EdgeType> & x)
   {
      for (IN_EDGE_ITERATOR it = x.begin(); it != x.end(); ++it)
         inEdges.push_back(*it);
   }

   /*****************************************************************************/
   /*!
    * @brief Removes an input edge
    */
   /*****************************************************************************/
   bool removeInEdge(IdType src)
   {
      for (IN_EDGE_ITERATOR it = inEdges.begin(); it != inEdges.end(); ++it)
         if (it->getNbr() == src)
         {
            inEdges.erase(it);
            return true;
         }
      return false;
   }

   /*****************************************************************************/
   /*!
    * @brief Returns the number of input edges
    */
   /*****************************************************************************/
   uint_fast32_t get_num_in_edges()
   {
      return inEdges.size();
   }

   /*****************************************************************************/
   /*!
    * @brief Returns id of all parents
    * @param buffer for writing the id's
    */
   /*****************************************************************************/
   void get_parents(vector<IdType> & vectParents)
   {
      for (IN_EDGE_ITERATOR it = inEdges.begin(); it != inEdges.end(); ++it)
         vectParents.push_back(it->getNbr());
   }

   /*****************************************************************************/
   /*!
    * @brief Returns the memory allocated for edge buffers
    */
   /*****************************************************************************/
   uint_fast32_t getEdgesSize()
   {
      return (inEdges.capacity() + (this->outEdges).capacity()) * sizeof(EdgeType);
   }

   /*****************************************************************************/
   /*!
    * @brief Returns pointer to the start of the input edge buffer
    */
   /*****************************************************************************/
   IN_EDGE_ITERATOR getBeginInEdgeIterator()
   {
      return inEdges.begin();
   }

   /*****************************************************************************/
   /*!
    * @brief Returns pointer to the end of the input edge buffer
    */
   /*****************************************************************************/
   IN_EDGE_ITERATOR getEndInEdgeIterator()
   {
      return inEdges.end();
   }

   /*****************************************************************************/
   /*!
    * @brief Checks if TIGLContainersBiVertex
    * @return true
    */
   /*****************************************************************************/
   bool isBiVertex()
   {
      return true;
   }   // overrides base class function
};

#endif /* TIGLCONTAINERSBIVERTEXBASE_HPP_ */
