/*****************************************************************************/
/*!
 * @file testGeneration.cpp
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/

/******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/
#include <iostream>
#include <vector>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "tiglContainersEdgeBase.hpp"
#include "tiglContainersVertexBase.hpp"
#include "tiglGenerationConstructGraphRmat.hpp"
#include "tiglGenerationGenerateEdgeList.hpp"
#include "tiglUtilsCountEdges.hpp"

/******************************************************************************
 *
 * Macros
 *
 ******************************************************************************/
#define NUM_VERTICES 	         1024
#define AV_OUT_EDGES	            8
#define	PROB_TEST_SEGMENTS	   5
#define	PROB_TEST_SIZE		      100000

/******************************************************************************
 *
 * type definitions
 *
 ******************************************************************************/
using namespace std;
typedef TIGLContainersEdgeBase<unsigned long> EDGE_TYPE;
typedef TIGLContainersVertexBase<int, double, EDGE_TYPE> CVertex;

/*****************************************************************************/
/*!
 * @ingroup tests
 * @defgroup gen_test Graph Generation
 */
/*****************************************************************************/

/*****************************************************************************/
/*!
 * @ingroup tests gen_test
 * @brief Testing the basic functionality of construct_graph_Rmat for constructing
 */
/*****************************************************************************/
int main(int argc, char *argv[])
{
   uint_fast32_t constructNumVertices;
   uint_fast32_t constructAvOutEdges;

   vector<CVertex> vertexList;
   vector<EDGE_TYPE> edgesList;
   double a = 0.55;
   double b = 0.1;
   double c = 0.1;
   double d = 0.25;

   if (argc == 1)
   {
      constructNumVertices = NUM_VERTICES;
      constructAvOutEdges = AV_OUT_EDGES;
   }
   else
   {
      constructNumVertices = atol(argv[1]);
      constructAvOutEdges = atol(argv[2]);
   }

   /// \b Procedure
   /// 1. Contsructing a graph using @link construct_graph_Rmat construct_graph_Rmat@endlink
   tiglGenerationConstructGraphRmat(constructNumVertices, constructAvOutEdges, vertexList, a, b, c, d, false, 0);

   cout << "total vertices = " << vertexList.size() << endl;
   /// 2. Run consistency check on the verticesLis id
   unsigned long id = 0;
   vector<CVertex>::iterator it;
   for (it = vertexList.begin(); it != vertexList.end(); ++it)
   {
      if (it->getId() != id)
      {
         cout << "!!!!!!! id error :" << id << "/" << it->getId() << endl;
         break;
      }
      id++;
   }
   if (it == vertexList.end())
      cout << "vertices id consistent " << endl;

   /// 3. Generate Edge List.
   tiglGenerationGenerateEdgeList(vertexList, edgesList);
   cout << "total Edges = " << edgesList.size() << endl;
   cout << endl;
   /// 4. Compute Edge Statistics to verify consistency with construction
   unsigned long Q1 = tiglUtilsCountEdges(vertexList, 0, constructNumVertices / 2 - 1, 0, constructNumVertices / 2 - 1);
   unsigned long Q2 = tiglUtilsCountEdges(vertexList, constructNumVertices / 2, constructNumVertices, 0, constructNumVertices / 2 - 1);
   unsigned long Q3 = tiglUtilsCountEdges(vertexList, 0, constructNumVertices / 2 - 1, constructNumVertices / 2, constructNumVertices);
   unsigned long Q4 = tiglUtilsCountEdges(vertexList, constructNumVertices / 2, constructNumVertices, constructNumVertices / 2, constructNumVertices);

   cout << "ratios: " << Q1 << ", " << Q2 << ", " << Q3 << ", " << Q4 << endl;

   /// 5. Verify the generation consistency
   double R1 = (double) Q1 / edgesList.size();
   double R2 = (double) Q2 / edgesList.size();
   double R3 = (double) Q3 / edgesList.size();
   double R4 = (double) Q4 / edgesList.size();

   cout << "ratios: " << R1 << ", " << R2 << ", " << R3 << ", " << R4 << endl;

   //now computing the error from theoretical values
   double NormError = fabs(R1 - a) + fabs(R2 - b) + fabs(R3 - c) + fabs(R4 - d);
   if (NormError > 1e-2)
      cout << "success !" << endl;
   else
      cout << "failed !" << "Error = " << NormError << endl;
}

