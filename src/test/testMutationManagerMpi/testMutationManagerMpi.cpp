/*****************************************************************************/
/*!
 * @file testMutationManagerMpi.cpp
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/

/******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/
#include <iostream>
#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <mpi.h>

#include "tiglApiTester.hpp"
#include "tiglApiVertex.hpp"
#include "tiglApiCombiner.hpp"
#include "tiglApiMessage.hpp"
#include "tiglUtils.hpp"

/******************************************************************************
 *
 * MACROS
 *
 ******************************************************************************/
#define NUM_VERTICES	1024
#define	AV_OUT_EDGES	8

/*****************************************************************************/
/*!
 * @ingroup tests
 * @defgroup mutation_test_mpi Mutation Manager with MPI
 */

/*****************************************************************************/
/*!
 * @ingroup tests mutation_test_mpi
 *
 * @brief Test Procedure mutation manager module for multiple devices
 */
/*****************************************************************************/

int main(int argc, char *argv[])
{
   uint_fast32_t constructNumVertices;

   /// \b Procedure
   /// 1. MPI intialization
   int rank, numDevices;
   MPI_Init(&argc, &argv);
   /* starts MPI */
   MPI_Comm_rank(MPI_COMM_WORLD, &rank);
   /* get current process id */
   MPI_Comm_size(MPI_COMM_WORLD, &numDevices);

   cout << endl << "-------------- TESTING MUTATION MANAGER MODULE ------------------" << endl << endl;

#if defined(_OPENMP)
   cout << "num of cores = " << omp_get_num_procs() << ", max threads =  " << omp_get_max_threads()<<
   ", num devices = " << numDevices << endl;
#else
   cout << "Single-ARM core" << ", num devices = " << numDevices << endl;
#endif

   if (argc != 3)
   {
      cout << "ERROR usage: testMutationManagerMPI <NumVertices> <AvEdgesPerVertex> <GraphFileName>!" << endl;
      cout << "  e.g.: testMutationManager1D 1024 adjList1024x8.txt" << endl;
#if defined(_MPI)
      MPI_Finalize();
#endif
      return -1;
   }
   constructNumVertices = atol(argv[1]);

   char adjFileName[1000];
   strcpy(adjFileName,argv[2]);

   // graph generation parameters

   double startTimeW = MPI_Wtime();
   //double startTime = tiglUtilsGetTimeUs();

   /// 2. Customizing the vertex object
   typedef TIGLApiVertex<char, char, TIGLContainersEdgeBase<uint_fast32_t>, char, uint_fast32_t, DblMessage> CVertex;

   /// 3. Initiating the TIGLApiTester object
   TIGLApiTester<CVertex, TIGLApiCombiner<DblMessage> > tester(1, (uint64_t) constructNumVertices, rank, numDevices);

   char processor_name[320];
   int name_len;
   MPI_Get_processor_name(processor_name, &name_len);

   /// 4. Testing ADD_EMPTY_VERTEX, ADD_OUT_EDGES with by calling @link TIGLApiTester::testMpiMutationManagerModule testMpiMutationManagerModule @endlink
   if (tester.testMpiMutationManagerModule(1, adjFileName) == false)
   {
      cout << "dev " << rank << " (" << processor_name << ") : XXXX TEST FAILS " << endl;
   }
   else
   {
      cout << "dev " << rank << " (" << processor_name << ") : +++ passed !" << endl;
   }
   double endTimeW = MPI_Wtime();
   cout << "dev " << rank << " (" << processor_name << ") : elapsed time = " << endTimeW - startTimeW << " sec" << endl;

   /// 6. Finalizing MPI
   MPI_BARRIER();
   MPI_Finalize();

   return 0;
}
