/*****************************************************************************/
/*!
 * @file testMutationManager1D.cpp
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/

/******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/
#include <iostream>
#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include "tiglApiTester.hpp"
#include "tiglApiVertex.hpp"
#include "tiglApiCombiner.hpp"
#include "tiglApiMessage.hpp"

/******************************************************************************
 *
 * MACROS
 *
 ******************************************************************************/
#define NUM_VERTICES	1024
#define	AV_OUT_EDGES	8

/*****************************************************************************/
/*!
 * @ingroup tests
 * @defgroup mutation_test_1D Mutation Manager with a single device
 */

/*****************************************************************************/
/*!
 * @ingroup tests  mutation_test_1D
 *
 * @brief Test procedure for mutation manager module for single device
 * @details It calls the TIGLApiTester procedure for mutation manager
 *
 * @remarks usage testTiglMutationManager1D.arm <NumVertices> <avEdgesPerVertex>
 */
/*****************************************************************************/
int main(int argc, char *argv[])
{
   uint_fast32_t constructNumVertices;

   cout << endl << "-------------- TESTING MUTATION MANAGER MODULE ------------------" << endl << endl;

#if defined(_OPENMP)
   cout << "num of cores = " << omp_get_num_procs() << ", max threads =  " << omp_get_max_threads()<<
   ", num devices = 1" << endl;
#else
   cout << "Single-ARM core" << ", num devices = 1" << endl;
#endif


   if (argc != 3)
   {
      cout << "ERROR usage: testMutationManager1D <NumVertices> <GraphFileName>!" << endl;
      cout << "  e.g.: testMutationManager1D 1024 adjList1024x8.txt" << endl;
      return -1;
   }

   constructNumVertices = atol(argv[1]);

   char adjFileName[1000];
   strcpy(adjFileName,argv[2]);

   /// \b Procedure

   /// 1. Customize vertex definition
   typedef TIGLApiVertex<char, char, TIGLContainersEdgeBase<uint_fast32_t>, char, uint_fast32_t, DblMessage> CVertex;

   /// 2. Initiating the TIGLApiTester object
   TIGLApiTester<CVertex, TIGLApiCombiner<DblMessage> > tester(1, (uint64_t) constructNumVertices, 0, 1);

   /// 3. Testing all mutation messages using @link TIGLApiTester::testMutationManagerModule testMutationManagerModule@endlink
   if (tester.testMutationManagerModule(adjFileName) == false)
   {
      cout << "XXXX TEST FAILS " << endl;
   }
   else
   {
      cout << "+++ passed !" << endl;
   }

   return 0;
}
