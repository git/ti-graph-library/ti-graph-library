################################################################################
#
# makeMpi.inc
#
# Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
#    Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#    Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the
#    distribution.
#
#    Neither the name of Texas Instruments Incorporated nor the names of
#    its contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
################################################################################

export OPAL_PREFIX=${TARGET_ROOTDIR}/opt/ti-openmpi
export PATH:=${TARGET_ROOTDIR}/opt/ti-openmpi/bin:$(PATH)
export LD_LIBRARY_PATH=${TARGET_ROOTDIR}/opt/ti-openmpi/lib:${TARGET_ROOTDIR}/lib
export C_INCLUDE_PATH=${TARGET_ROOTDIR}/opt/ti-openmpi/include


UNAME_M :=$(shell uname -m)

MPICC= ${TARGET_ROOTDIR}/opt/ti-openmpi/bin/mpic++

ifneq (,$(findstring 86, $(UNAME_M)))
    #.DEFAULT_GOAL := cross

    # In a cross compile environment we are assuming that the EVM file system
    # is located on the build host and necessary ARM libraries are installed
    # on that file system. 
    ifneq ($(MAKECMDGOALS),clean)
       ifeq ($(TARGET_ROOTDIR),)
         $(error Environment variable TARGET_ROOTDIR must be defined. Set it to point at the EVM root file system)
       endif
    endif

    # gcc ARM cross compiler will not, by default, search the host's
    # /usr/include.  Explicitly specify here to find dependent vendor headers
    #cross: override CPP = arm-linux-gnueabihf-g++ -I$(TARGET_ROOTDIR)/usr/include -idirafter /usr/include
	#override CPP = arm-linux-gnueabihf-g++ -I$(TARGET_ROOTDIR)/usr/include -idirafter /usr/include
	
    # If cross-compilineg, provide path to dependent ARM libraries on the 
    # target filesystem
    #cross: LD_FLAGS = -Wl,-rpath-link,$(TARGET_ROOTDIR)/opt/ti-openmpi/lib
    MPI_LD_FLAGS = -Wl,-rpath-link,$(TARGET_ROOTDIR)/opt/ti-openmpi/lib
endif


