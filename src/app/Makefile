################################################################################
#
# makefile
#
# Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
#    Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#    Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the
#    distribution.
#
#    Neither the name of Texas Instruments Incorporated nor the names of
#    its contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
################################################################################

################################################################################
#
# USE
#
#  make [TIGLDIR=<tigL source directory>][APPDIR=<examples directory>][HOST={X86,ARM}][NATIVE_COMPILE={Yes(default),No}][OPENMP={Yes(default),No}][DEBUG_ENABLE={No(defualt),Yes}] [PROFILE_ENABLE={No(defualt),Yes}]

# must define TARGET_ROOTDIR (NFS directory for cross-compile)
################################################################################
#default arguments
HOST := ARM#X86#
OPENMP := Yes
DEBUG_ENABLE := No
PROFILE_ENABLE := No
DEBUG_MPI := No
NATIVE_COMPILE := No

################################################################################
#
# DIRECTORIES
#
################################################################################

ifeq ($(HOST), ARM)
INCLUDEDIR = ${TARGET_ROOTDIR}/usr/include/
else	
INCLUDEDIR = /usr/include
endif  
#TIGLDIR := ..
TIGLDIR := $(TARGET_ROOTDIR)/usr/src/tigl/src
APPDIR := .
SRCDIR = ${TIGLDIR}/common
HEADERFILES = $(wildcard $(SRCDIR)/*/*.hpp)
INCLUDEDIR +=  $(sort $(dir $(wildcard $(SRCDIR)/*/)))

AUXSRC = $(wildcard $(SRCDIR)/utils/*.cpp)
AUXSRC += $(wildcard $(SRCDIR)/generation/*.cpp)
AUXSRC += $(wildcard $(SRCDIR)/communication/*.cpp)

##################################################################################
###
### COMPILER AND FLAGS
###
##################################################################################
ifeq ($(HOST), ARM)	
include ${APPDIR}/makeMpi.inc
else
MPICC = mpic++
MPI_LD_FLAGS = 
endif

MPI_BUILD_FLAGS += -D_MPI

ifeq ($(DEBUG_MPI), Yes)
  MPI_BUILD_FLAGS += -DDEBUG_MPI
endif

ifeq ($(HOST), X86)
CPP	=  g++
else #ARM
#ifeq ($(NATIVE_COMPILE), No)
CPP	= arm-linux-gnueabihf-g++
#else
#CPP	=  g++
#endif  
endif


 # setting compiling flags
CPPFLAGS := $(foreach dir, $(INCLUDEDIR),-I$(dir)) 
CPPFLAGS += -O3 -Wall -Wfatal-errors
# adding optional flags
ifeq ($(OPENMP), Yes)
  CPPFLAGS += -fopenmp
endif

ifeq ($(DEBUG_ENABLE), Yes)
  CPPFLAGS += -DDEBUG_ENABLE
endif

ifeq ($(PROFILE_ENABLE),Yes)
   CPPFLAGS += -DPROFILE_ENABLE
endif


################################################################################
#
# LIBRARIES
#
################################################################################
ifeq ($(HOST), ARM)
ifeq ($(NATIVE_COMPILE), No)
LD_FLAGS += -L$(TARGET_ROOTDIR)/lib -L$(TARGET_ROOTDIR)/usr/lib -Wl,-rpath-link,$(TARGET_ROOTDIR)/lib -Wl,-rpath-link,$(TARGET_ROOTDIR)/usr/lib
else
LD_FLAGS += -L/usr/lib -Wl,-rpath-link,/usr/lib
endif
endif

ifeq ($(HOST), X86)
LD_FLAGS += -lm -lrt
else
LD_FLAGS += -lrt
endif
CPP_LINK_FLAGS = -Wall

EXES=${APPDIR}/appPageRank/appPageRank
EXES+=${APPDIR}/appSSSP/appSSSP 
EXES+=${APPDIR}/appGenerateGraph/appGenerateGraph

MPICC_PRESENT := $(shell which $(MPICC) 2>/dev/null)

# IF MPI is present, add the below targets
ifdef MPICC_PRESENT
	EXES+= ${APPDIR}/appPageRank/appPageRankMpi 
	EXES+= ${APPDIR}/appSSSP/appSSSPMpi 
endif

all:$(EXES)

################################################################################
#
# BUILD
#
################################################################################

${APPDIR}/appPageRank/appPageRank: INCLUDEDIR += appPageRank
${APPDIR}/appPageRank/appPageRank: ${APPDIR}/appPageRank/appPageRank.cpp $(AUXSRC)
	@echo building appPageRank.cpp to $@
	@$(CPP) $(CPPFLAGS) $(CPP_LINK_FLAGS) $(AUXSRC) ${APPDIR}/appPageRank/appPageRank.cpp $(LD_FLAGS) -o $@ 

${APPDIR}/appPageRank/appPageRankMpi: INCLUDEDIR += appPageRank
${APPDIR}/appPageRank/appPageRankMpi: ${APPDIR}/appPageRank/appPageRank.cpp $(AUXSRC)
	@echo building appPageRank.cpp to $@
	@$(MPICC) $(CPPFLAGS) $(MPI_BUILD_FLAGS) $(CPP_LINK_FLAGS) $(AUXSRC) ${APPDIR}/appPageRank/appPageRank.cpp $(MPI_LD_FLAGS) $(LD_FLAGS) -o $@ 
	

${APPDIR}/appSSSP/appSSSP: INCLUDEDIR += appSSSP
${APPDIR}/appSSSP/appSSSP: ${APPDIR}/appSSSP/appSSSP.cpp $(AUXSRC)
	@echo building  appSSSP.cpp to $@
	@$(CPP) $(CPPFLAGS) $(CPP_LINK_FLAGS) $(AUXSRC)  ${APPDIR}/appSSSP/appSSSP.cpp $(LD_FLAGS) -o $@ 

${APPDIR}/appSSSP/appSSSPMpi: INCLUDEDIR += appSSSP
${APPDIR}/appSSSP/appSSSPMpi: ${APPDIR}/appSSSP/appSSSP.cpp $(AUXSRC)
	@echo building  appSSSP.cpp to $@
	@$(MPICC) $(CPPFLAGS) $(MPI_BUILD_FLAGS) $(CPP_LINK_FLAGS) $(AUXSRC) ${APPDIR}/appSSSP/appSSSP.cpp $(MPI_LD_FLAGS) $(LD_FLAGS) -o $@ 

${APPDIR}/appGenerateGraph/appGenerateGraph: ${APPDIR}/appGenerateGraph/appGenerateGraph.cpp $(AUXSRC)
	@echo building  appGenerateGraph.cpp to $@
	@$(CPP) $(CPPFLAGS) $(CPP_LINK_FLAGS) $(AUXSRC) ${APPDIR}/appGenerateGraph/appGenerateGraph.cpp $(LD_FLAGS) -o $@ 

clean: 
	# no object files are built	 
 
