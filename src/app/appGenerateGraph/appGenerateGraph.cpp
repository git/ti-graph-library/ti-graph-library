/*****************************************************************************/
/*!
 * @file appGenerateGraph.cpp
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/

/******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/
// general
#include <iostream>
#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include  <stdint.h>

//tools
#include "tiglUtils.hpp"
#include "tiglRW.hpp"
#include "tiglGenerationConstructGraphRmat.hpp"
#include "tiglGenerationRemoveSelfCycles.hpp"

/******************************************************************************
 *
 * MACROS, GLOBAL DEFINITIONS
 *
 ******************************************************************************/
#define DIRECTED_FLAG	(bool)false
#define NUM_PARTITIONS   4

using namespace std;

/*****************************************************************************/
/*!
 * @ingroup appGroup
 * @defgroup graphGenertor Graph Generation
 * @brief The application for generating graphs with different statistics
 */
/*****************************************************************************/

/*****************************************************************************/
/*!
 * @ingroup   appGroup graphGenertor
 * @brief The main() function of the generateGraph application.
 * @details Generating a graph using R-mat and writing it to a file. It also generate NUM_PARTITIONS partitions for subgraphs.
 * @param   argc Argument count
 * @param   argv Argument vector
 * @remark \b usage:  generateGraph <N> <e>    (generates a graph with N vertices and an average of e edges/vertex)
 * @return  Status
 */
/*****************************************************************************/

int main(int argc, char *argv[])
{
   uint_fast32_t constructNumVertices;
   uint_fast32_t constructAvOutEdges;

   /// body
   if (argc != 4)
   {
      cout << "ERROR usage: appGenerateGraph <NumVertices> <AvEdgesPerVertex> <outGraphFileName>!" << endl;
      cout << "  e.g.: appGenerateGraph 1024 8 adjList1024x8.txt" << endl;
#if defined(_MPI)
      MPI_Finalize();
#endif
      return -1;
   }
   else
   {
      constructNumVertices = atol(argv[1]);
      constructAvOutEdges = atol(argv[2]);
   }
   cout << "------ generating a graph with " << constructNumVertices << " vertices and " << constructAvOutEdges << " edges " << endl;

   // graph generation parameters
   char adjFileName[1000];
   strcpy(adjFileName,argv[3]);

   /// 1. Defining the edge and vertex classes
   typedef TIGLContainersEdgeBase<uint_fast32_t> EDGE_TYPE;
   typedef TIGLContainersVertexBase<char, char, EDGE_TYPE, uint_fast32_t> CVertex;
   vector<CVertex> initVerticesList;
   double a = 0.55;
   double b = 0.1;
   double c = 0.1;
   double d = 0.25;
   /// 2. Calling the Rmat graph generation function
   tiglGenerationConstructGraphRmat(constructNumVertices, constructAvOutEdges, initVerticesList, a, b, c, d, DIRECTED_FLAG, 0);

   if (tiglUtilsCheckSymmetry(initVerticesList) == false)
      cout << " !!!!!! Warning: symmetry test failed ! " << endl;

   /// 3. Removing self cycles
   tiglGenerationRemoveSelfCycles(initVerticesList);
   /// 4. Writing the whole graph to a file
   TIGLRwWriteAdjList(initVerticesList, adjFileName);

   /// 5. Writing partitioned graph to a file
   tiglGenerationPartitionGraph(adjFileName, NUM_PARTITIONS);
   cout << " Graph generation completed " << endl;
   return 0;
}
