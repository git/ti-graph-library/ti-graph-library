#ifndef APPSSSPVERTEXSEQUENTIAL_HPP_
#define APPSSSPVERTEXSEQUENTIAL_HPP_

/*****************************************************************************/
/*!
 * @file appSSSPVertexSequential.hpp
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/

/******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/
#include "tiglContainersVertexBase.hpp"
#include "tiglUtilsMacros.h"

/******************************************************************************
 *
 * SSSPNodeSeq structure
 *
 ******************************************************************************/

/*****************************************************************************/
/*!
 * @ingroup appGroup SSSP
 *
 * @struct SSSPNodeSeq
 * @brief  A structure that contains algorithm data for Dijkstra vertex
 * @details It is a template class that is parameterized by the distance type and the ID type
 */
/*****************************************************************************/
template<typename DIST_TYPE, typename ID_TYPE>
struct SSSPNodeSeq
{
   /// @param holds distance to the source.
   DIST_TYPE distance;

   /// @param holds the parent in the shortest route to the source.
   ID_TYPE parent;
};

/******************************************************************************
 *
 * COLOR enumeration
 *
 ******************************************************************************/

/*****************************************************************************/
/*!
 * @ingroup appGroup SSSP
 *
 * @enum COLOR
 * @brief Possible colors of the vertices during the algorithm execution
 */
/*****************************************************************************/
enum COLOR
{
   WHITE = 0, GRAY, PSEUDO_BLACK, BLACK
};

/******************************************************************************
 *
 * SSSPVertexSeq class
 *
 ******************************************************************************/

/*****************************************************************************/
/*!
 * @ingroup appGroup SSSP
 * @class SSSPVertexSeq
 * @brief Vertex class for the sequential implementation of the SSSP algorithm
 * @details It inherits TIGLContainersVertexBase class and with algorithm data of type SSSPNodeSeq
 */
/*****************************************************************************/
template<typename DataType, typename EdgeType, typename IdType = unsigned long>
class SSSPVertexSeq: public TIGLContainersVertexBase<DataType, SSSPNodeSeq<typename EdgeType::DATA_TYPE, IdType>, EdgeType, IdType>
{
public:
   typedef SSSPNodeSeq<typename EdgeType::DATA_TYPE, IdType> ALG_DATA_TYPE;

   /******************************************************************************
    *
    * constructor/destructor
    *
    ******************************************************************************/
public:
   SSSPVertexSeq(IdType key) :
         TIGLContainersVertexBase<DataType, ALG_DATA_TYPE, EdgeType, IdType>(key)
   {
   }
   ~SSSPVertexSeq()
   {
   }

   /******************************************************************************
    *
    * attributes
    *
    ******************************************************************************/
protected:
   COLOR color;

   /******************************************************************************
    *
    * methods
    *
    ******************************************************************************/
public:

   /*****************************************************************************/
   /*!
    * @brief initialing each vertex (other than the source vertex)
    */
   /*****************************************************************************/
   void init()
   {
      /*initializing the algData field and calling the base initialization function*/
      this->algData.parent = ILLEGAL_NODE;
      this->algData.distance = BIG_DBL_NUMBER;   //LARGE_NUMBER;
      TIGLContainersVertexBase<DataType, ALG_DATA_TYPE, EdgeType, IdType>::init();
   }

   /*****************************************************************************/
   /*!
    *  @brief initialing the source vertex
    */
   /*****************************************************************************/
   void init_src()
   {
      /*initializing the algData field and calling the base initialization function*/
      this->algData.parent = ILLEGAL_NODE;
      this->algData.distance = 0;
      color = WHITE;
      TIGLContainersVertexBase<DataType, ALG_DATA_TYPE, EdgeType, IdType>::init();
   }

   /*****************************************************************************/
   /*!
    * @brief updating the distance
    */
   /*****************************************************************************/
   bool compute(void * pIn = 0, void * pOut = 0)
   {
      ALG_DATA_TYPE * p = (ALG_DATA_TYPE *) pIn;
      if ((p->distance) < this->algData.distance)
      {
         this->algData = *p;
         return true;
      }
      else
         return false;
   }

   /*****************************************************************************/
   /*!
    *  @brief changing the vertex color after processing
    */
   /*****************************************************************************/
   void changeColor()
   {
      switch (color) {
         case WHITE:
            color = GRAY;
            break;
         case GRAY:
            color = PSEUDO_BLACK;
            break;
         case PSEUDO_BLACK:
            color = BLACK;
            break;
         case BLACK:
            break;
      }
   }

   /*****************************************************************************/
   /*!
    * @brief returns the vertex color
    */
   /*****************************************************************************/
   COLOR getColor()
   {
      return color;
   }
};
#endif /* APPSSSPVERTEXSEQUENTIAL_HPP_ */
