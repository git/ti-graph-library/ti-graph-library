#ifndef APPSSSP_HPP_
#define APPSSSP_HPP_

/*****************************************************************************/
/*!
 * @file appSSSP.hpp
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/

/******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/
#include "tiglApiVertex.hpp"
#include "tiglApiMessage.hpp"
#include "tiglApiCombiner.hpp"
/******************************************************************************
 *
 * GLOBAL DEFINITIONS
 *
 ******************************************************************************/
using namespace std;

/******************************************************************************
 *
 * SSSPInfo class
 *
 ******************************************************************************/

/*****************************************************************************/
/*!
 * @ingroup appGroup SSSP
 *
 * @class SSSPInfo
 * @brief Defines the algorithm data field of the customized vertex class of the SSSP algorithm
 * @details It contains customized definition of the basic operators
 */
/*****************************************************************************/

class SSSPInfo
{
   /******************************************************************************
    *
    * Attributes
    *
    ******************************************************************************/
public:
   /// parent node in the shortest path
   uint_fast32_t parent;
   /// distance to the source
   double distance;

   /******************************************************************************
    *
    * Operator definitions
    *
    ******************************************************************************/
public:

   /*****************************************************************************/
   /*!
    * @brief subtracts the distance
    */
   /*****************************************************************************/
   template<typename T>
   double operator -(T &ref)
   {
      return ref.distance - distance;
   }

   /*****************************************************************************/
   /*!
    * @brief multiplies the distance
    */
   /*****************************************************************************/
   template<typename T>
   double operator *(T &ref)
   {
      return ref.distance * distance;
   }

   /*****************************************************************************/
   /*!
    /// @brief add the distance
    */
   /*****************************************************************************/
   template<typename T>
   double operator +(T &ref)
   {
      return ref.distance + distance;
   }

   /*****************************************************************************/
   /*!
    * @brief compare the distance field
    */
   /*****************************************************************************/
   bool operator <(SSSPInfo &ref)
   {
      return distance < ref.distance;
   }

   /*****************************************************************************/
   /*!
    * @brief compares the distance field
    */
   /*****************************************************************************/
   bool operator >(SSSPInfo &ref)
   {
      return distance > ref.distance;
   }

   /*****************************************************************************/
   /*!
    *  @brief adds the distance field
    */
   /*****************************************************************************/
   SSSPInfo & operator +=(SSSPInfo &ref)
   {
      distance += ref.distance;
      return *this;
   }

   /*****************************************************************************/
   /*!
    * @brief sets the distance field
    */
   /*****************************************************************************/
   template<typename T>
   void operator =(T distanceVal)
   {
      distance = distanceVal;
   }
};

/*****************************************************************************/
/*!
 * @brief prints the algorithm field to stdout
 */
/*****************************************************************************/
ostream& operator<<(ostream& os, const SSSPInfo& x)
{
   os << "(" << x.distance << ", " << x.parent << ")";
   return os;
}

/******************************************************************************
 *
 * SSSPVertex class
 *
 ******************************************************************************/

/*****************************************************************************/
/*!
 * @ingroup appGroup SSSP
 *
 * @class SSSPVertex
 * @brief Customized TIGLApiVertex class for the Single Source Shortest Path using the Dijkstra Algorithm
 * @details it inherits the core TIGLApiVertex class and defines the procedures for computing the SSSP algorithm.
 * it is a template class with the following parameter
 * @param DataType the type of the data field in a vertex
 * @param EdgeType  the type of the edge class
 */
/*****************************************************************************/

template<typename DataType = char, typename EdgeType = TIGLContainersEdgeBase<uint_fast32_t> >
class SSSPVertex: public TIGLApiVertex<DataType, SSSPInfo, EdgeType, uint_fast32_t, uint_fast32_t, DblMessage>
{
   /******************************************************************************
    *
    * type definitions
    *
    ******************************************************************************/
public:
   typedef typename TIGLApiVertex<DataType, double, TIGLContainersEdgeBase<uint_fast32_t>, uint_fast32_t, uint_fast32_t, DblMessage>::MESSAGE_BUF_TYPE MESSAGE_BUF_TYPE;
   typedef typename TIGLApiVertex<DataType, SSSPInfo, EdgeType, uint_fast32_t, uint_fast32_t, DblMessage>::OUT_EDGE_ITERATOR OUT_EDGE_ITERATOR;

   /******************************************************************************
    *
    * constructor/destructor
    *
    ******************************************************************************/
public:
   /*****************************************************************************/
   /*!
    * @brief constructor customizes TIGLApiVertex by disabling vertex processing in the absence of messages
    */
   /*****************************************************************************/
   SSSPVertex(uint_fast32_t key) :
         TIGLApiVertex<DataType, SSSPInfo, EdgeType, uint_fast32_t, uint_fast32_t, DblMessage>(key, false, false)
   {
   }

   ~SSSPVertex()
   {
   }

   /******************************************************************************
    *
    * class methods
    *
    ******************************************************************************/
public:

   /*****************************************************************************/
   /*!
    * @brief customized compute function
    * @param pIn pointer to the input message buffer
    * @param pOut pointer to the output message buffer
    * @param numMessages the number of input data messages
    * @return the number of output data messages
    */
   /*****************************************************************************/
   template<typename ALLOCATOR_TYPE>
   uint_fast32_t compute(MESSAGE_BUF_TYPE *pIn, vector<DblMessage, ALLOCATOR_TYPE> *pOut, uint_fast32_t numMessages)
   {
      uint_fast32_t t = this->aggregateInfoPtr->getNumSS();
      /// body

      /// 1. check the consistency of the input messages
//      if (numMessages > 1)
//         cout << "!!! Warning PregelSSSP compute: numMessages(" << this->getId() << ") = " << numMessages << endl;   //cout<<"!!! Warning : numMessages(" << this->getId() << ") = " << pIn->size() << endl;
      if (numMessages > pIn->size())
         cout << "!!! Warning PregelSSSP compute: numMessages is bigger than buffer size" << endl;

      /// 2. compute the new distance to source
      vector<DblMessage>::iterator itm = pIn->begin();
      double newDistance = itm->getContent();
      double newParent = itm->getSender();
      if(numMessages > 1)
      {
         for (uint_fast32_t loopCount = 1; loopCount < numMessages; loopCount++)   //itm != pIn->end(); ++itm)
         {
            ++itm;
            if (newDistance > itm->getContent())
            {
               newDistance = itm->getContent();
               newParent = itm->getSender();
            }
         }
      }

      /// 3. If the distance changes, write the new distance in messages to all neighbors
      uint_fast32_t numOutMessages = 0;
      if (newDistance < this->algData.distance)   // or visited before but we have bettern score
      {
         this->algData.distance = newDistance;
         this->algData.parent = newParent;

         // now sending message to all children excluding any nodes which already sent messages
         for (OUT_EDGE_ITERATOR ite = this->getBeginOutEdgeIterator(); ite != this->getEndOutEdgeIterator(); ++ite)
         {
            uint_fast32_t dst = ite->getNbr();
            if (dst == this->id)
               continue;   // ignoring self reference
            // checking if it already sent a message to current node don't sent back a message
            int flag = 0;
            for(vector<DblMessage>::iterator itIn = pIn->begin(); itIn != pIn->end(); ++itIn)
            {
               if(dst == itIn->getSender())
               {
                  double messContent = itIn->getContent();
                  if(ite->hasVal())
                  {
                     if(messContent  < newDistance + 2*ite->getData())
                     {
                        flag = 1;
                        break;
                     }
                  }
                  else if (messContent < newDistance + 2)
                  {
                     flag = 1;
                     break;
                  }
               }
            }
            if(flag)
               continue;


            double messageVal = ite->hasVal() ? newDistance + ite->getData() : newDistance + 1;

            DblMessage newMessage(this->id, dst, messageVal);

            pOut->push_back(newMessage);
            numOutMessages++;
         }
      }

      /// 4. If the distance equals the minimum distance, deactivate the vertex
      if (t > 1 && this->algData.distance <= (this->aggregateInfoPtr->getPastMinActiveAlgData()).distance)
      {
         this->vote_to_halt();
      }

      return numOutMessages;
   }   // end of compute

   /*****************************************************************************/
   /*!
    * @brief customized init function
    * @param src pointer to uint_fast32_t parameter that has the ID of the source vertex
    * @param pOut pointer to the output message buffer
    * @return the number of output data messages
    */
   /*****************************************************************************/
   template<typename ALLOCATOR_TYPE>
   uint_fast32_t init(uint_fast32_t *src, vector<DblMessage, ALLOCATOR_TYPE> *pOut)
   {
      /// body

      /// 1. If the vertex is the source vertex, send message to all neighbors and deactivate
      if (this->id == *src)
      {
         this->algData.distance = 0;
         this->algData.parent = *src;

         // now sending messages to children nodes to the act
         uint_fast32_t numOutMessages = 0;
         for (OUT_EDGE_ITERATOR ite = this->getBeginOutEdgeIterator(); ite != this->getEndOutEdgeIterator(); ++ite)
         {
            uint_fast32_t dst = ite->getNbr();
            if (dst == this->id)
               continue;   // ignoring self reference
            double messageVal = ite->hasVal() ? ite->getData() : 1;

            DblMessage newMessage(this->id, dst, messageVal);

            pOut->push_back(newMessage);
            numOutMessages++;
         }

         this->vote_to_halt();
         return numOutMessages;
      }

      /// 2. If the vertex is not the source vertex, activate and wait for messages
      else
      {
         this->algData.distance = BIG_DBL_NUMBER;
         this->algData.parent = UNDEFINED_NODE;

         this->activate();
         return 0;
      }
   }
};

/******************************************************************************
 *
 * SSSPCombiner class
 *
 ******************************************************************************/

/*****************************************************************************/
/*!
 * @ingroup appGroup SSSP
 *
 * @class SSSPCombiner
 * @brief Combiner class for the SSSP algorithm
 * @details it inherits the generic TIGLApiCombiner class and customizes the combining function to minimum combining
 */
/*****************************************************************************/
class SSSPCombiner: public TIGLApiCombiner<DblMessage>
{
   /******************************************************************************
    *
    * type definitions
    *
    ******************************************************************************/
protected:
   typedef typename vector<DblMessage>::iterator MESSAGE_ITERATOR;

   /******************************************************************************
    *
    * constructor/destructor
    *
    ******************************************************************************/
public:
   SSSPCombiner()
   {
   }
   ~SSSPCombiner()
   {
   }

   /******************************************************************************
    *
    * class methods
    *
    ******************************************************************************/
public:
   /*****************************************************************************/
   /*!
    * @brief customized combine function the combines a set of messages that calls the generic minimum combine
    * @param itIn Iterator to input messages
    * @param numMessages number of input messages
    * @param itOut iterator to the start of the ouput message
    * @return the number of combined output data messages
    */
   /*****************************************************************************/
   uint_fast32_t combineMessages(MESSAGE_ITERATOR itIn, uint_fast32_t numMessages, MESSAGE_ITERATOR itOut)
   {
//      return minCombineMessages(itIn, numMessages, itOut);
      // copying input to output
      return bypassCombineMessages(itIn, numMessages, itOut);
   }

   /*****************************************************************************/
   /*!
    * @brief overloaded customized combine function that combines a single message
    * @param newMessage the new message
    * @param itOut iterator to the start of the ouput message
    * @return the number of combined output data messages
    */
   /*****************************************************************************/
   uint_fast32_t combineMessages(DblMessage & newMessage, MESSAGE_ITERATOR itOut)
   {
      return minCombineMessages(newMessage, itOut);
   }

};

#endif /* APPSSSP_HPP_ */
