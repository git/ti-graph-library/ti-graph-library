#ifndef APPSSPSEQUENTIAL_HPP_
#define APPSSPSEQUENTIAL_HPP_

/*****************************************************************************/
/*!
 * @file appSSSPSequential.hpp
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/

/******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/
#include "appSSSPVertexSequential.hpp"
#include <vector>
#include <map>

/******************************************************************************
 *
 * MACROS
 *
 ******************************************************************************/
#define		ILLEGAL_NODE   -1
/*****************************************************************************/
/*!
 * @ingroup appGroup SSSP
 * @brief Sequential implementation of the single-source shortest path algorithm.
 *
 * @details This is a template function that runs the SSSP algorithm on an adjacency list of vertices. The output of the PageRank algorithm is stored in the algData field of each vertex
 * @param vertexList a vector that contains the adjacency list. The input vertices must be of type SSSPVertexSeq
 * @param src the ID of the source vertex
 */
template<typename CVertex>
void dijkstra(vector<CVertex> & vertexList, typename CVertex::ID_TYPE src)
{

   typedef typename CVertex::ID_TYPE ID_TYPE;
   typedef typename CVertex::EDGE_TYPE::DATA_TYPE DIST_TYPE;
   typedef typename CVertex::OUT_EDGE_ITERATOR OUT_EDGE_ITERATOR;
   typedef typename CVertex::ALG_DATA_TYPE ALG_NODE;
   typedef std::multimap<DIST_TYPE, ID_TYPE> DIJKSTRA_MAP; /*parameters denote distance and vertex id*/

   typedef typename vector<CVertex>::iterator VERTEX_ITERATOR;

   DIJKSTRA_MAP Q;   // queue list
   ALG_NODE tmpNode; /*parametes denote distance and parent*/

   /// body
   /// 1. initializing all vertices
   for (VERTEX_ITERATOR it = vertexList.begin(); it != vertexList.end(); ++it)
      it->init();   // set it to while

   /// 2. push the source vertex to the active queue
   Q.insert(std::pair<DIST_TYPE, ID_TYPE>(0, src));
   vertexList[src].init_src();
   vertexList[src].changeColor();   //color[src] = GRAY;

   while (Q.empty() == false)
   {
      /// 3. Pop all vertices from active list and update their children
      typename DIJKSTRA_MAP::iterator itq = Q.begin();
      ID_TYPE nodeIndex = itq->second;

      if (vertexList[nodeIndex].getColor() == BLACK)
      { /* already visited with better distance*/
         Q.erase(itq);
         continue;
      }
      /* now studying the impact on children*/
      for (OUT_EDGE_ITERATOR it = vertexList[nodeIndex].getBeginOutEdgeIterator(); it != vertexList[nodeIndex].getEndOutEdgeIterator(); ++it)
      {
         ID_TYPE childIndex = it->getNbr();
         if (vertexList[childIndex].getColor() == BLACK)
            continue;

         tmpNode.distance = vertexList[nodeIndex].getAlgData().distance + it->getData();
         tmpNode.parent = nodeIndex;

         bool isChanged = vertexList[childIndex].compute(&tmpNode);

         if (isChanged == true)
         { /*If there is improvement insert the update distance in teh queue*/
            /*If not already in the QUEUE-> change color*/
            if (vertexList[childIndex].getColor() == WHITE)
               vertexList[childIndex].changeColor();

            Q.insert(std::pair<DIST_TYPE, ID_TYPE>(tmpNode.distance, childIndex));
         }

      }
      /// 4. continue until the active queue becomes empty

      /*after processing the node: change color to BLACK*/
      vertexList[nodeIndex].changeColor();
      vertexList[nodeIndex].changeColor();
      Q.erase(Q.begin());
   }
}

#endif /* APPSSPSEQUENTIAL_HPP_ */
