/*****************************************************************************/
/*!
 * @file appSSSP.cpp
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/
/******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/
// general
#include <iostream>
#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

// TIGL
#include "appSSSP.hpp"
#include "tigl.hpp"
#include "appSSSPSequential.hpp"

/******************************************************************************
 *
 * MACROS, GLOBAL DEFINITIONS
 *
 ******************************************************************************/
#define DIRECTED_FLAG         (bool)false
#define	MAX_SS			       30//2//1//
#define RUN_REPETITION	       1
#define COMPARE_SEQUENTIAL	    1//1
#define	SRC_NODE		          0
/*****************************************************************************/

/*****************************************************************************/
/*!
 * @ingroup appGroup
 * @defgroup SSSP The Single-Source Shortest Path (SSSP) Algorithm
 * @brief The application for demonstrating the Single-Source Shortest Path (SSSP) algorithm using TIGL
 */
/*****************************************************************************/

/*****************************************************************************/
/*!
 * @ingroup appGroup SSSP
 * @brief The main() function of the SSSP application.
 * @details Runs the Single-Source Shortest Path (SSSP) algorithm on a graph and stores the distance and parent of each vertex in the algorithm data field
 * @param   argc Argument count
 * @param   argv Argument vector
 * @remark \b usage: <outFileName>  <numVerteices>  <avNumEdges>
 * @return  Status
 */
/*****************************************************************************/

int main(int argc, char *argv[])
{
   uint_fast32_t numVertices;
   uint_fast32_t numEdges;
   uint_fast32_t constructNumVertices;
//   uint_fast32_t constructAvOutEdges;
   int rank, numDevices;

   /// body

   /// 1. MPI initialization (only in case of multiple devices)
#if defined(_MPI)
   MPI_Init (&argc, &argv);
   /* starts MPI */
   MPI_Comm_rank (MPI_COMM_WORLD, &rank);
   /* get current process id */
   MPI_Comm_size (MPI_COMM_WORLD, &numDevices);
   /* get number of processes */
#else
   rank = 0;
   numDevices = 1;
#endif

   if (rank == 0)
      cout << "------ Single-Source Shortest-Path ALGORITHM" << endl;

#if defined(_OPENMP) 
   if(rank == 0)
   cout << "num of cores = " << omp_get_num_procs() << ", max threads =  " << omp_get_max_threads()<<
   ", num devices = " << numDevices << endl;
#else
   if (rank == 0)
      cout << "Single-ARM core" << ", num devices = " << numDevices << endl;
#endif

   if (argc != 4)
   {
      cout << "ERROR usage: appSSSP <NumVertices> <Compare = Yes/No> <GraphFilename>!" << endl;
      cout << "  e.g.: appSSSP 1024 Yes adjList1024x8.txt" << endl;
#if defined(_MPI)
      MPI_Finalize();
#endif
      return -1;
   }

   /// 2. Reading Graph Parameters
   constructNumVertices = atol(argv[1]);
//   constructAvOutEdges = atol(argv[2]);
   int COMPARE_FLAG = 1;
   if (strcmp(argv[2], "no") == 0 || strcmp(argv[2], "No") == 0 || strcmp(argv[2], "NO") == 0)
      COMPARE_FLAG = 0;

   char adjFileName[1000];
   strcpy(adjFileName,argv[3]);

   /// 3. Initializing the %TIGL engine with proper vertex definition for the SSSP algorithm
   TIGL<SSSPVertex<char, TIGLContainersEdgeBase<uint_fast32_t> >, SSSPCombiner> tiglInterface(1, (uint64_t) constructNumVertices, rank, numDevices);

   /// 4. Reading the Graph file
//   cout << "tigl initialized" << endl;

   if (rank == 0)
      cout << "Available Memory = " << tiglUtilsAvailableMemory() << endl;
#if defined(_MPI)
   if (tiglInterface.networkReadGraph(0, adjFileName) == false)
   {
      cout << "ERROR: cannot read graph file " << adjFileName << endl;
      return -1;
   }
#else
   if (tiglInterface.readSubgraphFromFile(adjFileName) == false)
   {
      cout << "ERROR: cannot read graph file " << adjFileName << endl;
      return -1;
   }
#endif

   if (rank == 0)
      cout << "building graph " << endl;
   MPI_BARRIER();

   numVertices = tiglInterface.getNumVertices();
   numEdges = tiglInterface.getNumEdges();

   if (rank == 0)
   {
      cout << "dev" << rank << ":total vertices = " << numVertices << ", total edges = " << numEdges << endl;
      cout << "dev" << rank << ":max collision = " << tiglInterface.MaxCollision() << endl;
      cout << "dev" << rank << ":Graph Initialized " << endl;
   }

   double startTime = tiglUtilsGetTimeUs();

   uint_fast32_t initParams = SRC_NODE;

   /// 5. running the %TIGL engine to execute the SSSP algorithm on the graph
   for (int i = 0; i < RUN_REPETITION; i++)
      tiglInterface.run(MAX_SS, &initParams);

   /// 6. Printing profiling information
   double endTime = tiglUtilsGetTimeUs();
   if (rank == 0)
   {
      cout << "Execution time = " << (uint_fast32_t)(endTime - startTime) / 1000 / RUN_REPETITION << " ms" << endl;

      cout << "total Memory = " << (double) tiglInterface.memoryUsage() / 1000000 << " MB" << endl;
      cout << "numSS = " << tiglInterface.numSS() << endl;
   }

#if COMPARE_SEQUENTIAL
   /// 7. (optional) Running the reference sequential implementation of the page rank algorithm
   if (COMPARE_FLAG)
   {
      typedef SSSPVertexSeq<char, TIGLContainersEdgeBase<uint_fast32_t>, uint_fast32_t> CVertex;
      vector<CVertex> seqVector;

      TIGLRwReadAdjList(seqVector, adjFileName);

      startTime = tiglUtilsGetTimeUs();
      dijkstra(seqVector, SRC_NODE);
      endTime = tiglUtilsGetTimeUs();
      if (rank == 0)
         cout << "Sequential Execution time = " << (uint_fast32_t)(endTime - startTime) / 1000 << " ms" << endl;

      /// 8. (optional) Comparing the output results with the reference implementation
      double mse = tiglInterface.computeMSE(seqVector);
      double snr = tiglInterface.computeSNR(seqVector);

      if (rank == 0)
      {
         cout << "SNR = " << snr << " dB" << endl;
         cout << "MSE = " << mse << endl;
      }
   }
#endif

#ifdef	PROFILE_ENABLE
   /// 9. (optional) Printing profiling informaiton
   tiglInterface.PrintProfilingInfo();
   tiglInterface.printAlgData2File("pregelPageR.txt");
#endif

#if defined(_MPI)
   /// 10. clean MPI
   MPI_Finalize();
#endif
   return 0;
}
