#ifndef APPPAGERANKSEQUENTIAL_HPP_
#define APPPAGERANKSEQUENTIAL_HPP_

/*****************************************************************************/
/*!
 * @file appPageRankSequential.hpp
 */

/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/

/******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/
#include <vector>
#include <set>
#include "appPageRankVertexSequential.hpp"

/******************************************************************************
 *
 * MACROS
 *
 ******************************************************************************/

#define  MAX_ITER       100

/*****************************************************************************/
/*!
 * @ingroup appGroup pageRank
 *
 * @brief Sequential implementation of the Page Rank algorithm
 * @details This is a template function that runs the Page Rank algorithm on an adjacency list of vertices. The output of the PageRank algorithm is stored in the algData field of each vertex
 * @param vertexList a vector that contains the adjacency list
 * @param maxIter the maximum number of iterations in the Page Rank algorithm
 * @return the number of iterations
 */
/*****************************************************************************/
template<typename CVertexPageRank>
unsigned long PageRank(vector<CVertexPageRank> & vertexList, double maxIter = MAX_ITER)
{
   typedef typename vector<CVertexPageRank>::iterator VERTEX_ITERATOR;
   typedef typename CVertexPageRank::ID_TYPE IdType;

   vector<VERTEX_ITERATOR> Q;   // queue list that holds iterators to active nodes
   set<IdType> Q2;   //queue list that holds the indices of active node for the following iteration

   IdType numVertices = vertexList.size();

   /// body

   /// 1. Initializing all nodes
   for (VERTEX_ITERATOR it = vertexList.begin(); it != vertexList.end(); ++it)
   {
      it->init(numVertices);
      Q.push_back(it);   // list of active nodes(initially everything is active)
   }

   /// 2. update the page rank of all vertices until the maximum number of iterations is reached
   unsigned long numIter = 0;
   while (Q.empty() == false && numIter < maxIter)
   {
      Q2.clear();
      while (Q.empty() == false)
      {   // update R loop
         VERTEX_ITERATOR verIter = Q.back();
         bool isChanged = verIter->compute(&vertexList);
         if (isChanged == true)
         {
            vector<IdType> children;
            verIter->getChildren(children);
            for (typename vector<IdType>::iterator it = children.begin(); it != children.end(); ++it)
               Q2.insert(*it);
         }
         Q.pop_back();
      }

      VERTEX_ITERATOR itStart = vertexList.begin();
      /*copying to Q for the following iteration*/
      for (typename set<IdType>::iterator itm = Q2.begin(); itm != Q2.end(); ++itm)
      {
         Q.push_back(itStart + *itm);
      }
      numIter++;
   }
   return numIter;
}

#endif
