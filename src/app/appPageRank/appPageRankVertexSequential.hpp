#ifndef APPPAGERANKVERTEXSEQUENTIAL_HPP_
#define APPPAGERANKVERTEXSEQUENTIAL_HPP_

/*****************************************************************************/
/*!
 * @file appPageRankVertexSequential.hpp
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/

/******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/
#include "tiglContainersBiVertexBase.hpp"
#include <vector>

/******************************************************************************
 *
 * PageRankVertexSeq class
 *
 ******************************************************************************/

/*****************************************************************************/
/*!
 * @ingroup appGroup pageRank
 *
 * @class PageRankVertexSeq
 * @brief Customized vertex class for sequential computation of the Page Rank algorithm.
 * @param DataType the type of the data field in a vertex
 * @param IdType the type of the ID field of the vertex
 */
/*****************************************************************************/
template<typename DataType, typename IdType = unsigned long>
class PageRankVertexSeq: public TIGLContainersBiVertexBase<DataType, double, TIGLContainersEdgeBase<IdType>, IdType>
{
   /******************************************************************************
    *
    * class typedefs
    *
    ******************************************************************************/
protected:
   typedef TIGLContainersEdgeBase<IdType> EdgeType;
   typedef TIGLContainersBiVertexBase<DataType, double, EdgeType, IdType> vertex_base;
   typedef typename vertex_base::IN_EDGE_ITERATOR IN_EDGE_ITERATOR;
   typedef typename vertex_base::OUT_EDGE_ITERATOR OUT_EDGE_ITERATOR;

   /******************************************************************************
    *
    * constructor/destructor
    *
    ******************************************************************************/
public:
   PageRankVertexSeq(IdType key) :
         TIGLContainersBiVertexBase<DataType, double, TIGLContainersEdgeBase<IdType>, IdType>(key)
   {
   }
   ~PageRankVertexSeq()
   {
   }

   /******************************************************************************
    *
    * class attributes
    *
    ******************************************************************************/
protected:
   IdType numVertices;
   double alpha;
   double threshold;

   /******************************************************************************
    *
    * class methods
    *
    ******************************************************************************/
public:

   /*****************************************************************************/
   /*!
    * @brief initialization function
    * @param totalVertics total number of vertices in the graph
    */
   /*****************************************************************************/
   void init(IdType totalVertics)
   {
      /*initializing the algData field and calling the base initialization function*/
      numVertices = totalVertics;
      alpha = 0.85;
      threshold = 1e-10;
      this->algData = 1.0 / numVertices;
      vertex_base::init();
   }

   /*****************************************************************************/
   /*!
    * @brief compute function
    * @param pIn pointer to the vertex list
    */
   /*****************************************************************************/
   bool compute(void *pIn = 0)
   {

      typename vector<PageRankVertexSeq>::iterator itStart = (*((vector<PageRankVertexSeq>*) pIn)).begin();

      double newRank = 0;
      /*combining the output*/
      for (IN_EDGE_ITERATOR it = this->getBeginInEdgeIterator(); it != this->getEndInEdgeIterator(); ++it)
      {
         if (it->getNbr() == this->id)
            continue;   // ignoring self loops
         typename vector<PageRankVertexSeq>::iterator itSrc = itStart + it->getNbr();
         newRank += itSrc->getAlgData() / itSrc->getNumOutEdges();   //it->getAlgData(); /*note that *it is itself an iterator */
      }
      newRank *= alpha;
      newRank += (1.0 - alpha) / numVertices;

      if (newRank - this->algData > threshold || newRank - this->algData < -threshold)
      {/* now putting the newRank at the output edges if it is significantly different from the old value*/
         this->algData = newRank;
         return true;
      }
      else
         return false;
   }
};
#endif /* APPPAGERANKVERTEXSEQUENTIAL_HPP_ */
