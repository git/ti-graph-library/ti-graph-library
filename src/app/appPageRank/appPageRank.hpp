#ifndef APPPAGERANK_HPP_
#define APPPAGERANK_HPP_

/*****************************************************************************/
/*!
 * @file appPageRank.hpp
 */
/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************/

/******************************************************************************
 *
 * INCLUDES
 *
 ******************************************************************************/

#include <vector>
#include "tiglApiVertex.hpp"
#include "tiglApiMessage.hpp"
#include "tiglApiCombiner.hpp"

/******************************************************************************
 *
 * GLOBAL DEFINITIONS
 *
 ******************************************************************************/

using namespace std;

/******************************************************************************
 *
 * PageRankVertex class
 *
 ******************************************************************************/

/*****************************************************************************/
/*!
 *
 * @ingroup appGroup pageRank
 *
 * @class PageRankVertex
 * @brief Customized class for the Page Rank algorithm
 * @details It customizes the TIGLApiVertex object to define procedures for the page rank algorithm.
 * it is a template class with the following parameter
 * @param DataType the type of the data field in a vertex
 */
/*****************************************************************************/
template<typename DataType = char>
class PageRankVertex:
      public TIGLApiVertex<DataType, double, TIGLContainersEdgeBase<uint_fast32_t>, uint_fast32_t, uint_fast32_t, DblMessage>
{
public:
   typedef typename TIGLApiVertex<DataType, double, TIGLContainersEdgeBase<uint_fast32_t>, uint_fast32_t, uint_fast32_t, DblMessage>::MESSAGE_BUF_TYPE MESSAGE_BUF_TYPE;
   typedef typename TIGLApiVertex<DataType, double, TIGLContainersEdgeBase<uint_fast32_t>, uint_fast32_t, uint_fast32_t, DblMessage>::OUT_EDGE_ITERATOR OUT_EDGE_ITERATOR;

   /******************************************************************************
    *
    * Attributes
    *
    ******************************************************************************/
protected:
   /// numVertices: a static member that holds the total number of vertices
   static uint_fast32_t numVertices;

   /******************************************************************************
    *
    * constructor/destructor
    *
    ******************************************************************************/
public:

   /*****************************************************************************/
   /*!
    * @brief constructor enable vertex processing even in the absence of messages
    * @param key the ID of the vertex
    */
   /*****************************************************************************/
   PageRankVertex(uint_fast32_t key) :
         TIGLApiVertex<char, double, TIGLContainersEdgeBase<uint_fast32_t>, uint_fast32_t, uint_fast32_t, DblMessage>(key, true, false)
   {
   }

   /*****************************************************************************/
   /*!
    * @brief destructor
    */
   /*****************************************************************************/
   ~PageRankVertex()
   {
   }

   /******************************************************************************
    *
    * public Methods
    *
    ******************************************************************************/
public:

   /*****************************************************************************/
   /*!
    * @brief customized compute function
    * @param pIn pointer to the input message buffer
    * @param pOut pointer to the output message buffer
    * @param numMessages the number of input data messages
    * @return the number of output data messages
    */
   /*****************************************************************************/
   template<typename ALLOCATOR_TYPE>
   uint_fast32_t compute(MESSAGE_BUF_TYPE *pIn, vector<DblMessage, ALLOCATOR_TYPE> *pOut, uint_fast32_t numMessages)
   {
      /// body

      double newRank = 0.0;
      /// 1. check the consistency of the input messages
      if (numMessages > 1)
         cout << "!!! Warning PageRankVertex compute: numMessages(" << this->getId() << ") = " << numMessages << endl;

      if (numMessages > pIn->size())
         cout << "!!! Warning PageRankVertex compute: numMessages is bigger than buffer size" << endl;

      /// 2. Compute the new rank from the input messages and the old rank value
      vector<DblMessage>::iterator itm = pIn->begin();
      for (uint_fast32_t loopCount = 0; loopCount < numMessages; loopCount++)
      {
         newRank += itm->getContent();
         ++itm;
      }

      newRank = newRank * 0.85 + 0.15 / (double) numVertices;
      this->algData = newRank;

      /// 3. Write the new rank in messages to all neighbors
      uint_fast32_t numOutMessages = 0;
      double messageVal = (this->algData) / (double) (this->getNumOutEdges());
      for (OUT_EDGE_ITERATOR ite = this->getBeginOutEdgeIterator(); ite != this->getEndOutEdgeIterator(); ++ite)
      {
         uint_fast32_t dst = ite->getNbr();
         if (dst == this->id)
            continue;   // ignoring self reference
         DblMessage newMessage(this->id, dst, messageVal);
         pOut->push_back(newMessage);
         numOutMessages++;
      }

      //this->flagActive = true;
      return numOutMessages;
   }

   /*****************************************************************************/
   /*!
    * @brief customized init function
    * @param pNumVertices pointer to uint_fast32_t parameter that has the total number of vertices in the graph
    * @param pOut pointer to the output message buffer
    * @return the number of output data messages
    */
   /*****************************************************************************/
   template<typename ALLOCATOR_TYPE>
   uint_fast32_t init(uint_fast32_t *pNumVertices, vector<DblMessage, ALLOCATOR_TYPE> *pOut)
   {
      numVertices = *pNumVertices;
      this->algData = 1.0 / numVertices;
      double messageVal = (this->algData) / (double) (this->getNumOutEdges());

      uint_fast32_t numOutMessages = 0;
      for (OUT_EDGE_ITERATOR ite = this->getBeginOutEdgeIterator(); ite != this->getEndOutEdgeIterator(); ++ite)
      {
         uint_fast32_t dst = ite->getNbr();
         if (dst == this->id)
            continue;   // ignoring self reference
         DblMessage newMessage(this->id, dst, messageVal);
         pOut->push_back(newMessage);
         numOutMessages++;
      }
      this->flagActive = true;
      return numOutMessages;
   }

};

template<typename DataType>
uint_fast32_t PageRankVertex<DataType>::numVertices;

/******************************************************************************
 *
 * PageRankCombiner class
 *
 ******************************************************************************/

/*****************************************************************************/
/*!
 * @ingroup appGroup pageRank
 * @class PageRankCombiner
 * @brief Combiner class for the Page Rank algorithm
 * @details it inherits the generic TIGLApiCombiner class and customizes the combining function to sum combining
 */
/*****************************************************************************/
class PageRankCombiner: public TIGLApiCombiner<DblMessage>
{
public:
   typedef typename vector<DblMessage>::iterator MESSAGE_ITERATOR;

public:
   PageRankCombiner()
   {
   }
   ~PageRankCombiner()
   {
   }

   /******************************************************************************
    *
    * class methods
    *
    ******************************************************************************/
public:
   /*****************************************************************************/
   /*!
    * @brief customized combine function the combines a set of messages that calls the generic sum combine
    * @param itIn Iterator to input messages
    * @param numMessages number of input messages
    * @param itOut iterator to the start of the ouput message
    * @return the number of combined output data messages
    */
   /*****************************************************************************/
   inline uint_fast32_t combineMessages(MESSAGE_ITERATOR itIn, uint_fast32_t numMessages, MESSAGE_ITERATOR itOut)
   {
      return this->sumCombineMessages(itIn, numMessages, itOut);
   }

   /*****************************************************************************/
   /*!
    * @brief overloaded customized combine function that combines a single message
    * @param newMessage the new message
    * @param itOut iterator to the start of the ouput message
    * @return the number of combined output data messages
    */
   /*****************************************************************************/
   inline uint_fast32_t combineMessages(DblMessage & newMessage, MESSAGE_ITERATOR itOut)
   {
      return this->sumCombineMessages(newMessage, itOut);
   }
};

#endif /* APPPAGERANK_HPP_ */
