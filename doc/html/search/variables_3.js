var searchData=
[
  ['datamanager',['dataManager',['../classTIGL.html#a59fcdfd2bacee67418e182f347f898f7',1,'TIGL::dataManager()'],['../classTIGLComputationManager.html#a9a1d7cc592962f35c0b009f44b52beac',1,'TIGLComputationManager::dataManager()'],['../classTIGLMutationManager.html#a6ddb2bb4d75734599ef7a39fd0291b8e',1,'TIGLMutationManager::dataManager()']]],
  ['dataptr',['dataPtr',['../classTIGLCommunicationManager.html#aad508b38f058c54ba1ac037995be39b3',1,'TIGLCommunicationManager']]],
  ['datatxcount',['dataTxCount',['../classTIGLCommunicationManager.html#a906895636bcf6765c145fee98f783a6a',1,'TIGLCommunicationManager']]],
  ['devtxmessages',['devTxMessages',['../classTIGLCommunicationManager.html#a96784692fde5caddb9e66d0a09e558ce',1,'TIGLCommunicationManager']]],
  ['distance',['distance',['../classSSSPInfo.html#ae3fdd56480d5c6fe95984fa25bef2f1f',1,'SSSPInfo::distance()'],['../structSSSPNodeSeq.html#aafa47ebec2f000a1a7187feb7dac89a0',1,'SSSPNodeSeq::distance()']]]
];
