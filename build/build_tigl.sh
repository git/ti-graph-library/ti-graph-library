################################################################################
#
# build_tigl.sh
#
# Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
#    Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#    Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the
#    distribution.
#
#    Neither the name of Texas Instruments Incorporated nor the names of
#    its contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
################################################################################

################################################################################
#
# USE
#
#    ./build_tigl.sh <tigl_base_directory> <mcsdk_linux_dir> <mcsdk_hpc_dir> <output_dir>
#
# NOTES
#
#    1. For an ARM only build the following evnironment variables need to be set 
#		to the appropriate values before running the script, e.g.,:
#
#       export TI_INSTALL_DIR=/opt/ti
#       export LINARO_INSTALL_PATH=/opt/ti/gcc-arm-linux
#       export TI_OCL_INSTALL_DIR=/opt/ti/mcsdk-hpc_03_00_01_04/ti-opencl_0.14.0/
#
################################################################################
#!/bin/bash
set -e

if [ $# -ne 4 ]; then
  echo "Usage: $0 <tigl_base_dir> <mcsdk_dir>  <mcsdk_hpc_dir> <output_dir>";
  exit 1;
fi

cd $1
TIGL_BASE_DIR=`pwd`
cd - 

cd $2
MCSDK_DIR=`pwd`
cd - 

cd $3
MCSDK_HPC_DIR=`pwd`
cd ..
MCSDK_HPC_TOP_DIR=`pwd`
cd -

cd $4
OUTPUT_DIR=`pwd`
cd -

[ ! -z $MCSDK_DIR ] || { echo "Error: $MCSDK_DIR not set!"; exit 1; }
[ ! -z $MCSDK_HPC_DIR ] || { echo "Error: $MCSDK_HPC_DIR not set!"; exit 1; }
[ ! -z $TIGL_BASE_DIR ] || { echo "Error: $TIGL_BASE_DIR not set!"; exit 1; }
[ ! -z $OUTPUT_DIR ] || { echo "Error: $OUTPUT_DIR not set!"; exit 1; }

 cd $MCSDK_HPC_DIR/scripts
 set +e
  pwd	
 . setup_hpc_env.sh -s $TI_INSTALL_DIR:$LINARO_INSTALL_PATH:$MCSDK_DIR:$MCSDK_HPC_TOP_DIR -t $TARGET_ROOTDIR
 set -e

echo "TIGL_BASE_DIR = $TIGL_BASE_DIR"
echo "MCSDK_HPC_DIR = $MCSDK_HPC_DIR"
echo "OUTPUT_DIR = $OUTPUT_DIR"
echo "TI_OCL_INSTALL_DIR = $TI_OCL_INSTALL_DIR"
echo "TARGET_ROOTDIR = $TARGET_ROOTDIR"

echo "env" 
env

#set -x
#echo "Debug: Locating header files of OpenCL"
#CPP_FILE=`ls $TI_OCL_INSTALL_DIR/opencl/include/CL/*`
#set +x
set -x
cd $TIGL_BASE_DIR

echo "Generating Debian package ..."
TIGL_VER=`cat build/build_version.txt`
tar -czf tigl_$TIGL_VER.tar.gz --exclude='*.git' --exclude="*.obj" --exclude="*.o" --exclude="*.obj" --exclude="*.a" --exclude="*.ae66" --files-from=build/build_files.txt
mkdir tigl_$TIGL_VER
tar -xf tigl_$TIGL_VER.tar.gz -C tigl_$TIGL_VER
tar -czf tigl_$TIGL_VER.tar.gz tigl_$TIGL_VER

mv -v tigl_$TIGL_VER.tar.gz $OUTPUT_DIR
#removing temporary file
rm -r tigl_$TIGL_VER

make build

echo "Generating IPK ..."
mkdir -pv ./ipk_install
sudo make ipk DESTDIR=./ipk_install

$MCSDK_HPC_DIR/mkrel/ipk/create_ipk.sh $MCSDK_HPC_DIR/mkrel/ipk/tigl ./ipk_install
mv -v *.ipk $OUTPUT_DIR

#removing temporary file
echo "Removing temporary data ..."
sudo rm -r ./ipk_install


